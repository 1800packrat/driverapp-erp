﻿namespace PR.Entities
{
    public class LDMCancelQORIDs
    {
        //public int LDMUnitId { get; set; }
        public int SLQORId { get; set; }
        //public int SLQORId { get; set; }

        public int tenantId { get; set; }
        public string LocationCode { get; set; }
      
        public int QTRentalID { get; set; }
       // public int DestinationQTRentalID { get; set; }
        public string STARSUnitId { get; set; }

        public bool IsOrigin { get; set; }
    }
}
