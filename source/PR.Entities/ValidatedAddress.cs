using System;

namespace PR.Entities
{
    public class ValidatedAddress
    {
        private string _addressLine1 = String.Empty;
        private string _addressLine2 = String.Empty;
        private string _city = String.Empty;
        private string _state = String.Empty;
        private string _zip = String.Empty;
        private string _country = String.Empty;
        private string _error = String.Empty;
        private string _warning = String.Empty;
        private string _dpv = String.Empty;
        private bool _isAddressvalid = false;

        #region Test method properties
        //private string _latitude = string.Empty;
        //private string _longitude = string.Empty;

        //public string Latitude
        //{
        //    get { return _latitude; }
        //    set { _latitude = value; }
        //}
        //public string Longitude
        //{
        //    get { return _longitude; }
        //    set { _longitude = value; }
        //}
        #endregion


        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }
        public string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
        public string Warning
        {
            get { return _warning; }
            set { _warning = value; }
        }
        public string DPV
        {
            get { return _dpv; }
            set { _dpv = value; }
        }
        public bool IsAddressValid
        {
            get { return _isAddressvalid; }
            set { _isAddressvalid = value; }
        }
    }
}
