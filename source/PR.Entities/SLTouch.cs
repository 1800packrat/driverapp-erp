﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class SLTouch
    {
        public virtual string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public string TouchKey
        {
            get { return $"{Qorid}_{TouchType}_{SequenceNO}"; }
        }

        public int Qorid { get; set; }

        public string TouchType { get; set; }

        public int SequenceNO { get; set; }

        public string UnitNumber { get; set; }

        public string Status { get; set; }

        public string RentalStatus { get; set; }

        public string RentalType { get; set; }

        public string Directions { get; set; }

        //// SPERP-TODO-RMVNICD - may need to get directly from ESB method call. Added by Sohan
        //public bool IsLDMOrderTouch { get { return StarsUnitId > 0; } }
        public bool IsLDMOrderTouch { get; set; }

        public bool IsWeightTicket { get; set; }

        public int StarsUnitId { get; set; }

        public decimal SiteLinkMileage { get; set; }

        public string FacCode { get; set; }

        public string Instructions { get; set; }

        public string Obstacles { get; set; }

        public string DriveWay { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        /// <summary>
        /// This is LDMOrderNum
        /// </summary>
        public string StarsID { get; set; }

        public string Unitsize { get; set; }

        public string Email { get; set; }

        public string EmailAlt { get; set; }

        public bool DoorToFront { get; set; }

        public bool DoorToRear { get; set; }

        public bool doorposSLupdated { get; set; }

        public DateTime ScheduledDate { get; set; }

        public string ScheduledDateStr
        {
            get { return (this.ScheduledDate != DateTime.MinValue) ? this.ScheduledDate.ToShortDateString() : ""; }
        }

        public Address OriginAddress { get; set; }

        public Address DestAddress { get; set; }

        public string DoorPOS { get { return DoorToFront ? "Door Front" : "Door Rear"; } }

        public ETASettings ProvidedETASettings { get; set; }

        public string OrderNumber
        {
            get { return GetTouchType(TouchType) + "-" + Qorid + "-" + SequenceNO; }
        }

        public string GetTouchType(string TouchType)
        {
            switch (TouchType)
            {
                default:
                    break;
                case "DeliverEmpty":
                case "WarehouseToCurbEmpty":
                    return "DE";
                case "DeliverFull":
                case "WarehouseToCurbFull":
                    return "DF";
                case "CurbToCurb":
                    return "CC";
                case "ReturnEmpty":
                case "ReturnToWarehouseEmpty":
                    return "RE";
                case "ReturnFull":
                case "ReturnToWarehouseFull":
                    return "RF";
                case "WarehouseAccess":
                    return "WA";
                case "InByOwner":
                    return "IBO";
                case "OutByOwner":
                    return "OBO";
                case "LDMTransferIn":
                    return "TI";
                case "LDMTransferOut":
                    return "TO";

            }
            return TouchType;
        }

        public string OrderNumberDisp
        {
            get { return TouchType + "-" + Qorid; }
        }

        public bool IsZippyShellQuote { get; set; }
    }
}
