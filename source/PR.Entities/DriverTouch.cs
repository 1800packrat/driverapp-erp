﻿using System;

namespace PR.Entities
{
    public class DriverTouch
    {
        private string _qorid = String.Empty;
        private string _touchType = String.Empty;
        private string _sequenceno = String.Empty;
        private string _customerName = String.Empty;
        private string _addressLine1 = String.Empty;
        private string _addressLine2 = String.Empty;
        private string _city = String.Empty;
        private string _state = String.Empty;
        private string _zip = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _containerNumber = String.Empty;
        private int _containerSize = 0;
        private string _status = String.Empty;
        private string _stopStatus = String.Empty;
        private decimal _balanceDue = 0;
        private string _scheduledstarttime = string.Empty;
        private string _scheduledendtime = string.Empty;
        public string _actualStartTime = string.Empty;
        public string _actualEndTime = string.Empty;

        private TransiteCustomReferences _tcr = new TransiteCustomReferences();
        private string _transiteTouchType = String.Empty;
        private Address _originAddress = new Address();
        private Address _destinationAddress = new Address();
        private Address _weightstationAddress = new Address();
        private string _stopType = String.Empty;
        private string _orderNum = String.Empty;
        private int _stopId = 0;
        private bool _isCombinedTouch = false;
        private bool _isTrailerNeeded = false;
        private string _LoadId = String.Empty;
        private string _DriverId = String.Empty;
        private string _TruckId = String.Empty;
        private string _TruckNum = String.Empty;
        private string _TrailerNum = String.Empty;
        private string _TrailerId = String.Empty;
        private bool _isLockedIndicator = false;
        private int _stopsequence = 0;
        private int _slSeqNo = 0;
        private int _starsUnitId = 0;

        public string LoadId
        {
            get { return _LoadId; }
            set { _LoadId = value; }
        }
        public string DriverId
        {
            get { return _DriverId; }
            set { _DriverId = value; }
        }
        public string TruckId
        {
            get { return _TruckId; }
            set { _TruckId = value; }
        }
        public string TrailerId
        {
            get { return _TrailerId; }
            set { _TrailerId = value; }
        }
        public string TruckNumber
        {
            get { return _TruckNum; }
            set { _TruckNum = value; }
        }
        public string TrailerNumber
        {
            get { return _TrailerNum; }
            set { _TrailerNum = value; }
        }

        public bool IsLockedIndicator
        {
            get { return _isLockedIndicator; }
            set { _isLockedIndicator = value; }
        }

        public int StopSequence
        {
            get { return _stopsequence; }
            set { _stopsequence = value; }
        }


        public string QORID
        {
            get { return _qorid; }
            set { _qorid = value; }
        }
        public string TouchType
        {
            get { return _touchType; }
            set { _touchType = value; }
        }
        public string SequenceNumber
        {
            get { return _sequenceno; }
            set { _sequenceno = value; }
        }
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }
        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public string ContainerNumber
        {
            get { return _containerNumber; }
            set { _containerNumber = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public string StopStatus
        {
            get { return _stopStatus; }
            set { _stopStatus = value; }
        }
        public decimal BalanceDue
        {
            get { return _balanceDue; }
            set { _balanceDue = value; }
        }
        public string ScheduledStartTime
        {
            get { return _scheduledstarttime; }
            set { _scheduledstarttime = value; }
        }
        public string ScheduledEndTime
        {
            get { return _scheduledendtime; }
            set { _scheduledendtime = value; }
        }

        public string ActualStartTime
        {
            get { return _actualStartTime; }
            set { _actualStartTime = value; }
        }
        public string ActualEndTime
        {
            get { return _actualEndTime; }
            set { _actualEndTime = value; }
        }

        public int ContainerSize
        {
            get { return _containerSize; }
            set { _containerSize = value; }
        }
        public TransiteCustomReferences TransiteCustRef
        {
            get { return _tcr; }
            set { _tcr = value; }
        }

        public string TransiteTouchType
        {
            get { return _transiteTouchType; }
            set { _transiteTouchType = value; }
        }

        public Address OriginAddress
        {
            get { return _originAddress; }
            set { _originAddress = value; }
        }

        public Address DestinationAddress
        {
            get { return _destinationAddress; }
            set { _destinationAddress = value; }
        }
        public Address WeightStationAddress
        {
            get { return _weightstationAddress; }
            set { _weightstationAddress = value; }
        }

        public string StopType
        {
            get { return _stopType; }
            set { _stopType = value; }
        }

        public string OrderNum
        {
            get { return _orderNum; }
            set { _orderNum = value; }
        }

        public int StopId
        {
            get { return _stopId; }
            set { _stopId = value; }
        }

        public bool IsCombinedTouch
        {
            get { return _isCombinedTouch; }
            set { _isCombinedTouch = value; }
        }

        public bool IsTrailerNeeded
        {
            get { return _isTrailerNeeded; }
            set { _isTrailerNeeded = value; }
        }

        public DateTime TouchDate { get; set; }

        public string ScheduleStartDateTime { get; set; }
        public string ScheduleEndDateTime { get; set; }

        public int OrderId { get; set; }

        public bool IgnoreConstraints { get; set; }

        public string Color { get; set; }

        public string TouchComments { get; set; }
        public bool IsFakeTouch { get; set; }
        public Int32 TouchId { get; set; }
        public Int32 TouchStopId { get; set; }
        public bool SkipTouch { get; set; }
        public string FacilityName { get; set; }
        public string EstimatedStartTime { get; set; }
        public string EstimatedEndTime { get; set; }

        public string Company { get; set; }

        public bool IsExceptionalTouch { get; set; }

        public int SLSeqNo {
            get { return _slSeqNo; }
            set { _slSeqNo = value; }
        }

        public int StarsUnitId {
            get { return _starsUnitId; }
            set { _starsUnitId = value; }
        }

        public bool IsZippyShellMove { get; set; }
    }
}
