﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class TouchLogging
    {
        public int OrderNumber { get; set; }
        public int QORID { get; set; }

        public string LocationCode { get; set; }
        public string UnitNumber { get; set; }
        public int UnitSize { get; set; }
        public string TouchType { get; set; }

        public int SequenceNumber { get; set; }
        public DateTime? TouchDate { get; set; }
        public string TouchTime { get; set; }
        public string ApplicationName { get; set; }
        public string AddedBy { get; set; }
       // public DateTime CreatedDateTime { get; set; }
        public int AddedByUserRoleID { get; set; }
        public string ModifiedBy { get; set; }
        public int ModifiedByUserRoleID { get; set; }
      //  public DateTime ModifiedDateTime { get; set; }
        public bool IsActive { get; set; }
        public string MoveType { get; set; }

        public int ConfigLogDays { get; set; }

      
       // public List<TouchLogDetail> TouchLogDetails { get;set;}

    }
}
