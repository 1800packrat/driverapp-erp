﻿using System;
using System.Collections.Generic;

namespace PR.Entities
{
    public class TransiteCustomReferences
    {

        private string _qorid = String.Empty;
        private string _touchType = String.Empty;
        private string _sequenceno = String.Empty;
        private Boolean  _confirmed = false;
        private Boolean _adjneeded = false;
        private Boolean _needcall = false;
        private string _obstacles = String.Empty;
        private string _driveway = String.Empty;
        private string _doorpos = String.Empty;
        private decimal _touchprice = 0;
        private Boolean _donotcombine = false;
        private Boolean _donotoptimize = false;
        private string _starsid = String.Empty;
        private string _unitid = String.Empty;
        private int _additionalhandlingtime = 0;
        private Boolean _requestedweightticket = false;
        private Boolean _requiresweightticket = false;
        private int _blankets = 0;
        private string _orderurl = String.Empty;
        private Boolean _istouchstarted = false;
        private decimal _balancedue = 0;
        private int _requiredcontainersize = 0;
        private List<string> _allowedcontainertype = new List<string>();

        public string QORID
        {
            get { return _qorid; }
            set { _qorid = value; }
        }
        public string MovementType
        {
            get { return _touchType; }
            set { _touchType = value; }
        }
        public string SEQNUM
        {
            get { return _sequenceno; }
            set { _sequenceno = value; }
        }
        public Boolean Confirmed
        {
            get { return _confirmed; }
            set { _confirmed = value; }
        }
        public Boolean AdjustmentNeeded
        {
            get { return _adjneeded; }
            set { _adjneeded = value; }
        }
        public Boolean NeedCall
        {
            get { return _needcall; }
            set { _needcall = value; }
        }
        public string Obstacles
        {
            get { return _obstacles; }
            set { _obstacles = value; }
        }
        public string DriveWay
        {
            get { return _driveway; }
            set { _driveway = value; }
        }
        public string DoorPos
        {
            get { return _doorpos; }
            set { _doorpos = value; }
        }
        public decimal TouchPrice
        {
            get { return _touchprice; }
            set { _touchprice = value; }
        }
        public Boolean DoNotCombine
        {
            get { return _donotcombine; }
            set { _donotcombine = value; }
        }
        public Boolean IsTouchStarted
        {
            get { return _istouchstarted; }
            set { _istouchstarted = value; }
        }
        public Boolean DoNotOptimize
        {
            get { return _donotoptimize; }
            set { _donotoptimize = value; }
        }
        public string STARSID
        {
            get { return _starsid; }
            set { _starsid = value; }
        }
        public string UNITID
        {
            get { return _unitid; }
            set { _unitid = value; }
        }
        public int Additionalhandlingtime
        {
            get { return _additionalhandlingtime; }
            set { _additionalhandlingtime = value; }
        }
        public Boolean RequestedWeightTicket
        {
            get { return _requestedweightticket; }
            set { _requestedweightticket = value; }
        }
        public Boolean RequiresWeightTicket
        {
            get { return _requiresweightticket; }
            set { _requiresweightticket = value; }
        }
        public int Blankets
        {
            get { return _blankets; }
            set { _blankets = value; }
        }
        public string OrderUrl
        {
            get { return _orderurl; }
            set { _orderurl = value; }
        }
        public decimal BalanceDue
        {
            get { return _balancedue; }
            set { _balancedue = value; }
        }
        public List<string> AllowedContainerType
        {
            get { return _allowedcontainertype; }
            set { _allowedcontainertype = value; }
        }
        public int RequiredContainerSize
        {
            get { return _requiredcontainersize; }
            set { _requiredcontainersize = value; }
        }

    }
}
