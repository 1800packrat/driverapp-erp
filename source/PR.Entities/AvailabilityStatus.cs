using System;

namespace PR.Entities
{
    public class AvailabilityStatus
    {
         private bool _isAvailable = false;
        private string _message = string.Empty;
        private decimal _expediteFee = 0;
        private string _expediteFeeSKUID = String.Empty;
        private string _itemDescription = String.Empty;
        private string _messageToDisplay = String.Empty;

        public bool IsAvailable
        {
            get
            {
                return _isAvailable;
            }
            set
            {
                _isAvailable = value;
            }
        }

        public string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }

        public decimal ExpediteFee
        {
            get
            {
                return _expediteFee;
            }
            set
            {
                _expediteFee = value;
            }
        }

       public string ExpediteFeeSKUID
       {
           get
           {
               return _expediteFeeSKUID;
           }
           set
           {
               _expediteFeeSKUID = value;
           }
       }

       public string ItemDescription
       {
           get
           {
               return _itemDescription;
           }
           set
           {
               _itemDescription = value;
           }
       }

       public string MessageToDisplay
       {
           get
           {
               return _messageToDisplay;
           }
           set
           {
               _messageToDisplay = value;
           }
       }
    } 
}
