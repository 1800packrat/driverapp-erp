﻿using System;
using PR.UtilityLibrary;

namespace PR.Entities
{
    [Serializable]
    public class PricingEntity
    {
        private bool _isForDisplay = true;
        public string ItemDescription { get; set; }
        public decimal BasePrice { get; set; }
        public int Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal SubTotal
        {
            get
            {
                return BasePrice * Quantity;
            }
           
        }
        public decimal Tax { get; set; }
        public decimal Total
        {
            get
            {
                return (SubTotal - Discount) + Tax;
            }
        }
        public bool IsForDisplay
        {
            get
            {
                return _isForDisplay;
            }
            set
            {
                _isForDisplay = value;
            }
        }
        public PREnums.ChargeType ChargeType { get; set; }

        public bool IsActive { get; set; }
    }
}
