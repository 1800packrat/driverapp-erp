﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class CPPItem
    {
        public string Price
        {
            get;
            set;
        }
        public string Description
        {
            get;
            set;
        }

        public bool IsWeekly
        {
            get;
            set;
        }
    }
}
