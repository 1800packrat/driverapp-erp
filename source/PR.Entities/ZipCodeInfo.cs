using System;

namespace PR.Entities
{
    public class ZipCodeInfo
    {
        private string _zipCode = String.Empty;
        private bool _isServiced = false;
        private bool _isESAT = false;
        private bool _isLDMServiced = false;
        private string _corpCode = String.Empty;
        private string _locationCode = String.Empty;
        private string _errorCode = String.Empty;
        private string _errorMessage = String.Empty;
        private int _marketId = 0;
        private int _siteId = 0;
        private decimal _distanceFromSite = 0M;

        /// <summary>
        /// PAS-264 Allow Mil type Sites to be selected for LDM MIL, LOG, COR, COM type orders
        /// Added by SFT Amit to get the StatusRowId
        /// </summary>
        public int StatusRowID { get; set; }

        public string ZipCode
        {
            get
            {
                return _zipCode;
            }
            set
            {
                _zipCode = value;
            }
        }

        public bool IsServiced
        {
            get
            {
                return _isServiced;
            }
            set
            {
                _isServiced = value;
            }
        }

        public bool IsESAT
        {
            get
            {
                return _isESAT;
            }
            set
            {
                _isESAT = value;
            }
        }

        public bool IsLDMServiced
        {
            get
            {
                return _isLDMServiced;
            }
            set
            {
                _isLDMServiced = value;
            }
        }

        public string CorpCode
        {
            get
            {
                return _corpCode;
            }
            set
            {
                _corpCode = value;
            }
        }

        public string LocationCode
        {
            get
            {
                return _locationCode;
            }
            set
            {
                _locationCode = value;
            }
        }

        public string ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value;
            }
        }

        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
            }
        }

        public int MarketId
        {
            get
            {
                return _marketId;
            }
            set
            {
                _marketId = value;
            }
        }

        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                _siteId = value;
            }
        }

        public decimal DistanceFromSite
        {
            get
            {
                return _distanceFromSite;
            }
            set
            {
                _distanceFromSite = value;
            }
        }
    }
}
