using System;
using System.Collections.Generic;
using static PR.UtilityLibrary.PREnums;

namespace PR.Entities
{
    public class SiteInfo
    {
        private int _siteId = 0;
        private string _locationCode = String.Empty;
        private string _globalSiteNum = String.Empty;
        private string _contactName = String.Empty;
        private string _siteName = String.Empty;
        Address _siteAddress = new Address();
        private string _legalName = String.Empty;
        private string _email = String.Empty;
        private int? _marketId = null;
        //private string _facCode = String.Empty;
        private List<SiteInfo> _lstMarketSiteInfo = new List<SiteInfo>();

        public int SiteId
        {
            get { return _siteId; }
            set { _siteId = value; }
        }
        public string LocationCode
        {
            get { return _locationCode; }
            set { _locationCode = value; }
        }

        /// <summary>
        /// StoreNo from RDFacility table
        /// </summary>
        public string GlobalSiteNum
        {
            get { return _globalSiteNum; }
            set { _globalSiteNum = value; }
        }
        public string ContactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }
        public string SiteName
        {
            get { return _siteName; }
            set { _siteName = value; }
        }
        public Address SiteAddress
        {
            get { return _siteAddress; }
            set { _siteAddress = value; }
        }

        /// <summary>
        /// CompDBAName from RDFacility table
        /// </summary>
        public string LegalName
        {
            get { return _legalName; }
            set { _legalName = value; }
        }
        public string EMail
        {
            get { return _email; }
            set { _email = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int? MarketId
        {
            get { return _marketId; }
            set { _marketId = value; }
        }
        //public string FacCode
        //{
        //    get { return _facCode; }
        //    set { _facCode = value; }
        //}

        public bool IsMarket { get { return MarketFacilities?.Count > 0; } }

        public List<SiteInfo> MarketFacilities
        {
            get { return _lstMarketSiteInfo; }
            set { _lstMarketSiteInfo = value; }
        }

        /// <summary>
        /// This property will determine how may miles this facility can serve (local touches)
        /// </summary>
        [System.ComponentModel.DefaultValue(200)]
        public decimal MaxDistanceLocalTouchesCanServe { get; set; } = 200;

        public int AccessLevel { get; set; }
    }
}
