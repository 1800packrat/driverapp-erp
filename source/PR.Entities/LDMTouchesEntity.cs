﻿using System;
using PR.UtilityLibrary;

namespace PR.Entities
{
    public class LDMTouchesEntity
    {
        /// <summary>
        /// Enum PR.UtilityLibrary touch type 
        /// </summary>
        public PREnums.TouchType TouchType { get; set; }


        /// <summary>
        /// Touch Schedule Date nullable
        /// </summary>
        public DateTime TouchScheduleDate { get; set; }


        /// <summary>
        /// true if origin touch.
        /// </summary>
        public bool IsOrigin { get; set; }


        public PREnums.TouchStatus TouchStatus { get; set; }

        public string Comment { get; set; }
        public string Directions { get; set; }

        public bool IsDoorToFront { get; set; }

        public bool IsPaved { get; set; }

        public bool IsCurbed { get; set; }

        public bool IsDirt { get; set; }

        public bool IsGravel { get; set; }

        public bool IsSloped { get; set; }

        public bool IsSoft { get; set; }
        public bool IsFenced { get; set; }
        public bool IsLandscaping { get; set; }
        public bool IsPowerline { get; set; }
        public bool IsSeptic { get; set; }
        public bool IsSprinkler { get; set; }
        public bool IsTree { get; set; }

        public string TouchScheduleTime { get; set; }
   
        public int TouchTime { get; set; }
        public string Instructions { get; set; }

        public decimal TouchDistance { get; set; }







    }
}
