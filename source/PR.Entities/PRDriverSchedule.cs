﻿namespace PR.Entities
{
    using System;

    public class PRDriverSchedule
    {
        public int ID { get; set; } //ScheduleDate	ScheduleStartTime	ScheduleEndTime	WeekDay	OperatingTimeMinutes	DayWork
        public DateTime ScheduleDate { get; set; }
        public TimeSpan ScheduleStartTime { get; set; }
        public TimeSpan ScheduleEndTime { get; set; }
        public string WeekDay { get; set; }
        public int OperatingTimeMinutes { get; set; }
        public string DayWork { get; set; }
    }
}
