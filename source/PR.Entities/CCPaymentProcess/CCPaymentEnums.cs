﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.CCPaymentProcess
{
    public class CCPaymentEnums
    {
        public enum OrderType
        {
            PackratLocal = 1,
            PackratLDM = 2,
            ZippyLocal = 3,
            ZippyLDM = 4
        };

        public enum RequestedApp
        {
            Passport = 1,
            ConsumerWeb = 2,
            STARS = 3,
            CATS = 4,
            CW = 5
        }

        public enum RequestType
        {
            Payment = 1,
            Refund = 2,
            AuthorizationAddCard = 3,
            RemoveCard = 4,
            NA = 5
        };

        public enum PaymentRequestStatus
        {
            InProgress = 1,
            Success = 2,
            Cancelled = 3,
            Error = 4,
            SessionOut = 5
        }

        public enum CreditCardType
        {
            Amex = 1,
            Disc = 2,
            MasterCard = 3,
            Visa = 4,
            Others = 5
        }
    }
}