﻿using System;
using System.Collections.Generic;

namespace PR.Entities.EsbEntities.LocalDispatch
{
    public class SFTransportationTouch
    {
        public string SvcAssignID { get; set; }
        public DateTime ScheduledDate { get; set; }
        public string Customer_Name { get; set; }
        public string PhoneNo { get; set; }
        public string QORID { get; set; }
        public string TouchTypeShort { get; set; }
        public string SequenceNo { get; set; }
        public string UnitName { get; set; }
        public string TouchStatus { get; set; }
        public string RentalStatus { get; set; }
        public string RentalType { get; set; }
        public string Directions { get; set; }
        public string IsLDMOrderTouch { get; set; }
        public string UnitId { get; set; }
        public string LocationCode { get; set; }
        public string Instructions { get; set; }
        public string TouchTime { get; set; }
        public string OrderNo { get; set; }
        public string UnitSize { get; set; }
        public string TouchMiles { get; set; }
        public string Customer_Email { get; set; }
        public string DoorToFront { get; set; }
        public string DoorToRear { get; set; }
        public string Origin_Address1 { get; set; }
        public string Origin_City { get; set; }
        public string Origin_State { get; set; }
        public string Origin_Zip { get; set; }
        public string Origin_Company { get; set; }
        public string Origin_Country { get; set; }
        public string Destination_Address1 { get; set; }
        public string Destination_City { get; set; }
        public string Destination_State { get; set; }
        public string Destination_Zip { get; set; }
        public string Destination_Company { get; set; }
        public string Destination_Country { get; set; }
        public string TouchKey { get; set; }
        public string IsWeightTicket { get; set; }
        public string IsZippyShellQuote { get; set; }
    }

    public class RootObject
    {
        public List<SFTransportationTouch> SF_TransportationTouch { get; set; }
    }

}
