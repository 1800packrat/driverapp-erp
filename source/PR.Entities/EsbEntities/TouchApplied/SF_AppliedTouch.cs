﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.TouchApplied
{
    public class SFAppliedTouch
    {
        public string QORID { get; set; }
        public string TouchTypeShort { get; set; }
        public string Comments { get; set; }
        public string Directions { get; set; }
        public string Instructions { get; set; }
        public string DeliveryDate { get; set; }
        public string Origin_Address1 { get; set; }
        public string Origin_City { get; set; }
        public string Origin_State { get; set; }
        public string Origin_Zip { get; set; }
        public string Origin_FirstName { get; set; }
        public string Origin_LastName { get; set; }
        public string Origin_PhoneNo { get; set; }
        public string Origin_Company { get; set; }
        public string Destination_Address1 { get; set; }
        public string Destination_City { get; set; }
        public string Destination_State { get; set; }
        public string Destination_Zip { get; set; }
        public string Destination_FirstName { get; set; }
        public string Destination_LastName { get; set; }
        public string Destination_PhoneNo { get; set; }
        public string Destination_Company { get; set; }
        public string Drivewaytype_Sloped { get; set; }
        public string Drivewaytype_Soft { get; set; }
        public string Drivewaytype_Dirt { get; set; }
        public string Drivewaytype_Grvel { get; set; }
        public string Drivewaytype_Paved { get; set; }
        public string Drivewaytype_Bricked { get; set; }
        public string Drivewaytype_Curbed { get; set; }
        public string DoorFacesRear { get; set; }
        public string Obstacle_Powerlines { get; set; }
        public string Obstacle_Fences { get; set; }
        public string Obstacle_Trees { get; set; }
        public string Obstacle_Sprinklers { get; set; }
        public string Obstacle_Landscaping { get; set; }
        public string Obstacle_Septic { get; set; }
        public string Obstacle_Other { get; set; }
        public string TouchMiles { get; set; }
        public string SvcAssignID { get; set; }
        public string TouchStatus { get; set; }
        public string Subtotal { get; set; }
        public string Discounts { get; set; }
        public string Net { get; set; }
        public string Tax { get; set; }
        public string TouchTypeFull { get; set; }
        public string SequenceNo { get; set; }
        public string UnitSize { get; set; }
    }

    public class RootObject
    {
        public List<SFAppliedTouch> SF_AppliedTouch { get; set; }
    }
}
