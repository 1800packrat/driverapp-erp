﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.CustomerData
{
    public class SFCustomerInfo
    {
        public string CustomerId { get; set; }
        public string Customer_FirstName { get; set; }
        public string Customer_LastName { get; set; }
        public string Customer_Company { get; set; }
        public string Customer_DOB { get; set; }
        public string Customer_Fax { get; set; }
        public string Customer_MobileNo { get; set; }
        public string Customer_Address1 { get; set; }
        public string Customer_City { get; set; }
        public string Customer_Region { get; set; }
        public string Customer_Zip { get; set; }
        public string Customer_Country { get; set; }
        public string Customer_PhoneNo { get; set; }
        public string Customer_PhoneNoAlt { get; set; }
        public string Customer_Email { get; set; }
    }

    public class RootObject
    {
        public SFCustomerInfo SF_CustomerInfo { get; set; }
    }
}
