﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities
{
    public class WarehouseTouchRequest
    {
        public string locationCode { get; set; }
        public string waStartDate { get; set; }
        public string waEndDate { get; set; }
        public string trStartDate { get; set; }
        public string endDate { get; set; }
    }

    public class TouchAvailableScheduleRequest
    {
        public string locationCode { get; set; }
        public string startDate { get; set; }
        public int numberOfDays { get; set; }
        public string endDate { get; set; }
    }

    public class StagingUnassignTouchRequest
    {
        public string locationCode { get; set; }
        public string touchDate { get; set; }
        public string touchTypeIDs { get; set; }
        public string touchStatusIDs { get; set; }
    }

    public class UnitInfoRequest
    {
        public string qorId { get; set; }
        public string locationCode { get; set; }
        public string unitName { get; set; }
    }


    public class TouchStatusRequest
    {
        public string qorId { get; set; }
        public string touchTypeId { get; set; }
    }

    public class BrandInfoRequest
    {
        public string qorId { get; set; }
    }

    public class UpdateTouchDataRequest
    {
        public int qorId { get; set; }
        public int touchTypeId { get; set; }
        public int sequenceNo { get; set; }
        public string instructions { get; set; }

        public bool updateFromAddress { get; set; }
        public bool updateToAddress { get; set; }

        public string FromAddress_FirstName { get; set; }
        public string FromAddress_LastName { get; set; }
        public string FromAddress_Company { get; set; }
        public string FromAddress_AddressLine1 { get; set; }
        public string FromAddress_AddressLine2 { get; set; }
        public string FromAddress_City { get; set; }
        public string FromAddress_State { get; set; }
        public string FromAddress_Zip { get; set; }
        public string FromAddress_PhoneNumber { get; set; }
        public string FromAddress_MobilePhoneNumber { get; set; }
        public string FromAddress_AlternatePhoneNumber { get; set; }

        public string ToAddress_FirstName { get; set; }
        public string ToAddress_LastName { get; set; }
        public string ToAddress_Company { get; set; }
        public string ToAddress_AddressLine1 { get; set; }
        public string ToAddress_AddressLine2 { get; set; }
        public string ToAddress_City { get; set; }
        public string ToAddress_State { get; set; }
        public string ToAddress_Zip { get; set; }
        public string ToAddress_PhoneNumber { get; set; }
        public string ToAddress_MobilePhoneNumber { get; set; }
        public string ToAddress_AlternatePhoneNumber { get; set; }

        public string updatedBy { get; set; }


    }
}
