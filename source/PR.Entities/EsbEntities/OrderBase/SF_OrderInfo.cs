﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.OrderBase
{
    public class SFOrderInfo
    {
        public bool IsSettled { get; set; }
        public bool WeightTicket { get; set; }
    }

    public class RootObject
    {
        public SFOrderInfo SF_OrderInfo { get; set; }
    }
}
