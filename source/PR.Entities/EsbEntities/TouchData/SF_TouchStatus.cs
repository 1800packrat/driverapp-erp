﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.TouchData
{
    public class SFTouchStatus
    {
        public string TouchStatusId { get; set; }
    }

    public class RootObject
    {
        public SFTouchStatus SF_TouchStatus { get; set; }
    }
}
