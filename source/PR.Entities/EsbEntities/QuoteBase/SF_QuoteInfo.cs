﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.EsbEntities.QuoteBase
{
    public class SFQuoteInfo
    {
        public string LocationCode { get; set; }
        public string RentalID { get; set; }
        public string CustomerId { get; set; }
        public string UnitId { get; set; }
        public string IsQuoteMovedIn { get; set; }
        public string IsLdmOrder { get; set; }
    }

    public class RootObject
    {
        public SFQuoteInfo SF_QuoteInfo { get; set; }
    }
}
