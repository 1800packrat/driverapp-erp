﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class SiteLinkLedgerInfo
    {
        public string sFName { get; set; }

        public string sLName { get; set; }
        
        public int TenantID { get; set; }

        public int SiteID { get; set; }

        public string sPhone { get; set; }

        public string sCountry { get; set; }

        public string sPostalCode { get; set; }

        public string sRegion { get; set; }

        public string sCity { get; set; }

        public string sAddr2 { get; set; }

        public string sAddr1 { get; set; }

        public string sCompany { get; set; }

        public string sEmail { get; set; }

        public int UnitID { get; set; }

        public string sUnitName { get; set; }

        public decimal dcChargeBalance { get; set; }

        public decimal dcTotalDue { get; set; }

        public decimal dcRent { get; set; }

        public DateTime dMovedIn { get; set; }

        public DateTime dAnniv { get; set; }
    }
}
