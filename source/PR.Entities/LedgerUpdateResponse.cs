﻿namespace PR.Entities
{
    public class LedgerUpdateResponse
    {
        public LDMPricingEntity NonRecurringItem { get; set; }

        ///// <summary>
        ///// manual error to update ledger if has -ve value
        ///// </summary>
        //public string Errors { get; set; }

      


        public LedgerChargeErrorItemDetail LedgerChargeItemDetail { get; set; }


       

     
    }
}
