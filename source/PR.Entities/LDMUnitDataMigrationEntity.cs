﻿using System;

namespace PR.Entities
{
    public class LDMUnitDataMigrationEntity : LDMUnitEntity
    {
        /// <summary>
        /// set /get to movein billing qorid 
        /// </summary>
        public bool IsAtleastOneUnitDEActualCompleted { get; set; }

          /// <summary>
        /// set /get to movein billing qorid 
        /// </summary>
        public DateTime OneUnitDEActualCompleted { get; set; }



        /// <summary>
        ///  if TO is completed make it as settled order 
        /// </summary>
        public bool IsAtleastWareHouseAccessCompleted { get; set; }

        public DateTime WareHouseAccessActualCompleted { get; set; }

        public int SLQORIDActualUnit { get; set; }


        public string DummyBillingUnitHexa { get; set; }

        public string DestLocationCode { get; set; }
        public string OriginLocationCode { get; set; }

        public decimal ReservationDeposit { get; set; }

        /// <summary>
        /// this object hold data from datamigration table 
        /// </summary>
        public dynamic DataMigrationObjectEntity { get; set; }
    }
}
