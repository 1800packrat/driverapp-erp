﻿namespace PR.Entities
{
    public class LDMInfo
    {
        private int _ldmId = 0;
        private LDMUnits _ldmUnits = new LDMUnits();
        private string _errorMsg = string.Empty;
        private bool _isAlreadyAvailable = false;

        public int LDMID
        {
            get { return _ldmId; }
            set { _ldmId = value; }
        }
        public LDMUnits LDMUnits
        {
            get { return _ldmUnits; }
            set { _ldmUnits = value; }
        }
        public string ErrorMsg
        {
            get { return _errorMsg; }
            set { _errorMsg = value; }
        }
        public bool IsAlreadyAvailable
        {
            get { return _isAlreadyAvailable; }
            set { _isAlreadyAvailable = value; }
        }

    }

}
