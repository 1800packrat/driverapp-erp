﻿namespace PR.Entities
{
    public class LDMPricingDetails : BaseLDMPricingDetails
    {
        public decimal ExcessMilesOrigBaseCost { get; set; } // Base Cost before tax

        public decimal ExcessMilesOrigTax { get; set; } // tax

        public decimal ExcessMilesDestTax { get; set; } // Tax

        public decimal ExcessMilesDestBaseCost { get; set; } // Base Cost

        public decimal BudgetTransportationBaseCost { get; set; } // Base Cost

        public decimal BudgetTransportationTax { get; set; } // tax

        public decimal ExpediteFeeBaseCost { get; set; }

        public decimal ExpediteFeeTax { get; set; }

        public decimal PreCreditCardFeeBaseCost { get; set; }

        public decimal PreCreditCardFeeTax { get; set; }

        public decimal TotalCostBaseCost { get; set; }

        public decimal TotalCostTax { get; set; }

        public decimal WeightticketBaseCost { get; set; }

        public decimal MonthlyOriginRentTax { get; set; }

        public decimal PriceOffered { get; set; }

        public decimal MonthlyOrigin8FtRentTax { get; set; }

        public decimal MonthlyDest8FTRentTax { get; set; }

        public decimal RepositionFeeBaseCost { get; set; }

        public decimal RepositionFeeTax { get; set; }

        public decimal OriginBaseBaseCost { get; set; }

        public decimal OriginBaseTax { get; set; }

        public decimal FlexDiscountValue { get; set; }

        public int CPPQuantity { get; set; }

        public int LocksQuantity { get; set; }

        public decimal LDMAdminFee { get; set; }

        public decimal FuelAdjustment { get; set; }

        public string FuelAdjustmentDisplayName { get; set; }

        public decimal OldQuoteAdjustmentPrice { get; set; }

        public bool IsOverRide { get; set; }

        // Facility Admin Cost starts

        public bool IsRepositioningFee { get; set; }

        public bool IsOriginBase { get; set; }

        public bool IsOriginAddlBase { get; set; }

        public bool IsOriginAdminFee { get; set; }

        public bool IsDestBase { get; set; }

        public bool IsDestAddlBase { get; set; }

        public bool IsDestAdminFee { get; set; }

        public bool IsFirstMonthFee { get; set; }

        public bool IsPrapAfflAmount { get; set; }

        // Facility Admin cost ends

        public decimal OriginExcessMileageAmount { get; set; }

        public decimal DestinationExcessMileageAmount { get; set; }

        public decimal OriginAdminFeeBaseCost { get; set; }

        public decimal OriginAdminFeeTax { get; set; }

        public decimal DestBaseBaseCost { get; set; }

        public decimal DestBaseTax { get; set; }

        public decimal MonthlyDestRentTax { get; set; }

        public decimal DeliverySurchargeFeeBaseCost { get; set; }

        public decimal FacilityAdminCostBaseCost { get; set; }// Base Cost

        public decimal FacilityAdminCostTax { get; set; } // tax

        public int NoOfUnit { get; set; }

        public decimal OriginAddlBaseBaseCost { get; set; }

        public decimal OriginAddlBaseTax { get; set; }

        public decimal DestAddlBaseCost { get; set; }

        public decimal DestAddlBaseTax { get; set; }

        public decimal DestAdminFeeBaseCost { get; set; }

        public decimal DestAdminFeeTax { get; set; }

        public decimal FirstMonthStorageBaseCost { get; set; }

        public decimal FirstMonthStorageTax { get; set; }

        public decimal PrapAmountBaseCost { get; set; }

        public decimal PrapAmountTax { get; set; }

        public decimal LocksTaxValue { get; set; }

        public decimal BaseCOGSBaseCost { get; set; }

        public decimal BaseCOGSTax { get; set; }
    }

    public class LDMTaxPercent
    {
        public decimal ExpediteTaxPercent { get; set; }

        public decimal ExcessMilesOriginTaxPercent { get; set; }

        public decimal ExcessMilesDestTaxPercent { get; set; }

        public decimal BudgetedTaxPercent { get; set; }

        public decimal ReposTaxPer { get; set; }

        public decimal CPPTaxPer { get; set; }

        public decimal StdBlanketTaxPer { get; set; }

        public decimal LocksTaxPer { get; set; }

        public decimal OriginAddBaseTaxPercent { get; set; }

        public decimal DestAddBaseTaxPercent { get; set; }

        public decimal FMSOriginTaxPer { get; set; }

        public decimal FMSDestinationTaxPer { get; set; }

        public decimal WTTaxPer { get; set; }

        public decimal OriginBaseTaxPer { get; set; }

        public decimal DestBaseTaxPer { get; set; }

        public decimal OrgLDMAdminTaxPercent { get; set; }

        public decimal DestLDMAdminFeeTaxPercent { get; set; }
    }

    public class BaseLDMPricingDetails
    {
        public BaseLDMPricingDetails()
        {
            LDMTaxPercent = new LDMTaxPercent();
        }

        public string FromZip { get; set; }

        public string ToZip { get; set; }

        public decimal ExcessMilessOrigin { get; set; } // Qty

        public decimal ExcessMilesOrigAmount { get; set; } // Total price

        public decimal ExcessMilesDest { get; set; } // qty

        public decimal ExcessMilesDestAmount { get; set; } // Total price

        public decimal BudgetTransportation { get; set; } // Total price

        public decimal ExpediteFee { get; set; }

        public decimal FacilityAdminCost { get; set; } // Total price

        public decimal DeliverySurchargeFee { get; set; }

        public decimal Weightticket { get; set; }

        public decimal WeightTicketTaxValue { get; set; }

        public decimal StandardBundled { get; set; }

        public decimal AdditionPOS { get; set; }
        
        public decimal CreditCardFee { get; set; }
        
        public decimal MonthlyOriginRent { get; set; }

        public decimal MonthlyDestRent { get; set; }


        public decimal MonthlyOrigin8FtRent { get; set; }

        public decimal MonthlyDest8FTRent { get; set; }

        public decimal DepositMinimum { get; set; }

        public decimal UnbundledPrice { get; set; }

        public decimal BundledPrice { get; set; }

        public decimal FacToFacPrice { get; set; }

        public decimal CCFeePercent { get; set; }

        public decimal TargetPercent { get; set; }

        public decimal MinimumPercent { get; set; }

        public decimal RepositionFee { get; set; }

        public decimal OriginBase { get; set; }

        public decimal OriginAddlBase { get; set; }

        public decimal OriginAdminFee { get; set; }

        public decimal DestBase { get; set; }

        public decimal DestAdminFee { get; set; }

        public decimal DestAddlBase { get; set; }

        public decimal FirstMonthStorage { get; set; }

        public decimal PrapAmount { get; set; }

        public decimal Locks { get; set; }

        public decimal ContentProtection { get; set; }

        public decimal StandardBlankets { get; set; }

        public int NoOfBlankets { get; set; }

        public decimal BaseCOGS { get; set; }

        public decimal TargetPercentAmount { get; set; }

        public decimal MinimumPercentAmount { get; set; }

        public decimal ManagerPercent { get; set; }

        public decimal ManagerMinAmount { get; set; }

        public decimal AgentPercent { get; set; }

        public decimal AgentMinAmount { get; set; }

        public decimal SupervisorPercent { get; set; }

        public decimal SupervisorMinAmount { get; set; }

        public decimal ExpediteTaxValue { get; set; }

        public decimal ExcessOriginTaxValue { get; set; }

        public decimal ExcessDestTaxValue { get; set; }

        public decimal BudgetTaxValue { get; set; }

        public decimal RepositionTaxValue { get; set; }

        public decimal CPPTaxValue { get; set; }

        public decimal StandardBlanketTaxValue { get; set; }

        public decimal BaseCOGSTaxValue { get; set; }

        public decimal OriginTouchBaseTaxValue { get; set; }

        public decimal DestTouchbaseTaxValue { get; set; }

        public decimal FirstMonthTaxValue { get; set; }

        public decimal SumOfPropertyAndGovermentFee { get; set; }

        public decimal LDMPercent { get; set; }

        public decimal OrgFeeTaxValue { get; set; }

        public decimal DestFeeTaxValue { get; set; }

        public decimal OrgLDMAdminFeeTaxValue { get; set; }

        public decimal DestLDMAdminFeeTaxValue { get; set; }

        public LDMTaxPercent LDMTaxPercent { get; set; }

        public decimal PreCreditCardFee { get; set; }

        public decimal LDmAdminFee { get; set; }

        public decimal TotalCost { get; set; }
        
        public decimal TargetPriceToCustomer { get; set; }
        
        public decimal MinimumAmountPrice { get; set; }

        //public decimal AgentFinalAmount
        //{
        //    get
        //    {
        //        decimal _agentFinalAmount = 0;
        //        decimal _AgentAmount = System.Math.Round(AgentMinAmount, 2);
        //        decimal CalculateAgentMinimumAmount = System.Math.Round(((BaseCOGS / (1 - AgentPercent)) - BaseCOGS), 2);
        //        decimal _decLdmFee = System.Math.Round(((PreCreditCardFee * LDMPercent) / 100), 2);

        //        if (CalculateAgentMinimumAmount < _AgentAmount)
        //            _agentFinalAmount = PreCreditCardFee + CreditCardFee + _decLdmFee + _AgentAmount;
        //        else if (CalculateAgentMinimumAmount > _AgentAmount || CalculateAgentMinimumAmount == _AgentAmount)
        //            _agentFinalAmount = PreCreditCardFee + CreditCardFee + _decLdmFee + CalculateAgentMinimumAmount;

        //        return _agentFinalAmount;
        //    }
        //}
    }
}
