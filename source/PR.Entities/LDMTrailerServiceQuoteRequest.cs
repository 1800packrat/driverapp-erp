﻿using System;
using System.Text;

namespace PR.Entities
{
    public class LDMTrailerServiceQuoteRequest
    {


        public string VendorName { get; set; }


        public string VendorCode { get; set; }

        public string VendorId { get; set; }

        public string VendorPass { get; set; }

        public string EntryDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }

        public string Phone { get; set; }


        public string Email { get; set; }

        public string UnitCalcMethod { get; set; }

        public string UnitCalcvalue { get; set; }

        public string OriginZipCode { get; set; }

        public string DestinationZipCode { get; set; }

        public string DryVan { get; set; }

        public string EstInitialDelivery { get; set; }

        public string EstDateAtDestination { get; set; }

        public string EstMosStorage { get; set; }

        public string Guarrantee { get; set; }

        public string Promo { get; set; }

        public string SendEmail { get; set; }

        public string CppValue { get; set; }

        public string AddEmails { get; set; }

        public string WebUnitCalcMethod { get; set; }


        public string WebUnitCalcValue { get; set; }

        public string WebSourceReferralCodes { get; set; }

        public string QuoteCreatedBy { get; set; }

        public string ConfirmationScreenName { get; set; }

        public string GetLDMXMLForPosting()
        {
            StringBuilder sb = new StringBuilder();

            //Vendor login section
            sb.Append("<PackRatXMLImport Version = \"1.0\" System = \"STARS\" >");
            sb.Append("<VendorLogin>");
            sb.Append("<element name=\"VendorName\" value=");
            sb.Append("\"" + this.VendorName + "\" />");
            sb.Append("<element name=\"VendorCode\" value=");
            sb.Append("\"" + this.VendorCode + "\" />");
            sb.Append("<element name=\"VendorID\" value=");
            sb.Append("\"" + this.VendorId + "\" />");
            sb.Append("<element name=\"VendorPass\" value=");
            sb.Append("\"" + this.VendorPass + "\" />");
            sb.Append("</VendorLogin>");

            //Set the right values for the parameters.
            if (this.DryVan == String.Empty) this.DryVan = "0";
            if (this.Guarrantee == String.Empty) this.Guarrantee = "0";
            if (this.EstMosStorage == String.Empty) this.EstMosStorage = "1";
            if (this.SendEmail == String.Empty) this.SendEmail = "F";


            //Quote section
            sb.Append("<Quote>");
            sb.Append("<element name=\"EntryDate\" value=");
            sb.Append("\"" + this.EntryDate + "\" />");
            sb.Append("<element name=\"ConfirmationScreenName\" value=");
            sb.Append("\"" + this.ConfirmationScreenName + "\" />");
            sb.Append("<element name=\"FirstName\" value=");
            sb.Append("\"" + this.FirstName + "\" />");
            sb.Append("<element name=\"LastName\" value=");
            sb.Append("\"" + this.LastName + "\" />");
            sb.Append("<element name=\"Address1\" value=");
            sb.Append("\"" + this.AddressLine1 + "\" />");
            sb.Append("<element name=\"Address2\" value=");
            sb.Append("\"" + this.AddressLine2 + "\" />");
            sb.Append("<element name=\"City\" value=");
            sb.Append("\"" + this.City + "\" />");
            sb.Append("<element name=\"State\" value=");
            sb.Append("\"" + this.State + "\" />");
            sb.Append("<element name=\"ZipCode\" value=");
            sb.Append("\"" + this.ZipCode + "\" />");
            sb.Append("<element name=\"Phone\" value=");
            sb.Append("\"" + this.Phone + "\" />");
            sb.Append("<element name=\"EMail\" value=");
            sb.Append("\"" + this.Email + "\" />");
            sb.Append("<element name=\"StorageTransport\" value=");
            sb.Append("\"" + this.UnitCalcMethod + "\" />");
            sb.Append("<element name=\"OptionSpeed\" value=");
            sb.Append("\"" + this.UnitCalcvalue + "\" />");
            sb.Append("<element name=\"OriginCity\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"OriginState\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"OriginZipCode\" value=");
            sb.Append("\"" + this.OriginZipCode + "\" />");
            sb.Append("<element name=\"DestinationCity\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"DestinationState\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"DestinationZipCode\" value=");
            sb.Append("\"" + this.DestinationZipCode + "\" />");
            sb.Append("<element name=\"DryVan\" value=");
            sb.Append("\"" + this.DryVan + "\" />");
            sb.Append("<element name=\"EstInitialDelivery\" value=");
            sb.Append("\"" + this.EstInitialDelivery + "\" />");
            sb.Append("<element name=\"EstDateAtDestination\" value=");
            sb.Append("\"" + this.EstDateAtDestination + "\" />");
            sb.Append("<element name=\"EstMosStorage\" value=");
            sb.Append("\"" + this.EstMosStorage + "\" />");
            sb.Append("<element name=\"Guarantee\" value=");
            sb.Append("\"" + this.Guarrantee + "\" />");
            sb.Append("<element name=\"PromoCode\" value=");
            sb.Append("\"" + this.Promo + "\" />");
            sb.Append("<element name=\"SLPromo\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"SendEMail\" value=");
            sb.Append("\"" + this.SendEmail + "\" />");
            sb.Append("<element name=\"orderid\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"CPPLevel\" value=");
            sb.Append("\"" + this.CppValue + "\" />");
            sb.Append("<element name=\"AddEmails\" value=");
            sb.Append("\"" + this.AddEmails + "\" />");
            sb.Append("<element name=\"WebUnitCalcMethod\" value=");
            sb.Append("\"" + this.WebUnitCalcMethod + "\" />");
            sb.Append("<element name=\"WebUnitCalcValue\" value=");
            sb.Append("\"" + this.WebUnitCalcValue + "\" />");
            sb.Append("<element name=\"WebSourceReferralCodes\" value=");
            sb.Append("\"" + this.WebSourceReferralCodes + "\" />");
            sb.Append("<element name=\"QuoteCreatedBy\" value=");
            sb.Append("\"" + this.QuoteCreatedBy + "\" />");
            sb.Append("</Quote>");
            sb.Append("</PackRatXMLImport>");

            return sb.ToString();
        
        }

    }
}
