﻿namespace PR.Entities
{
    public class LDMSiteLinkWithBillingEntity
    {

        public int OriginSLQORId { get; set; }
        public int DestinationSLQORId { get; set; }
        public int BillingSLQORId { get; set; }
        public int OriginQTRentalID { get; set; }
        public int DestinationQTRentalID { get; set; }
        public int STARSUnitId { get; set; }
        public bool IsBillingUnit { get; set; }
        public bool IsDeleted { get; set; }
    }
}
