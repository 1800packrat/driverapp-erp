﻿using System;

namespace PR.Entities
{
    public class IVRZipValidationInfo
    {
        private string _zipCode1 = String.Empty;
        private string _zipCode2 = String.Empty;
        private string _serviceType = String.Empty;

        public string ZipCode1
        {
            get { return _zipCode1; }
            set { _zipCode1 = value; }
        }

        public string ZipCode2
        {
            get { return _zipCode2; }
            set { _zipCode2 = value; }
        }

        public string ServiceType
        {
            get { return _serviceType;}
            set { _serviceType = value; }
        }
    }
}
