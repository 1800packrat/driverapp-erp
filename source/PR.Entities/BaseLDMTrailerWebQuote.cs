﻿using System.Linq;
using System.Xml.Linq;


namespace PR.Entities
{
    public class BaseLDMTrailerWebQuote
    {

        public bool IsQuoteSuccess { get; set; }

        public string QuoteError { get; set; }

        public  BaseLDMTrailerWebQuote AssignXMLValuesToProperties(BaseLDMTrailerWebQuote childClassObj, string xmlString)
        {

            if (string.IsNullOrEmpty(xmlString))
            {
             
                return childClassObj;
            }
            XDocument xmldoc = XDocument.Parse(xmlString);
            var properties = childClassObj.GetType().GetProperties();
            foreach (var property in properties)
            {

                if (xmldoc.Descendants(property.Name).Any())
                    property.SetValue(childClassObj, xmldoc.Descendants(property.Name).Single().Value, null);


            }
            this.IsQuoteSuccess = true;
            return childClassObj;
        }

        public void SetTrailerProperties(LDMWebQuote ldmWebQuote)
        { 
           
        }
    }
}
