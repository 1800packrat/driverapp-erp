﻿using PR.UtilityLibrary;

namespace PR.Entities
{
    public class LedgerChargeErrorItemDetail
    {

        public int QorId { get; set; }
        public int UnitNo { get; set; }
        public string ChargeItemName { get; set; }
        public decimal ExistingChargeAmount { get; set; }
        public decimal NewChargeAmount { get; set; }

        public decimal VarianceAmount
        {
            get
            {
                return ExistingChargeAmount - NewChargeAmount;
            }

        }

        /// <summary>
        /// return DB log format string for current item
        /// </summary>
        public string ErrorLogString {
            get {

                return string.Format("QorId: {0} :: Charge Item : {1}  :: New Charge : {2}  ::  Existing Charge : {3} ", this.QorId, this.ChargeItemName, this.NewChargeAmount.ToString().ToLeadingMinusCurrency(), this.ExistingChargeAmount.ToString().ToLeadingMinusCurrency());

            }
        }


        /// <summary>
        ///  return email format string for current item
        /// </summary>
        public string EmailStringItem
        {
            get
            {
              return string.Format("<br><b> QorId :</b> {0}  ::  <b> Unit # : </b> {1}  ::<b>Charge Item :</b> {2}   :: <b> Existing Charge :</b> {3}  :: <b> New Charge :</b> {4}  ::  <b> Variance :   </b> {5} ", 
                                                                                this.QorId,
                                                                                this.UnitNo,
                                                                                this.ChargeItemName,
                                                                                this.ExistingChargeAmount.ToString().ToLeadingMinusCurrency(),
                                                                                this.NewChargeAmount.ToString().ToLeadingMinusCurrency(),
                                                                                this.VarianceAmount.ToString().ToLeadingMinusCurrency());
            }


        }
    }
}
