﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class ETASettings
    {
        public ETASettings()
        {
            //assigning default values.
            this.Status = "ETANotProvided";
            this.BackGroundColor = "#FFFFFF";
            this.TextColor = "#000000";
            this.CanDisplayETAStatus = false;
            this.ProvidedETARange = string.Empty;
        }

        public string Status { get; set; }
        public string BackGroundColor { get; set; }
        public string TextColor { get; set; }
        public bool CanDisplayETAStatus { get; set; }
        public string ProvidedETARange { get; set; }
        public string TouchKey { get; set; }
        public string BackGroundColorKey { get; set; }
        public string TextColorKey { get; set; }
    }
}
