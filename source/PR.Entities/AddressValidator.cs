﻿// -----------------------------------------------------------------------
// <copyright file="AddressValidator.cs" Name="Rajib basu" Date="July 22,2014">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PR.Entities
{
    using System;

    /// <summary>
    /// Set/Get  origin/destination address, distance, duration
    /// </summary>
    public class AddressValidator
    {
        private Address _originationAddress = new Address();
        private Address _destinationAddress = new Address();
        private decimal _distance = 0.0M;
        private string _duration = String.Empty;
        private string _errorMessage = String.Empty;
        private bool _isStraightLineDisdtance = false;

        public Address OriginationAddress
        {
            get { return _originationAddress; }
            set { _originationAddress = value; }
        }
        public Address DestinationAddress
        {
            get { return _destinationAddress; }
            set { _destinationAddress = value; }
        }
        public decimal Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }
        public string Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public bool IsStraightLineDistance
        {
            get { return _isStraightLineDisdtance; }
            set { _isStraightLineDisdtance = value; }
        }
    }


    /// <summary>
    /// Store/Retrieve  Latitude & Longitude
    /// </summary>
    public class GeoInfo
    {
        private string _latitude = String.Empty;
        private string _longitude = String.Empty;
        private string _error = String.Empty;

        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
    }
}
