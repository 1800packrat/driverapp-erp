﻿using System;

namespace PR.Entities
{
    public class PCMilerGeoinfo
    {
        private string _latitude = String.Empty;
        private string _longitude = String.Empty;
        private string _error = String.Empty;

        public string Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public string Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
    }
}
