﻿namespace PR.Entities
{
    public class LDMTrailerWebQuoteResponse
    {
        public LDMWebQuote LDMWebQuoteResponse { get; set; }

        public TrailerWebQuote TrailerWebQuoteResponse { get; set; }
    }
}
