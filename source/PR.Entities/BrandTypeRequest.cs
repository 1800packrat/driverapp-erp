﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    [DataContract]
    public class BrandTypeValidator
    {
        public BrandTypeValidator(string UnitNo)
        {
            this.UnitNo = UnitNo;
        }

        [DataMember]
        public int QORID { get; set; }

        [DataMember]
        public int StarsQuoteID { get; set; }

        [DataMember]
        public int? StarsUnitID { get; set; }

        [DataMember]
        public string UnitNo { get; private set; }

        [DataMember]
        public BrandType BrandType { get; set; }

        public bool IsTemplateUnit
        {
            get
            {
                string[] TemplateUnitNo = new string[3] { "000016", "000012", "000008" };
                
                return TemplateUnitNo.Contains(UnitNo);
            }
        }

        public bool IsLocal { get; set; }

    }
}
