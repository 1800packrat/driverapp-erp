using System;

namespace PR.Entities
{
    public class PCMilerReurnClass
    {
        private Address _originationAddress = new Address();
        private Address _destinationAddress = new Address();
        private decimal _distance = 0.0M;
        private string _duration = String.Empty;
        private string _errorMessage = String.Empty;
        private bool _isStraightLineDisdtance = false;

        public Address OriginationAddress
        {
            get { return _originationAddress; }
            set { _originationAddress = value; }
        }
        public Address DestinationAddress
        {
            get { return _destinationAddress; }
            set { _destinationAddress = value; }
        }
        public decimal Distance
        {
            get { return _distance; }
            set { _distance = value; }
        }
        public string Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        public bool IsStraightLineDistance
        {
            get { return _isStraightLineDisdtance; }
            set { _isStraightLineDisdtance = value; }
        }
    }
}
