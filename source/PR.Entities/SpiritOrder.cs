﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities
{
    public class SpiritOrder
    {
        public Guid GUID { get; set; }

        public int QORID { get; set; }

        public string UnitNumber { get; set; }

        public string LocationCode { get; set; }

        public List<SpiritTouchUpdate> TouchUpdates { get; set; }
    }
}
