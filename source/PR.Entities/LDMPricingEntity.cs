﻿namespace PR.Entities
{
    public class LDMPricingEntity 
    {

        /// <summary>
        /// Site link Description ID
        /// </summary>
      //   public int ChargeDescriptionID{ get; set; }


        /// <summary>
        /// POS/Recurring Item Description
        /// </summary>
        public string ItemDescription { get; set; }


        /// <summary>
        /// POS/Recurring Item Name
        /// </summary>
        public string LDMPricingEntityName { get; set; }
        
        
        /// <summary>
        /// no. of itmes 
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Price of the Item
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// returns totla price(price * Quantity) of this item.
        /// </summary>
        public decimal Total
        {
            get 
            {
                return (Quantity * Price) - Discount;
            }
        }


        /// <summary>
        /// Discount on total 
        /// </summary>
        public decimal Discount  { get; set; }



      
    }
}
