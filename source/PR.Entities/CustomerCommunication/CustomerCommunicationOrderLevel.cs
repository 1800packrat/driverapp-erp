﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.CustomerCommunication
{
    public class CustomerCommunicationOrderLevel : BaseCustomerCommunication
    {
        public int StarsID { get; set; }
        public int Qorid { get; set; }
    }
}
