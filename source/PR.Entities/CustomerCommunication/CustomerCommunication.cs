﻿
using System.Collections.Generic;

namespace PR.Entities.CustomerCommunication
{

    public class CustomerCommunication : CustomerCommunicationOrderLevel
    {
        public string Email { get; set; }

        public string Phone { get; set; }

        public int StarsCustId { get; set; }
    }
}