﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Entities.CustomerCommunication
{
    public class CustomerCommunicationPreferences : CustomerCommunicationOrderLevel
    {
        public bool SetBillingPreference { get; set; }
        [Required]
        public IEnumerable<CommunicationPreferences> dataTableList { get; set; }
    }
}
