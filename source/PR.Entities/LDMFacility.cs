﻿namespace PR.Entities
{
    public class LDMFacility
    {
            private string _billingFName = string.Empty;
            private string _billingLName = string.Empty;
            private string _billingAddr1 = string.Empty;
            private string _billingAddr2 = string.Empty;
            private string _billingCity = string.Empty;
            private string _billingState = string.Empty;
            private string _billingZip = string.Empty;
            private string _billingPhone = string.Empty;
            private string _billingEmail = string.Empty;
            private string _deliveryFName = string.Empty;
            private string _deliveryLName = string.Empty;
            private string _deliveryAddr1 = string.Empty;
            private string _deliveryAddr2 = string.Empty;
            private string _deliveryCity = string.Empty;
            private string _deliveryState = string.Empty;
            private string _deliveryZip = string.Empty;
            private string _deliveryPhone = string.Empty;
            private string _deliveryEMail = string.Empty;


            public string BillingFName
            {
                get { return _billingFName; }
                set { _billingFName = value; }
            }
            public string BillingLName
            {
                get { return _billingLName; }
                set { _billingLName = value; }
            }
            public string BillingAddr1
            {
                get { return _billingAddr1; }
                set { _billingAddr1 = value; }
            }
            public string BillingAddr2
            {
                get { return _billingAddr2; }
                set { _billingAddr2 = value; }
            }
            public string BillingCity
            {
                get { return _billingCity; }
                set { _billingCity = value; }
            }
            public string BillingState
            {
                get { return _billingState; }
                set { _billingState = value; }
            }
            public string BillingZip
            {
                get { return _billingZip; }
                set { _billingZip = value; }
            }
            public string BillingPhone
            {
                get { return _billingPhone; }
                set { _billingPhone = value; }
            }
            public string BillingEmail
            {
                get { return _billingEmail; }
                set { _billingEmail = value; }
            }
            public string DeliveryFName
            {
                get { return _deliveryFName; }
                set { _deliveryFName = value; }
            }
            public string DeliveryLName
            {
                get { return _deliveryLName; }
                set { _deliveryLName = value; }
            }
            public string DeliveryAddr1
            {
                get { return _deliveryAddr1; }
                set { _deliveryAddr1 = value; }
            }
            public string DeliveryAddr2
            {
                get { return _deliveryAddr2; }
                set { _deliveryAddr2 = value; }
            }
            public string DeliveryCity
            {
                get { return _deliveryCity; }
                set { _deliveryCity = value; }
            }
            public string DeliveryState
            {
                get { return _deliveryState; }
                set { _deliveryState = value; }
            }
            public string DeliveryZip
            {
                get { return _deliveryZip; }
                set { _deliveryZip = value; }
            }
            public string DeliveryPhone
            {
                get { return _deliveryPhone; }
                set { _deliveryPhone = value; }
            }
            public string DeliveryEMail
            {
                get { return _deliveryEMail; }
                set { _deliveryEMail = value; }
            }
        }
    
    
}
