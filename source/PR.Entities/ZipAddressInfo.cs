using System;

namespace PR.Entities
{
    public class ZipAddressInfo
    {
        private string _city1 = String.Empty;
        private string _state1 = String.Empty;
        private string _zip1 = String.Empty;
        private string _city2 = String.Empty;
        private string _state2 = String.Empty;
        private string _zip2 = String.Empty;


        public string City1
        {
            get { return _city1; }
            set { _city1 = value; }
        }
        public string State1
        {
            get { return _state1; }
            set { _state1 = value; }
        }
        public string Zip1
        {
            get { return _zip1; }
            set { _zip1 = value; }
        }
        public string City2
        {
            get { return _city2; }
            set { _city2 = value; }
        }
        public string State2
        {
            get { return _state2; }
            set { _state2 = value; }
        }
        public string Zip2
        {
            get { return _zip2; }
            set { _zip2 = value; }
        }
    }
}
