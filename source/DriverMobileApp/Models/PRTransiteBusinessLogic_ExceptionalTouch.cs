﻿using DriverMobileApp.Helper;
using PR.BusinessLogic;
using PR.Entities;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace DriverMobileApp.Models
{
    public class PRTransiteBusinessLogicExceptionTouch
    {
        #region Construtor and DataMembers

        ISitelinkRepository oISiteLinkrepository;
        ITouchService oITouchService;

        public PRTransiteBusinessLogicExceptionTouch(ISitelinkRepository _oISiteLinkrepository, ITouchService _oITouchService)
        {
            if (oISiteLinkrepository == null)
                oISiteLinkrepository = _oISiteLinkrepository;

            if (oITouchService == null)
                oITouchService = _oITouchService;
        }

        #endregion

        #region Methods

        public List<Touch> SearchTouch(string searchString, DateTime dtTouchDate, SiteInfo oSiteInfo)
        {
            List<Touch> whDataTable = GetMarketSearchWareHouseTouchesByFacility(searchString, dtTouchDate, oSiteInfo);

            return whDataTable;
        }

        public Touch AddExceptionalTouchToExitingRoute(int loadId, int previousTouchId, int previousStopId, Touch exceptionTouch, SiteInfo site)
        {
            int exceptionTouchId = oITouchService.HandleAddTouchToRoute(exceptionTouch, site, loadId, previousTouchId, previousStopId);
            Touch otouch = null;

            if (exceptionTouchId > 0)
                otouch = oITouchService.FetchTouchByTouchId(exceptionTouchId);

            return otouch;
        }

        public Touch RetrieveSearchedToucheByQorid(DateTime TouchOnDate, int LoadId, string storeNumber, int QorId, SiteInfo site, string touchType)
        {
            //--------------- ToDO - change following code-----------
            var TouchList = (from _TouchList in RetieveSearchTouchesFromSession(TouchOnDate, site).Where(t => t.Qorid == QorId && t.TouchType == touchType)
                             select new Touch
                             {
                                 Qorid = Convert.ToInt32(_TouchList.Qorid),
                                 LoadId = _TouchList.LoadId > 0 ? _TouchList.LoadId : LoadId,
                                 CustomerName = _TouchList.CustomerName,
                                 OriginAddress = _TouchList.OriginAddress,
                                 DestAddress = _TouchList.DestAddress,
                                 UnitNumber = _TouchList.UnitNumber,
                                 Unitsize = _TouchList.Unitsize,
                                 StartTime = _TouchList.StartTime,
                                 ScheduledDate = _TouchList.ScheduledDate,
                                 GlobalSiteNum = _TouchList.GlobalSiteNum,
                                 PhoneNumber = _TouchList.PhoneNumber,
                                 SequenceNO = _TouchList.SequenceNO,
                                 TouchType = _TouchList.TouchType,
                                 Status = _TouchList.Status,
                                 IsGrouped = _TouchList.IsGrouped,
                                 TouchId = _TouchList.TouchId,
                                 GroupGUID = _TouchList.GroupGUID,
                                 RouteSequence = _TouchList.RouteSequence,
                                 FacCode = _TouchList.FacCode,
                                 IsWeightTicket = _TouchList.IsWeightTicket,
                                 IsZippyShellQuote = _TouchList.IsZippyShellQuote,
                                 StarsID = _TouchList.StarsID,
                                 StarsUnitId = _TouchList.StarsUnitId,
                                 Instructions = _TouchList.Instructions
                             }).ToList().FirstOrDefault();
            return TouchList;
        }

        private List<Touch> RetieveSearchTouchesFromSession(DateTime dtTouchDate, SiteInfo site)
        {
            if (HttpContext.Current.Session["SearchTouchList"] == null)
            {
                //need to think another logic here
                HttpContext.Current.Session["SearchTouchList"] = SearchTouch(string.Empty, dtTouchDate, site);
                return (List<Touch>)HttpContext.Current.Session["SearchTouchList"];
            }
            else
            {
                return (List<Touch>)HttpContext.Current.Session["SearchTouchList"];
            }
        }

        private List<Touch> GetMarketSearchWareHouseTouchesByFacility(string searchTerm, DateTime dtTouchDate, SiteInfo site)
        {
            List<Touch> openSLTouches = new List<Touch>();
            List<string> completedTouchesOrderNo = new List<string>();

            List<Touch> allDbTouches = oITouchService.GetAllTouchesForFacility(site.GlobalSiteNum, dtTouchDate);

            openSLTouches = SearchWareHouseTouchesByFacility(searchTerm, dtTouchDate, site);

            foreach (var marketFacility in site.MarketFacilities)
            {
                List<Touch> marketFacilityWHDataTable = new List<Touch>();
                marketFacilityWHDataTable = SearchWareHouseTouchesByFacility(searchTerm, dtTouchDate, marketFacility);

                allDbTouches.AddRange(oITouchService.GetAllTouchesForFacility(site.GlobalSiteNum, dtTouchDate));

                if (marketFacilityWHDataTable != null && marketFacilityWHDataTable.Count > 0)
                    openSLTouches.AddRange(marketFacilityWHDataTable);
            }

            if (allDbTouches != null && allDbTouches.Count > 0)
            {
                completedTouchesOrderNo = allDbTouches.Where(t => t.Status == TouchStatus.Completed.ToString() || t.Status == TouchStatus.InProgress.ToString()).Select(s => s.OrderNumber).ToList();

                if (completedTouchesOrderNo.Count > 0 && openSLTouches.Count > 0)
                    openSLTouches = openSLTouches.Where(p => !completedTouchesOrderNo.Any(db => db == p.OrderNumber)).ToList();

                openSLTouches.ForEach(sl =>
                {
                    var dbt = allDbTouches.Where(o => o.OrderNumber == sl.OrderNumber).FirstOrDefault();
                    if (dbt != null)
                    {
                        sl.TouchId = dbt.TouchId;
                        sl.GroupGUID = dbt.GroupGUID;
                        sl.RouteSequence = dbt.RouteSequence;
                        sl.LoadId = dbt.LoadId;
                        sl.IsZippyShellQuote = dbt.IsZippyShellQuote || sl.IsZippyShellQuote;
                    }
                });
            }

            return openSLTouches;
        }

        private List<Touch> SearchWareHouseTouchesByFacility(string searchTerm, DateTime dtTouchDate, SiteInfo site)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic("DriverApplication");
            DataTable whDataTable = new DataTable();

            List<Touch> lstTouches = oISiteLinkrepository.GetUnassignedTouches(site, dtTouchDate);

            if (lstTouches != null && lstTouches.Count > 0)
            {
                searchTerm = searchTerm.ToLower();
                lstTouches = lstTouches.Where(s => s.Status == TouchStatus.Open.ToString()).Where(t =>
                                                t.GlobalSiteNum.ToLower().Contains(searchTerm) ||
                                                t.CustomerName.ToLower().Contains(searchTerm) ||

                                                Regex.Replace(t.PhoneNumber, @"\D", String.Empty, RegexOptions.Compiled).Contains(searchTerm) ||
                                                t.OriginAddress.FullAddress.ToLower().Contains(searchTerm) ||
                                                t.DestAddress.FullAddress.ToLower().Contains(searchTerm) ||
                                                t.UnitNumber.ToLower().Contains(searchTerm)
                                            ).ToList();
            }

            return lstTouches;
        }

        #endregion
    }
}