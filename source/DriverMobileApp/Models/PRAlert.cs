﻿using System;
using System.Collections.Generic;
using System.Linq;
using DriverMobileApp.Models.DBclass;
using System.Web.Mvc;

namespace DriverMobileApp.Models
{
    /// <summary>
    /// PRAlert class contains MessageTransaction db related actions goes on here
    /// </summary>
    public class PRAlert
    {
        PRMessageDBDataContext oDBDataContext = new PRMessageDBDataContext();
        private static string MessageApplicationName = "Driver Application";
        private static string MessageSourceType = "Driver";
        private static string MessageStatus = "Waiting for response";

        #region Get List Methods

        public List<MessageType> GetMessageType()
        {
            var PRMessageType = (from _PRMessageType in oDBDataContext.PRMessageTypes.AsEnumerable()
                                 select new MessageType
                                 {
                                     MType = _PRMessageType.MessageType,
                                     TypeID = Convert.ToInt32(_PRMessageType.TypeID)
                                 }).DefaultIfEmpty().ToList();

            return PRMessageType;
        }

        public List<MessagePriority> GetMessagePriority()
        {
            var PRMessagePriority = (from _PRMessagePriority in oDBDataContext.PRMessagePriorities.AsEnumerable()
                                     select new MessagePriority
                                     {
                                         PrioriryID = _PRMessagePriority.PriorityID,
                                         Priority = _PRMessagePriority.Priority
                                     }).DefaultIfEmpty().ToList();

            return PRMessagePriority;
        }

        public List<MessageSourceType> GetMessageSourceType()
        {
            var PRMessageSourceType = (from _PRMessageSourceType in oDBDataContext.PRMessageSourceTypes.AsEnumerable()
                                       select new MessageSourceType
                                     {
                                         SourceTypeID = _PRMessageSourceType.SourceTypeID,
                                         SourceType = _PRMessageSourceType.SourceType
                                     }).DefaultIfEmpty().ToList();

            return PRMessageSourceType;
        }

        public List<MessageStatus> GetMessageStatus()
        {
            var PRMessageStatus = (from _PRMessageStatus in oDBDataContext.PRMessageStatus.AsEnumerable()
                                   select new MessageStatus
                                       {
                                           StatusID = _PRMessageStatus.StatusID,
                                           Status = _PRMessageStatus.Status
                                       }).DefaultIfEmpty().ToList();

            return PRMessageStatus;
        }

        public List<MessageApplication> GetMessageApplication()
        {
            var PRMessageApplication = (from _PRPRMessageApplication in oDBDataContext.PRMessageApplications.AsEnumerable()
                                        select new MessageApplication
                                        {
                                            ApplicationID = _PRPRMessageApplication.ApplicationID,
                                            ApplicationName = _PRPRMessageApplication.ApplicationName
                                        }).DefaultIfEmpty().ToList();

            return PRMessageApplication;
        }

        #endregion

        #region Selection List Methods

        public List<SelectListItem> GetMessageTypeS()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            //for (Int16 intCount = 1; intCount <= 12; intCount++)
            foreach (MessageType item in GetMessageType())
            {
                items.Add(new SelectListItem { Text = item.MType, Value = Convert.ToString(item.TypeID) });
            }

            return items;
        }

        public List<SelectListItem> GetMessagePriorityS()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            //for (Int16 intCount = 1; intCount <= 12; intCount++)
            foreach (MessagePriority item in GetMessagePriority())
            {
                items.Add(new SelectListItem { Text = item.Priority, Value = Convert.ToString(item.PrioriryID) });
            }

            return items;
        }

        public List<SelectListItem> GetMessageSourceTypeS()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            //for (Int16 intCount = 1; intCount <= 12; intCount++)
            foreach (MessageSourceType item in GetMessageSourceType())
            {
                items.Add(new SelectListItem { Text = item.SourceType, Value = Convert.ToString(item.SourceTypeID) });
            }

            return items;
        }

        public List<SelectListItem> GetMessageStatusS()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            //for (Int16 intCount = 1; intCount <= 12; intCount++)
            foreach (MessageStatus item in GetMessageStatus())
            {
                items.Add(new SelectListItem { Text = item.Status, Value = Convert.ToString(item.StatusID) });
            }

            return items;
        }

        #endregion

        public void SaveMessageTransaction(MessageTransaction oMessageTransaction)
        {
            PRMessageTransaction alertmessage = new PRMessageTransaction
            {
                TransactionID = -1,
                PriorityID = oMessageTransaction.PriorityID,
                TypeID = oMessageTransaction.TypeID,
                TransiteDriverID = oMessageTransaction.TransiteDriverID,
                QORId = oMessageTransaction.QORId,
                //STARSId = oMessageTransaction.STARSId,
                //TouchType = oMessageTransaction.TouchType,
                SourceTypeID = oMessageTransaction.SourceTypeID,
                Source = oMessageTransaction.Source,
                ApplicationID = oMessageTransaction.ApplicationID,
                Message = oMessageTransaction.Message,
                StatusID = oMessageTransaction.StatusID,
                DateCreated = oMessageTransaction.DateCreated,
                CreateUserID = oMessageTransaction.CreateUserID
            };
            oDBDataContext.PRMessageTransactions.InsertOnSubmit(alertmessage);

            oDBDataContext.SubmitChanges();
        }

        public int GetMessageApplicationID()
        {
            return GetMessageApplication().First(x => x.ApplicationName == MessageApplicationName).ApplicationID;
        }
        public int GetMessageSourceTypeID()
        {
            return GetMessageSourceType().First(x => x.SourceType == MessageSourceType).SourceTypeID;
        }
        public int GetMessageStatusID()
        {
            return GetMessageStatus().First(x => x.Status == MessageStatus).StatusID;
        }
    }

    public class AlertInfo : MessageTransaction
    {
        public List<SelectListItem> MessageType { get; set; }

        public List<SelectListItem> MessagePriority { get; set; }

        public List<SelectListItem> MessageSourceType { get; set; }

        public List<SelectListItem> MessageStatus { get; set; }
    }

    /// <summary>
    /// MessageType stores list of Types
    /// </summary>
    public class MessageType
    {
        public Int32 TypeID { get; set; }
        public string MType { get; set; }
    }

    /// <summary>
    /// MessageType stores list of Types
    /// </summary>
    public class MessageApplication
    {
        public Int32 ApplicationID { get; set; }
        public string ApplicationName { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// MessagePriority stores list of priorities
    /// </summary>
    public class MessagePriority
    {
        public Int32 PrioriryID { get; set; }
        public string Priority { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// MessageSourceType stores list of SourceTypes
    /// </summary>
    public class MessageSourceType
    {
        public Int32 SourceTypeID { get; set; }
        public string SourceType { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// MessageStatus stores list of status
    /// </summary>
    public class MessageStatus
    {
        public Int32 StatusID { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// MessageTransaction stores complete alert information
    /// </summary>
    public class MessageTransaction
    {
        public Int32 TransactionID { get; set; }
        public Int32 PriorityID { get; set; }
        public Int32 TypeID { get; set; }
        public Int32 TransiteDriverID { get; set; }
        public Int32 QORId { get; set; }
        public Int32 STARSId { get; set; }
        public Int32 TouchType { get; set; }
        public Int32 SourceTypeID { get; set; }
        public string Source { get; set; }
        public Int32 ApplicationID { get; set; }
        public string Message { get; set; }
        public Int32 StatusID { get; set; }
        public DateTime? LastResponseDate { get; set; }
        public string LastResponseUserID { get; set; }
        public bool LastResponseFlag { get; set; }
        public DateTime DateCreated { get; set; }
        public string CreateUserID { get; set; }
        public DateTime? DateModified { get; set; }
        public string ModifyUserID { get; set; }
        public string Lat { get; set; }
        public string Long { get; set; }
    }
}