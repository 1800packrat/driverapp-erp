﻿using System;
using PR.Entities;
using System.Collections.Generic;
using AuditLog.DBStructure;
using System.Web.Mvc;

namespace DriverMobileApp.Models
{


    /// <summary>
    /// TouchData stores complete Touch information from PR.BusinessLogic
    /// </summary>
    public class TouchData
    {
        public Int32 QORID { get; set; }
        public Int32 QTID { get; set; }
        public string TouchType { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string AddrTo_Addr1 { get; set; }
        public string AddrTo_Addr2 { get; set; }
        public string AddrTo_City { get; set; }
        public string AddrTo_State { get; set; }
        public string AddrTo_Zip { get; set; }
        public string AddreTo_FName { get; set; }
        public string AddrTo_PhoneNum { get; set; }
        public string AddrTo_AltPhoneNum { get; set; }
        public string AddrTo_MobilePhoneNum { get; set; }
        public string AddrFrom_Addr1 { get; set; }
        public string AddrFrom_Addr2 { get; set; }
        public string AddrFrom_City { get; set; }
        public string AddrFrom_State { get; set; }
        public string AddrFrom_Zip { get; set; }
        public string AddrFrom_FName { get; set; }
        public string AddrFrom_LName { get; set; }
        public string AddrFrom_PhoneNum { get; set; }
        public string AddrFrom_MobilePhoneNumber { get; set; }
        public string AddrTo_LName { get; set; }
        public string Comments { get; set; }
        public string Directions { get; set; }
        public string Instructions { get; set; }
        public bool Drivewaytype_Sloped { get; set; }
        public bool Drivewaytype_Soft { get; set; }
        public bool Drivewaytype_Dirt { get; set; }
        public bool Drivewaytype_Grvel { get; set; }
        public bool Drivewaytype_Paved { get; set; }
        public bool Drivewaytype_Bricked { get; set; }
        public bool Drivewaytype_Curbed { get; set; }
        public bool Drivewaytype_Other { get; set; }
        public bool DoorFacesRear { get; set; }
        public bool Obstacle_PowerLines { get; set; }
        public bool Obstacle_Fences { get; set; }
        public bool Obstacle_Trees { get; set; }
        public bool Obstacle_Sprinklers { get; set; }
        public bool Obstacle_Landscaping { get; set; }
        public bool Obstacle_Septic { get; set; }
        public bool Obstacle_Other { get; set; }
        public decimal Distance { get; set; }
        public string EditIdentifier { get; set; }
        public string Status { get; set; }
        public string Starts { get; set; }
        public string End { get; set; }
        public string AddrTo_Company { get; set; }
        public string AddrFrom_Company { get; set; }
        public Int32 ChargeDescriptionID { get; set; }
        public decimal Charge { get; set; }
        public decimal ExcessDistance { get; set; }
        public decimal ExcessCharge { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Discounts { get; set; }
        public decimal Net { get; set; }
        public decimal Tax { get; set; }
        public string ServiceType { get; set; }
        public Int32 SequenceNumber { get; set; }
        public Int32 UnitLength { get; set; }
    }

    public class DriverTouchLocal
    {
        public string Company { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public decimal BalanceDue { get; set; }
        public string City { get; set; }
        public string ContainerNumber { get; set; }
        public int ContainerSize { get; set; }
        public string CustomerName { get; set; }
        public Address DestinationAddress { get; set; }
        public bool IsCombinedTouch { get; set; }
        public bool IsTrailerNeeded { get; set; }
        public string OrderNum { get; set; }
        public Address OriginAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string QORID { get; set; }
        public string ScheduledEndTime { get; set; }
        public string ScheduledStartTime { get; set; }
        public string EstimatedStartTime { get; set; }
        public string EstimatedEndTime { get; set; }
        public string ActualStartTime { get; set; }
        public string ActualEndTime { get; set; }
        public string SequenceNumber { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
        public int StopId { get; set; }
        public string StopType { get; set; }
        public string TouchType { get; set; }
        public TransiteCustomReferences TransiteCustRef { get; set; }
        public string TransiteTouchType { get; set; }
        public string Zip { get; set; }
        public Int32 TouchId { get; set; }
        public Int32 TouchStopId { get; set; }
        public bool SkipTouch { get; set; }
        public string FacilityName { get; set; }
        public ETASettings ETASettings { get; set; }
        public bool IsZippyShellMove { get; set; }
    }

    public class DriverTouchInfo
    {
        public CustomerInfo SiteLinkCustomerInfo { get; set; }
        public DriverTouchLocal DriverTouch { get; set; }
        public TouchData TouchData { get; set; }
        public IList<POSData> POSData { get; set; }
        public TouchInfoLog TouchInfoLog { get; set; }
        public TouchInfoUpdatedBySL TouchInfoUpdatedBySL { get; set; }
    }

    public class TouchInfoUpdatedBySL
    {
        public int TouchId { get; set; }
        public int TouchInfoSyncId { get; set; }

        public Address changedAddress { get; set; }

        public Address OriginAddress { get; set; }
        public Address DestAddress { get; set; }
        public int? SyncUpdatedOriginAddressId { get; set; }
        public int? SyncUpdatedDestAddressId { get; set; }
        public int? OriginAddressId { get; set; }
        public int? DestAddressId { get; set; }
        public string PhoneNumber { get; set; }
        public SLUpdateType SLUpdatedStatusType { get; set; }
        public int? SLUpdatedStatusTypeId { get; set; }
        public string CustomerName { get; set; }
        public string Comments { get; set; }
        public string Directions { get; set; }
        public string Instructions { get; set; }
        public bool Drivewaytype_Sloped { get; set; }
        public bool Drivewaytype_Soft { get; set; }
        public bool Drivewaytype_Dirt { get; set; }
        public bool Drivewaytype_Grvel { get; set; }
        public bool Drivewaytype_Paved { get; set; }
        public bool Drivewaytype_Bricked { get; set; }
        public bool Drivewaytype_Curbed { get; set; }
        public bool Drivewaytype_Other { get; set; }
        public bool DoorFacesRear { get; set; }
        public bool Obstacle_PowerLines { get; set; }
        public bool Obstacle_Fences { get; set; }
        public bool Obstacle_Trees { get; set; }
        public bool Obstacle_Sprinklers { get; set; }
        public bool Obstacle_Landscaping { get; set; }
        public bool Obstacle_Septic { get; set; }
        public bool Obstacle_Other { get; set; }
    }

    /// <summary>
    /// Populates data for TouchInfo controller
    /// </summary>
    public class TouchDataWithCustomerInfo
    {
        public TouchData TouchData { get; set; }
        public CustomerInfo CustomerInfo { get; set; }
        public SiteInfo FacilityInfo { get; set; }
        public IList<POSData> POSData { get; set; }
    }

    public enum SLUpdateType
    {
        Nochanges = 1,
        Sync = 2,
        Canceled = 3,
        OnlyAddressUpdated = 4,
        OnlyInfoUpdated = 5,
        AddressAndInfoUpdated = 6
    }

    /// <summary>
    /// Populates data for DashBoard controller
    /// </summary>
    public class DashBoardTouchinfo
    {
        public CustomerInfo CustomerInfo { get; set; }
        public List<DriverTouchLocal> RemainingTouchData { get; set; }

        public List<DriverTouchLocal> SkippedTouchData { get; set; }
        public List<DriverTouchLocal> CompletedTouchData { get; set; }
        public DriverTouchLocal NextTouchData { get; set; }
        public DriverTouchLocal CurrentTouchData { get; set; }
        public Int32 RemainingTouchCount { get; set; }

        public Int32 SkippedTouchCount { get; set; }
        public Int32 CompletedTouchCount { get; set; }
        public IList<POSData> POSData { get; set; }
    }

    /// <summary>
    /// Populates data for CustomerInfo controller ( Remaining Touches )
    /// </summary>
    public class RemainingToucheCustinfo
    {
        public DriverTouchLocal CustomerInfo { get; set; }
        public CustomerInfo SiteLinkCustomerInfo { get; set; }
        public TouchData TouchData { get; set; }
        public SiteInfo FacilityInfo { get; set; }
        public IList<POSData> POSData { get; set; }
    }

    /// <summary>
    /// POSData stores list of POS items
    /// </summary>
    public class POSData
    {
        public string ItemDescription { get; set; }
        public Int32 Quantity { get; set; }
    }
    /// <summary>
    /// Customer info class
    /// </summary>
    public class CustomerInfo
    {
        public string FirstName { get; set; }
        public string LName { get; set; }
        public string Company { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string PhoneAlt { get; set; }
        public string EmailID { get; set; }
    }

    /// <summary>
    /// Populates data for Bill Information
    /// </summary>
    public class BillDetailInfo
    {
        public TransportationItems TransportationItems { get; set; }
        public POSItems POSItems { get; set; }
        public RecurringItems RecurringItems { get; set; }
        public UnitInfo UnitInfo { get; set; }
        public PricingInfo PricingInfo { get; set; }
    }

    /// <summary>
    /// Populates dropdown for Skip Touch
    /// </summary>
    //public class SkipTouchDrpList
    //{
    //    public string SkipReasonId { get; set; }
    //    public string SkipReason { get; set; }
    //}

    public class ChangeCppInfo
    {
        public string SelectedCpp { get; set; }
        public List<SelectListItem> CppList { get; set; }
        public Int32 LockCount { get; set; }
        public Int32 Blankets { get; set; }
        public string Comments { get; set; }
        public Int32 Fk_Touch_StopId { get; set; }
        public Int32 DriverId { get; set; }
        public Int32 QorId { get; set; }
        public string TouchType { get; set; }
        public string StopType { get; set; }
        public string NewCPP { get; set; }
        public Int32 Lock { get; set; }
    }

    public enum TouchStatus
    {
        Open,
        OnHold,
        Completed,
        InProgress,
        Skipped,
    }

}