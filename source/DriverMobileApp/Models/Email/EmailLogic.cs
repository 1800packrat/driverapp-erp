﻿using PR.BusinessLogic;
using PR.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace DriverMobileApp.Models.Email
{
    public class EmailLogic
    {
        /// <summary>
        /// Method to send email for skip touch operation - HomeGrown
        /// </summary>
        /// <param name="mainReasonType"></param>
        /// <param name="drpOption"></param>
        /// <param name="drpCustReq"></param>
        /// <param name="oSkipTouchInfo"></param>
        /// <param name="qorid"></param>
        /// <param name="TouchStopId"></param>
        /// <param name="TouchType"></param>
        /// <param name="StopType"></param>
        /// <param name="MethodName"></param>
        /// <param name="UserID"></param>
        /// <param name="TouchDate"></param>
        /// <returns></returns>
        public bool SkipTouch_SendEmail(string mainReasonType, string drpOption, string drpCustReq, SkipTouchInfo oSkipTouchInfo, string qorid, string TouchStopId, string TouchType, string StopType, string MethodName, string UserID, string TouchDate, string LoginName)
        {
            PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();

            AppEmail oAppEmail = new AppEmail();
            EnDeCryption oEnDeCryption = new EnDeCryption();

            SkipTouch oSkipTouch = new SkipTouch();
            var reason = (from _reason in oSkipTouch.GetMainReasons() where _reason.Value == mainReasonType select _reason).SingleOrDefault();

            //SiteInfo oSiteInfo = (new PRbusinesslogic()).GetFacilityInfo(Convert.ToInt32(qorid));

            DateTime TouchOnDate = Convert.ToDateTime(TouchDate);
            DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(UserID), TouchOnDate, TouchType.ToString().Trim(), Convert.ToString(qorid), StopType.Trim(), TouchStatus.Skipped.ToString());

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            string strfacilityEmailId = oPRbusinesslogic.GetFacilityEmailId(Convert.ToInt32(qorid));

            string skipToEmailids = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" +
                                   ConfigurationManager.AppSettings["LocalLogistics"].ToString() + ";" +
                                   strfacilityEmailId;

            SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfo(Convert.ToInt32(qorid));

            oAppEmail.EmailTo = skipToEmailids;
            //oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
            //oAppEmail.EmailCc = strfacilityEmailId;
            //oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
            oAppEmail.EmailFrom = strfacilityEmailId;
            //oAppEmail.Subject = "Skip Touch";
            oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Skip Touch";

            StringBuilder emailBody = new StringBuilder();

            emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            emailBody.Append("<head><title>1800PACKRAT.COM | Skip Touch information</title></head>");
            emailBody.Append("<body>");

            emailBody.Append("<br />Skip Touch information<br /><br />");
            emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID / Name </td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oEnDeCryption.Decrypt(UserID) + " - " + LoginName + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Reason</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + reason.Text + "</td>");
            emailBody.Append("</tr>");

            if (mainReasonType == "1")
            {
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Did you pick up a container</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (oSkipTouchInfo.wContainer ? "Yes" : "No") + "<br />");
                emailBody.Append("</tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Container No</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oSkipTouchInfo.containerNo + "</td>");
                emailBody.Append("</tr>");
            }
            if (mainReasonType == "2")
            {
                var CNA_reason = (from _reason in oSkipTouch.GetCnotAccessible() where _reason.Value == drpOption select _reason).SingleOrDefault();


                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'></td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + CNA_reason.Text + "<br />");
                emailBody.Append("</tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Reason</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (oSkipTouchInfo.byVehicle ? "By Vehicle" : ""));
                emailBody.Append((oSkipTouchInfo.rain ? "Rain" : "") + "<Br/>");
                emailBody.Append((oSkipTouchInfo.mud ? "Mud" : "") + "<Br/>");
                emailBody.Append((oSkipTouchInfo.snow ? "Snow" : "") + "<Br/>");
                emailBody.Append((oSkipTouchInfo.other ? "Other" : "") + "<Br/>");
                emailBody.Append("</td>");
                emailBody.Append("</tr>");
            }
            if (mainReasonType == "3")
            {
                string concString = string.Empty;
                if (oSkipTouchInfo.byCustRequest)
                {
                    var custReq = (from _custReq in oSkipTouch.GetCustRequest() where _custReq.Value == drpCustReq select _custReq).SingleOrDefault();
                    concString = " - " + "By Customer request" + "<br />";
                    concString = concString + custReq.Text.Trim() + "<br /><br />";
                }
                if (oSkipTouchInfo.containerLocked)
                {
                    concString = concString + "Container locked" + "<br />";
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer not Ready</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + concString);
                emailBody.Append("</tr>");
            }
            if (mainReasonType == "4")
            {
                var oWeight = (from _reason in oSkipTouch.GetCOverWeight() where _reason.Value == drpOption select _reason).SingleOrDefault();
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Weight</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oWeight.Text.Trim() + "<br />");
                emailBody.Append("</tr>");
            }

            if (oDriverTouch != null)
            {
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.CustomerName + "</td>");
                emailBody.Append("</tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Company</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (string.IsNullOrWhiteSpace(oDriverTouch.Company) ? "" : oDriverTouch.Company) + "</td>");
                emailBody.Append("</tr>");
            }

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>QORID</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + qorid + "</td>");
            emailBody.Append("</tr>");

            string unitNumber = string.Empty;
            if (oDriverTouch != null)
            {
                unitNumber = oDriverTouch.ContainerNumber;
            }
            else
            {
                try
                {
                    using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
                    {
                        var stopinfo = oDB.StopInformations.Where(s => s.Pk_Touch_StopId == Convert.ToInt32(TouchStopId)).FirstOrDefault();
                        unitNumber = stopinfo.TouchInformation.ContainerNo;
                    }
                }
                catch { }
            }
            if (string.IsNullOrEmpty(unitNumber) == false)
            {
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Unit Number</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.ContainerNumber + "</td>");
                emailBody.Append("</tr>");
            }

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Touch Type</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + TouchType + " - " + StopType + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver's comments</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oSkipTouchInfo.comments + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("</table><br /><br />");

            emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

            oAppEmail.EmailBody = emailBody.ToString();

            //Inserting Email Log
            Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
            el.ApplicationName = "Driver App";
            el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            el.User = oEnDeCryption.Decrypt(UserID);
            el.StartTime = DateTime.Now;
            el.Subject = oSiteInfo.LegalName.ToString() + " | Skip Touch";
            el.ToEmailIDs = skipToEmailids;
            el.FromEmailID = strfacilityEmailId;
            el.CCEmailIDs = null;
            el.MethodName = MethodName;
            el.EmailTypeID = 2;
            el.QORID = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.QORID);
            el.TouchType = oDriverTouch == null ? "" : oDriverTouch.TouchType;
            el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

            var LogId = LogEmails.InsertLog(el);
            if (oAppEmail.SendEMail() == "Success")
            {
                //updating endtime in email log
                Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
                eml.PK_ID = LogId;
                eml.EndTime = DateTime.Now;
                LogEmails.UpdateLog(eml);
                return true;
            }
            else
                return false;
        }
        
        /// <summary>
        /// Method to send email for touch complete operation - HomeGrown
        /// </summary>
        /// <param name="QorID"></param>
        /// <param name="oAuditLogMaster"></param>
        /// <param name="unitNo"></param>
        /// <param name="UserID"></param>
        /// <param name="TouchDate"></param>
        /// <param name="currentTouchType"></param>
        /// <param name="localDateTime"></param>
        /// <param name="driverName"></param>
        /// <param name="isThisTruckFromDiferentFacilityForDE"></param>
        /// <param name="unitFacilityName"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        public string TouchComplete_SendEmail(Int32 QorID, AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string UserID, DateTime TouchDate, string currentTouchType, string localDateTime, string driverName, bool isThisTruckFromDiferentFacilityForDE, string unitFacilityName, string remark, bool isUnitNoValidButNotMovedIn)
        {
            try
            {
                AppEmail oAppEmail = new AppEmail();
                PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
                CustomerInfo oCustomerInfo = null;
                try
                {
                    oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QorID);
                }
                catch (Exception ex)
                {
                    (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                }

                UnitInfo oUnitInfo = null;

                try
                {
                    oUnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
                }
                catch { }

                SiteInfo oSiteInfo = null;

                try { oSiteInfo = oPRbusinesslogic.GetFacilityInfo(QorID); }
                catch { }

                DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(UserID, TouchDate, currentTouchType, QorID.ToString(), oAuditLogMaster.StopType);

                string strfacilityEmailId = oPRbusinesslogic.GetFacilityEmailId(Convert.ToInt32(QorID));
                string touchcompleteEmailIds = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" + strfacilityEmailId;

                //oAppEmail.EmailTo = "gurupratap.chilakala@spectraforce.com";
                //oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();

                //// SFERP-TODO-CTUPD - Added by Sohan
                //// below 2 lines are Commented for testing purpose. 
                //oAppEmail.EmailTo = touchcompleteEmailIds;
                //if (oSiteInfo != null) oAppEmail.EmailFrom = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";

                oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString(); //Testing only
                oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString(); //Testing only

                if (oSiteInfo != null) oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Touch Complete Confirmation";

                // Added by Sohan
                // below 3 lines are Commented for testing purpose. 
                oAppEmail.Attachment = GetTouchPDF(oAuditLogMaster.LogID);
                oAppEmail.ReportFormat = "pdf";
                oAppEmail.AttachedFileName = "TouchTicket";

                oAppEmail.EmailCc = "";

                StringBuilder emailBody = new StringBuilder();
                emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
                emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                emailBody.Append("<head><title>1800PACKRAT.COM | Touch Complete Confirmation</title></head>");
                emailBody.Append("<body>");

                emailBody.Append("<br />Touch Complete Information<br /><br />");
                if (isThisTruckFromDiferentFacilityForDE && !string.IsNullOrEmpty(unitFacilityName))
                {
                    oAppEmail.EmailTo = touchcompleteEmailIds + ";" + ConfigurationManager.AppSettings["inventoryEmail"].ToString(); //Added InventoryEmail on 19-10-2016 as per LL-303 bug id
                    emailBody.Append("<br /><div style='color: red;'>Inventory Alert:  " + unitFacilityName + " Unit delivered to " + oSiteInfo.LegalName.ToString() + " customer</div><br /><br />");
                }

                if (isUnitNoValidButNotMovedIn && currentTouchType != "DE")
                {
                    emailBody.Append("<br /><div style='color: red;'>Not Auto Completed. DE touch not moved in.</div><br /><br />");
                }

                emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

                if (oCustomerInfo != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.FirstName + " " + oCustomerInfo.LName + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Billing Address</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Addr1 + "<br />" +
                                oCustomerInfo.Addr2 + "<br />" + oCustomerInfo.City + (String.IsNullOrWhiteSpace(oCustomerInfo.City) ? "" : ", ") + oCustomerInfo.Region + (String.IsNullOrWhiteSpace(oCustomerInfo.Region) ? "" : ", ") + "<br />" + oCustomerInfo.Country + " - " + oCustomerInfo.PostalCode + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Phone No</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Phone + ", Alt Phone No " + oCustomerInfo.PhoneAlt + "</td>");
                    emailBody.Append("</tr>");
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID / Name</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + UserID + " " + driverName + "</td>");
                emailBody.Append("</tr>");

                string purchaseOrder = oPRbusinesslogic.GetPRGQuoteOrderLogData(QorID);
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Purchase Order</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (String.IsNullOrWhiteSpace(purchaseOrder) ? "N/A" : purchaseOrder) + "</td>");
                emailBody.Append("</tr>");

                if (oDriverTouch != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Origin Address</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.OriginAddress.AddressLine1 + "<br />" +
                    oDriverTouch.OriginAddress.AddressLine2 + "<br />" + oDriverTouch.OriginAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.City) ? "" : ", ") + oDriverTouch.OriginAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.State) ? "" : ", ") + " - " + oDriverTouch.OriginAddress.Zip + "</td>");

                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Destination Address</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.DestinationAddress.AddressLine1 + "<br />" +
                    oDriverTouch.DestinationAddress.AddressLine2 + "<br />" + oDriverTouch.DestinationAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.City) ? "" : ", ") + oDriverTouch.DestinationAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.State) ? "" : ", ") + " - " + oDriverTouch.DestinationAddress.Zip + "</td>");

                    emailBody.Append("</tr>");
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>QORID</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + QorID.ToString() + "</td>");
                emailBody.Append("</tr>");

                string tmpUnitNo = unitNo;
                if (oUnitInfo != null && oUnitInfo.Tables.Count > 0 && oUnitInfo.Tables[0].Rows.Count > 0)
                {
                    tmpUnitNo = (string.IsNullOrWhiteSpace(unitNo) ? oUnitInfo.Tables[0].Rows[0]["UnitName"].ToString() : unitNo);
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Delivered Unit Number</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + tmpUnitNo + "</td>");
                emailBody.Append("</tr>");

                if (oUnitInfo != null && oUnitInfo.Tables.Count > 0 && oUnitInfo.Tables[0].Rows.Count > 0)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oUnitInfo.Tables[0].Rows[0]["UnitDescription"].ToString() + "</td>");
                    emailBody.Append("</tr>");
                }
                if (oSiteInfo != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Facility Name</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LegalName.ToString() + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Location Code</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LocationCode.ToString() + "</td>");
                    emailBody.Append("</tr>");
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Touch Type</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + currentTouchType + "</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Date</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + localDateTime + "</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Comments</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + remark + "</td>");
                emailBody.Append("</tr>");
                emailBody.Append("</table><br /><br />");

                emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

                oAppEmail.EmailBody = emailBody.ToString();

                //Inserting Email Log
                Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
                el.ApplicationName = "Driver App";
                el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                el.User = UserID;
                el.StartTime = DateTime.Now;
                el.Subject = oSiteInfo.LegalName.ToString() + " | Touch Complete Confirmation"; ;
                el.ToEmailIDs = touchcompleteEmailIds;
                el.FromEmailID = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" + strfacilityEmailId;
                el.CCEmailIDs = null;
                el.MethodName = "CompleteTouch";
                el.EmailTypeID = 1;
                el.QORID = QorID;
                el.TouchType = currentTouchType;
                el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

                var LogId = LogEmails.InsertLog(el);
                if (oAppEmail.SendEMailWithAttachment() == "Success")
                {
                    //updating endtime in email log
                    Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
                    eml.PK_ID = LogId;
                    eml.EndTime = DateTime.Now;
                    LogEmails.UpdateLog(eml);
                    return "Success";
                }
                else
                    return "Unable to send Email";
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return ex.Message + " , " + "Unable to send Email";
            }
        }

        public Byte[] GetTouchPDF(string LogID = null)
        {
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

            AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = oActivityLog.RetLogMaster(LogID);

            DriverMobileApp.Models.CustomerSignature oCustomerSignature = new Models.CustomerSignature();

            string en64PDF = string.Empty;
            PR.UtilityLibrary.ERP_Enums.eQTType qtType = new PR.UtilityLibrary.ERP_Enums.eQTType();
            switch (oAuditLogMaster.TransiteTouchType)
            {
                case "CC":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.CurbToCurb;
                    break;
                case "RF":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseFull;
                    break;
                case "RE":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseEmpty;
                    break;
                case "DE":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbEmpty;
                    break;
                case "DF":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbFull;
                    break;
            }

            en64PDF = oCustomerSignature.GetTouchInfo(Convert.ToInt32(oAuditLogMaster.Qorid), qtType);

            byte[] bytPDF = System.Convert.FromBase64String(en64PDF);

            return bytPDF;
            //return File(bytPDF, "application/pdf", "TouchData.pdf");

        }

        /// <summary>
        /// Method to send email for billing details to Customer
        /// </summary>
        /// <param name="CustomerEmail"></param>
        /// <param name="oBillDetailInfo"></param>
        /// <param name="oCustomerInfo"></param>
        /// <param name="oSiteInfo"></param>
        /// <param name="intQorId"></param>
        /// <returns></returns>
        public bool EmailBillInfo(string CustomerEmail, BillDetailInfo oBillDetailInfo, CustomerInfo oCustomerInfo, SiteInfo oSiteInfo, Int32 intQorId)
        {
            AppEmail oAppEmail = new AppEmail();

            oAppEmail.EmailTo = String.IsNullOrWhiteSpace(CustomerEmail) ? null : CustomerEmail;
            oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();

            oAppEmail.Subject = "1800PACKRAT.COM - " + oSiteInfo.LegalName + " | Bill copy for Customer";

            StringBuilder emailBody = new StringBuilder();
            emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            emailBody.Append("<head><title>1800PACKRAT.COM | Bill copy for Customer</title></head>");
            emailBody.Append("<body>");

            emailBody.Append(oCustomerInfo.FirstName + ",<br /><br />");
            emailBody.Append("Thank you for choosing 1800PackRat. Bill details for your order is as follows.<br /><br />");
            emailBody.Append("QORID : " + intQorId.ToString() + "<br /><br />");

            if (oBillDetailInfo.PricingInfo != null)
            {
                emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
                emailBody.Append("<tr>");
                emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Pricing Details</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white;'>Total</th></tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Total Due on Delivery</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.DueAtDelivery.ToString("c") + "</td>");
                emailBody.Append("</tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Recurring Monthly Charges</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.RecurringMonthlyCharge.ToString("c") + "</td>");
                emailBody.Append("</tr>");

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Future Transportation Charges</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.FutureTransportationCharge.ToString("c") + "</td>");
                emailBody.Append("</tr>");

            }
            emailBody.Append("</table><br /><br />");

            if (oBillDetailInfo.TransportationItems.Tables[0].Rows.Count > 0)
            {
                emailBody.Append("<table width='100%' cellpadding='2' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
                emailBody.Append("<tr>");
                emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Transportation Items</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white;'>Total</th></tr>");


                foreach (System.Data.DataRow oDataRow in oBillDetailInfo.TransportationItems.Tables[0].Rows)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["Description"].ToString() + "</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Net"]).ToString("c") + "</td>");
                    emailBody.Append("</tr>");
                }
                emailBody.Append("</table><br /><br />");
            }

            if (oBillDetailInfo.POSItems.Tables[0].Rows.Count > 0)
            {
                emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
                emailBody.Append("<tr>");
                emailBody.Append("<td colspan='3' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>POS Items</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Quantity</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Total</th></tr>");

                foreach (System.Data.DataRow oDataRow in oBillDetailInfo.POSItems.Tables[0].Rows)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["ItemDescription"].ToString() + "</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Quantity"]).ToString("c") + "</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Total"]).ToString("c") + "</td>");
                    emailBody.Append("</tr>");
                }
                emailBody.Append("</table><br /><br />");
            }

            if (oBillDetailInfo.UnitInfo.Tables[0].Rows.Count > 0)
            {
                emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
                emailBody.Append("<tr>");
                emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Unit Information</td>");
                emailBody.Append("</tr>");

                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit No</th>");
                emailBody.Append("</tr>");

                foreach (System.Data.DataRow oDataRow in oBillDetailInfo.UnitInfo.Tables[0].Rows)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["UnitDescription"].ToString() + "</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["UnitName"].ToString() + "</td>");
                    emailBody.Append("</tr>");
                }
                emailBody.Append("</table><br /><br />");
            }

            if (oBillDetailInfo.RecurringItems.Tables[0].Rows.Count > 0)
            {
                emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
                emailBody.Append("<tr>");
                emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Recurring Items</td>");
                emailBody.Append("</tr>");

                emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
                emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Quantity</th></tr>");

                foreach (System.Data.DataRow oDataRow in oBillDetailInfo.RecurringItems.Tables[0].Rows)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["ItemDescription"].ToString() + "</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Quantity"]).ToString() + "</td>");
                    emailBody.Append("</tr>");
                }
                emailBody.Append("</table><br /><br />");
            }



            CustomerSignature oCustomerSignature = new CustomerSignature();
            byte[] imageBytes = oCustomerSignature.GetSignatureByte(intQorId);   //Retrieving  Signature if exists

            if (imageBytes != null)
            {
                emailBody.Append("<img src=cid:CustSignature><br />");
                emailBody.Append("Customer's Signature<br /><br />");
                emailBody.Append("<table width='600px;'><tr><td>" + System.Configuration.ConfigurationManager.AppSettings["SigBelowLine"].ToString() + "</td></tr></table><br /><br /><br />");
                emailBody.Append("Regards<br />1800-PACK-RAT");

                oAppEmail.EmailBody = emailBody.ToString();

                //Customer signature attachment 
                oAppEmail.Attachment = imageBytes;
                oAppEmail.GetContentId = "CustSignature";


                oAppEmail.AttachedFileName = String.Empty;
                oAppEmail.ReportFormat = String.Empty;
                if (oAppEmail.SendAltEMail() == "Success")
                    return true;
                else
                    return false;
                //End customer signature attachment
            }
            else
            {
                emailBody.Append("Regards<br />1800-PACK-RAT");
                oAppEmail.EmailBody = emailBody.ToString();

                oAppEmail.AttachedFileName = String.Empty;
                oAppEmail.ReportFormat = String.Empty;
                if (oAppEmail.SendEMail() == "Success")
                    return true;
                else
                    return false;
            }

        }

        /// <summary>
        /// Method to send email for any exception while completing touch complete operation
        /// </summary>
        /// <param name="QorID"></param>
        /// <param name="oAuditLogMaster"></param>
        /// <param name="unitNo"></param>
        /// <param name="LogID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strTouchDate"></param>
        /// <param name="MethodName"></param>
        /// <returns></returns>
        public string Exception_SendEmail(Int32 QorID, AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string LogID, string strUserID, string strTouchDate, string MethodName)
        {
            try
            {
                EnDeCryption oEnDeCryption = new EnDeCryption();
                AppEmail oAppEmail = new AppEmail();
                PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
                CustomerInfo oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QorID);
                UnitInfo oUnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
                SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfo(QorID);
                DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(strUserID), Convert.ToDateTime(strTouchDate), oAuditLogMaster.TransiteTouchType.Trim(), QorID.ToString(), oAuditLogMaster.StopType);

                oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
                oAppEmail.EmailFrom = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";
                oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Error occurred while completing touch";

                #region This email is Not require as per Kelly and Joe Nolet.

                ////SFERP-TODO-CTRMV 
                //BillDetailInfo oBillDetailInfo = new BillDetailInfo(); 
                //oBillDetailInfo.TransportationItems = oPRbusinesslogic.GetTransportationItems(QorID);
                //oBillDetailInfo.POSItems = oPRbusinesslogic.GetPOSItemWithPrice(QorID);
                //oBillDetailInfo.UnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
                //oBillDetailInfo.RecurringItems = oPRbusinesslogic.GetRecurringItems(QorID);
                //oBillDetailInfo.PricingInfo = oPRbusinesslogic.GetPricing(QorID); 
                //bool retVal = EmailBillInfo(ConfigurationManager.AppSettings["mailFrom"].ToString(), oBillDetailInfo, oCustomerInfo, oSiteInfo, QorID); 
                ////bool retVal = oPRbusinesslogic.EmailBillInfo(ConfigurationManager.AppSettings["mailFrom"].ToString(), oBillDetailInfo, oCustomerInfo, oSiteInfo, QorID);

                #endregion This email is Not require as per Kelly and Joe Nolet.

                oAppEmail.EmailCc = "";

                StringBuilder emailBody = new StringBuilder();

                emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
                emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
                emailBody.Append("<head><title>1800PACKRAT.COM | Error occurred while completing touch</title></head>");
                emailBody.Append("<body>");

                emailBody.Append("<br />Error occurred while completing touch<br /><br />");
                emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

                if (oCustomerInfo != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.FirstName + " " + oCustomerInfo.LName + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Billing Address</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Addr1 + "<br />" +
                                oCustomerInfo.Addr2 + "<br />" + oCustomerInfo.City + (String.IsNullOrWhiteSpace(oCustomerInfo.City) ? "" : ", ") + oCustomerInfo.Region + (String.IsNullOrWhiteSpace(oCustomerInfo.Region) ? "" : ", ") + "<br />" + oCustomerInfo.Country + " - " + oCustomerInfo.PostalCode + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Phone No</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Phone + ", Alt Phone No " + oCustomerInfo.PhoneAlt + "</td>");
                    emailBody.Append("</tr>");
                }

                string purchaseOrder = oPRbusinesslogic.GetPRGQuoteOrderLogData(QorID);
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Purchase Order</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (String.IsNullOrWhiteSpace(purchaseOrder) ? "N/A" : purchaseOrder) + "</td>");
                emailBody.Append("</tr>");

                if (oDriverTouch != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Origin Address</td>");

                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.OriginAddress.AddressLine1 + "<br />" +
                    oDriverTouch.OriginAddress.AddressLine2 + "<br />" + oDriverTouch.OriginAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.City) ? "" : ", ") + oDriverTouch.OriginAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.State) ? "" : ", ") + " - " + oDriverTouch.OriginAddress.Zip + "</td>");

                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Destination Address</td>");

                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.DestinationAddress.AddressLine1 + "<br />" +
                    oDriverTouch.DestinationAddress.AddressLine2 + "<br />" + oDriverTouch.DestinationAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.City) ? "" : ", ") + oDriverTouch.DestinationAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.State) ? "" : ", ") + " - " + oDriverTouch.DestinationAddress.Zip + "</td>");

                    emailBody.Append("</tr>");
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>QORID</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + QorID.ToString() + "</td>");
                emailBody.Append("</tr>");

                if (oUnitInfo.Tables[0].Rows.Count > 0)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Name</td>");

                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + (string.IsNullOrWhiteSpace(unitNo) ? oUnitInfo.Tables[0].Rows[0]["UnitName"].ToString() : unitNo) + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oUnitInfo.Tables[0].Rows[0]["UnitDescription"].ToString() + "</td>");
                    emailBody.Append("</tr>");
                }
                if (oSiteInfo != null)
                {
                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Facility Name</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LegalName.ToString() + "</td>");
                    emailBody.Append("</tr>");

                    emailBody.Append("<tr>");
                    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Location Code</td>");
                    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LocationCode.ToString() + "</td>");
                    emailBody.Append("</tr>");
                }

                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Touch Type</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oAuditLogMaster.TransiteTouchType.Trim() + "</td>");
                emailBody.Append("</tr>");
                emailBody.Append("<tr>");
                emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Date</td>");
                emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + System.Web.HttpContext.Current.Session["localDateTime"].ToString() + "</td>");
                emailBody.Append("</tr>");
                emailBody.Append("</table><br /><br />");

                emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

                oAppEmail.EmailBody = emailBody.ToString();

                //Inserting Email Log
                Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
                el.ApplicationName = "Driver App";
                el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                el.User = strUserID;
                el.StartTime = DateTime.Now;
                el.Subject = oSiteInfo.LegalName.ToString() + " | Error occurred while completing touch";
                el.ToEmailIDs = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
                el.FromEmailID = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";
                el.CCEmailIDs = null;
                el.MethodName = MethodName;
                el.EmailTypeID = 4;
                el.QORID = Convert.ToInt32(QorID);
                el.TouchType = oDriverTouch == null ? "" : oDriverTouch.TouchType;
                el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

                var LogId = LogEmails.InsertLog(el);
                if (oAppEmail.SendEMail() == "Success")
                {
                    //updating endtime in email log
                    Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
                    eml.PK_ID = LogId;
                    eml.EndTime = DateTime.Now;
                    LogEmails.UpdateLog(eml);
                    return "Success";
                }
                else
                    return "Unable to send Email";
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return ex.Message + " , " + "Unable to send Email";
            }
        }

        public bool Error(string strError, string strStackTraceInfo)
        {
            try
            {
                AuditLog.DBStructure.StackTraceLog oStackTraceLog = new AuditLog.DBStructure.StackTraceLog();
                EnDeCryption oEnDeCryption = new EnDeCryption();

                oStackTraceLog.ErrorMsg = strError;
                oStackTraceLog.StackTraceInfo = strStackTraceInfo;

                bool cookieUserIDExists = System.Web.HttpContext.Current.Request.Cookies["UserID"] != null;

                if (cookieUserIDExists)
                    oStackTraceLog.DriverID = oEnDeCryption.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserID"].Value.ToString());
                else
                    oStackTraceLog.DriverID = null;

                bool cookieQORidExists = System.Web.HttpContext.Current.Request.Cookies["QORid"] != null;

                if (cookieQORidExists)
                    oStackTraceLog.QorID = System.Web.HttpContext.Current.Request.Cookies["QORid"].Value;
                else
                    oStackTraceLog.QorID = null;

                oStackTraceLog.IpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                return oAuditLog.addStackTrace(oStackTraceLog);
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        /// <summary>
        /// Method to send email for ChangeCPP operation - HomeGrown
        /// </summary>
        /// <param name="cppLevel"></param>
        /// <param name="lockNo"></param>
        /// <param name="blanketNo"></param>
        /// <param name="comments"></param>
        /// <param name="qorid"></param>
        /// <param name="TouchStopId"></param>
        /// <param name="TouchType"></param>
        /// <param name="StopType"></param>
        /// <returns></returns>
        public bool ChangeCPP_SendEmail(string cppLevel, string lockNo, string blanketNo, string comments, string qorid, string TouchStopId, string TouchType, string StopType, string UserID, string LoginName)
        {

            AppEmail oAppEmail = new AppEmail();
            EnDeCryption oEnDeCryption = new EnDeCryption();

            //IF cppLevel == "-" means, user did not select any cpp. so in this case we need not look for Database for description
            string strCppText = ((cppLevel == "-") ? "-" : (cppLevel == "RemoveCPP" ? "Remove CPP" : GetSelectedCPPTextForEmail(qorid, cppLevel)));

            oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
            oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
            oAppEmail.Subject = "Changed CPP items";

            StringBuilder emailBody = new StringBuilder();

            emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
            emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
            emailBody.Append("<head><title>1800PACKRAT.COM | New CPP items</title></head>");
            emailBody.Append("<body>");

            emailBody.Append("<br />Changed CPP information<br /><br />");
            emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID/ Name </td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oEnDeCryption.Decrypt(UserID) + " - " + LoginName + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Changed CPP item</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + strCppText + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Lock</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + lockNo + "<br />");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Blankets</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + blanketNo + "<br />");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>QORID</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + qorid + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Touch Type</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + TouchType + " - " + StopType + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("<tr>");
            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver's comments</td>");
            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + comments + "</td>");
            emailBody.Append("</tr>");

            emailBody.Append("</table><br /><br />");

            emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

            oAppEmail.EmailBody = emailBody.ToString();

            //Inserting Email Log
            Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
            el.ApplicationName = "Driver App";
            el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            el.User = oEnDeCryption.Decrypt(UserID);
            el.StartTime = DateTime.Now;
            el.Subject = "Changed CPP items";
            el.ToEmailIDs = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
            el.FromEmailID = ConfigurationManager.AppSettings["mailFrom"].ToString();
            el.CCEmailIDs = null;
            el.MethodName = "ChangeCPP";
            el.EmailTypeID = 3;
            el.QORID = Convert.ToInt32(qorid);
            el.TouchType = TouchType;
            el.SLSeqNo = Convert.ToInt32(TouchStopId);

            var LogId = LogEmails.InsertLog(el);
            if (oAppEmail.SendEMail() == "Success")
            {
                //updating endtime in email log
                Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
                eml.PK_ID = LogId;
                eml.EndTime = DateTime.Now;
                LogEmails.UpdateLog(eml);
                return true;
            }
            else
                return false;
        }


        /// <summary>
        /// PENDING-TEST
        /// Method to get CPP item text based on QORID and cppLevel selected for the Unit/QOR
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="cppLevel"></param>
        /// <returns></returns>
        private string GetSelectedCPPTextForEmail(string qorId, string cppLevel)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            System.Collections.Generic.List<PR.Entities.CPPItem> cppItems = smd.GetAppliedCppItems(qorId, cppLevel);
            var cppText = cppItems != null && cppItems.Count > 0 ? cppItems.Where(c => c.Price == cppLevel).SingleOrDefault().Description : "";
            // var cppText = cppItems.Where(c => c.Price == cppLevel).SingleOrDefault().Description; // Old code for SL

            return cppText;
        }

    }
}