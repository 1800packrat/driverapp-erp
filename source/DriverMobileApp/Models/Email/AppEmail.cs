﻿using System;
using System.IO;
using System.Net.Mail;
using System.Configuration;

namespace DriverMobileApp.Models.Email
{
    public class AppEmail
    {
        private byte[] bytes;
        private string rptFormat;
        private string mailTo;
        private string mailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
        private string emailBody;
        private string emailcc;
        private string emailbcc = string.Empty;
        private string subject;
        private string attachedFileName;
        private string contentId = String.Empty;

        private string smtpUsrName = ConfigurationManager.AppSettings["smtpUsrName"].ToString();
        private string smtpPass = ConfigurationManager.AppSettings["smtpPass"].ToString();
        private string hostname = ConfigurationManager.AppSettings["hostname"].ToString();



        public byte[] Attachment
        {
            set
            {
                bytes = value;
            }
        }
        public string ReportFormat
        {
            set
            {
                rptFormat = value;
            }
        }
        public string EmailTo
        {
            set
            {
                mailTo = value;
            }
        }
        public string EmailFrom
        {
            set
            {
                mailFrom = value;
            }
        }
        public string EmailBody
        {
            set
            {
                emailBody = value;
            }
        }
        public string EmailCc
        {
            set
            {
                emailcc = value;
            }
        }

        public string Subject
        {
            set
            {
                subject = value;
            }
        }
        public string AttachedFileName
        {
            set
            {
                attachedFileName = value;
            }
        }
        //public string smtpUserName
        //{
        //    set
        //    {
        //        smtpUsrName = value;
        //    }
        //}
        //public string smtpPassword
        //{
        //    set
        //    {
        //        smtpPass = value;
        //    }
        //}
        public string HostName
        {
            set
            {
                hostname = value;
            }
        }
        public string GetContentId
        {
            set
            {
                contentId = value;
            }
        }
        public string SendEMailWithAttachment()
        {
            try
            {
                string attachmntName = string.Empty;

                /*************************************************/
                if (attachedFileName == string.Empty)
                {
                    if (rptFormat.ToLower() == "excel")
                    {
                        attachmntName = "remoteMonitor_" + DateTime.Now.ToString() + ".xls";
                    }
                    else if (rptFormat.ToLower() == "pdf")
                    {
                        attachmntName = "remoteMonitor_" + DateTime.Now.ToString() + ".pdf";
                    }
                    else if (rptFormat.ToLower() == "doc")
                    {
                        attachmntName = "remoteMonitor_" + DateTime.Now.ToString() + ".doc";
                    }

                }
                else
                {
                    if (rptFormat.ToLower() == "excel")
                    {
                        attachmntName = attachedFileName + ".xls";
                    }
                    else if (rptFormat.ToLower() == "pdf")
                    {
                        attachmntName = attachedFileName + ".pdf";
                    }
                    else if (rptFormat.ToLower() == "doc")
                    {
                        attachmntName = attachedFileName + ".doc";
                    }

                }
                /*************************************************/

                MemoryStream memoryStream = new MemoryStream(bytes);
                memoryStream.Seek(0, SeekOrigin.Begin);

                MailMessage message = new MailMessage();
                Attachment attachment = new Attachment(memoryStream, attachmntName);
                message.Attachments.Add(attachment);

                //message.From = new MailAddress(smtpUsrName);
                message.From = new MailAddress(mailFrom);
                message.ReplyToList.Add(new MailAddress(mailFrom));
                //message.To.Add(new MailAddress(mailTo));
                foreach (var to in mailTo.Split(';'))
                {
                    if (!string.IsNullOrEmpty(to))
                        message.To.Add(new MailAddress(to.Trim()));
                }

                if (emailcc != string.Empty)
                {
                    message.CC.Add(new MailAddress(emailcc));
                }

                if (emailbcc != string.Empty)
                {
                    message.Bcc.Add(new MailAddress(emailbcc));
                }

                if (subject == string.Empty)
                {
                    message.Subject = "1800PACKRAT.COM | " + DateTime.Now.ToString();
                }
                else
                {
                    message.Subject = subject;
                }


                if (emailBody == string.Empty)
                {
                    message.Body = emailBody;
                }
                else
                {
                    message.Body = emailBody;
                }

                message.IsBodyHtml = true;


                message.Priority = MailPriority.Normal;

                SmtpClient smtp = new SmtpClient(hostname);
                if (!String.IsNullOrWhiteSpace(smtpPass.Trim()))
                    smtp.Credentials = new System.Net.NetworkCredential(smtpUsrName, smtpPass);
                smtp.EnableSsl = false;
                smtp.Port = 25;
                smtp.Send(message);

                memoryStream.Close();
                memoryStream.Dispose();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SendEMail()
        {
            try
            {

                /*************************************************/


                MailMessage message = new MailMessage();


                //message.From = new MailAddress(smtpUsrName);
                message.From = new MailAddress(mailFrom);
                message.ReplyToList.Add(new MailAddress(mailFrom));
                //message.To.Add(new MailAddress(mailTo));
                foreach (var to in mailTo.Split(';'))
                {
                    if (!string.IsNullOrEmpty(to))
                        message.To.Add(new MailAddress(to.Trim()));
                }

                if (!String.IsNullOrEmpty(emailcc))
                {
                    message.CC.Add(new MailAddress(emailcc));
                }

                if (!String.IsNullOrWhiteSpace(ConfigurationManager.AppSettings["Bcc"].ToString()))
                {
                    message.Bcc.Add(new MailAddress(ConfigurationManager.AppSettings["Bcc"].ToString()));
                }

                if (String.IsNullOrEmpty(subject))
                {
                    message.Subject = "1800PACKRAT.COM | " + DateTime.Now.ToString();
                }
                else
                {
                    message.Subject = subject;
                }

                message.IsBodyHtml = true;

                if (!String.IsNullOrEmpty(emailBody))
                {
                    message.Body = emailBody;
                }
                else
                {
                    message.Body = emailBody;
                }

                message.IsBodyHtml = true;
                message.Priority = MailPriority.Normal;

                SmtpClient smtp = new SmtpClient(hostname);
                if (!String.IsNullOrWhiteSpace(smtpPass.Trim()))
                    smtp.Credentials = new System.Net.NetworkCredential(smtpUsrName, smtpPass);
                smtp.EnableSsl = false;
                smtp.Port = 25;
                smtp.Send(message);


                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SendAltEMail()
        {
            try
            {
                MailMessage message = new MailMessage();

                message.From = new MailAddress(mailFrom);
                message.ReplyToList.Add(new MailAddress(mailFrom));
                //message.To.Add(new MailAddress(mailTo));
                foreach (var to in mailTo.Split(';'))
                {
                    if (!string.IsNullOrEmpty(to))
                        message.To.Add(new MailAddress(to.Trim()));
                }

                if (!String.IsNullOrWhiteSpace(emailcc))
                {
                    message.CC.Add(new MailAddress(emailcc));
                }

                if (!String.IsNullOrWhiteSpace(emailbcc))
                {
                    message.Bcc.Add(new MailAddress(emailbcc));
                }

                if (subject == string.Empty)
                {
                    message.Subject = "1800PACKRAT.COM | " + DateTime.Now.ToString();
                }
                else
                {
                    message.Subject = subject;
                }

                message.IsBodyHtml = true;

                MemoryStream memoryStream = new MemoryStream(bytes);
                LinkedResource oLinkedResource = new LinkedResource(memoryStream, "image/jpeg");
                oLinkedResource.ContentId = contentId;
                oLinkedResource.TransferEncoding = System.Net.Mime.TransferEncoding.Base64;

                AlternateView oAlternateView = AlternateView.CreateAlternateViewFromString(emailBody, null, "text/html");
                oAlternateView.LinkedResources.Add(oLinkedResource);


                message.Priority = MailPriority.Normal;
                message.AlternateViews.Add(oAlternateView);

                SmtpClient smtp = new SmtpClient(hostname);
                if (!String.IsNullOrWhiteSpace(smtpPass.Trim()))
                    smtp.Credentials = new System.Net.NetworkCredential(smtpUsrName, smtpPass);
                smtp.EnableSsl = false;
                smtp.Port = 25;
                smtp.Send(message);

                memoryStream.Close();
                memoryStream.Dispose();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


    }
}