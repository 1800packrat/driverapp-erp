﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.ComponentModel.DataAnnotations;
//using System.Web.Mvc;
//using PR.BusinessLogic;
//using System.Data;
//using System.Configuration;
//using PR.Entities;
//using PR.UtilityLibrary;

//namespace DriverMobileApp.Models
//{
//    /// <summary>
//    /// CreditCardInfo is for capturing card information
//    /// </summary>
//    public class CreditCardInfo
//    {
//        public string CustomerName { get; set; }
//        public string CustomerAddress { get; set; }
//        public string CustomerPostalCode { get; set; }
//        public string ContactNo { get; set; }

//        [Required]
//        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid EmailID")]
//        [IntegerValidator(ExcludeRange = false, MaxValue = 9999, MinValue = 1111)]
//        public string CardNo1 { get; set; }

//        [Required]
//        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid EmailID")]
//        [IntegerValidator(ExcludeRange = false, MaxValue = 9999, MinValue = 1111)]
//        public string CardNo2 { get; set; }

//        [Required]
//        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid EmailID")]
//        [IntegerValidator(ExcludeRange = false, MaxValue = 9999, MinValue = 1111)]
//        public string CardNo3 { get; set; }

//        [Required]
//        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid EmailID")]
//        [IntegerValidator(ExcludeRange = false, MaxValue = 9999, MinValue = 1111)]
//        public string CardNo4 { get; set; }



//        [Required]
//        public List<SelectListItem> CardType { get; set; }
//        public string CardTypeValue { get; set; }

//        [Required]
//        [StringLength(4, MinimumLength = 3)]
//        public string CvvNo { get; set; }

//        [Required]
//        public List<SelectListItem> Exp_mm { get; set; }
//        public string Exp_mmValue { get; set; }

//        [Required]
//        public List<SelectListItem> Exp_yyyy { get; set; }
//        public string Exp_yyyyValue { get; set; }
//    }
//    public class PaymentInfo
//    {
//        public List<SelectListItem> GetCardTypes(string defaultValue)
//        {
//            List<SelectListItem> items = new List<SelectListItem>();
//            items.Add(new SelectListItem { Text = "AMEX", Value = "AMEX", Selected = (defaultValue == "AMEX") });
//            items.Add(new SelectListItem { Text = "MASTER", Value = "MASTER", Selected = (defaultValue == "MASTER") });
//            items.Add(new SelectListItem { Text = "VISA", Value = "VISA", Selected = (defaultValue == "VISA") });
//            items.Add(new SelectListItem { Text = "DISCOVER", Value = "DISCOVER", Selected = (defaultValue == "DISCOVER") });
//            return items;
//        }

//        public List<SelectListItem> GetExpMonth(string defaultValue)
//        {
//            List<SelectListItem> items = new List<SelectListItem>();

//            for (Int16 intCount = 1; intCount <= 12; intCount++)
//            {
//                string monthNumber = (intCount.ToString().Trim().Length == 1 ? "0" + intCount.ToString() : intCount.ToString());
//                items.Add(new SelectListItem { Text = monthNumber, Value = monthNumber, Selected = (defaultValue == monthNumber) });
//            }

//            return items;
//        }

//        public List<SelectListItem> GetExpYear(string defaultValue)
//        {
//            List<SelectListItem> items = new List<SelectListItem>();

//            for (int intYear = System.DateTime.Now.Year; intYear <= (System.DateTime.Now.Year + 15); intYear++)
//            {
//                items.Add(new SelectListItem { Text = intYear.ToString(), Value = intYear.ToString(), Selected = (defaultValue == intYear.ToString()) });
//            }

//            return items;
//        }

//    }
//}