﻿using System;
using System.Collections.Generic;
using System.Linq;
using DriverMobileApp.Models.DBclass;
using System.Data.Objects.SqlClient;
using System.Web.Mvc;
using PR.BusinessLogic;
using System.Data;

namespace DriverMobileApp.Models
{
    public class ChangeCPP
    {

        //SFERP-TODO-RMVNICD 
        public List<SelectListItem> GetCPPitems_Old(int? Qorid)
        {
            bool isLDM = false;
            List<SelectListItem> cppResult = new List<SelectListItem>();
            ////separated retrieving local and ldm cpp binding values
            //using (var oDBRoute = new DriverMobileApp.Models.DBclass.RouteOptimzation())
            //{
            //    oDBRoute.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

            //    var TouchInfo = (from _TouchInfo in oDBRoute.TouchInformations
            //                     where _TouchInfo.QORId == Qorid //&& _TouchInfo.StarsUnitId > 0
            //                     select _TouchInfo).FirstOrDefault();
            //    if (TouchInfo.StarsUnitId > 0)
            //    {
            //        isLDM = true;
            //    }
            //}

            //SelectListItem oItem = new SelectListItem();
            //oItem.Selected = true;
            //oItem.Value = "";
            //oItem.Text = "Select CPP";
            //cppResult.Add(oItem);

            //oItem = new SelectListItem();
            //oItem.Selected = true;
            //oItem.Value = "RemoveCPP";
            //oItem.Text = "Remove CPP";
            //cppResult.Add(oItem);
            
            //if (isLDM)
            //{
            //    using (var oDB = new DBclass.STARSDBEntities())
            //    {
            //        oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
            //        //LDM order                       
            //        cppResult.AddRange(
            //                    (from _cppResult in oDB.T66_Contentprotection
            //                     where _cppResult.T66_SlabID == (oDB.T66_Contentprotection.Select(t => t.T66_SlabID).Max())
            //                         && _cppResult.T66_Special != "Special"
            //                     select _cppResult
            //                    ).AsEnumerable().
            //                         Select(dr => new SelectListItem
            //                         {
            //                             Selected = false,
            //                             Value = dr.T66_DeclaredValue,
            //                             Text = ((decimal)dr.T66_MonthlyPremium).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us")) + " - " + dr.T66_DeclaredValue
            //                         }).DefaultIfEmpty().ToList()
            //                    );
            //    }
            //}
            //else
            //{
            //    //Local order
            //    LocalComponent lc = new LocalComponent();

            //    //As per Paula inputs, Need not to display IsWeekly Weekly DW CPP
            //    cppResult.AddRange(
            //        lc.GetCPPInfo().Tables[0].AsEnumerable().Where(s => !s.Field<bool>("IsWeekly")).Select(dr => new SelectListItem
            //        {
            //            Selected = false,
            //            Value = dr.Field<decimal>("Price").ToString(),
            //            Text = dr.Field<decimal>("Price").ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us")) + " - " + dr.Field<string>("CPPDescription").ToString()
            //        }).ToList()
            //    );
            //}
            
            return cppResult;

        }

        /// <summary>
        /// Get CPP items master list to show in DDL.
        /// </summary>
        /// <param name="Qorid"></param>
        /// <returns></returns>
        public List<SelectListItem> GetCPPitems(int? Qorid)
        {            
            List<SelectListItem> cppResult = new List<SelectListItem>(); 

            SelectListItem oItem = new SelectListItem();
            oItem.Selected = true;
            oItem.Value = "";
            oItem.Text = "Select CPP";
            cppResult.Add(oItem);

            oItem = new SelectListItem();
            oItem.Selected = true;
            oItem.Value = "RemoveCPP";
            oItem.Text = "Remove CPP";
            cppResult.Add(oItem);

            SMDBusinessLogic smd = new SMDBusinessLogic();
            System.Collections.Generic.List<PR.Entities.CPPItem> cppItems = smd.GetCppItems();

            cppResult.AddRange(cppItems.Select(dr => new SelectListItem
            {
                Selected = false,
                Value = dr.Price,
                Text = Convert.ToDecimal(dr.Price).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us")) + " - " + dr.Description
            }).DefaultIfEmpty().ToList());
             

            return cppResult;

        }


        /// <summary>
        /// PENDING-TEST
        /// SFERP-TODO-CTUPD
        /// Need to check if we need to call ESB method to save CPP details in Salesforce.
        /// </summary>
        /// <param name="strStopId"></param>
        /// <param name="strDriverId"></param>
        /// <param name="strQorId"></param>
        /// <param name="strTouchType"></param>
        /// <param name="strStopType"></param>
        /// <param name="strCpp"></param>
        /// <param name="strLock"></param>
        /// <param name="strBlanket"></param>
        /// <param name="strComments"></param>
        /// <returns></returns>
        public bool AddCppItem(string strStopId, string strDriverId, string strQorId, string strTouchType, string strStopType, string strCpp, string strLock, string strBlanket, string strComments)
        {

            using (var oDB = new RouteOptimzation())
            {

                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                DBclass.ChangeCPP oChangeCPP = new DBclass.ChangeCPP()
                {
                    FK_Touch_StopId = Convert.ToInt32(strStopId),
                    DriverId = Convert.ToInt32(strDriverId),
                    QorId = Convert.ToInt32(strQorId),
                    TouchType = strTouchType,
                    StopType = strStopType,
                    NewCPP = strCpp,
                    Lock = string.IsNullOrEmpty(strLock) ? 0 : Convert.ToInt32(strLock),
                    Blankets = string.IsNullOrEmpty(strBlanket) ? 0 : Convert.ToInt32(strBlanket),
                    Comments = strComments,
                    AddedOn = DateTime.Now
                }; 
               
                var getCpp = (from _getCpp in oDB.ChangeCPPs where _getCpp.FK_Touch_StopId == oChangeCPP.FK_Touch_StopId select _getCpp).SingleOrDefault();

                if (getCpp == null)
                {
                    oDB.ChangeCPPs.AddObject(oChangeCPP);
                    oDB.SaveChanges();
                }
                else
                {
                    getCpp.DriverId = oChangeCPP.DriverId;
                    getCpp.QorId = oChangeCPP.QorId;
                    getCpp.TouchType = oChangeCPP.TouchType;
                    getCpp.StopType = oChangeCPP.StopType;
                    getCpp.NewCPP = oChangeCPP.NewCPP;
                    getCpp.Lock = oChangeCPP.Lock;
                    getCpp.Blankets = oChangeCPP.Blankets;
                    getCpp.Comments = oChangeCPP.Comments;
                    getCpp.AddedOn = DateTime.Now;
                    oDB.SaveChanges();
                }
            }

            return true;
        }
    }
}