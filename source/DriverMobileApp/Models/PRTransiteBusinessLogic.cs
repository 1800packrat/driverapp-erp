﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PR.BusinessLogic;
using System.Data;
using PR.Entities;
using System.Configuration;
using AuditLog.DBStructure;
using DriverMobileApp.Models.DBclass;
using DriverMobileApp.Models.Email;
using System.Text;

namespace DriverMobileApp.Models
{
    public class PRTransiteBusinessLogic
    {
        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();

        private SMDBusinessLogic oSMDBusinessLogic;

        private TouchInfoUpdatedBySL GetTouchUpdates(DriverTouchLocal oDriverTouchLocal)
        {
            TouchInfoUpdatedBySL oTouchInfoUpdatedBySL = new TouchInfoUpdatedBySL();

            using (var oDB = new RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                oTouchInfoUpdatedBySL = (from touchInfo in oDB.TouchInformations.Where(t => t.PK_TouchId == oDriverTouchLocal.TouchId)
                                         join tchSync in oDB.TouchInformationSyncs on touchInfo.PK_TouchId equals tchSync.FK_TouchId into tInfo
                                         from touchSync in tInfo.DefaultIfEmpty()
                                         select new TouchInfoUpdatedBySL
                                         {
                                             CustomerName = touchSync.CustomerName,
                                             DoorFacesRear = touchSync.DoorToRear.HasValue ? touchSync.DoorToRear.Value : false,
                                             SLUpdatedStatusTypeId = (touchInfo.FK_SLStatus.HasValue ? touchInfo.FK_SLStatus : 1),
                                             PhoneNumber = touchSync.PhoneNumber,
                                             OriginAddressId = touchSync.FK_OriginAddressId,
                                             DestAddressId = touchSync.FK_DestinationAddressId,
                                             SyncUpdatedOriginAddressId = touchInfo.FK_OriginAddressId,
                                             SyncUpdatedDestAddressId = touchInfo.FK_DestinationAddressId
                                         }).AsEnumerable().FirstOrDefault();

                if (oTouchInfoUpdatedBySL != null && oTouchInfoUpdatedBySL.SLUpdatedStatusTypeId != null && oTouchInfoUpdatedBySL.SLUpdatedStatusTypeId > 0)
                {
                    oTouchInfoUpdatedBySL.SLUpdatedStatusType = (SLUpdateType)oTouchInfoUpdatedBySL.SLUpdatedStatusTypeId;

                    if (oTouchInfoUpdatedBySL.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated || oTouchInfoUpdatedBySL.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated)
                    {
                        switch (oDriverTouchLocal.TouchType)
                        {
                            case "DE":
                                if (!string.IsNullOrEmpty(oDriverTouchLocal.StopType) && oDriverTouchLocal.StopType.ToLower() == DriverMobileApp.Models.PR_Action_Constant.Pickup.ToLower())
                                {
                                    if (oTouchInfoUpdatedBySL != null && oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId != null && oDriverTouchLocal.DestinationAddress.ID != oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId)
                                    {
                                        oTouchInfoUpdatedBySL.changedAddress = GetAddressById(oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId.Value);
                                    }
                                }
                                break;
                            case "CC":
                                if (!string.IsNullOrEmpty(oDriverTouchLocal.StopType) && oDriverTouchLocal.StopType.ToLower() == DriverMobileApp.Models.PR_Action_Constant.Pickup.ToLower())
                                {
                                    if (oTouchInfoUpdatedBySL != null && oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId != null && oDriverTouchLocal.DestinationAddress.ID != oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId)
                                    {
                                        oTouchInfoUpdatedBySL.changedAddress = GetAddressById(oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId.Value);
                                    }
                                }
                                if (!string.IsNullOrEmpty(oDriverTouchLocal.StopType) && oDriverTouchLocal.StopType.ToLower() == DriverMobileApp.Models.PR_Action_Constant.Dropoff.ToLower())
                                {
                                    if (oTouchInfoUpdatedBySL != null && oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId != null && oDriverTouchLocal.DestinationAddress.ID != oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId)
                                    {
                                        oTouchInfoUpdatedBySL.changedAddress = GetAddressById(oTouchInfoUpdatedBySL.SyncUpdatedDestAddressId.Value);
                                    }
                                }
                                break;
                            case "RE":
                                if (!string.IsNullOrEmpty(oDriverTouchLocal.StopType) && oDriverTouchLocal.StopType.ToLower() == DriverMobileApp.Models.PR_Action_Constant.Pickup.ToLower())
                                {
                                    if (oTouchInfoUpdatedBySL != null && oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId != null && oDriverTouchLocal.OriginAddress.ID != oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId)
                                    {
                                        oTouchInfoUpdatedBySL.changedAddress = GetAddressById(oTouchInfoUpdatedBySL.SyncUpdatedOriginAddressId.Value);
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            return oTouchInfoUpdatedBySL;
        }

        private PR.Entities.Address GetAddressById(int addressId)
        {
            PR.Entities.Address address = null;
            using (var oDB = new RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                address = (from destAddress in oDB.Addresses.Where(a => a.PK_AddressId == addressId)
                           select new PR.Entities.Address
                           {
                               ID = destAddress.PK_AddressId,
                               AddressLine1 = destAddress.AddressLine1,
                               AddressLine2 = destAddress.AddressLine2,
                               City = destAddress.City,
                               State = destAddress.State,
                               Zip = destAddress.Zip,
                               Country = destAddress.Country,
                               Latitude = destAddress.Latitude,
                               Longitude = destAddress.Longitude,
                               Company = destAddress.Company
                               //,
                               //AddressType = destAddress.FK_AddressTypeId != null && destAddress.FK_AddressTypeId == 1 ? PR.UtilityLibrary.PREnums.AddressType.Warehouse : PR.UtilityLibrary.PREnums.AddressType.Customer,
                           }).FirstOrDefault();
            }

            return address;
        }

        private List<DriverTouch> RetieveTouchesFromDB(string DriverNo, DateTime dtTouchDate)
        {
            //---------------- Setting date in proper format------------------
            string[] arrTouchDate = dtTouchDate.ToString().Split(' ');
            arrTouchDate = arrTouchDate[0].Split('/');
            string strTouchDate = (arrTouchDate[0].Length == 1 ? "0" + arrTouchDate[0] : arrTouchDate[0]) + "/" + (arrTouchDate[1].Length == 1 ? "0" + arrTouchDate[1] : arrTouchDate[1]) + "/" + arrTouchDate[2];
            //---------------- End Setting date in proper format------------------

            // Completing first touch if it is Warehouse
            CompleteFirstWarehouseStop(DriverNo, strTouchDate);

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            List<DriverTouch> lstDriverTouch = new List<DriverTouch>();

            using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var TouchList = (from _TouchList in oDB.USP_DriverTouchInfo(Convert.ToInt32(DriverNo), strTouchDate)
                                 orderby _TouchList.Sequence
                                 select new DriverTouch
                                 {
                                     QORID = _TouchList.QORId.ToString(),
                                     CustomerName = _TouchList.CustomerName,
                                     DestinationAddress = new PR.Entities.Address
                                     {
                                         ID = _TouchList.DestAddressId.HasValue ? _TouchList.DestAddressId.Value : 0,
                                         AddressLine1 = _TouchList.DestAddresssLn1,
                                         AddressLine2 = _TouchList.DestAddressLn2,
                                         AlternatePhoneNumber = _TouchList.PhoneNumber,
                                         City = _TouchList.DestCity,
                                         State = _TouchList.DestState,
                                         Company = _TouchList.DestCountry,
                                         Zip = _TouchList.DestZip,
                                         Country = _TouchList.DestCountry,
                                         FirstName = _TouchList.FirstName,
                                         LastName = _TouchList.LastName,
                                         Latitude = _TouchList.DestLatitude,
                                         Longitude = _TouchList.DestLongitude,
                                         MobilePhoneNumber = _TouchList.PhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim(),
                                         PhoneNumber = _TouchList.PhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim(),
                                         SLLocCode = _TouchList.DestSLLocCode,
                                         AddressType = (_TouchList.DestAddressType == "Warehouse" ? PR.UtilityLibrary.PREnums.AddressType.Warehouse :
                                         (_TouchList.StopType == "DropOff" && _TouchList.TouchType != "CC" ? PR.UtilityLibrary.PREnums.AddressType.Warehouse :
                                         (_TouchList.TouchType == "CC" && _TouchList.StopType == "ToFacility" ? PR.UtilityLibrary.PREnums.AddressType.Warehouse : PR.UtilityLibrary.PREnums.AddressType.Customer))),
                                     },
                                     OriginAddress = new PR.Entities.Address
                                     {
                                         ID = _TouchList.OriginAddressId.HasValue ? _TouchList.OriginAddressId.Value : 0,
                                         AddressLine1 = _TouchList.OriginAddressLn1,
                                         AddressLine2 = _TouchList.OriginAddressLn2,
                                         AlternatePhoneNumber = _TouchList.PhoneNumber,
                                         City = _TouchList.OriginCity,
                                         State = _TouchList.OriginState,
                                         Company = _TouchList.Company,
                                         Zip = _TouchList.OriginZip,
                                         Country = _TouchList.OriginCountry,
                                         FirstName = _TouchList.FirstName,
                                         LastName = _TouchList.LastName,
                                         Latitude = _TouchList.OrgLatitude,
                                         Longitude = _TouchList.OrgLongitude,
                                         SLLocCode = _TouchList.OrgSLLocCode,
                                         MobilePhoneNumber = _TouchList.PhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim(),
                                         PhoneNumber = _TouchList.PhoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Trim(),
                                     },
                                     ContainerNumber = _TouchList.ContainerNo,
                                     ContainerSize = _TouchList.UnitSize,


                                     ScheduledStartTime = Convert.ToString(Convert.ToDateTime(_TouchList.ScheduledStartTime).ToString("t")),
                                     ScheduledEndTime = Convert.ToString(Convert.ToDateTime(_TouchList.ScheduledEndTime).ToString("t")),

                                     ActualStartTime = (_TouchList.ActualStartTime == null ? string.Empty : Convert.ToString(Convert.ToDateTime(_TouchList.ActualStartTime.ToString()).ToString("t"))),
                                     ActualEndTime = (_TouchList.ActualEndTime == null ? string.Empty : Convert.ToString(Convert.ToDateTime(_TouchList.ActualEndTime.ToString()).ToString("t"))),

                                     EstimatedStartTime = (_TouchList.EstimatedStartTime == null ? Convert.ToString(Convert.ToDateTime(_TouchList.ScheduledStartTime).ToString("t")) : Convert.ToString(Convert.ToDateTime(_TouchList.EstimatedStartTime).ToString("t"))),
                                     EstimatedEndTime = (_TouchList.EstimatedEndTime == null ? Convert.ToString(Convert.ToDateTime(_TouchList.ScheduledEndTime).ToString("t")) : Convert.ToString(Convert.ToDateTime(_TouchList.EstimatedEndTime).ToString("t"))),
                                     SequenceNumber = _TouchList.Sequence.ToString(),
                                     TouchType = _TouchList.TouchType,
                                     BalanceDue = 0,
                                     SLSeqNo = Convert.ToInt32(_TouchList.SLSeqNo),
                                     Status = _TouchList.Status,
                                     StopStatus = _TouchList.TouchStatus,
                                     OrderNum = _TouchList.OrderNumber,
                                     StopType = _TouchList.StopType,
                                     IsTrailerNeeded = false,
                                     TouchId = _TouchList.TouchId,
                                     TouchStopId = _TouchList.TouchStopId,
                                     SkipTouch = (bool)_TouchList.SkipTouch,
                                     FacilityName = _TouchList.DestFacilityName, ///since Driver is hedding to destination address, so we are showing only destination facility warehouse
                                     IsExceptionalTouch = _TouchList.IsExceptionalTouch ?? false,
                                     StarsUnitId = Convert.ToInt32(_TouchList.StarsUnitId),
                                 }).DefaultIfEmpty().ToList();

                GetZippyShellDetails(TouchList);

                if (TouchList[0] == null)
                    return lstDriverTouch;
                else
                    return TouchList;
            }
        }

        private void GetZippyShellDetails(List<DriverTouch> lstDriverTouches)
        {
            oSMDBusinessLogic = new SMDBusinessLogic();

            lstDriverTouches.ForEach(slt =>
            {
                slt.IsZippyShellMove = oSMDBusinessLogic.GetBrandInfo(Convert.ToInt32(slt.QORID)).BrandType == BrandType.ZippyShell; //false;
            });
        }

        /// <summary>
        /// Complete first Warehouse stop
        /// </summary>
        /// <param name="DriverNo"></param>
        /// <param name="strTouchDate"></param>
        private void CompleteFirstWarehouseStop(string DriverNo, string strTouchDate)
        {
            using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var TouchList = (from _TouchList in oDB.USP_DriverTouchInfo(Convert.ToInt32(DriverNo), strTouchDate)
                                 where _TouchList.DestAddressType == DriverMobileApp.Models.PR_AddressType.Warehouse
                                 orderby Convert.ToInt32(_TouchList.Sequence)
                                 select _TouchList).FirstOrDefault();
                if (TouchList != null)
                {
                    if (TouchList.Sequence == 1)
                    {
                        UpdateIsTouchStartedOrder(TouchList.TouchId, TouchList.TouchStopId);

                        var StopInformation = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == TouchList.TouchStopId).FirstOrDefault();

                        StopInformation.ActualEndTime = Convert.ToDateTime(HttpContext.Current.Session["localDateTime"]);
                        oDB.SaveChanges();

                        var UpdateScheduleTime = oDB.USP_Update_ScheduledTime(Convert.ToInt32(DriverNo), StopInformation.ActualEndTime, 1, "S");
                    }
                }
            }
        }

        /// <summary>
        /// Storing Touch list in session for faster access
        /// </summary>
        /// <param name="DriverNo"></param>
        /// <param name="dtTouchDate"></param>
        /// <returns></returns>
        private List<DriverTouch> RetieveTouchesFromSession(string DriverNo, DateTime dtTouchDate)
        {
            if (HttpContext.Current.Session["TouchList"] == null)
            {
                HttpContext.Current.Session["TouchList"] = RetieveTouchesFromDB(DriverNo, dtTouchDate);
                return (List<DriverTouch>)HttpContext.Current.Session["TouchList"];
            }
            else
            {
                return (List<DriverTouch>)HttpContext.Current.Session["TouchList"];
            }
        }


        public List<DriverTouch> RetievePendingTouches(string DriverNo, DateTime TouchOnDate)
        {
            try
            {
                var TouchList = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 where _TouchList.StopStatus == "Open"
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).DefaultIfEmpty().ToList();

                return TouchList;
            }
            catch (Exception ex)
            {
                List<DriverTouch> oDriverTouch = new List<DriverTouch>();
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                throw new System.InvalidOperationException(ex.Message);
            }
        }

        public TouchInfoUpdatedBySL RetieveTouchSLUpdates(DriverTouchLocal oDriverTouchLocal)
        {
            try
            {
                return GetTouchUpdates(oDriverTouchLocal);
            }
            catch (Exception ex)
            {
                List<DriverTouch> oDriverTouch = new List<DriverTouch>();
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                throw new System.InvalidOperationException(ex.Message);
            }
        }

        public List<DriverTouch> RetieveSkippedTouches(string DriverNo, DateTime TouchOnDate)
        {
            try
            {
                var TouchList = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 where _TouchList.SkipTouch == true
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).DefaultIfEmpty().ToList();
                return TouchList;
            }
            catch (Exception ex)
            {
                List<DriverTouch> oDriverTouch = new List<DriverTouch>();
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return oDriverTouch;
            }
        }

        public DriverTouch RetieveCurrentTouchByStopId(string DriverNo, DateTime TouchOnDate, int touchStopId)
        {
            try
            {
                var TouchList = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).Where(s => s.TouchStopId == touchStopId).FirstOrDefault();
                return TouchList;
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return new DriverTouch();
            }
        }

        public DriverTouch RetieveCurrentTouchPickupStopByQORId(string DriverNo, DateTime TouchOnDate, string qorId)
        {
            try
            {
                var TouchList = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).Where(s => s.QORID == qorId && s.TouchType == "DE" && s.StopType == "Pickup").FirstOrDefault();
                return TouchList;
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return new DriverTouch();
            }
        }

        public List<DriverTouch> RetieveCompletedTouches(string DriverNo, DateTime TouchOnDate)
        {
            try
            {
                var TouchList = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 where _TouchList.StopStatus == "Completed"
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).DefaultIfEmpty().ToList();

                return TouchList;
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return null;
            }
        }

        public DriverTouch RetieveCurrentTouches(string DriverNo, DateTime TouchOnDate)
        {
            try
            {
                var CurrentTouchOpt1 = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                        where _TouchList.ActualStartTime != "" && _TouchList.ActualEndTime == "" && _TouchList.SkipTouch != true
                                        orderby int.Parse(_TouchList.SequenceNumber)
                                        select _TouchList).FirstOrDefault();

                if (CurrentTouchOpt1 != null)
                {
                    return CurrentTouchOpt1;
                }
                else
                {
                    var CurrentTouchOpt2 = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                            where (_TouchList.StopStatus == "InProgress" || _TouchList.StopStatus == "Open") && _TouchList.SkipTouch != true
                                            orderby int.Parse(_TouchList.SequenceNumber)
                                            select _TouchList).FirstOrDefault();
                    return CurrentTouchOpt2;
                }

            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return null;
            }
        }

        public DriverTouch RetieveNextTouches(string DriverNo, DateTime TouchOnDate)
        {
            try
            {
                var allStops = RetieveTouchesFromSession(DriverNo, TouchOnDate);

                //IF one stop is inprogress then we next immediate stop is next stop, other wise next open touch is next stop
                int whatIsNextStop = allStops.Any(s => s.StopStatus == "InProgress") ? 0 : 1;

                var NextTouch = (from _TouchList in RetieveTouchesFromSession(DriverNo, TouchOnDate)
                                 where _TouchList.StopStatus == "Open"
                                 orderby int.Parse(_TouchList.SequenceNumber)
                                 select _TouchList).Skip(whatIsNextStop).FirstOrDefault();

                return NextTouch;
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return null;
            }

        }

        public bool IsNextTouchWareHouseTouch(string DriverNo, DateTime TouchOnDate)
        {
            DriverTouch dt = RetieveCurrentTouches(DriverNo, TouchOnDate);
            bool isWHTouch = false;

            if (dt != null)
            {
                isWHTouch = dt.DestinationAddress.AddressType == DriverMobileApp.Models.PR_AddressType.AddressType.Warehouse;
            }

            return isWHTouch;
        }

        public DriverTouch RetieveToucheByQorid(string DriverNo, DateTime TouchOnDate, string TouchType, string QORID, string StopType, string Status = "Open")//TouchStatus.Open.ToString())
        {
            string ifOpenNeedInProgress = string.Empty;

            if (Status == TouchStatus.Open.ToString())
                ifOpenNeedInProgress = TouchStatus.InProgress.ToString();

            //--------------- ToDO - change following code-----------
            List<DriverTouch> driverTouchList = RetieveTouchesFromSession(DriverNo, TouchOnDate);

            var TouchList = (from _TouchList in driverTouchList
                             where _TouchList.TouchType == TouchType && _TouchList.QORID == QORID &&
                             _TouchList.StopType == StopType && (_TouchList.Status == Status || _TouchList.Status == ifOpenNeedInProgress)
                             select new DriverTouch
                             {
                                 QORID = _TouchList.QORID,
                                 CustomerName = _TouchList.CustomerName,
                                 DestinationAddress = _TouchList.DestinationAddress,
                                 WeightStationAddress = _TouchList.WeightStationAddress,
                                 OriginAddress = _TouchList.OriginAddress,
                                 ContainerNumber = _TouchList.ContainerNumber,
                                 ContainerSize = _TouchList.ContainerSize,
                                 ScheduledStartTime = _TouchList.ScheduledStartTime,
                                 ScheduledEndTime = _TouchList.ScheduledEndTime,

                                 EstimatedStartTime = (_TouchList.EstimatedStartTime == null ? _TouchList.ScheduledStartTime : _TouchList.EstimatedStartTime),
                                 EstimatedEndTime = (_TouchList.EstimatedEndTime == null ? _TouchList.ScheduledEndTime : _TouchList.EstimatedEndTime),
                                 SequenceNumber = _TouchList.SequenceNumber,
                                 TouchType = _TouchList.TouchType,
                                 BalanceDue = _TouchList.BalanceDue,
                                 Status = _TouchList.Status,
                                 OrderNum = _TouchList.OrderNum,
                                 StopType = _TouchList.StopType,
                                 TransiteCustRef = _TouchList.TransiteCustRef,
                                 TransiteTouchType = _TouchList.TransiteTouchType,
                                 IsCombinedTouch = _TouchList.IsCombinedTouch,
                                 IsTrailerNeeded = _TouchList.IsTrailerNeeded,
                                 TouchId = _TouchList.TouchId,
                                 TouchStopId = _TouchList.TouchStopId,
                                 SkipTouch = _TouchList.SkipTouch,
                                 FacilityName = _TouchList.FacilityName,
                                 IsExceptionalTouch = _TouchList.IsExceptionalTouch,
                                 StarsUnitId = _TouchList.StarsUnitId,
                             }).ToList();
            GetZippyShellDetails(TouchList);
            return TouchList.FirstOrDefault();
        }


        /// <summary>
        /// Updating stop start time
        /// </summary>
        /// <param name="intTouchId"></param>
        /// <param name="intTouchStopId"></param>
        public void UpdateIsTouchStartedOrder(Int32 intTouchId, Int32 intTouchStopId)
        {
            using (var db = new DriverMobileApp.Models.DBclass.RouteOptimzation())
            {
                db.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var TouchInfo = db.TouchInformations.Where(t => t.PK_TouchId == intTouchId).FirstOrDefault();

                var StopInfo = db.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).FirstOrDefault();

                StopInfo.ActualStartTime = Convert.ToDateTime(HttpContext.Current.Session["localDateTime"]);
                TouchInfo.IsTouchStarted = true;
                TouchInfo.Status = StopInfo.FK_TouchStatus = "InProgress";

                db.SaveChanges();
            }
        }

        public string CompleteTouch(AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string strLogID, string strUserID, string strTouchDate, bool unitNoValidButNotMovedIn, string driverComments)
        {
            var orderInfo = oAuditLogMaster.OrderNo.Split('-');
            string touchType = orderInfo[0].Trim();
            int sequeceNo = Convert.ToInt16(orderInfo[2]);

            try
            {
                string retVal = "Success";
                string strStopType = (oAuditLogMaster.StopType == "WeighStati" ? "WeighStation" : oAuditLogMaster.StopType);

                if ((touchType == "CC" && strStopType == "Pickup") == false && unitNoValidButNotMovedIn == false)
                {
                    retVal = oSMDBusinessLogic.CompleteSitelinkTouch(Convert.ToInt32(oAuditLogMaster.Qorid),
                                                                    touchType,
                                                                    sequeceNo,
                                                                    Convert.ToDateTime(oAuditLogMaster.ScheduledStartTime),
                                                                    DateTime.Now,
                                                                    oAuditLogMaster.LoginName,
                                                                    unitNo,
                                                                    driverComments
                                                                    );

                }

                //If system doesnt complet touch in CompleteSitelinkTouch, then we should through error message to the user
                if (retVal == "Cannot complete a touch without doing a Movein First.")
                {
                    return touchType + " - Cannot complete a touch without doing a Movein First. Please contact OM.";
                }
                else if (retVal == "Invalid Touchtype")
                {
                    return touchType + " - Invalid touchtype error from SiteLink, Please contact OM OR Skip this touch and continue with next touch.";
                }
                else if (retVal == "Success")
                {
                    CompleteStop(oAuditLogMaster, TouchStatus.Completed.ToString());
                    HttpContext.Current.Session["TouchList"] = null;
                }
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                SendEmail(Convert.ToInt32(oAuditLogMaster.Qorid), oAuditLogMaster, unitNo, strLogID, strUserID, strTouchDate, "CompleteTouch");
                return touchType + " completed with error. Email sent";
            }
            return "Success";
        }

        /// <summary>
        /// Method to complete DE , RE touch 
        /// </summary>
        /// <param name="oAuditLogMaster"></param>
        /// <param name="unitNo"></param>
        /// <param name="strLogID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strTouchDate"></param>
        /// <returns></returns>
        public string CompleteDE_RE_touch(AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string strLogID, string strUserID, string strTouchDate, string driverComments)
        {
            var orderInfo = oAuditLogMaster.OrderNo.Split('-');
            string touchType = orderInfo[0].Trim();
            int sequeceNo = Convert.ToInt16(orderInfo[2]);

            try
            {
                string retVal = "Success";

                //Added by Sohan
                retVal = oSMDBusinessLogic.CompleteSitelinkTouch(Convert.ToInt32(oAuditLogMaster.Qorid),
                                                                  touchType,
                                                                  sequeceNo,
                                                                  Convert.ToDateTime(oAuditLogMaster.ScheduledStartTime),
                                                                  DateTime.Now,
                                                                  oAuditLogMaster.LoginName,
                                                                  unitNo,
                                                                  driverComments
                                                                  );
                if (retVal == "Success")
                {
                    CompleteStop(oAuditLogMaster, TouchStatus.Completed.ToString()); // As per Old Code
                }
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                SendEmail(Convert.ToInt32(oAuditLogMaster.Qorid), oAuditLogMaster, unitNo, strLogID, strUserID, strTouchDate, "CompleteDE_RE_touch");
                return touchType + " completed with error. Email sent";
            }
            return "Success";
        }

        public void UpdateUnitNumberForDE(AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo)
        {
            using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
            {
                Int32 intrefId = Convert.ToInt32(oAuditLogMaster.RefID);
                var StopInformation = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intrefId).SingleOrDefault();

                if (StopInformation != null && StopInformation.TouchInformation != null)
                {
                    StopInformation.TouchInformation.ContainerNo = unitNo;

                    oDB.SaveChanges();
                }
            }
        }

        public bool CompleteTouchStopsForCancelTouches(AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string strTouchStatus, int intTouchId)
        {
            using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                DateTime localDateTime = Convert.ToDateTime(HttpContext.Current.Session["localDateTime"]);
                int? sequence = 0;
                if (intTouchId > 0)
                {
                    var touchInfo = oDB.TouchInformations.Where(t => t.PK_TouchId == intTouchId).FirstOrDefault();
                    if (touchInfo != null && touchInfo.StopInformations != null && touchInfo.StopInformations.Count > 0)
                    {
                        foreach (var stop in touchInfo.StopInformations)
                        {
                            stop.ActualEndTime = localDateTime;
                            stop.FK_TouchStatus = strTouchStatus;

                            sequence = stop.Sequence;
                        }
                    }
                }

                oDB.SaveChanges();

                string[] compltSkipTouches = { "Completed", "Skipped" };
                var StopStatus = oDB.StopInformations.Where(t => (t.FK_TouchId == intTouchId) && !compltSkipTouches.Contains(t.FK_TouchStatus)).AsQueryable();

                if (StopStatus.Where(t => t.IsRequired == true).Count() == 0)
                {
                    var StopCombineStatus = StopStatus.Where(t => t.IsRequired == false).ToList();
                    if (StopCombineStatus.Count > 0)
                    {
                        StopCombineStatus.ForEach(t =>
                        {
                            t.FK_TouchStatus = strTouchStatus;
                            t.ActualStartTime = localDateTime;
                            t.ActualEndTime = localDateTime;
                            t.Sequence = sequence;
                        });
                    }

                    var TouchInfo = oDB.TouchInformations.Where(t => t.PK_TouchId == intTouchId).SingleOrDefault();
                    TouchInfo.Status = strTouchStatus;

                    oDB.SaveChanges();
                }

                //If it is a exception touch then we should not cascade timings for next touches
                bool IsExceptionTouch = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Cookies["IsExceptionTouch"].Value);
                if (!IsExceptionTouch)
                {
                    var UpdateScheduleTime = oDB.USP_Update_ScheduledTime(Convert.ToInt32(oAuditLogMaster.DriverID), localDateTime, sequence, "S");
                }
            }

            HttpContext.Current.Session["TouchList"] = null;
            System.Web.HttpContext.Current.Request.Cookies["IsExceptionTouch"].Value = "false";
            return true;
        }

        public bool IsQuoteMovedIn(int QorID)
        {
            oSMDBusinessLogic = new SMDBusinessLogic();
            return oSMDBusinessLogic.IsQuoteMovedIn(QorID);
        }


        /// <summary>
        /// Completing Stop but not Touch
        /// </summary>
        /// <param name="oAuditLogMaster"></param>
        /// <param name="strTouchStatus"></param>
        /// <returns></returns>
        public bool CompleteStop(AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string strTouchStatus)
        {
            using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
            {
                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Int32 intQorid = Convert.ToInt32(oAuditLogMaster.Qorid);
                string strCompleted = TouchStatus.Completed.ToString();
                string strSkipped = TouchStatus.Skipped.ToString();

                Int32 intrefId = Convert.ToInt32(oAuditLogMaster.RefID);
                var StopInformation = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intrefId).SingleOrDefault();

                StopInformation.ActualEndTime = Convert.ToDateTime(HttpContext.Current.Session["localDateTime"]);
                StopInformation.FK_TouchStatus = strTouchStatus;


                oDB.SaveChanges();

                int intTouchId = (Int32)StopInformation.FK_TouchId;

                string[] compltSkipTouches = { "Completed", "Skipped" };
                var StopStatus = oDB.StopInformations.Where(t => (t.FK_TouchId == intTouchId) && !compltSkipTouches.Contains(t.FK_TouchStatus)).AsQueryable();

                if (StopStatus.Where(t => t.IsRequired == true).Count() == 0)
                {
                    var StopCombineStatus = StopStatus.Where(t => t.IsRequired == false).ToList();
                    if (StopCombineStatus.Count > 0)
                    {
                        StopCombineStatus.ForEach(t =>
                        {
                            t.FK_TouchStatus = strTouchStatus;
                            t.ActualStartTime = StopInformation.ActualEndTime;
                            t.ActualEndTime = StopInformation.ActualEndTime;
                        });
                    }

                    var TouchInfo = oDB.TouchInformations.Where(t => t.PK_TouchId == intTouchId).SingleOrDefault();
                    TouchInfo.Status = strTouchStatus;

                    oDB.SaveChanges();
                }
                //If it is a exception touch then we should not cascade timings for next touches
                bool IsExceptionTouch = Convert.ToBoolean(System.Web.HttpContext.Current.Request.Cookies["IsExceptionTouch"].Value);
                if (!IsExceptionTouch)
                {
                    DateTime localDateTime = Convert.ToDateTime(HttpContext.Current.Session["localDateTime"]);
                    var UpdateScheduleTime = oDB.USP_Update_ScheduledTime(Convert.ToInt32(oAuditLogMaster.DriverID), localDateTime, StopInformation.Sequence, "S");
                }
            }

            HttpContext.Current.Session["TouchList"] = null;
            System.Web.HttpContext.Current.Request.Cookies["IsExceptionTouch"].Value = "false";
            return true;
        }

        public bool Error(string strError, string strStackTraceInfo)
        {
            try
            {
                StackTraceLog oStackTraceLog = new StackTraceLog();
                EnDeCryption oEnDeCryption = new EnDeCryption();

                oStackTraceLog.ErrorMsg = strError;
                oStackTraceLog.StackTraceInfo = strStackTraceInfo;

                bool cookieUserIDExists = System.Web.HttpContext.Current.Request.Cookies["UserID"] != null;

                if (cookieUserIDExists)
                    oStackTraceLog.DriverID = oEnDeCryption.Decrypt(System.Web.HttpContext.Current.Request.Cookies["UserID"].Value.ToString());
                else
                    oStackTraceLog.DriverID = null;

                bool cookieQORidExists = System.Web.HttpContext.Current.Request.Cookies["QORid"] != null;

                if (cookieQORidExists)
                    oStackTraceLog.QorID = System.Web.HttpContext.Current.Request.Cookies["QORid"].Value;
                else
                    oStackTraceLog.QorID = null;

                oStackTraceLog.IpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                return oAuditLog.addStackTrace(oStackTraceLog);
            }
            catch (Exception ex)
            {
                Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool weightTicket(decimal starsID)
        {
            oSMDBusinessLogic = new SMDBusinessLogic();
            return oSMDBusinessLogic.GetWeigthTicket(orderNo: Convert.ToInt32(starsID));

            //using (var db = new STARSDBEntities())
            //{
            //    db.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
            //    var wTicket = (from _wTicket in db.T25_tblQuote where _wTicket.T25_Quote_Id == starsID select _wTicket.T25_WeightTicket_Flag).SingleOrDefault();

            //    if (wTicket != 0)
            //        return true;
            //    else
            //        return false;
            //}
        }

        /// <summary>
        /// Method to send email for any exception while completing touch complete operation
        /// </summary>
        /// <param name="QorID"></param>
        /// <param name="oAuditLogMaster"></param>
        /// <param name="unitNo"></param>
        /// <param name="LogID"></param>
        /// <param name="strUserID"></param>
        /// <param name="strTouchDate"></param>
        /// <param name="MethodName"></param>
        private void SendEmail(Int32 QorID, AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string LogID, string strUserID, string strTouchDate, string MethodName)
        {
            EmailLogic objEmail = new Models.Email.EmailLogic();
            objEmail.Exception_SendEmail(QorID, oAuditLogMaster, unitNo, LogID, strUserID, strTouchDate, MethodName);
        }

        #region This method has been moved to Models/Email/EmailLogic.cs

        //private string Exception_SendEmail(Int32 QorID, AuditLog.DBStructure.AuditLogMaster oAuditLogMaster, string unitNo, string LogID, string strUserID, string strTouchDate, string MethodName)
        //{
        //    try
        //    {
        //        EnDeCryption oEnDeCryption = new EnDeCryption();
        //        AppEmail oAppEmail = new AppEmail();
        //        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //        PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
        //        CustomerInfo oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QorID);
        //        UnitInfo oUnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
        //        SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfo(QorID);
        //        DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(strUserID), Convert.ToDateTime(strTouchDate), oAuditLogMaster.TransiteTouchType.Trim(), QorID.ToString(), oAuditLogMaster.StopType);

        //        oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
        //        oAppEmail.EmailFrom = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";
        //        oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Error occurred while completing touch";


        //        BillDetailInfo oBillDetailInfo = new BillDetailInfo();

        //        oBillDetailInfo.TransportationItems = oPRbusinesslogic.GetTransportationItems(QorID);
        //        oBillDetailInfo.POSItems = oPRbusinesslogic.GetPOSItemWithPrice(QorID);
        //        oBillDetailInfo.UnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
        //        oBillDetailInfo.RecurringItems = oPRbusinesslogic.GetRecurringItems(QorID);
        //        oBillDetailInfo.PricingInfo = oPRbusinesslogic.GetPricing(QorID);

        //        EmailLogic objEmail = new Models.Email.EmailLogic();
        //        bool retVal = objEmail.EmailBillInfo(ConfigurationManager.AppSettings["mailFrom"].ToString(), oBillDetailInfo, oCustomerInfo, oSiteInfo, QorID);
        //        //bool retVal = oPRbusinesslogic.EmailBillInfo(ConfigurationManager.AppSettings["mailFrom"].ToString(), oBillDetailInfo, oCustomerInfo, oSiteInfo, QorID);
        //        oAppEmail.EmailCc = "";

        //        StringBuilder emailBody = new StringBuilder();

        //        emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //        emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //        emailBody.Append("<head><title>1800PACKRAT.COM | Error occurred while completing touch</title></head>");
        //        emailBody.Append("<body>");

        //        emailBody.Append("<br />Error occurred while completing touch<br /><br />");
        //        emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

        //        if (oCustomerInfo != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.FirstName + " " + oCustomerInfo.LName + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Billing Address</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Addr1 + "<br />" +
        //                        oCustomerInfo.Addr2 + "<br />" + oCustomerInfo.City + (String.IsNullOrWhiteSpace(oCustomerInfo.City) ? "" : ", ") + oCustomerInfo.Region + (String.IsNullOrWhiteSpace(oCustomerInfo.Region) ? "" : ", ") + "<br />" + oCustomerInfo.Country + " - " + oCustomerInfo.PostalCode + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Phone No</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Phone + ", Alt Phone No " + oCustomerInfo.PhoneAlt + "</td>");
        //            emailBody.Append("</tr>");
        //        }

        //        string purchaseOrder = oPRbusinesslogic.GetPRGQuoteOrderLogData(QorID);
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Purchase Order</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (String.IsNullOrWhiteSpace(purchaseOrder) ? "N/A" : purchaseOrder) + "</td>");
        //        emailBody.Append("</tr>");

        //        if (oDriverTouch != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Origin Address</td>");

        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.OriginAddress.AddressLine1 + "<br />" +
        //            oDriverTouch.OriginAddress.AddressLine2 + "<br />" + oDriverTouch.OriginAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.City) ? "" : ", ") + oDriverTouch.OriginAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.State) ? "" : ", ") + " - " + oDriverTouch.OriginAddress.Zip + "</td>");

        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Destination Address</td>");

        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.DestinationAddress.AddressLine1 + "<br />" +
        //            oDriverTouch.DestinationAddress.AddressLine2 + "<br />" + oDriverTouch.DestinationAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.City) ? "" : ", ") + oDriverTouch.DestinationAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.State) ? "" : ", ") + " - " + oDriverTouch.DestinationAddress.Zip + "</td>");

        //            emailBody.Append("</tr>");
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>QORID</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + QorID.ToString() + "</td>");
        //        emailBody.Append("</tr>");

        //        if (oUnitInfo.Tables[0].Rows.Count > 0)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Name</td>");

        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + (string.IsNullOrWhiteSpace(unitNo) ? oUnitInfo.Tables[0].Rows[0]["UnitName"].ToString() : unitNo) + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oUnitInfo.Tables[0].Rows[0]["UnitDescription"].ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        if (oSiteInfo != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Facility Name</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LegalName.ToString() + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Location Code</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LocationCode.ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Touch Type</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oAuditLogMaster.TransiteTouchType.Trim() + "</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Date</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + System.Web.HttpContext.Current.Session["localDateTime"].ToString() + "</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("</table><br /><br />");

        //        emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

        //        oAppEmail.EmailBody = emailBody.ToString();

        //        //Inserting Email Log
        //        Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
        //        el.ApplicationName = "Driver App";
        //        el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //        el.User = strUserID;
        //        el.StartTime = DateTime.Now;
        //        el.Subject = oSiteInfo.LegalName.ToString() + " | Error occurred while completing touch";
        //        el.ToEmailIDs = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
        //        el.FromEmailID = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";
        //        el.CCEmailIDs = null;
        //        el.MethodName = MethodName;
        //        el.EmailTypeID = 4;
        //        el.QORID = Convert.ToInt32(QorID);
        //        el.TouchType = oDriverTouch == null ? "" : oDriverTouch.TouchType;
        //        el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

        //        var LogId = LogEmails.InsertLog(el);
        //        if (oAppEmail.SendEMail() == "Success")
        //        {
        //            //updating endtime in email log
        //            Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
        //            eml.PK_ID = LogId;
        //            eml.EndTime = DateTime.Now;
        //            LogEmails.UpdateLog(eml);
        //            return "Success";
        //        }
        //        else
        //            return "Unable to send Email";
        //    }
        //    catch (Exception ex)
        //    {
        //        Error(ex.Message.ToString(), ex.StackTrace.ToString());
        //        return ex.Message + " , " + "Unable to send Email";
        //    }
        //}

        #endregion

        /// <summary>
        /// If Route is locked for a driver on the given date then it returns true
        /// other wise it returns false
        /// </summary>
        /// <param name="driverId"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsRouteLocked(int driverId, DateTime date)
        {
            using (var context = new DriverMobileApp.Models.DBclass.RouteOptimzation())
            {
                context.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return context.LoadInformations.Any(x => x.DriverID == driverId &&
                                                        (x.CreatedDate.Value.Year == date.Year && x.CreatedDate.Value.Month == date.Month && x.CreatedDate.Value.Day == date.Day) &&
                                                          x.IsLocked == true);
            }
        }
    }
}