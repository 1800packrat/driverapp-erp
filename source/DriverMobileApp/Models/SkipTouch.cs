﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.Web.Mvc;

namespace DriverMobileApp.Models
{
    public class SkipTouch
    {
        public List<SelectListItem> GetMainReasons()
        {
            DataSet oDataSet = new DataSet();
            string strXMLPath = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Models/DBclass/SkipTouchXML/MainReasons.xml"));
            oDataSet.ReadXml(strXMLPath);

            var reasonList = (from _reasonList in oDataSet.Tables[0].AsEnumerable()
                              select new SelectListItem
            {
                Selected = false,
                Text = _reasonList.ItemArray[0].ToString(),
                Value = _reasonList.ItemArray[1].ToString()
            }).DefaultIfEmpty().ToList();

            return reasonList;
        }

        public List<SelectListItem> GetCnotAccessible()
        {
            DataSet oDataSet = new DataSet();
            string strXMLPath = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Models/DBclass/SkipTouchXML/CnotAccessible.xml"));
            oDataSet.ReadXml(strXMLPath);

            var resultList = (from _resultList in oDataSet.Tables[0].AsEnumerable()
                              select new SelectListItem
                              {
                                  Selected = false,
                                  Text = _resultList.ItemArray[0].ToString(),
                                  Value = _resultList.ItemArray[1].ToString()
                              }).DefaultIfEmpty().ToList();

            return resultList;
        }

        public List<SelectListItem> GetCOverWeight()
        {
            DataSet oDataSet = new DataSet();
            string strXMLPath = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Models/DBclass/SkipTouchXML/OverWeight.xml"));
            oDataSet.ReadXml(strXMLPath);

            var resultList = (from _resultList in oDataSet.Tables[0].AsEnumerable()
                              select new SelectListItem
                              {
                                  Selected = false,
                                  Text = _resultList.ItemArray[0].ToString(),
                                  Value = _resultList.ItemArray[1].ToString()
                              }).DefaultIfEmpty().ToList();

            return resultList;
        }

        public List<SelectListItem> GetCustRequest()
        {
            DataSet oDataSet = new DataSet();
            string strXMLPath = Path.GetFullPath(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Models/DBclass/SkipTouchXML/CustRequest.xml"));
            oDataSet.ReadXml(strXMLPath);

            var reasonList = (from _reasonList in oDataSet.Tables[0].AsEnumerable()
                              select new SelectListItem
                              {
                                  Selected = false,
                                  Text = _reasonList.ItemArray[0].ToString(),
                                  Value = _reasonList.ItemArray[1].ToString()
                              }).DefaultIfEmpty().ToList();

            return reasonList;
        }

        //private void HandleSkipLegs(int TouchId, int StopId, string reason, bool blPickupContainer, string strContainerNo, string strComments, int DriverId, int Qorid, string strTouchType, string strStopType)
        private void HandleSkipLegs(DBclass.SkipTouch objSkipTouch, int TouchId)
        {
            using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
            {
                var touch = oDB.TouchInformations.Where(t => t.PK_TouchId == TouchId).FirstOrDefault();

                if (touch != null)
                {
                    //If skipped stop is CC and leg is Dropoff leg then we have to skip CC first leg as well, other wise it will be inconsistancy data
                    if (touch.TouchType == PR_TouchType_Constant.CC)
                    {
                        var stop = touch.StopInformations.Where(s => s.Pk_Touch_StopId == objSkipTouch.FK_Touch_StopId).FirstOrDefault();
                        if (stop != null)
                        {
                            if (stop.StopType.StopType1 == "DropOff")
                            {
                                var pickUpStop = touch.StopInformations.Where(s => s.StopType.StopType1 == "Pickup").FirstOrDefault();

                                if (pickUpStop != null)
                                {
                                    pickUpStop.FK_TouchStatus = "Skipped";
                                    pickUpStop.SkipTouch = true;
                                    pickUpStop.ActualEndTime = DateTime.Now;

                                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                                    {
                                        FK_Touch_StopId = pickUpStop.Pk_Touch_StopId,
                                        Reason = objSkipTouch.Reason,
                                        Wc_PickupContainer = objSkipTouch.Wc_PickupContainer,
                                        Wc_ContainerNo = objSkipTouch.Wc_ContainerNo,
                                        Comments = objSkipTouch.Comments,
                                        DriverId = objSkipTouch.DriverId,
                                        QorId = objSkipTouch.QorId,
                                        TouchType = objSkipTouch.TouchType,
                                        StopType = objSkipTouch.StopType,
                                        CNA_Reason = objSkipTouch.CNA_Reason,
                                        CNA_ByVehicle = objSkipTouch.CNA_ByVehicle,
                                        CNA_Other = objSkipTouch.CNA_Other,
                                        CNA_Rain = objSkipTouch.CNA_Rain,
                                        CNA_Mud = objSkipTouch.CNA_Mud,
                                        CNA_Snow = objSkipTouch.CNA_Snow,
                                        CNR_ByCustRequest = objSkipTouch.CNR_ByCustRequest,
                                        CNR_ContLocked = objSkipTouch.CNR_ContLocked,
                                        CNR_CustReqType = objSkipTouch.CNR_CustReqType
                                    };
                                    oDB.SkipTouches.AddObject(oSkipTouch);
                                }
                            }
                        }
                    }

                    foreach (var stopInfo in touch.StopInformations.Where(s => s.FK_TouchStatus == "Open"))
                    {
                        stopInfo.SkipTouch = true;
                        stopInfo.FK_TouchStatus = "Skipped";
                        stopInfo.ActualEndTime = DateTime.Now;

                        DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                        {
                            FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                            Reason = objSkipTouch.Reason,
                            Wc_PickupContainer = objSkipTouch.Wc_PickupContainer,
                            Wc_ContainerNo = objSkipTouch.Wc_ContainerNo,
                            Comments = objSkipTouch.Comments,
                            DriverId = objSkipTouch.DriverId,
                            QorId = objSkipTouch.QorId,
                            TouchType = objSkipTouch.TouchType,
                            StopType = objSkipTouch.StopType,
                            CNA_Reason = objSkipTouch.CNA_Reason,
                            CNA_ByVehicle = objSkipTouch.CNA_ByVehicle,
                            CNA_Other = objSkipTouch.CNA_Other,
                            CNA_Rain = objSkipTouch.CNA_Rain,
                            CNA_Mud = objSkipTouch.CNA_Mud,
                            CNA_Snow = objSkipTouch.CNA_Snow,
                            CNR_ByCustRequest = objSkipTouch.CNR_ByCustRequest,
                            CNR_ContLocked = objSkipTouch.CNR_ContLocked,
                            CNR_CustReqType = objSkipTouch.CNR_CustReqType
                        };
                        oDB.SkipTouches.AddObject(oSkipTouch);
                        //oDB.SaveChanges();
                    }
                }

                oDB.SaveChanges();
            }
        }


        public bool WCskipTouch(string intDriverId, string strReason, bool blPickupContainer, string strContainerNo, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                    var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                    stopInfo.SkipTouch = true; // Marking it for SkipTouch
                    stopInfo.FK_TouchStatus = "Skipped";

                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                    {
                        FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                        Reason = reason.Text.Trim(),
                        Wc_PickupContainer = blPickupContainer,
                        Wc_ContainerNo = strContainerNo.Trim(),
                        Comments = strComments,
                        DriverId = Convert.ToInt32(intDriverId),
                        QorId = Convert.ToInt32(strQorid),
                        TouchType = strTouchType,
                        StopType = strStopType,
                    };
                    oDB.SkipTouches.AddObject(oSkipTouch);
                    oDB.SaveChanges();
                    
                    HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool ContainerNotAccessible(string intDriverId, string strReason, string strCNAreason, bool blVehicle, bool blRain, bool blMud, bool blSnow, bool blOther, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                var CNA_reason = (from _reason in GetCnotAccessible() where _reason.Value == strCNAreason select _reason).SingleOrDefault();
                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                    var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                    stopInfo.SkipTouch = true; // Marking it for SkipTouch
                    stopInfo.FK_TouchStatus = "Skipped";

                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                    {
                        FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                        Reason = reason.Text.Trim(),
                        CNA_Reason = CNA_reason.Text.Trim(),
                        CNA_ByVehicle = blVehicle,
                        CNA_Other = blOther,
                        CNA_Rain = blRain,
                        CNA_Mud = blMud,
                        CNA_Snow = blSnow,
                        Comments = strComments,
                        DriverId = Convert.ToInt32(intDriverId),
                        QorId = Convert.ToInt32(strQorid),
                        TouchType = strTouchType,
                        StopType = strStopType,
                    };
                    oDB.SkipTouches.AddObject(oSkipTouch);
                    oDB.SaveChanges();

                    HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool CustNotReady(string intDriverId, string strReason, bool blCustReq, bool blContLocked, string strCustRequest, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                var custReq = (from _custReq in GetCustRequest() where _custReq.Value == strCustRequest select _custReq).SingleOrDefault();

                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                    var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                    stopInfo.SkipTouch = true; // Marking it for SkipTouch
                    stopInfo.FK_TouchStatus = "Skipped";

                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                    {
                        FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                        Reason = reason.Text.Trim(),
                        CNR_ByCustRequest = blCustReq,
                        CNR_ContLocked = blContLocked,
                        CNR_CustReqType = (custReq == null ? string.Empty : custReq.Text.Trim()),
                        Comments = strComments,
                        DriverId = Convert.ToInt32(intDriverId),
                        QorId = Convert.ToInt32(strQorid),
                        TouchType = strTouchType,
                        StopType = strStopType,
                    };
                    oDB.SkipTouches.AddObject(oSkipTouch);
                    oDB.SaveChanges();

                    HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool ContainerOverWeight(string intDriverId, string strReason, string strWeight, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                var oWeight = (from _reason in GetCOverWeight() where _reason.Value == strWeight select _reason).SingleOrDefault();

                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                    var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                    stopInfo.SkipTouch = true; // Marking it for SkipTouch
                    stopInfo.FK_TouchStatus = "Skipped";

                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                    {
                        FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                        Reason = reason.Text.Trim(),
                        COW_Weight = oWeight.Text.Trim(),
                        Comments = strComments,
                        DriverId = Convert.ToInt32(intDriverId),
                        QorId = Convert.ToInt32(strQorid),
                        TouchType = strTouchType,
                        StopType = strStopType,
                    };
                    oDB.SkipTouches.AddObject(oSkipTouch);
                    oDB.SaveChanges();

                    HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool CouldntReachCust(string intDriverId, string strReason, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                    var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                    stopInfo.SkipTouch = true; // Marking it for SkipTouch
                    stopInfo.FK_TouchStatus = "Skipped";

                    DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                    {
                        FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                        Reason = reason.Text.Trim(),
                        Comments = strComments,
                        DriverId = Convert.ToInt32(intDriverId),
                        QorId = Convert.ToInt32(strQorid),
                        TouchType = strTouchType,
                        StopType = strStopType,
                    };
                    oDB.SkipTouches.AddObject(oSkipTouch);
                    oDB.SaveChanges();

                    HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }

        public bool OtherReason(string intDriverId, string strReason, string strComments, string strQorid, string strTouchStopId, string strTouchType, string strStopType, bool? cancleTouch = null, int? touchId = null)
        {
            try
            {
                var reason = (from _reason in GetMainReasons() where _reason.Value == strReason select _reason).SingleOrDefault();
                using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    if (cancleTouch != null && cancleTouch == true)
                    {
                        var touchInfo = oDB.TouchInformations.Where(t => t.PK_TouchId == touchId).FirstOrDefault();
                        if (touchInfo != null && touchInfo.StopInformations != null && touchInfo.StopInformations.Count > 0)
                        {
                            foreach (var stop in touchInfo.StopInformations)
                            {
                                Int32 intTouchStopId = Convert.ToInt32(stop.Pk_Touch_StopId);
                                var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                                stopInfo.SkipTouch = true; // Marking it for SkipTouch
                                stopInfo.FK_TouchStatus = "Skipped";

                                DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                                {
                                    FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                                    Reason = reason.Text.Trim(),
                                    Comments = strComments,
                                    DriverId = Convert.ToInt32(intDriverId),
                                    QorId = Convert.ToInt32(strQorid),
                                    TouchType = touchInfo.TouchType,
                                    StopType = strStopType,
                                };
                                oDB.SkipTouches.AddObject(oSkipTouch);
                                oDB.SaveChanges();                                
                            }
                        }
                    }
                    else
                    {
                        Int32 intTouchStopId = Convert.ToInt32(strTouchStopId);
                        var stopInfo = oDB.StopInformations.Where(t => t.Pk_Touch_StopId == intTouchStopId).SingleOrDefault();
                        stopInfo.SkipTouch = true; // Marking it for SkipTouch
                        stopInfo.FK_TouchStatus = "Skipped";

                        DBclass.SkipTouch oSkipTouch = new DBclass.SkipTouch()
                        {
                            FK_Touch_StopId = stopInfo.Pk_Touch_StopId,
                            Reason = reason.Text.Trim(),
                            Comments = strComments,
                            DriverId = Convert.ToInt32(intDriverId),
                            QorId = Convert.ToInt32(strQorid),
                            TouchType = strTouchType,
                            StopType = strStopType,
                        };
                        oDB.SkipTouches.AddObject(oSkipTouch);
                        oDB.SaveChanges();

                        HandleSkipLegs(oSkipTouch, stopInfo.FK_TouchId.Value);
                    }
                }
                HttpContext.Current.Session["TouchList"] = null;    // Forcing Touchlist to get populated in dashboard
                return true;
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return false;
            }
        }


    }

    //public class SkipTouchInfo : MessageTransaction
    public class SkipTouchInfo
    {
        public List<SelectListItem> mainReasonType { get; set; }
        public bool wContainer { get; set; }
        public string containerNo { get; set; }
        public string comments { get; set; }
        public List<SelectListItem> cNotAccessible { get; set; }
        public List<SelectListItem> custRequest { get; set; }
        public bool byVehicle { get; set; }
        public bool other { get; set; }
        public bool rain { get; set; }
        public bool mud { get; set; }
        public bool snow { get; set; }
        public bool byCustRequest { get; set; }
        public bool containerLocked { get; set; }
        public List<SelectListItem> cOverWeight { get; set; }


    }
}