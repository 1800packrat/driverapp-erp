﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using PR.LocalLogisticsSolution.Interfaces;
using static PR.UtilityLibrary.PREnums;
using PR.LocalLogisticsSolution.Model;

namespace DriverMobileApp.Models
{
    public class UserLogin
    {

        public string UserID { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Login Name")]
        //[RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", ErrorMessage = "Invalid EmailID")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Invalid Password")]
        [StringLength(15, MinimumLength = 3)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public DateTime LastLoginDate { get; set; }
        
        public bool Status { get; set; }
     
        public Int64 UserTypeID { get; set; }
     
        public string StoreNumber { get; set; }

        public Int32 LoadId { get; set; }
    }

    public class Login
    {
        public string GetIPAddress
        {
            get
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }

                return context.Request.ServerVariables["REMOTE_ADDR"];
            }
        }

        public UserLogin ValidateUser(UserLogin UserLogin, IUserService _userService)
        {
            DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation();
            UserLogin oUserLogin = new UserLogin();

            LogisticsUser user = new LogisticsUser();
            FADALoginStatus status = _userService.Login(UserLogin.UserName.Trim(), PR.UtilityLibrary.CommonUtility.Encrypt(UserLogin.Password.Trim(), DriverMobileApp.Helper.Utility.EnDeKey), GetIPAddress, Application.DriverApp, ref user);

            //var LogisticsUserInfo = oDB.LogisticsUsers.Where(t => t.UserName == UserLogin.UserName && t.Password == UserLogin.Password && t.UserIsActive == true).FirstOrDefault();
            if (user.ID != 0)
            {
                oUserLogin.FirstName = user.FirstName;
                oUserLogin.LastName = user.LastName;
                oUserLogin.UserID = user.UniqueID.ToString();
                oUserLogin.Status = user.UserIsActive;

                var date = System.DateTime.Now;

                //var loadInfo = oDB.LoadInformations.Where(x => x.DriverID == LogisticsUserInfo.DriverId).FirstOrDefault();
                var loadInfo = oDB.LoadInformations.Where(x => x.DriverID == user.UniqueID &&
                                                        (x.CreatedDate.Value.Year == date.Year && x.CreatedDate.Value.Month == date.Month && x.CreatedDate.Value.Day == date.Day)).FirstOrDefault();

                if (loadInfo != null)
                {
                    oUserLogin.StoreNumber = loadInfo.StoreNumber;
                    oUserLogin.LoadId = loadInfo.PK_LoadID;
                }
            }
            else
            {
                oUserLogin = null;
            }
            return oUserLogin;


        }
    }
}