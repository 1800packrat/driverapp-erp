﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PR.BusinessLogic;
using System.Data;
using PR.Entities;
using System.Configuration;
using DriverMobileApp.Models.Email;
using System.Text;

namespace DriverMobileApp.Models
{
    public class PRbusinesslogic
    {
        private SMDBusinessLogic oSMDBusinessLogic;

        EmailLogic objEmail = new Models.Email.EmailLogic();

        protected string strLocationCode = null; //To store Location Code
        protected Int32 intRentalID = 0; //To Store RentalID
        protected Int32 intTenantID = 0; //To Store TenantID
        protected Int32 intUnitID = 0; //To Store UnitID

        /// <summary>
        /// Method to get Quote Info and assign LocationCode, RentalID, TenantID to the global variables
        /// </summary>
        /// <param name="QORid"> Accepts Int32 QORid</param>
        private void GetQuoteInfo(Int32 QORid)
        {
            DataSet oDataSet = new DataSet();
            try
            {
                ////added by Sohan.
                //intUnitID = 2912;
                //intTenantID = 57871;
                //strLocationCode = "L157";
                ////SPERP-TODO-CTRMV  , added by Sohan.

                oSMDBusinessLogic = new SMDBusinessLogic();
                PR.Entities.EsbEntities.QuoteBase.RootObject quote = oSMDBusinessLogic.GetQuoteInfo(QORid);

                if (quote != null && quote.SF_QuoteInfo != null)
                {
                    strLocationCode = quote.SF_QuoteInfo.LocationCode;
                    intRentalID = Convert.ToInt32(quote.SF_QuoteInfo.RentalID);
                    intTenantID = Convert.ToInt32(quote.SF_QuoteInfo.CustomerId);
                    intUnitID = Convert.ToInt32(quote.SF_QuoteInfo.UnitId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetCustomerAltPhoneNo returns customer Alternate phone No based on QORid
        /// Call this method when Corpcode, strLocationCode, username, password, intTenantID have not been populated
        /// </summary>
        /// <param name="QORid"> Accepts Int32 QORid</param>
        /// 
        public CustomerInfo GetCustomerInfo(Int32 QORid)
        {
            try
            {
                if (Convert.ToString(HttpContext.Current.Session["SLCustomerInfo_QORid"]) != Convert.ToString(QORid))
                {
                    HttpContext.Current.Session["SLCustomerInfo_QORid"] = QORid.ToString();
                    HttpContext.Current.Session["SLCustomerInfo"] = GetCustomerDetails(QORid);
                    return (CustomerInfo)HttpContext.Current.Session["SLCustomerInfo"];
                }
                else
                    return (CustomerInfo)HttpContext.Current.Session["SLCustomerInfo"];
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                HttpContext.Current.Session["SLCustomerInfo_QORid"] = QORid.ToString();
                HttpContext.Current.Session["SLCustomerInfo"] = GetCustomerDetails(QORid);
                return (CustomerInfo)HttpContext.Current.Session["SLCustomerInfo"];
            }
        }

        public CustomerInfo GetCustomerDetails(Int32 QORid)
        {
            DataSet oDataSet = new DataSet();
            CustomerInfo oCustomerInfo = new CustomerInfo();

            oSMDBusinessLogic = new SMDBusinessLogic();
            PR.Entities.EsbEntities.CustomerData.RootObject customer = oSMDBusinessLogic.GetCustomerInfo(qorId: QORid);

            oCustomerInfo.FirstName = customer.SF_CustomerInfo.Customer_FirstName;
            oCustomerInfo.LName = customer.SF_CustomerInfo.Customer_LastName;
            oCustomerInfo.Company = customer.SF_CustomerInfo.Customer_Company;
            oCustomerInfo.Addr1 = customer.SF_CustomerInfo.Customer_Address1;
            oCustomerInfo.City = customer.SF_CustomerInfo.Customer_City;
            oCustomerInfo.Region = customer.SF_CustomerInfo.Customer_Region;
            oCustomerInfo.PostalCode = customer.SF_CustomerInfo.Customer_Zip;
            oCustomerInfo.Country = customer.SF_CustomerInfo.Customer_Country;
            oCustomerInfo.Phone = customer.SF_CustomerInfo.Customer_PhoneNo;
            oCustomerInfo.PhoneAlt = customer.SF_CustomerInfo.Customer_PhoneNoAlt;
            oCustomerInfo.EmailID = customer.SF_CustomerInfo.Customer_Email;

            //oCustomerInfo.FirstName = oDataSet.Tables[0].Rows[0]["Customer_FirstName"].ToString();
            //oCustomerInfo.LName = oDataSet.Tables[0].Rows[0]["Customer_LastName"].ToString();
            //oCustomerInfo.Company = oDataSet.Tables[0].Rows[0]["Customer_Company"].ToString();
            //oCustomerInfo.Addr1 = oDataSet.Tables[0].Rows[0]["Customer_Address1"].ToString(); 
            //oCustomerInfo.City = oDataSet.Tables[0].Rows[0]["Customer_City"].ToString();
            //oCustomerInfo.Region = oDataSet.Tables[0].Rows[0]["Customer_Region"].ToString();
            //oCustomerInfo.PostalCode = oDataSet.Tables[0].Rows[0]["Customer_Zip"].ToString();
            //oCustomerInfo.Country = oDataSet.Tables[0].Rows[0]["Customer_Country"].ToString();
            //oCustomerInfo.Phone = oDataSet.Tables[0].Rows[0]["Customer_PhoneNo"].ToString();
            //oCustomerInfo.PhoneAlt = oDataSet.Tables[0].Rows[0]["Customer_PhoneNoAlt"].ToString();
            //oCustomerInfo.EmailID = oDataSet.Tables[0].Rows[0]["Customer_Email"].ToString();

            //oCustomerInfo.FirstName = oDataSet.Tables[0].Rows[0]["sFName"].ToString();
            //oCustomerInfo.LName = oDataSet.Tables[0].Rows[0]["sLName"].ToString();
            //oCustomerInfo.Company = oDataSet.Tables[0].Rows[0]["sCompany"].ToString();
            //oCustomerInfo.Addr1 = oDataSet.Tables[0].Rows[0]["sAddr1"].ToString();
            //oCustomerInfo.Addr2 = oDataSet.Tables[0].Rows[0]["sAddr2"].ToString();
            //oCustomerInfo.City = oDataSet.Tables[0].Rows[0]["sCity"].ToString();
            //oCustomerInfo.Region = oDataSet.Tables[0].Rows[0]["sRegion"].ToString();
            //oCustomerInfo.PostalCode = oDataSet.Tables[0].Rows[0]["sPostalCode"].ToString();
            //oCustomerInfo.Country = oDataSet.Tables[0].Rows[0]["sCountry"].ToString();
            //oCustomerInfo.Phone = oDataSet.Tables[0].Rows[0]["sPhone"].ToString();
            //oCustomerInfo.PhoneAlt = oDataSet.Tables[0].Rows[0]["sPhoneAlt"].ToString();
            //oCustomerInfo.EmailID = oDataSet.Tables[0].Rows[0]["sEmail"].ToString();

            return oCustomerInfo;
        }

        /// <summary>
        /// Get Facility email Id
        /// </summary>
        /// <param name="strLocationCode"></param>
        /// <returns></returns>
        public string GetFacilityEmailId(Int32 strQorId)
        {
            GetQuoteInfo(strQorId);

            DataSet oDataSet = new DataSet();
            LocalComponent oLocalComponent = new LocalComponent();
            oDataSet = oLocalComponent.GetCallBlastData(strLocationCode);
            return oDataSet.Tables[0].Rows[0]["Email1"].ToString().Trim();
        }

        /// <summary>
        /// Method returns Unit Length based on QORid
        /// </summary>
        /// <param name="QORid"> Accepts Int32 QORid</param>
        private Int32 GetUnitLength(Int32 QORid)
        {
            ////SFERP-TODO-CTRMV 
            //GetQuoteInfo(QORid); 
            //UnitInfo oUnitInfo = oSMDBusinessLogic.GetAddedUnit(QORid,""); 
            //return Convert.ToInt32(oUnitInfo.Tables[0].Rows[0]["UnitLength"]);

            oSMDBusinessLogic = new SMDBusinessLogic();
            UnitInfo oUnitInfo = oSMDBusinessLogic.GetUnitInfoByQorId(QORid);
            return Convert.ToInt32(oUnitInfo.Tables[0].Rows[0]["UnitLength"]);
        }

        /// <summary>
        /// Method returns UnitName/Number based on QORid
        /// </summary>
        /// <param name="QORid">Accepts Int32 QORid</param>
        /// <returns>Int32 Unit ID</returns>
        public string GetUnitName(Int32 QORid)  //GetUnitID(Int32 QORid)
        {
            ////SFERP-TODO-CTRMV 
            // GetQuoteInfo(QORid); 
            // UnitInfo oUnitInfo = oSMDBusinessLogic.GetAddedUnit(oSLAPIMobile);
            // return oUnitInfo.Tables[0].Rows[0]["UnitName"].ToString().Trim().ToUpper();

            oSMDBusinessLogic = new SMDBusinessLogic();
            UnitInfo oUnitInfo = oSMDBusinessLogic.GetUnitInfoByQorId(QORid);

            return Convert.ToString(oUnitInfo.Tables[0].Rows[0]["UnitName"]);
        }

        /// <summary>
        /// This method to validate unit name exists with the given location or not
        /// </summary>
        /// <param name="unitName"></param>
        /// <returns></returns>
        public int GetUnitByUnitName(Int32 QORid, string unitName, out bool isTruckFromDiffLocation, string marketLocCode = null)
        {
            ////SFERP-TODO-CTRMV 
            //GetQuoteInfo(QORid);

            isTruckFromDiffLocation = false;

            if (!string.IsNullOrEmpty(marketLocCode))
            {
                isTruckFromDiffLocation = marketLocCode != strLocationCode;
            }

            string slLocCode = isTruckFromDiffLocation ? marketLocCode : strLocationCode;

            if (string.IsNullOrEmpty(strLocationCode) == false)
                return oSMDBusinessLogic.ValidateUnitByUnitName(slLocCode, unitName);
            else
                return -1;
        }

        public List<POSData> GetPOSitem(Int32 QORid)
        {
            ////SFERP-TODO-CTRMV 
            //GetQuoteInfo(QORid);

            //POSItems oPOSItems = oSMDBusinessLogic.GetAppliedPOSItems(GetUnitID(QORid));  // Old Code

            oSMDBusinessLogic = new SMDBusinessLogic();
            POSItems oPOSItems = oSMDBusinessLogic.GetAppliedPOSItems(QORid); // QORid - Added by Sohan

            var POSinfo = (from _POSinfo in oPOSItems.Tables[0].AsEnumerable()
                           select new POSData
                           {
                               ItemDescription = (string)_POSinfo.ItemArray[0],
                               Quantity = Convert.ToInt32(_POSinfo.ItemArray[1])
                           }).DefaultIfEmpty().ToList();
            if (POSinfo[0] != null)
                return POSinfo;
            else
                return null;
        }

        /// <summary>
        /// SFERP-TODO-RMVNICD
        /// Method to get POS Items check for Unit
        /// </summary>
        /// <param name="starsUnitId"></param>
        /// <returns></returns>
        public List<POSData> GetPOSitemFromStarsDB(int starsUnitId)
        {
            //DBclass.STARSDBEntities sd = new DBclass.STARSDBEntities();
            //var posItems = sd.USP_DA_GetCheckList(starsUnitId, ConfigurationManager.AppSettings["DACheckList"].ToString());

            //var POSinfo = (from _POSinfo in posItems
            //               select new POSData
            //               {
            //                   ItemDescription = (string)_POSinfo.ItemDescription,
            //                   Quantity = Convert.ToInt32(_POSinfo.Quantity)
            //               }).DefaultIfEmpty().ToList();

            //if (POSinfo != null && POSinfo.Count > 0 && POSinfo[0] != null)
            //    return POSinfo;
            //else
            return null;
        }

        /// <summary>
        /// GetFacilityInfo returns an object having Facility Information
        /// </summary>
        /// <param name="QORid"></param>
        /// <returns></returns>
        public SiteInfo GetFacilityInfo(Int32 QORid)
        {
            GetQuoteInfo(QORid);

            oSMDBusinessLogic = new SMDBusinessLogic();
            SiteInfo oSiteInfo = oSMDBusinessLogic.GetSiteInformation(strLocationCode);

            return oSiteInfo;
        }

        /// <summary>
        /// GetFacilityInfo returns an object having Facility Information
        /// </summary>
        /// <param name="QORid"></param>
        /// <returns></returns>
        public SiteInfo GetFacilityInfoByStoreNumber(string storeNumber)
        {
            SiteInfo oSiteInfo = (new SMDBusinessLogic()).GetSiteInformationWithMarkets(storeNumber);

            return oSiteInfo;
        }

        public List<TouchData> GetLDMTouchInfo(string TouchType, Int32 QORid)
        {
            return GetTouchInfo(TouchType, QORid, true);
        }

        /// <summary>
        /// GetTouchInfo returns touch info based on Touch Type
        /// </summary>
        /// <param name="TouchType"> Accepts String TouchType</param>
        /// 
        public List<TouchData> GetTouchInfo(string TouchType, Int32 QORid)
        {
            try
            {
                if ((List<TouchData>)HttpContext.Current.Session["SLTouchInfo"] != null)
                {
                    List<TouchData> touchData = (List<TouchData>)HttpContext.Current.Session["SLTouchInfo"];

                    if (touchData[0].QORID.ToString().Trim() == QORid.ToString() && touchData[0].TouchType.ToString().Trim() == PR_TouchType_Constant.GetTouchType(TouchType).ToString().Trim())
                        return (List<TouchData>)HttpContext.Current.Session["SLTouchInfo"];
                    else
                    {
                        HttpContext.Current.Session["SLTouchInfo"] = GetTouchInfo(TouchType, QORid, true);
                        return (List<TouchData>)HttpContext.Current.Session["SLTouchInfo"];
                    }
                }
                else
                {
                    HttpContext.Current.Session["SLTouchInfo"] = GetTouchInfo(TouchType, QORid, true);
                    return (List<TouchData>)HttpContext.Current.Session["SLTouchInfo"];
                }
            }
            catch (Exception ex)
            {
                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
                HttpContext.Current.Session["SLTouchInfo"] = GetTouchInfo(TouchType, QORid, true);
                return (List<TouchData>)HttpContext.Current.Session["SLTouchInfo"];
            }
        }

        private List<TouchData> GetTouchInfo(string TouchType, Int32 QORid, bool flag)
        {
            TouchData touch = null;
            List<TouchData> TouchInfo = new List<TouchData>();

            oSMDBusinessLogic = new SMDBusinessLogic();
            DataSet dsTouches = oSMDBusinessLogic.GetAppliedTouches(QORid);

            if (dsTouches != null && dsTouches.Tables[0] != null && dsTouches.Tables[0].Rows.Count > 0)
            {
                DataRow[] drTouches = dsTouches.Tables[0].Select("TouchTypeShort = '" + TouchType + "'");

                foreach (DataRow drTouch in drTouches)
                {
                    touch = new TouchData();

                    touch.QORID = QORid;
                    touch.TouchType = PR_TouchType_Constant.GetTouchType(drTouch["TouchTypeShort"].ToString()).ToString().Trim();
                    touch.DeliveryDate = Convert.ToDateTime(drTouch["DeliveryDate"]);

                    touch.Comments = Convert.ToString(drTouch["Comments"]);
                    touch.Directions = Convert.ToString(drTouch["Directions"]);
                    touch.Instructions = Convert.ToString(drTouch["Instructions"]);

                    touch.AddrFrom_Addr1 = Convert.ToString(drTouch["Origin_Address1"]);
                    touch.AddrFrom_City = Convert.ToString(drTouch["Origin_City"]);
                    touch.AddrFrom_State = Convert.ToString(drTouch["Origin_State"]);
                    touch.AddrFrom_Zip = Convert.ToString(drTouch["Origin_Zip"] == null ? 0 : drTouch["Origin_Zip"]);
                    touch.AddrFrom_FName = Convert.ToString(drTouch["Origin_FirstName"]);
                    touch.AddrFrom_LName = Convert.ToString(drTouch["Origin_LastName"]);
                    touch.AddrFrom_PhoneNum = Convert.ToString(drTouch["Origin_PhoneNo"]);
                    touch.AddrFrom_Company = Convert.ToString(drTouch["Origin_Company"]);

                    touch.AddrTo_Addr1 = Convert.ToString(drTouch["Destination_Address1"]);
                    touch.AddrTo_City = Convert.ToString(drTouch["Destination_City"]);
                    touch.AddrTo_State = Convert.ToString(drTouch["Destination_State"]);
                    touch.AddrTo_Zip = Convert.ToString(drTouch["Destination_Zip"] == null ? 0 : drTouch["Destination_Zip"]);
                    touch.AddreTo_FName = Convert.ToString(drTouch["Destination_FirstName"]);
                    touch.AddrTo_LName = Convert.ToString(drTouch["Destination_LastName"]);
                    touch.AddrTo_PhoneNum = Convert.ToString(drTouch["Destination_PhoneNo"]);
                    touch.AddrTo_Company = Convert.ToString(drTouch["Destination_Company"]);

                    touch.Drivewaytype_Sloped = Convert.ToBoolean(drTouch["Drivewaytype_Sloped"] == null ? 0 : drTouch["Drivewaytype_Sloped"]);
                    touch.Drivewaytype_Soft = Convert.ToBoolean(drTouch["Drivewaytype_Soft"] == null ? 0 : drTouch["Drivewaytype_Soft"]);
                    touch.Drivewaytype_Dirt = Convert.ToBoolean(drTouch["Drivewaytype_Dirt"] == null ? 0 : drTouch["Drivewaytype_Dirt"]);
                    touch.Drivewaytype_Grvel = Convert.ToBoolean(drTouch["Drivewaytype_Grvel"] == null ? 0 : drTouch["Drivewaytype_Grvel"]);
                    touch.Drivewaytype_Paved = Convert.ToBoolean(drTouch["Drivewaytype_Paved"] == null ? 0 : drTouch["Drivewaytype_Paved"]);
                    touch.Drivewaytype_Bricked = Convert.ToBoolean(drTouch["Drivewaytype_Bricked"] == null ? 0 : drTouch["Drivewaytype_Bricked"]);
                    touch.Drivewaytype_Curbed = Convert.ToBoolean(drTouch["Drivewaytype_Curbed"] == null ? 0 : drTouch["Drivewaytype_Curbed"]);

                    touch.DoorFacesRear = Convert.ToBoolean(drTouch["DoorFacesRear"] == null ? 0 : drTouch["DoorFacesRear"]);
                    touch.Obstacle_PowerLines = Convert.ToBoolean(drTouch["Obstacle_PowerLines"] == null ? 0 : drTouch["Obstacle_PowerLines"]);
                    touch.Obstacle_Fences = Convert.ToBoolean(drTouch["Obstacle_Fences"] == null ? 0 : drTouch["Obstacle_Fences"]);
                    touch.Obstacle_Trees = Convert.ToBoolean(drTouch["Obstacle_Trees"] == null ? 0 : drTouch["Obstacle_Trees"]);
                    touch.Obstacle_Sprinklers = Convert.ToBoolean(drTouch["Obstacle_Sprinklers"] == null ? 0 : drTouch["Obstacle_Sprinklers"]);
                    touch.Obstacle_Landscaping = Convert.ToBoolean(drTouch["Obstacle_Landscaping"] == null ? 0 : drTouch["Obstacle_Landscaping"]);
                    touch.Obstacle_Septic = Convert.ToBoolean(drTouch["Obstacle_Septic"] == null ? 0 : drTouch["Obstacle_Septic"]);
                    touch.Obstacle_Other = Convert.ToBoolean(drTouch["Obstacle_Other"] == null ? 0 : drTouch["Obstacle_Other"]);

                    //TouchID from Salesforce.
                    touch.EditIdentifier = String.IsNullOrWhiteSpace(drTouch["SvcAssignID"].ToString()) ? "" : drTouch["SvcAssignID"].ToString();
                    touch.Status = Convert.ToString(drTouch["TouchStatus"]);
                    touch.Distance = Convert.ToDecimal(drTouch["TouchMiles"] == null ? 0 : drTouch["TouchMiles"]);

                    touch.SubTotal = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["SubTotal"].ToString()) == true ? 0 : drTouch["SubTotal"]);
                    touch.Discounts = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["Discounts"].ToString()) == true ? 0 : drTouch["Discounts"]);
                    touch.Net = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["Net"].ToString()) == true ? 0 : drTouch["Net"]);
                    touch.Tax = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["Tax"].ToString()) == true ? 0 : drTouch["Tax"]);
                    touch.ServiceType = Convert.ToString(drTouch["TouchTypeFull"]);
                    touch.SequenceNumber = Convert.ToInt32(String.IsNullOrWhiteSpace(drTouch["SequenceNo"].ToString()) == true ? 0 : drTouch["SequenceNo"]);
                    touch.UnitLength = Convert.ToInt32(String.IsNullOrWhiteSpace(drTouch["UnitSize"].ToString()) == true ? 0 : drTouch["UnitSize"]);

                    //Below are not being used anywhere. - TBD 
                    // touch.AddrFrom_Addr2 = Convert.ToString(drTouch["Origin_Address2"]);
                    //touch.AddrTo_Addr2 = Convert.ToString(drTouch["Destination_Address2"]);
                    //touch.Drivewaytype_Other = Convert.ToBoolean(drTouch["Drivewaytype_Other"] == null ? 0 : drTouch["Drivewaytype_Other"]);
                    //touch.QTID = Convert.ToInt32(drTouch["QTID"] == null ? 0 : drTouch["QTID"]);
                    //touch.AddrFrom_Company = Convert.ToString(drTouch["AddrFrom_Company"]);
                    //touch.AddrFrom_MobilePhoneNumber = Convert.ToString(drTouch["AddrFrom_MobilePhoneNumber"]);
                    //touch.AddrTo_MobilePhoneNum = Convert.ToString(drTouch["AddrTo_MobilePhoneNum"]);
                    //touch.Starts = Convert.ToString(drTouch["Starts"]);
                    //touch.End = Convert.ToString(drTouch["End"]);
                    //touch.ExcessCharge = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["ExcessCharge"].ToString()) == true ? 0 : drTouch["ExcessCharge"]);
                    //touch.ExcessDistance = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["ExcessDistance"].ToString()) == true ? 0 : drTouch["ExcessDistance"]);
                    //touch.Charge = Convert.ToDecimal(String.IsNullOrWhiteSpace(drTouch["Charge"].ToString()) == true ? 0 : drTouch["Charge"]);
                    //touch.ChargeDescriptionID = Convert.ToInt32(String.IsNullOrWhiteSpace(drTouch["ChargeDescriptionID"].ToString()) == true ? 0 : drTouch["ChargeDescriptionID"]);

                    TouchInfo.Add(touch);
                }
            }
            else
            {
                throw new Exception("EsbMethod.GetAppliedTouches - No touch info available.");
            }

            return TouchInfo;
        }

        public UnitInfo GetUnitInfo(Int32 QORid)
        {
            ////SFERP-TODO-CTRMV 
            //GetQuoteInfo(QORid);
            //return oSMDBusinessLogic.GetAddedUnit(oSLAPIMobile);

            return oSMDBusinessLogic.GetUnitInfoByQorId(QORid);
        }

        public string GetPRGQuoteOrderLogData(int qorid)
        {
            LocalComponent oLocalComponent = new LocalComponent();
            DataSet oDataSet = new DataSet();
            oDataSet = oLocalComponent.GetPRGQuoteOrderLogData(qorid);
            if (oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0)
                return oDataSet.Tables[0].Rows[0]["PurchaseOrder"].ToString();
            else
                return "";
        }

        public string GetFacilityNameByLocCode(string slLocCode)
        {
            LocalComponent oLocalComponent = new LocalComponent();
            DataSet oDataSet = new DataSet();
            oDataSet = oLocalComponent.GetRDFacilityDataByLocationCode(slLocCode);
            if (oDataSet.Tables.Count > 0 && oDataSet.Tables[0].Rows.Count > 0)
                return oDataSet.Tables[0].Rows[0]["CompDBAName"].ToString();
            else
                return "";
        }

        #region This method has been moved to Models/Email/EmailLogic.cs

        //public bool EmailBillInfo(string CustomerEmail, BillDetailInfo oBillDetailInfo, CustomerInfo oCustomerInfo, SiteInfo oSiteInfo, Int32 intQorId)
        //{
        //    AppEmail oAppEmail = new AppEmail();     

        //    oAppEmail.EmailTo = String.IsNullOrWhiteSpace(CustomerEmail) ? null : CustomerEmail;
        //    oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();

        //    oAppEmail.Subject = "1800PACKRAT.COM - " + oSiteInfo.LegalName + " | Bill copy for Customer";

        //    StringBuilder emailBody = new StringBuilder();
        //    emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //    emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //    emailBody.Append("<head><title>1800PACKRAT.COM | Bill copy for Customer</title></head>");
        //    emailBody.Append("<body>");

        //    emailBody.Append(oCustomerInfo.FirstName + ",<br /><br />");
        //    emailBody.Append("Thank you for choosing 1800PackRat. Bill details for your order is as follows.<br /><br />");
        //    emailBody.Append("QORID : " + intQorId.ToString() + "<br /><br />");

        //    if (oBillDetailInfo.PricingInfo != null)
        //    {
        //        emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Pricing Details</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white;'>Total</th></tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Total Due on Delivery</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.DueAtDelivery.ToString("c") + "</td>");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Recurring Monthly Charges</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.RecurringMonthlyCharge.ToString("c") + "</td>");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>Future Transportation Charges</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oBillDetailInfo.PricingInfo.FutureTransportationCharge.ToString("c") + "</td>");
        //        emailBody.Append("</tr>");

        //    }
        //    emailBody.Append("</table><br /><br />");

        //    if (oBillDetailInfo.TransportationItems.Tables[0].Rows.Count > 0)
        //    {
        //        emailBody.Append("<table width='100%' cellpadding='2' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Transportation Items</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white;'>Total</th></tr>");


        //        foreach (System.Data.DataRow oDataRow in oBillDetailInfo.TransportationItems.Tables[0].Rows)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["Description"].ToString() + "</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Net"]).ToString("c") + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        emailBody.Append("</table><br /><br />");
        //    }

        //    if (oBillDetailInfo.POSItems.Tables[0].Rows.Count > 0)
        //    {
        //        emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td colspan='3' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>POS Items</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Quantity</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Total</th></tr>");

        //        foreach (System.Data.DataRow oDataRow in oBillDetailInfo.POSItems.Tables[0].Rows)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["ItemDescription"].ToString() + "</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Quantity"]).ToString("c") + "</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Total"]).ToString("c") + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        emailBody.Append("</table><br /><br />");
        //    }

        //    if (oBillDetailInfo.UnitInfo.Tables[0].Rows.Count > 0)
        //    {
        //        emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Unit Information</td>");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit No</th>");
        //        emailBody.Append("</tr>");

        //        foreach (System.Data.DataRow oDataRow in oBillDetailInfo.UnitInfo.Tables[0].Rows)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["UnitDescription"].ToString() + "</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["UnitName"].ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        emailBody.Append("</table><br /><br />");
        //    }

        //    if (oBillDetailInfo.RecurringItems.Tables[0].Rows.Count > 0)
        //    {
        //        emailBody.Append("<table width='100%' cellpadding='3' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td colspan='2' align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>Recurring Items</td>");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr><th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Description</th>");
        //        emailBody.Append("<th align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Quantity</th></tr>");

        //        foreach (System.Data.DataRow oDataRow in oBillDetailInfo.RecurringItems.Tables[0].Rows)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oDataRow["ItemDescription"].ToString() + "</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + Convert.ToDecimal(oDataRow["Quantity"]).ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        emailBody.Append("</table><br /><br />");
        //    }



        //    CustomerSignature oCustomerSignature = new CustomerSignature();
        //    byte[] imageBytes = oCustomerSignature.GetSignatureByte(intQorId);   //Retrieving  Signature if exists

        //    if (imageBytes != null)
        //    {
        //        emailBody.Append("<img src=cid:CustSignature><br />");
        //        emailBody.Append("Customer's Signature<br /><br />");
        //        emailBody.Append("<table width='600px;'><tr><td>" + System.Configuration.ConfigurationManager.AppSettings["SigBelowLine"].ToString() + "</td></tr></table><br /><br /><br />");
        //        emailBody.Append("Regards<br />1800-PACK-RAT");

        //        oAppEmail.EmailBody = emailBody.ToString();

        //        //Customer signature attachment 
        //        oAppEmail.Attachment = imageBytes;
        //        oAppEmail.GetContentId = "CustSignature";


        //        oAppEmail.AttachedFileName = String.Empty;
        //        oAppEmail.ReportFormat = String.Empty;
        //        if (oAppEmail.SendAltEMail() == "Success")
        //            return true;
        //        else
        //            return false;
        //        //End customer signature attachment
        //    }
        //    else
        //    {
        //        emailBody.Append("Regards<br />1800-PACK-RAT");
        //        oAppEmail.EmailBody = emailBody.ToString();

        //        oAppEmail.AttachedFileName = String.Empty;
        //        oAppEmail.ReportFormat = String.Empty;
        //        if (oAppEmail.SendEMail() == "Success")
        //            return true;
        //        else
        //            return false;
        //    }

        //}

        #endregion


        #region This email is Not require as per Kelly and Joe Nolet.
        //public PricingInfo GetPricing(Int32 QORid)
        //{
        //    return oSMDBusinessLogic.GetPricingInfoData(QORid);
        //}

        //public TransportationItems GetTransportationItems(Int32 QORid)
        //{
        //    return oSMDBusinessLogic.GetAppliedTransportationCharges(QORid);
        //}

        //public POSItems GetPOSItemWithPrice(Int32 QORid)
        //{
        //    return oSMDBusinessLogic.GetAppliedPOSItems(QORid);
        //}
        //public RecurringItems GetRecurringItems(Int32 QORid)
        //{
        //    return oSMDBusinessLogic.GetAppliedRecurringItems(QORid);
        //}

        #endregion This email is Not require as per Kelly and Joe Nolet.

    }
}