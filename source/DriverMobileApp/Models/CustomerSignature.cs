﻿using System;
using System.Linq;
using System.Data;

namespace DriverMobileApp.Models
{
    /// <summary>
    /// capture Customer Signature and other relevant information
    /// </summary>
    public class SignatureData
    {
        public Int64 QORid { get; set; }
        public string TouchType { get; set; }
        public Int64 SequenceNumber { get; set; }
        public string CustomerNumber { get; set; }
        public string SignatureImage { get; set; }
        public string Comments { get; set; }
        public string TouchTicket { get; set; }
        public string BinaryPDFdata { get; set; }
    }

    /// <summary>
    /// Coustomer Signature CRUD class
    /// </summary>
    public class CustomerSignature
    {
        DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DBclass.RouteOptimzation();

        public bool addSignatureInfo(SignatureData oSignatureData)
        {
            var retVal = (from _retVal in oDB.CustomerSignature_Add(oSignatureData.QORid, oSignatureData.TouchType, oSignatureData.SequenceNumber,
                              oSignatureData.CustomerNumber, Convert.FromBase64String(oSignatureData.SignatureImage), oSignatureData.Comments, oSignatureData.TouchTicket,
                              null)
                          select _retVal.Result).ToList().SingleOrDefault();

            if (retVal == 0)
                return true;
            return false;
        }

        public SignatureData GetSignature(Int32 Qorid)
        {
            SignatureData oSignatureData = new SignatureData();
            oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

            var retVal = oDB.CustomerSignatures.Where(t => t.QORid == Qorid).FirstOrDefault();
            if (retVal != null)
            {

                oSignatureData.QORid = (long)retVal.QORid;
                oSignatureData.TouchType = retVal.TouchType;
                oSignatureData.SequenceNumber = (long)retVal.SequenceNumber;
                oSignatureData.CustomerNumber = retVal.CustomerNumber;
                oSignatureData.SignatureImage = Convert.ToString(retVal.SignatureImage);
                oSignatureData.Comments = retVal.Comments;
                oSignatureData.TouchTicket = retVal.TouchTicket;
                oSignatureData.BinaryPDFdata = null;
            }
            //var retVal = (from _retVal in oDB.CustomerSignatures
            //              where _retVal.QORid == Qorid
            //              select new SignatureData
            //{
            //    QORid = (long)_retVal.QORid,
            //    TouchType = _retVal.TouchType,
            //    SequenceNumber = (long)_retVal.SequenceNumber,
            //    CustomerNumber = _retVal.CustomerNumber,
            //    SignatureImage = Convert.ToString(_retVal.SignatureImage),
            //    Comments = _retVal.Comments,
            //    TouchTicket = _retVal.TouchTicket,
            //    BinaryPDFdata = null
            //}).ToList().FirstOrDefault();

            return oSignatureData;
        }

        public byte[] GetSignatureByte(Int32 Qorid)
        {
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();

            oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
            var retVal = (from _retVal in oDB.CustomerSignatures
                          where _retVal.QORid == Qorid
                          select _retVal.SignatureImage).ToList().FirstOrDefault();

            return retVal == null ? null : (byte[])retVal.ToArray();
            //return (byte[])retVal.ToArray();
        }


        public string GetTouchInfo(Int32 QORid, PR.UtilityLibrary.ERP_Enums.eQTType touchType)
        {
            //PENDING 
            // Method to get Touch info for particular QORID
            #region SFERP-TODO-ESBMTD
            var requestObject = new
            {
                qorId = QORid,
                touchType = touchType
            };

            #endregion

            //PRTransiteService.PRTransiteServiceClient oPRT = new PRTransiteService.PRTransiteServiceClient();
            //return oPRT.GetTouchTicketPDF(QORid, touchType);

            return ""; // TBD - Need to get it from Salesforce.
        }
    }

}