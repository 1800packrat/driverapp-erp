﻿using DriverMobileApp.Models.DBclass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DriverMobileApp.Models
{
    public class LogEmails
    {
        public static int InsertLog(EmailLog LogDetails)
        {
            using (var oDBRoute = new RouteOptimzation())
            {
                EmailLog el = new EmailLog()
                {
                    ApplicationName = LogDetails.ApplicationName,
                    IPAddress = LogDetails.IPAddress,
                    User = LogDetails.User,
                    StartTime = LogDetails.StartTime,
                    Subject = LogDetails.Subject,
                    ToEmailIDs = LogDetails.ToEmailIDs,
                    FromEmailID = LogDetails.FromEmailID,
                    CCEmailIDs = LogDetails.CCEmailIDs,
                    MethodName = LogDetails.MethodName,
                    EmailTypeID = LogDetails.EmailTypeID,
                    QORID= LogDetails.QORID,
                    TouchType = LogDetails.TouchType,
                    SLSeqNo = LogDetails.SLSeqNo 
                };

                oDBRoute.EmailLog.AddObject(el);
                oDBRoute.SaveChanges();

                return el.PK_ID;
            }
        }

        public static void UpdateLog(EmailLog LogDetails)
        {
            using (var oDBRoute = new RouteOptimzation()) {
                EmailLog el = oDBRoute.EmailLog.Where(y => y.PK_ID == LogDetails.PK_ID).FirstOrDefault();

                if (el != null)
                {
                    el.EndTime = LogDetails.EndTime;

                    oDBRoute.SaveChanges();
                }
            }
        }
    }
}