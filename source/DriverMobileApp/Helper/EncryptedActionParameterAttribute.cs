﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Web;
using DriverMobileApp.Models;
using System.Web.Routing;

namespace DriverMobileApp.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DecryptActionParameterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
             
            if (HttpContext.Current.Request.QueryString.Get("q") != null)
            {
                EnDeCryption endecrypt = new EnDeCryption();
                string encryptedQueryString = HttpContext.Current.Request.QueryString.Get("q");
                string decrptedString = endecrypt.Decrypt(encryptedQueryString.Replace(" ", "+").ToString());

                if (string.IsNullOrEmpty(decrptedString))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "ErrorOnUrlTampar" })); 
                }
                else
                {
                    string[] paramsArrs = decrptedString.Split('?');
                    var paramInfos = ((ReflectedActionDescriptor)filterContext.ActionDescriptor).MethodInfo.GetParameters();

                    for (int i = 0; i < paramsArrs.Length; i++)
                    {
                        var paramArr = paramsArrs[i].Split('=');
                        var paramInfo = paramInfos.First(x => x.Name.ToLower() == paramArr[0].ToLower());
                        var paramType = paramInfo.ParameterType; 
                        filterContext.ActionParameters[paramArr[0]] = Convert.ChangeType(paramArr[1], paramType);
                    }
                }
            }
            
        }
    }
}