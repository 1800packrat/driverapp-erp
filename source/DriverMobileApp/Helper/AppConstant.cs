﻿namespace DriverMobileApp.Models
{
    public class PR_TouchType_Constant
    {
        private PR_TouchType_Constant() { }
        public const string DE = "DE";
        public const string RF = "RF";
        public const string DF = "DF";
        public const string RE = "RE";
        public const string RD = "RD";
        public const string CC = "CC";

        public static string GetTouchType(string TType)
        {
            switch (TType)
            {
                case DE:
                    TType = "DE-Deliver Empty";
                    break;
                case RF:
                    TType = "RF-Return Full";
                    break;
                case DF:
                    TType = "DF-Deliver Full";
                    break;
                case RE:
                    TType = "RE-Return Empty";
                    break;
                case RD:
                    TType = "RD-Return Delivery";
                    break;
                case CC:
                    TType = "CC-Curb To Curb";
                    break;
            }

            return TType;
        }
    }

    public class PR_AddressType: PR.UtilityLibrary.PREnums
    {
        private PR_AddressType() { }
        public const string Warehouse = "WH";
        public const string Customer = "Customer";
    }



    public class PR_Action_Constant
    {
        private PR_Action_Constant() { }
        public const string Pickup = "Pickup";
        public const string Dropoff = "Dropoff";
        public const string WS = "WeighStation";
    }

    public class PRTransite_TouchType_Constant
    {
        private PRTransite_TouchType_Constant() { }
        public const string EmptyPickupAtCustomer = "EmptyPickupAtCustomer";
        public const string EmptyDeliveryAtCustomer = "EmptyDeliveryAtCustomer";
        public const string FullPickupAtCustomer = "FullPickupAtCustomer";
        public const string FullDeliveryAtCustomer = "FullDeliveryAtCustomer";
        public const string WarehouseLoad = "WarehouseLoad";
        public const string WarehouseUnload = "WarehouseUnload";
        public const string ContainerOnTruckForDelivery = "ContainerOnTruckForDelivery";
        public const string ContainerLoadingOnTruck = "ContainerLoadingOnTruck";
    }

}