﻿using System.Configuration;

namespace DriverMobileApp.Helper
{
    public class Utility
    {
        public static string LOCATIONCODE = "sLocationCode";
        public static string RENTALID = "QTRentalID";
        public static string TENANTID = "TenantID";

        public static string Corpcode { get { return ConfigurationManager.AppSettings["corpcode"]; } }
        public static string username { get { return ConfigurationManager.AppSettings["username"]; } }
        public static string password { get { return ConfigurationManager.AppSettings["Password"]; } }

        public static string AppVersion { get { return ConfigurationManager.AppSettings["AppVersion"]; } }

        public static string EnDeKey { get { return ConfigurationManager.AppSettings["EnDeKey"]; } }
    }
}