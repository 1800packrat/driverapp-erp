﻿using DriverMobileApp.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DriverMobileApp.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            PRTransiteBusinessLogic oPRT = new PRTransiteBusinessLogic();
            EnDeCryption oEnDeCryption = new EnDeCryption();
            int driverId = 0;
            DateTime date;
            int.TryParse(oEnDeCryption.Decrypt(filterContext.HttpContext.Request.Cookies["UserID"].Value), out driverId);
            date = Convert.ToDateTime(filterContext.HttpContext.Request.Cookies["TouchDate"].Value);

            if (oPRT.IsRouteLocked(driverId, date) == false)
            {
                HttpContext ctx = HttpContext.Current;

                //if (ctx.Session != null && ctx.Session.IsNewSession)
                {
                    string sessionCookie = ctx.Request.Headers["Cookie"];
                    if ((null != sessionCookie) && (sessionCookie.IndexOf("ASP.NET_SessionId") >= 0))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "DashBoard", action = "ErrorOnRouteLock" }));
                       
                        return;
                    }
                }
            }
            //else
            //    this.OnActionExecuting(filterContext);
        }
    }
}