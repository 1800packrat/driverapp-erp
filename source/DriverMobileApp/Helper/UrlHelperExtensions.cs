﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using System.Text;
using DriverMobileApp.Models;

namespace DriverMobileApp.Helper
{
    public static class UrlHelperExtensions
    {
        public static string EncryptAction(this UrlHelper urlHelper, string actionName, string controllerName, object routeValues)
        {
            EnDeCryption endecrypt = new EnDeCryption();
            string queryString = string.Empty;
            string htmlAttributesString = string.Empty;
            if (routeValues != null)
            {
                RouteValueDictionary d = new RouteValueDictionary(routeValues);
                for (int i = 0; i < d.Keys.Count; i++)
                {
                    if (i > 0)
                    {
                        queryString += "?";
                    }
                    queryString += d.Keys.ElementAt(i) + "=" + d.Values.ElementAt(i);
                }
            }
            StringBuilder ancor = new StringBuilder();
            if (controllerName != string.Empty)
            {
                ancor.Append("/" + controllerName);
            }

            if (actionName != "Index")
            {
                ancor.Append("/" + actionName);
            }
            if (queryString != string.Empty)
            {
                ancor.Append("?q=" + endecrypt.Encrypt(queryString));
            }
            return ancor.ToString();
        }
    }
}