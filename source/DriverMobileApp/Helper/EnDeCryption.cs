﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace DriverMobileApp.Models
{
    public class EnDeCryption
    {
        Enterprise.Service.Cryptography oCryptography = new Enterprise.Service.Cryptography();

        public string Encrypt(string text)
        {
            return oCryptography.Encrypt(text);
        }

        public string Decrypt(string text)
        {
            return oCryptography.Decrypt(text);
        }        
    }
}