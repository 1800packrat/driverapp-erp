﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DriverMobileApp.Helper
{
    public static class RegularExpressionFilter
    {
        private static readonly string rgxAllowAlphaNumWithoutSpecialChars = @"^$|^[a-zA-Z0-9 ]+([,.\s ][a-zA-Z0-9]?[a-zA-Z0-9]*)*$";
        private static readonly string rgxAllowAlphaNumWithoutDotAndSpecialChars = @"^$|^[a-zA-Z0-9 ]+([,\s ][a-zA-Z0-9]?[a-zA-Z0-9]*)*$";
        private static readonly string rgxAllowAlphaNumeric = @"^[a-zA-Z0-9]*$";
        private static readonly string rgxAllowIntegersOnly = @"^[0-9]*$";
        private static readonly string rgxAllowDecimals = @"^$|^((\d+)((\.\d{1,2})?))$";

        //comments in change cpp, comments in skip touch, remark in touch complete
        public static bool chkRgxAllowAlphaNumWithoutSpecialChars(this string input)
        {
            return Regex.Match(input, rgxAllowAlphaNumWithoutSpecialChars, RegexOptions.IgnoreCase).Success;
        }

        //SearchTerm in exceptional Touch
        public static bool chkRgxAllowAlphaNumWithoutDotAndSpecialChars(this string input)
        {
            return Regex.Match(input, rgxAllowAlphaNumWithoutDotAndSpecialChars, RegexOptions.IgnoreCase).Success;
        }

        //containerNo in skip touch, unitNo in touch complete
        public static bool chkRgxAllowAlphaNumeric(this string input)
        {
            return Regex.Match(input, rgxAllowAlphaNumeric, RegexOptions.IgnoreCase).Success;
        }

        //lockNo, blanketNo in change cpp, mainReasonType, CNAreason, custRequest, weight in Skip Touch
        public static bool chkRgxAllowIntegersOnly(this string input)
        {
            return Regex.Match(input, rgxAllowIntegersOnly, RegexOptions.IgnoreCase).Success;
        }

        //cppLevel in change cpp
        public static bool chkRgxAllowDecimals(this string input)
        {
            return Regex.Match(input, rgxAllowDecimals, RegexOptions.IgnoreCase).Success;
        }
    }
}