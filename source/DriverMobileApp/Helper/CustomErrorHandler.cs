﻿using System;
using System.Web.Mvc;

namespace DriverMobileApp.Helper
{
    public class CustomErrorHandler : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Exception e = filterContext.Exception;
            filterContext.ExceptionHandled = true;
            var result = new ViewResult()
            {
                ViewName = "Error"
            };
            result.ViewBag.Error = "Error Occurred While Processing Your Request Please Reload Page And Try Again.";
            filterContext.Result = result;            
        }
    }    
}