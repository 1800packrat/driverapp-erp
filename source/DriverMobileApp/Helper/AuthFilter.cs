﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


namespace DriverMobileApp.Helper
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current; 
            string sessionCookie = ctx.Request.Headers["Cookie"];
            if (sessionCookie == null || (sessionCookie != null && sessionCookie.IndexOf("LoginName") <= 0))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }
}