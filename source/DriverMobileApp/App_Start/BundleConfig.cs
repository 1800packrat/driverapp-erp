﻿using System.Web.Optimization;

namespace DriverMobileApp
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/Libraries/jquery-3.1.1.min.js",
                        "~/Scripts/Libraries/jquery-ui.min.js"));


            bundles.Add(new ScriptBundle("~/bundles/mcontentjs").Include(
                        "~/Mcontent/js/pagecontrol.js",
                        "~/Mcontent/js/slider.js",
                        "~/Mcontent/js/tile-slider.js",
                        "~/Mcontent/js/dropdown.js",
                        "~/Mcontent/js/accordion.js",
                        "~/Mcontent/js/jquery.metro.js",
                        "~/Mcontent/js/pagecontrol.js",
                        "~/Mcontent/js/jquery.bpopup-0.8.0.min.js"));
         
            bundles.Add(new StyleBundle("~/Mcontent/css/modern").Include(
                       "~/Mcontent/css/modern.css",
                       "~/Mcontent/css/modern-responsive.css",
                       "~/Mcontent/css/site.css"));

            ///BELOW BUNDLE USED FOR INNERSITE LAYOUT PAGE
            bundles.Add(new ScriptBundle("~/bundles/innersite").Include(
                        "~/Scripts/innersite.js"));

            // below code is for bundling javascript of Touch Info
            bundles.Add(new ScriptBundle("~/bundles/TouchInfo").Include(
                        "~/Scripts/touchinfo.js"));


            // below code is for bundling javascript of Customer Address
            bundles.Add(new ScriptBundle("~/bundles/CustomerAddress").Include(
                        "~/Scripts/customerAddress.js"));

            // below code is for bundling javascript of skip touch
            bundles.Add(new ScriptBundle("~/bundles/SkipTouch").Include(
                        "~/Scripts/skipTouch.js"));

            // below code is for bundling javascript of change cpp
            bundles.Add(new ScriptBundle("~/bundles/ChangeCPP").Include(
                        "~/Scripts/changeCPP.js"));

            // below code is for bundling javascript of exceptional Touch Info
            bundles.Add(new ScriptBundle("~/bundles/ExcepTouchInfo").Include(
                        "~/Scripts/execpTouchInfo.js"));

            // below code is for bundling javascript of Touch Complete
            bundles.Add(new ScriptBundle("~/bundles/TouchComplete").Include(
                        "~/Scripts/touchComplete.js"));
        }
    }
}