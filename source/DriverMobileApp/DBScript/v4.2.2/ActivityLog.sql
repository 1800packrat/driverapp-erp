﻿USE [RouteOptimzation]
GO

/****** Object:  Table [dbo].[EmailType]    Script Date: 10/27/2016 7:44:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailType](
	[PK_ID] [int] NOT NULL,
	[EmailType] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](150) NULL,
 CONSTRAINT [PK_EmailType] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



USE [RouteOptimzation]
GO

/****** Object:  Table [dbo].[EmailLog]    Script Date: 10/27/2016 7:41:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[EmailLog](
	[PK_ID] [int] IDENTITY(1,1) NOT NULL,
	[ApplicationName] [nvarchar](250) NOT NULL,
	[IPAddress] [nvarchar](250) NULL,
	[User] [nvarchar](250) NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[Subject] [nvarchar](max) NULL,
	[ToEmailIDs] [nvarchar](max) NULL,
	[FromEmailID] [nvarchar](250) NULL,
	[CCEmailIDs] [nvarchar](max) NULL,
	[MethodName] [nvarchar](150) NULL,
	[EmailTypeID] [int] NULL,
	[QORID] [int] NULL,
	[TouchType] [nvarchar](50) NULL,
	[SLSeqNo] [int] NULL,
 CONSTRAINT [PK_EmailLog] PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[EmailLog]  WITH CHECK ADD  CONSTRAINT [FK_EmailLog_EmailLog] FOREIGN KEY([EmailTypeID])
REFERENCES [dbo].[EmailType] ([PK_ID])
GO

ALTER TABLE [dbo].[EmailLog] CHECK CONSTRAINT [FK_EmailLog_EmailLog]
GO




USE [RouteOptimzation]
GO
INSERT [dbo].[EmailType] ([PK_ID], [EmailType], [CreatedDate], [CreatedBy]) VALUES (1, N'TouchComplete', CAST(N'2016-10-23T00:00:00.000' AS DateTime), N'venkat')
GO
INSERT [dbo].[EmailType] ([PK_ID], [EmailType], [CreatedDate], [CreatedBy]) VALUES (2, N'SkipTouch', CAST(N'2016-10-23T00:00:00.000' AS DateTime), N'venkat')
GO
INSERT [dbo].[EmailType] ([PK_ID], [EmailType], [CreatedDate], [CreatedBy]) VALUES (3, N'ChangeCPP', CAST(N'2016-10-24T00:00:00.000' AS DateTime), N'pratap')
GO
INSERT [dbo].[EmailType] ([PK_ID], [EmailType], [CreatedDate], [CreatedBy]) VALUES (4, N'PRBusinessLogic', CAST(N'2016-10-24T00:00:00.000' AS DateTime), N'pratap')
GO
