﻿using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Service.Implementation;
using System;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SimpleInjector.Advanced;
using System.ComponentModel.Composition;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector;
using System.Reflection;
using System.Web.UI;
using PR.LocalLogisticsSolution.Model;
using System.Web;

namespace DriverMobileApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Container container;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            BundleTable.EnableOptimizations = true;
            MvcHandler.DisableMvcResponseHeader = true;

            ConfigureDependencies();
        }

        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove("Server");
            Response.Headers.Remove("X-AspNet-Version");

            Response.CacheControl = "No-cache";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.AppendCacheExtension("no-store, must-revalidate");
            Response.AppendHeader("Pragma", "no-cache");
            Response.AppendHeader("Expires", "0");
            //Response.Headers.Set("X-Frame-Options", "DENY")
            Response.Headers.Remove("X-Frame-Options");
            Response.Headers.Add("X-Frame-Options", "DENY");
        }

        public class ImportAttributePropertySelectionBehavior : IPropertySelectionBehavior
        {
            public bool SelectProperty(Type serviceType, PropertyInfo propertyInfo)
            {
                // Makes use of the System.ComponentModel.Composition assembly
                return typeof(Page).IsAssignableFrom(serviceType) &&
                propertyInfo.GetCustomAttributes(typeof(ImportAttribute), true).Any();
            }
        }

        private void ConfigureDependencies()
        {
            var container = new Container();
            //  container.Options.ConstructorResolutionBehavior = InternalConstructorResolutionBehavior(container);
            container.Options.PropertySelectionBehavior =
                new ImportAttributePropertySelectionBehavior();
            container.Register<IUserService, UserService>();
            container.Register<ITouchService, TouchService>();
            container.Register<IStagingTouchService, StagingTouchService>();
            // container.Register<IDBRepository, JaguarRepository>();
            container.Register<IDBRepository>(() => new JaguarRepository(container.GetInstance<IRouteRepository>()));
            container.Register<ISitelinkRepository, SiteLinkrepository>();
            container.Register<IRouteRepository, RouteRepository>();
            container.Register<IStarsDbRepository, StarsRepository>();

            //  RegisterWebPages(container);

            // 3. Store the container for use by Page classes.
            MvcApplication.container = container;
            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.Verify();
            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }
    }

    public class RemoveDefaultHeaderModule : IHttpModule
    {
        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += Context_PreSendRequestHeaders;
        }

        private void Context_PreSendRequestHeaders(object sender, EventArgs e)
        {
            //Remove the header you wanted
            (sender as HttpApplication).Response.Headers.Remove("Server");
            (sender as HttpApplication).Response.Headers.Remove("X-AspNet-Version");
        }
    }
}