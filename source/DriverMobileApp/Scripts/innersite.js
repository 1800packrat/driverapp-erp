﻿//Disable Bakc Button functionality Start
function DisableBackButton() {
    window.history.forward()
}

DisableBackButton();
window.onload = DisableBackButton;
window.onpageshow = function (evt) { if (evt.persisted) DisableBackButton() }
window.onunload = function () { void (0) }
//Disable Bakc Button functionality End

$(document).ready(function () {

    var defaults = {
        animationDuration: 150,
        headerOpacity: 0.5,
        fixedHeaders: false,
        headerSelector: function (item) {
            return item.children("h3").first();
        },
        itemSelector: function (item) {
            return item.children(".pivot-item");
        },
        headerItemTemplate: function () {
            return $("<span class='header' />");
        },
        pivotItemTemplate: function () {
            return $("<div class='pivotItem' />");
        },
        itemsTemplate: function () {
            return $("<div class='items' />");
        },
        headersTemplate: function () {
            return $("<div class='headers' />");
        },
        controlInitialized: undefined,
        selectedItemChanged: undefined
    };
    $("div.metro-pivot").metroPivot(defaults);

    $("#gonext").click(function () {
        $("div.metro-pivot").data("controller").goToNext();
    });

    $("#goprev").click(function () {
        $("div.metro-pivot").data("controller").goToPrevious();
    });
});

function showLoadng() {
    $('.mainblock').show();
}

function hideLoading() {
    $('.mainblock').hide();
}

$(document).ready(function () {
    var localDateTime = new Date();
    var localDate = (localDateTime.getMonth() + 1) + '/' + localDateTime.getDate() + '/' + localDateTime.getFullYear();
    var localTime = localDateTime.getHours() + ':' + localDateTime.getMinutes();

    var postData = { localDate: localDate, localTime: localTime };
    $.ajax({
        url: URLHelperGlobal.GetLocalTimeUrl,
        type: 'GET',
        data: postData,
        success: function (result) {
            // process the results from the controller action
            $("#showdate").append(result);
            $('.mainblock').hide();
        }
    });
});