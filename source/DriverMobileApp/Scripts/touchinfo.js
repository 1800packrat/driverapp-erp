﻿$(document).ready(function () {
    $("#dlgSlError").bPopup();
});

$("#btnSlErrorClose").click(function () {
    showLoadng();
    window.location = "/";
});

function WeightTicket() {
    if ($('#liWeightTicket').length) {
        if ($('#WeightTicket').is(':checked')) {
            return true;
        }
        else {
            alert("Weight Ticket Required");
            return false;
        }
    }
    else
        return true;
}

function POScheck() {
    if ($('#Visuallyinspectedthecontainer').is(':checked') || $('#Containerislockedpropertly').is(':checked') || $('#VisualInspectionStatus').is(':checked') || $('#Truckisempty').is(':checked')) {
        $.ajax({
            url: URLHelperTouchInfo.getContainerLocked,
            type: 'POST',
            //data: postData,
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                if (result == "true") {
                    $('#Visuallyinspectedthecontainer').attr('disabled', 'disabled');
                    $('#Containerislockedpropertly').attr('disabled', 'disabled');
                }
            },
            complete: function () {
                hideLoading();
            }
        });
        return true;
    } else {
        alert('Update visual inspection status');
        return false;
    }
}

$("#btnStart").click(function () {
    var form = $('#frmTouchInfo');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    var OrderInfo = {
        __RequestVerificationToken: token,
        QORid: touchInfoJSVariables.QORID,
        TouchType: touchInfoJSVariables.TouchType,
        UnitNo: touchInfoJSVariables.UnitNo,
        StopType: touchInfoJSVariables.StopType,
        intTouchId: touchInfoJSVariables.TouchID,
        intTouchStopId: touchInfoJSVariables.TouchStopId
    };

    $.ajax({
        url: URLHelperTouchInfo.getReadyToStart,
        type: 'POST',
        data: OrderInfo,
        beforeSend: function () {
            showLoadng();
        },
        success: function (result) {
            // process the results from the controller action
            var res = JSON.parse(result);

            if (res.success == true) {
                window.location = URLHelperTouchInfo.redirectToCustAddress;// "/CustAddress?TouchType=" + touchInfoJSVariables.TouchType + "&QORid=" + touchInfoJSVariables.QORID + "&StopType=" + touchInfoJSVariables.StopType + "&TouchStopId=" + touchInfoJSVariables.TouchStopId;
            } else {
                alert('Error: ' + res.msg);
            }
        },
        complete: function () {
            hideLoading();
        }
    });
});

$("#btnSkip").click(function () {
    $("#dlgSkipTouch").bPopup();
});

$("#btnConfSkipTouch").click(function () {
    showLoadng();
    window.location = URLHelperTouchInfo.redirectToConfirmSkipTouch;//"/SkipTouch?TouchType=" + touchInfoJSVariables.TouchType + "&QORid=" + touchInfoJSVariables.QORID + "&StopType=" + touchInfoJSVariables.StopType + "&TouchStopId=" + touchInfoJSVariables.TouchStopId + "&pg=" + 'ti';
});

$(".btnConfSkipTouchFromRemoved").click(function () {
    showLoadng();
    window.location = URLHelperTouchInfo.redirectToCancelTouch;//"/SkipTouch/CancelTouchForCancledOrRescheduledTouch?TouchId=" + touchInfoJSVariables.TouchID + "&QORid=" + touchInfoJSVariables.QORID + "&StopType=" + touchInfoJSVariables.StopType + "&TouchStopId=" + touchInfoJSVariables.TouchStopId + "&pg=" + 'ti';
});

if ($('#gotoNextPage')) {
    $('#gotoNextPage').click(function (e) {
        window.location = URLHelperTouchInfo.redirectToCustAddress;// "/CustAddress?TouchType=" + touchInfoJSVariables.TouchType + "&QORid=" + touchInfoJSVariables.QORID + "&StopType=" + touchInfoJSVariables.StopType + "&TouchStopId=" + touchInfoJSVariables.TouchStopId;
    });
}

$('#btnRetDashboard').click(function () {
    document.location = '@Url.Action("Index", "Dashboard")';
});

$("#viewMap").click(function () {
    $("div.metro-pivot").data("controller").goToNext();

});

$("#gotoDetails").click(function () {
    $("div.metro-pivot").data("controller").goToItemByName('Details');

});