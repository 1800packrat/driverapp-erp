﻿$("#back").click(function () {
    window.location = URLHelperChangeCPP.redirectToTouchComplete;    
});

// Submit button click ------------------------------------------------
$("#submit").click(function () {

    //var reasonType = $("#SelectedCpp").val();
    // Send Wrong Container information ---------------------------------------------------------------------------------------------
    //if (reasonType == "90001") {
    //    alert("Please select CPP level");
    //    return false;
    //}
    var form = $('#_ChangeCppForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();

    if (validateInputs()) {
        $.ajax({
            url: URLHelperChangeCPP.postChangeCPPReq,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                cppLevel: $("#SelectedCpp").val(),
                lockNo: $('#Lock').val(),
                blanketNo: $('#Blankets').val(),
                comments: $('#Comments').val(),
                qorid: changeCPPJSVariables.QORID,
                TouchStopId: changeCPPJSVariables.TouchStopId,
                TouchType: changeCPPJSVariables.TouchType,
                StopType: changeCPPJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);
               
                if (res.success == true) {                    
                    window.location = URLHelperChangeCPP.redirectToTouchComplete;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });
    }
});

$(document).ready(function () {
    hideErrorControls();
});

function hideErrorControls() {
    $('#dvErrorLock').hide();
    $('#dvErrorBlankets').hide();
    $('#dvErrorComments').hide();
}

function validateInputs() {
    hideErrorControls();

    var isValid = true;
    var regexLockBlankets = new RegExp(LocksValidationExpression);

    if (!regexLockBlankets.exec($('#Lock').val())) {
        $('#dvErrorLock').show();
        isValid = false;
    }

    if (!regexLockBlankets.exec($('#Blankets').val())) {
        $('#dvErrorBlankets').show();
        isValid = false;
    }
    
    return isValid;
}