﻿$(document).ready(function () {
    $('#dvError').hide();

    $('#btnSkipTouch').click(function () {
        document.location = URLHelperTouchComplete.redirectToSkipTouch;
    });

    $('.btnRetDashboard').click(function () {
        document.location = URLHelperTouchComplete.redirectToDashBoard;
    });

    $('.dlErrorMsgCl').click(function () {
        document.location = URLHelperTouchComplete.redirectToDashBoard;
    });

    $("#btnHideErrorMsg").click(function () {
        $("#dlErrorMsgCustMessage").bPopup().close();
        return false;
    })

    $('#btnCancel').click(function () {
        showLoadng();
        window.location = URLHelperTouchComplete.redirectToInnderDashBoard;
    });

    $("#btnPOSchange").click(function () {
        showLoadng();
        window.location = URLHelperTouchComplete.rediectToChangeCPP;
    });

    $('#btnCompleteTch').click(function (e) {
        if (touchCompleteJSVariables.StopType != "WeighStati") {
            $('#dlgEnterUnitNumber').bPopup();
        }
        else {


            //add addforgerytoken 
            var form = $('#__TouchCompleteForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();

            $.ajax({
                url: URLHelperTouchComplete.postTouchComplete,
                type: 'POST',
                data: { __RequestVerificationToken: token, unitNo: $('#UnitNo').val(), remark: $('#Remark').val() },
                beforeSend: function () {
                    showLoadng();
                },
                success: function (result) {
                    // process the results from the controller action
                    var res = JSON.parse(result);

                    if (res.success == true) {
                        $("#dlgEnterUnitNumber").bPopup().close();
                        $("#dlRetDashboard").bPopup();
                    }
                    else {
                        if (res.msg == 'InvalidUnitNumber')
                            $('#custErrorMsg').html("Invalid Unit number");
                        else
                            $('#custErrorMsg').html(res.msg);

                        $("#dlErrorMsgCustMessage").bPopup();
                    }
                },
                complete: function () {
                    hideLoading();
                }
            });

        }
    });

    $("#SubmitEnterUnitNumber").click(function () {
        if ($.trim($('#UnitNo').val()).length <= 0 || $.trim($('#ReUnitNo').val()).length <= 0) {
            alert('Please enter UnitNo');
            return false;
        }
        //Validate to check ReUnit and Unit numbers are same or not
        if ($.trim($('#UnitNo').val()) != $.trim($('#ReUnitNo').val())) {
            alert("Please enter Re Enter Unit No same as Unit No.");
            return false;
        }

        if (validateContainerNo()) {
            //add addforgerytoken 
            var form = $('#__TouchCompleteUnitNumberForm');
            var token = $('input[name="__RequestVerificationToken"]', form).val();

            $.ajax({
                url: URLHelperTouchComplete.postTouchComplete,
                type: 'POST',
                data: { __RequestVerificationToken: token, unitNo: $('#UnitNo').val(), remark: $('#Remark').val() },
                beforeSend: function () {
                    showLoadng();
                },
                success: function (result) {
                    // process the results from the controller action
                    var res = JSON.parse(result);

                    if (res.success == true) {
                        $("#dlgEnterUnitNumber").bPopup().close();
                        $("#dlRetDashboard").bPopup();
                    }
                    else {
                        if (res.msg == 'InvalidUnitNumber')
                            $('#custErrorMsg').html("Invalid Unit number");
                        else
                            $('#custErrorMsg').html(res.msg);

                        $("#dlErrorMsgCustMessage").bPopup();
                    }
                },
                complete: function () {
                    hideLoading();
                }
            });
        }
        else {
            $("#dlgEnterUnitNumber").bPopup().close();
        }
    });

   
    function validateContainerNo() {

        var isValid = true;
        var regexContainerNo = new RegExp(ValidationExpression);

        console.log($('#UnitNo').val());

        if (!regexContainerNo.exec($('#UnitNo').val())) {
            alert("Please enter valid Unit No.");
            return false;
        }

        return isValid;
    }
});