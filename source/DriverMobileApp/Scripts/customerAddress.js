﻿$(document).ready(function () {
    $("#reached-btn").click(function () {
        $.ajax({
            url: URLHelperCustAddr.postReachedDestination,
            type: 'POST',
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                if (result == "true") {
                    window.location = URLHelperCustAddr.redirectToInnderDashBoard; //"/InnerDashBoard?TouchType=" + custAddrJSVariables.TouchType + "&QORid=" + custAddrJSVariables.QORID + "&StopType=" + custAddrJSVariables.StopType + "&TouchStopId=" + custAddrJSVariables.TouchStopId;
                }
            },
            complete: function () {
                hideLoading();
            }
        });
    });

    $("#Next").click(function () {
        window.location = URLHelperCustAddr.redirectToInnderDashBoard; // "/InnerDashBoard?TouchType=" + custAddrJSVariables.TouchType + "&QORid=" + custAddrJSVariables.QORID + "&StopType=" + custAddrJSVariables.StopType + "&TouchStopId=" + custAddrJSVariables.TouchStopId;
    });

    $("#Wreached-btn").click(function () {
        var form = $('#frmCustAddress');
        var token = $('input[name="__RequestVerificationToken"]', form).val();
        var OrderInfo = {
            __RequestVerificationToken: token,
            OrderNo: custAddrJSVariables.OrderNum,
            QORid: custAddrJSVariables.QORID,
            intTouchId: custAddrJSVariables.TouchID,
            intTouchStopId: custAddrJSVariables.TouchStopId
        };

        $.ajax({
            url: URLHelperCustAddr.postWareHouseTouchComplete,
            type: 'POST',
            data: OrderInfo,
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    $("#dlRetDashboard").bPopup();
                } else {
                    alert('Error: ' + res.msg);
                }
            },
            complete: function () {
                hideLoading();
            }
        });
    });
});

$('#btnRetDashboard').click(function () {
    document.location = URLHelperCustAddr.redirectToDashBoard;
});

//we are not using this method, in future if need then we need to change window.location string 
if ($('#gotoNextPage')) {
    $('#gotoNextPage').click(function (e) {
        navigator.geolocation.getCurrentPosition(function (position) {
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            var postData = { latitude: latitude, longtitude: longitude };
            var toaddress = $.trim($('#ToAddressVal').text().replace(/\s/g, ""));

            window.location = "/Map?latitude=" + latitude + "&longitude=" + longitude + "&toaddress=" + toaddress;
        });
    });
}