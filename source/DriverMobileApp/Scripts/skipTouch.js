﻿$(document).ready(function () {
    HideAll();
});
// Hide all option except Reason Dropdown -------------------
function HideAll() {
    $("#liComments").hide();
    $("#OptWcontainer").hide();
    $("#CNotAccessible").hide();
    $("#submit").hide();
    $("#cWasBlocked").hide();
    $("#uToAppContainer").hide();
    $("#CustNotReady").hide();
    $("#COverWeight").hide();
    $("#CantReachCust").hide();
    $("#Other").hide();

    $('#dvErrorComments').hide();
    $('#dvErrorcontainerNo').hide();
}
// End Hide all option except Reason Dropdown -------------------

$("#back").click(function () {
    if (skipTouchJSVariables.Pg == 'id') {
        window.location = URLHelperSkipTouch.redirectToInnderDashBoard //"/InnerDashBoard?QORid=" + skipTouchJSVariables.QORID + "&StopType=" + skipTouchJSVariables.StopType + "&TouchType=" + skipTouchJSVariables.TouchType + "&TouchStopId=" + skipTouchJSVariables.TouchStopId;
    } else if (skipTouchJSVariables.Pg == 'exceptiontouch') {
        window.location = URLHelperSkipTouch.redirectToExceptionTouch; //"/ExceptionTouch/TouchInfo?QORid=" + skipTouchJSVariables.QORID + "&TouchType=" + skipTouchJSVariables.TouchType;
    }
    else {
        window.location = URLHelperSkipTouch.redirectToTouchInfo; // "/TouchInfo?QORid=" + skipTouchJSVariables.QORID + "&StopType=" + skipTouchJSVariables.StopType + "&TouchType=" + skipTouchJSVariables.TouchType + "&TouchStopId=" + skipTouchJSVariables.TouchStopId;
    }
});

$("#byCustRequest").click(function () {
    if ($("#byCustRequest").is(":checked")) {
        $("#spnCustRequest").css("display", "block");
    }
    else {
        $("#spnCustRequest").css("display", "none");
    }
});

// Reason Dropdown -------------------------------
$("#mainReasonType").change(function () {
    var reasonType = $(this).val();
    switch (reasonType) {
        case '0':
            HideAll();
            $("#OptMainReason").show();
            break;
        case '1':
            HideAll();
            $("#liComments").show();
            $("#OptWcontainer").show();
            $("#submit").show();
            break;
        case '2':
            HideAll();
            $("#liComments").show();
            $("#CNotAccessible").show();
            $("#cWasBlocked").show();
            $("#submit").show();
            break;
        case '3':
            HideAll();
            $("#liComments").show();
            $("#CustNotReady").show();
            $("#submit").show();
            break;

        case '4':
            HideAll();
            $("#liComments").show();
            $("#COverWeight").show();
            $("#submit").show();
            break;
        case '5':
            HideAll();
            $("#liComments").show();
            $("#CantReachCust").show();
            $("#submit").show();
            break;
        case '6':
            HideAll();
            $("#liComments").show();
            $("#Other").show();
            $("#submit").show();
            break;
    }
});
// End Reason Dropdown -------------------------------

// Container Not Accessible -----------------------------
$("#cNotAccessible").change(function () {
    var reasonType = $(this).val();
    switch (reasonType) {
        case '1':
            $("#cWasBlocked").show();
            $("#uToAppContainer").hide();
            break;
        case '2':
            $("#uToAppContainer").show();
            $("#cWasBlocked").hide();
            break;
    }
});
// End Container Not Accessible -----------------------------

// Submit button click ------------------------------------------------
$("#submit").click(function () {
    var reasonType = $("#mainReasonType").val();
    //add addforgerytoken 
    var form = $('#__SkipForm');
    var token = $('input[name="__RequestVerificationToken"]', form).val();
    // Send Wrong Container information ---------------------------------------------------------------------------------------------
    if (reasonType == "1") {
        if ($("#wContainer_true").is(":checked"))
            if (($.trim($('#containerNo').val()) != $.trim($('#ReEnterContainerNo').val())) || ($.trim($('#containerNo').val()) == "")) {
                alert("Please enter Re-enter Container No same as Container No.");
                return false;
            }
        if (validateContainerNo()) {
            $.ajax({
                url: URLHelperSkipTouch.getWrongContainer,
                type: 'POST',
                data: {
                    __RequestVerificationToken: token,
                    mainReasonType: $("#mainReasonType").val(),
                    wContainer: ($('#wContainer_true').is(':checked') ? 'true' : 'false'),
                    containerNo: $('#containerNo').val(),
                    comments: $('#comments').val(),
                    qorid: skipTouchJSVariables.QORID,
                    TouchStopId: skipTouchJSVariables.TouchStopId,
                    TouchType: skipTouchJSVariables.TouchType,
                    StopType: skipTouchJSVariables.StopType
                },
                beforeSend: function () {
                    showLoadng();
                },
                success: function (result) {
                    // process the results from the controller action
                    var res = JSON.parse(result);

                    if (res.success == true) {
                        showLoadng();
                        document.location = URLHelperSkipTouch.redirectToDashBoard;
                    }
                    else
                        alert('Error: ' + res.msg);
                },
                complete: function () {
                    hideLoading();
                }
            });
        }
    }
    // End Send Wrong Container information ---------------------------------------------------------------------------------------------

    // Container Not Accessible ---------------------------------------------------------------------------------------------------------
    if (reasonType == "2") {
        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }


        $.ajax({
            url: URLHelperSkipTouch.getContainerNotAccess,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                mainReasonType: $('#mainReasonType').val(),
                CNAreason: $('#cNotAccessible').val(),
                byVehicle: ($('#byVehicle').is(':checked') ? 'true' : 'false'),
                rain: ($('#rain').is(':checked') ? 'true' : 'false'),
                mud: ($('#mud').is(':checked') ? 'true' : 'false'),
                snow: ($('#snow').is(':checked') ? 'true' : 'false'),
                other1: ($('#other1').is(':checked') ? 'true' : 'false'),
                other2: ($('#other2').is(':checked') ? 'true' : 'false'),
                comments: $('#comments').val(),
                qorid: skipTouchJSVariables.QORID,
                TouchStopId: skipTouchJSVariables.TouchStopId,
                TouchType: skipTouchJSVariables.TouchType,
                StopType: skipTouchJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    showLoadng();
                    document.location = URLHelperSkipTouch.redirectToDashBoard;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });
    }
    // End Container Not Accessible ---------------------------------------------------------------------------------------------------------

    // Customer not ready ---------------------------------------------------------------------------------------------------------
    if (reasonType == "3") {

        if ($("#custRequest").val() == 0 && $("#byCustRequest").is(":checked")) {
            alert("Select request option ");
            return false;
        }

        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }


        $.ajax({
            url: URLHelperSkipTouch.getCustNotReady,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                mainReasonType: $('#mainReasonType').val(),
                byCustRequest: ($('#byCustRequest').is(':checked') ? 'true' : 'false'),
                containerLocked: ($('#containerLocked').is(':checked') ? 'true' : 'false'),
                custRequest: ($('#byCustRequest').is(':checked') ? $("#custRequest").val() : ''),
                comments: $('#comments').val(),
                qorid: skipTouchJSVariables.QORID,
                TouchStopId: skipTouchJSVariables.TouchStopId,
                TouchType: skipTouchJSVariables.TouchType,
                StopType: skipTouchJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    showLoadng();
                    document.location = URLHelperSkipTouch.redirectToDashBoard;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });
    }
    // End Customer not ready ---------------------------------------------------------------------------------------------------------

    // Container over weight  ---------------------------------------------------------------------------------------------------------
    if (reasonType == "4") {
        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }


        $.ajax({
            url: URLHelperSkipTouch.getContainerOverWeight,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                mainReasonType: $('#mainReasonType').val(),
                weight: $('#cOverWeight').val(),
                comments: $('#comments').val(),
                qorid: skipTouchJSVariables.QORID,
                TouchStopId: skipTouchJSVariables.TouchStopId,
                TouchType: skipTouchJSVariables.TouchType,
                StopType: skipTouchJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    showLoadng();
                    document.location = URLHelperSkipTouch.redirectToDashBoard;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });

    }
    // End Container over weight  ---------------------------------------------------------------------------------------------------------

    // Could not reach Customer  ---------------------------------------------------------------------------------------------------------
    if (reasonType == "5") {
        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }


        $.ajax({
            url: URLHelperSkipTouch.getCouldNotReachCustomer,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                mainReasonType: $('#mainReasonType').val(),
                comments: $('#comments').val(),
                qorid: skipTouchJSVariables.QORID,
                TouchStopId: skipTouchJSVariables.TouchStopId,
                TouchType: skipTouchJSVariables.TouchType,
                StopType: skipTouchJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    showLoadng();
                    document.location = URLHelperSkipTouch.redirectToDashBoard;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });

    }
    // End Could not reach Customer  ---------------------------------------------------------------------------------------------------------

    // Other Reason  ---------------------------------------------------------------------------------------------------------
    if (reasonType == "6") {
        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }

        if ($.trim($('#comments').val()) == "") {
            alert("Please enter Comments");
            return false;
        }


        $.ajax({
            url: URLHelperSkipTouch.getOtherReason,
            type: 'POST',
            data: {
                __RequestVerificationToken: token,
                mainReasonType: $('#mainReasonType').val(),
                comments: $('#comments').val(),
                qorid: skipTouchJSVariables.QORID,
                TouchStopId: skipTouchJSVariables.TouchStopId,
                TouchType: skipTouchJSVariables.TouchType,
                StopType: skipTouchJSVariables.StopType
            },
            beforeSend: function () {
                showLoadng();
            },
            success: function (result) {
                // process the results from the controller action
                var res = JSON.parse(result);

                if (res.success == true) {
                    showLoadng();
                    document.location = URLHelperSkipTouch.redirectToDashBoard;
                }
                else
                    alert('Error: ' + res.msg);
            },
            complete: function () {
                hideLoading();
            }
        });
    }
    // End Other Reason  ---------------------------------------------------------------------------------------------------------
});

// End Submit button click --------------------------------------------

function validateContainerNo() {
    $('#dvErrorcontainerNo').hide();

    var isValid = true;
    var regexContainerNo = new RegExp(ValidationExpression);

    if (!regexContainerNo.exec($('#containerNo').val())) {
        $('#dvErrorcontainerNo').show();
        return false;
    }

    return isValid;
}
