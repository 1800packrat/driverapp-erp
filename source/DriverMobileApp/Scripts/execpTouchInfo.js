﻿$(document).ready(function () {
    $("#dlgSlError").bPopup();
});

$("#btnSlErrorClose").click(function () {
    showLoadng();
    window.location = "/";
});

$("#btnSkipThisTouch").click(function () {
    $("#dlErrorMsgCustMessage").bPopup().close();
    showLoadng();
    document.location = URLHelperExecpTouchInfo.skipCurrentWareHouseTouch;
    return false;
});

$('.btnRetDashboard').click(function () {
    document.location = URLHelperExecpTouchInfo.redirectToDashBoard;
});

$("#btnConfStartTouch").click(function () {
    var form = $('#frmExceptionTouch');
    var token = $('input[name="__RequestVerificationToken"]', form).val();


    var OrderInfo = {
        __RequestVerificationToken: token,
        TouchId: execpTouchInfoJSVariables.TouchID,
        QorId: execpTouchInfoJSVariables.QORID,
        TouchType: execpTouchInfoJSVariables.TouchType
    };

    $.ajax({
        url: URLHelperExecpTouchInfo.postOptimizeTouch,
        type: 'POST',
        data: OrderInfo,
        beforeSend: function () {
            showLoadng();
        },
        success: function (result) {
            $("#dlgStartTouch").bPopup().close();

            if (result.success == true) {
                if (result.canHeProceedWithExceptionTouch == true) {
                    showLoadng();
                    console.log(result.RedirectURL);
                    // process the results from the controller action
                    window.location = result.RedirectURL;
                        //"/TouchInfo?TouchType=" + result.TouchType + "&QORid=" + execpTouchInfoJSVariables.QORID + "&StopType=" + result.StopType;
                } else {
                    $("#dlErrorMsgCustMessage").bPopup();
                }
            }
            else {
                alert(result.errorMsg);
            }
        },
        complete: function () {
            hideLoading();
        }
    });
});

$("#btnStart").click(function () {
    $("#dlgStartTouch").bPopup();
});

$('#btnRetDashboard').click(function () {
    document.location = URLHelperExecpTouchInfo.redirectToDashBoard;
});

$("#viewMap").click(function () {
    $("div.metro-pivot").data("controller").goToNext();

});

$("#gotoDetails").click(function () {
    $("div.metro-pivot").data("controller").goToItemByName('Details');

});