﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DriverMobileApp.Models;
using PR.Entities;
using AuditLog.DBStructure;
using System.Web.Script.Serialization;

namespace DriverMobileApp.Controllers
{
    public class CustAddressController : BaseAuthenticatedController
    {
        //
        // GET: /CustAddress/
        EnDeCryption oEnDeCryption = new EnDeCryption();

        public ActionResult Index(string TouchType, Int32 QORid, string StopType)
        {

            CurrentTouchType = TouchType.ToString().Trim();
            //had doubt with the following line
            Response.Cookies["currentTouchType"].Expires = DateTime.Now.AddDays(1); // Validity of touch type is one day
                        
            QORID = QORid.ToString().Trim();
            Response.Cookies["QORid"].Expires = DateTime.Now.AddDays(1); // Validity of touch type is one day

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();

            DateTime TouchOnDate = Convert.ToDateTime(TouchDate);

            DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(UserID), TouchOnDate, TouchType.ToString().Trim(), Convert.ToString(QORid), StopType.Trim());
            DriverTouchLocal oDriverTouchLocal = new DriverTouchLocal();
            if (oDriverTouch != null)
            {
                oDriverTouchLocal.AddressLine1 = oDriverTouch.AddressLine1;
                oDriverTouchLocal.AddressLine2 = oDriverTouch.AddressLine2;
                oDriverTouchLocal.BalanceDue = oDriverTouch.BalanceDue;
                oDriverTouchLocal.City = oDriverTouch.City;
                oDriverTouchLocal.ContainerNumber = oDriverTouch.ContainerNumber;
                oDriverTouchLocal.ContainerSize = oDriverTouch.ContainerSize;
                oDriverTouchLocal.CustomerName = oDriverTouch.CustomerName;
                oDriverTouchLocal.DestinationAddress = (oDriverTouch.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? oDriverTouch.DestinationAddress : oDriverTouch.WeightStationAddress);
                oDriverTouchLocal.IsCombinedTouch = oDriverTouch.IsCombinedTouch;
                oDriverTouchLocal.IsTrailerNeeded = oDriverTouch.IsTrailerNeeded;
                oDriverTouchLocal.OrderNum = oDriverTouch.OrderNum;
                oDriverTouchLocal.OriginAddress = oDriverTouch.OriginAddress;
                oDriverTouchLocal.PhoneNumber = oDriverTouch.PhoneNumber;
                oDriverTouchLocal.QORID = oDriverTouch.QORID;
                oDriverTouchLocal.ScheduledEndTime = oDriverTouch.ScheduledEndTime;
                oDriverTouchLocal.ScheduledStartTime = oDriverTouch.ScheduledStartTime;
                oDriverTouchLocal.EstimatedStartTime = oDriverTouch.EstimatedStartTime;
                oDriverTouchLocal.EstimatedEndTime = oDriverTouch.EstimatedEndTime;
                oDriverTouchLocal.SequenceNumber = oDriverTouch.SequenceNumber;
                oDriverTouchLocal.State = oDriverTouch.State;
                oDriverTouchLocal.Status = oDriverTouch.Status;
                oDriverTouchLocal.StopId = oDriverTouch.StopId;
                oDriverTouchLocal.StopType = oDriverTouch.StopType;
                oDriverTouchLocal.TouchType = oDriverTouch.TouchType;
                oDriverTouchLocal.TransiteCustRef = oDriverTouch.TransiteCustRef;
                oDriverTouchLocal.TransiteTouchType = oDriverTouch.TransiteTouchType;
                oDriverTouchLocal.TouchId = oDriverTouch.TouchId;
                oDriverTouchLocal.TouchStopId = oDriverTouch.TouchStopId;
                oDriverTouchLocal.Zip = oDriverTouch.Zip;
                oDriverTouchLocal.FacilityName = oDriverTouch.FacilityName;
                oDriverTouchLocal.IsZippyShellMove = oDriverTouch.IsZippyShellMove;
            }

            DriverTouchInfo model = new DriverTouchInfo();
            if (oDriverTouchLocal.DestinationAddress.AddressType != PR.UtilityLibrary.PREnums.AddressType.Warehouse)
            {
                model.POSData = oPRbusinesslogic.GetPOSitem(QORid);
                List<TouchData> lstModel = oPRbusinesslogic.GetTouchInfo(TouchType.ToString().Trim(), QORid);
                model.TouchData = (lstModel.Count > 0 && lstModel[0] != null) ? lstModel[0] : new TouchData();
                model.SiteLinkCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QORid);
            }
            else
            {
                model.POSData = null;
                model.TouchData = null;
                model.SiteLinkCustomerInfo = null;
            }
            model.DriverTouch = oDriverTouchLocal;

            ViewBag.TouchType = CurrentTouchType;

            string _LogID = LogID;

            //Update Touch info log table
            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
            TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
            if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                oTouchInfoLog = new TouchInfoLog();
            else
                oTouchInfoLog.LogID = _LogID;

            ViewBag.TouchInfoLog = oTouchInfoLog;
            ViewBag.TouchType = CurrentTouchType;

            return View(model);
            //}
        }

        public ActionResult ReachedDestination()
        {
            try
            {
                string _LogID = LogID;

                //Update Touch info log table
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                    oTouchInfoLog = new TouchInfoLog();
                else
                    oTouchInfoLog.LogID = _LogID;

                oTouchInfoLog.ReachedDestination = true;

                oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

                return Content("true");
            }
            catch
            {
                return Content("false");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WarehouseTouchComplete(string OrderNo, Int32 QORid, Int32 intTouchId, Int32 intTouchStopId)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                string _LogID = LogID;

                //Update Touch info log table
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                    oTouchInfoLog = new TouchInfoLog();
                else
                    oTouchInfoLog.LogID = _LogID;

                oTouchInfoLog.ReachedDestination = true;

                oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

                //---------------------- Complete touch ------------------------------------------
                AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster();
                oAuditLogMaster = oAuditLog.RetLogMaster(LogID);


                AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog(); // Ctearing instance of Audit log

                //AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster(); // Creation instant for AuditLogMaster for adding Start Date And Time
                oAuditLogMaster.LogID = LogID; //Passing existing LogID
                oAuditLogMaster.ScheduledEndTime = System.DateTime.Now.ToString(); // Adding current date and Time
                _LogID = oActivityLog.AddActivityMaster(oAuditLogMaster);


                ///Store Login info in AuditLog DB
                ///populating values in Auditlog class

                AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();

                //string _LogID = Request.Cookies["LogID"].Value;
                oAuditLogDetail.LogID = LogID;
                oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                oAuditLogDetail.Description = LoginName + " has confirmed end time at " + System.DateTime.Now.ToString();
                oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.CompleteTouch.ToString();

                ///Storing log values in Log DB
                oActivityLog.AddActivityDetail(oAuditLogDetail);

                PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oPRTransiteBusinessLogic.CompleteStop(oAuditLogMaster, TouchStatus.Completed.ToString());

                //This is added just for timebeeing, we need to remove it later. once they dont want to see touch complete emails for all touches then we need to remove it
                using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
                {
                    //bool isTouchCompleted = oDB.TouchInformations.Any(t => t.PK_TouchId = intTouchId && t.Status == "Completed");
                    var stopinfo = oDB.StopInformations.FirstOrDefault(s => s.Pk_Touch_StopId == intTouchStopId);
                    if (stopinfo != null && stopinfo.TouchInformation.Status == TouchStatus.Completed.ToString())
                    {
                        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                        bool isThisTruckFromDiferentFacilityForDE = false;
                        string unitFacilityName = string.Empty;

                        if (oAuditLogMaster.TransiteTouchType == "DE")
                        {
                            int touchStopID = 0;
                            int.TryParse(oAuditLogMaster.RefID, out touchStopID);
                            DriverTouch currentTouchPickupStop = new DriverTouch();
                            if (touchStopID > 0)
                            {
                                currentTouchPickupStop = oPRTransiteBusinessLogic.RetieveCurrentTouchPickupStopByQORId(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate), oAuditLogMaster.Qorid);

                                oPRbusinesslogic.GetUnitByUnitName(Convert.ToInt32(oAuditLogMaster.Qorid), stopinfo.TouchInformation.ContainerNo, out isThisTruckFromDiferentFacilityForDE, currentTouchPickupStop.OriginAddress.SLLocCode);

                                if (isThisTruckFromDiferentFacilityForDE && !string.IsNullOrEmpty(currentTouchPickupStop.OriginAddress.SLLocCode))
                                {
                                    unitFacilityName = oPRbusinesslogic.GetFacilityNameByLocCode(currentTouchPickupStop.OriginAddress.SLLocCode.ToString());
                                }
                            }
                        }

                        //As per Kelly request: For now we are sending this email to all touches but not required for all touches, this is required only for DE & RE
                        string strEmailMsg = (new TouchCompleteController()).TouchComplete_SendEmail(QORid, oAuditLogMaster, stopinfo.TouchInformation.ContainerNo, oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate), CurrentTouchType, Session["localDateTime"].ToString(), LoginName, isThisTruckFromDiferentFacilityForDE, unitFacilityName, stopinfo.TouchInformation.Remarks);

                        if (strEmailMsg != "Success")
                        {
                            flag = false;
                            msg = strEmailMsg;
                        }
                    }
                }
                //---------------------- End Complete touch --------------------------------------
            }
            catch (Exception ex)
            {
                flag = false;
                msg = ex.Message;
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));
        }
    }
}
