﻿using System.Web.Mvc;

namespace DriverMobileApp.Controllers
{
    public class InnerDashBoardController : BaseAuthenticatedController
    {
        //
        // GET: /InnerDashBoard/

        public ActionResult Index(string QORid, string StopType, string TouchType, string TouchStopId)
        {
            ViewBag.QORid = QORid;
            ViewBag.StopType = StopType;
            ViewBag.TouchType = TouchType;
            ViewBag.TouchStopId = TouchStopId;

            return View();
        }
    }
}
