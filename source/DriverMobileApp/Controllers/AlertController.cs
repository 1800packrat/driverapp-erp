﻿using System;
using System.Web.Mvc;
using DriverMobileApp.Models;
using DriverMobileApp.Models.Email;
using System.Configuration;
using System.Text;

namespace DriverMobileApp.Controllers
{
    //SEFRP-TODO-RMVNICD -- Not is use code. remove from the app.
    //public class AlertController : BaseAuthenticatedController
    //{ 
    //    public ActionResult Index()
    //    {
    //        AlertInfo model = new AlertInfo();
    //        PRAlert oPRA = new PRAlert();
    //        model.MessageType = oPRA.GetMessageTypeS();
    //        model.MessagePriority = oPRA.GetMessagePriorityS();
    //        model.MessageSourceType = oPRA.GetMessageSourceTypeS();
    //        model.MessageStatus = oPRA.GetMessageStatusS();

    //        return View(model);
    //    }

    //    public ActionResult SaveAlertInfo(MessageTransaction oMessageTransaction)
    //    {
    //        bool retVal = true;
    //        try
    //        {
    //            PRAlert oAlertInfo = new PRAlert();

    //            oMessageTransaction.PriorityID = Convert.ToInt32(Request.Form["MessagePriority"]);
    //            oMessageTransaction.TypeID = 1; // Convert.ToInt32(Request.Form["MessageType"]);  // making Messagetype disabled
    //            oMessageTransaction.TransiteDriverID = Convert.ToInt32((new EnDeCryption()).Decrypt(UserID));
    //            //oMessageTransaction.STARSId = 1;//TODO
    //            //oMessageTransaction.TouchType = 1;//TODO 
    //            oMessageTransaction.SourceTypeID = oAlertInfo.GetMessageSourceTypeID();
    //            //oMessageTransaction.Source = Convert.ToString((new EnDeCryption()).Decrypt(Request.Cookies["UserID"].Value));

    //            try
    //            {
    //                oMessageTransaction.Source = Convert.ToString((new EnDeCryption()).Decrypt(UserID)) + "|" + LoginName + "|Lat:" + Session["latitude"].ToString() + "|Lon" + Session["longtitude"].ToString();
    //            }
    //            catch (Exception ex)
    //            {
    //                (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
    //                oMessageTransaction.Source = Convert.ToString((new EnDeCryption()).Decrypt(UserID)) + "|" + LoginName;
    //            }
    //            oMessageTransaction.ApplicationID = oAlertInfo.GetMessageApplicationID();
    //            oMessageTransaction.StatusID = oAlertInfo.GetMessageStatusID();
    //            oMessageTransaction.DateCreated = DateTime.Now;
    //            oMessageTransaction.CreateUserID = Convert.ToString((new EnDeCryption()).Decrypt(UserID));

    //            int qorid = -1;
    //            if (QORID != null && !string.IsNullOrEmpty(QORID) && int.TryParse(QORID, out qorid) && qorid > 0)
    //                oMessageTransaction.QORId = qorid;

    //            oAlertInfo.SaveMessageTransaction(oMessageTransaction);

    //            if ((oMessageTransaction.TypeID == 1 || oMessageTransaction.TypeID == 2 || oMessageTransaction.TypeID == 4) && (oMessageTransaction.PriorityID == 1 || oMessageTransaction.PriorityID == 4))
    //                retVal = EmailAlert(oMessageTransaction);

    //        }
    //        catch (Exception ex)
    //        {
    //            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
    //            oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

    //            retVal = false;
    //        }

    //        return Content(retVal.ToString());
    //    }

    //    private bool EmailAlert(MessageTransaction oMessageTransaction)
    //    {

    //        AppEmail oAppEmail = new AppEmail();
    //        oAppEmail.EmailTo = ConfigurationManager.AppSettings["LocalLogistics"].ToString();
    //        oAppEmail.EmailFrom = ConfigurationManager.AppSettings["DriverEmail"].ToString();

    //        oAppEmail.Subject = "Alert message from " + LoginName + "-" + Convert.ToString((new EnDeCryption()).Decrypt(UserID));

    //        oAppEmail.EmailCc = "";

    //        StringBuilder emailBody = new StringBuilder();

    //        emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
    //        emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
    //        emailBody.Append("<head><title>Driver - Alert</title></head>");
    //        emailBody.Append("<body>");

    //        emailBody.Append("<br />Following alert sent by " + LoginName + "-" + Convert.ToString((new EnDeCryption()).Decrypt(UserID)) + "<br /><br /><br />");
    //        emailBody.Append(oMessageTransaction.Message + "<br /><br />");

    //        try
    //        {
    //            emailBody.Append(" Lat:" + Session["latitude"].ToString() + " |Lon:" + Session["longtitude"].ToString() + "<br />");
    //        }
    //        catch (Exception ex)
    //        {
    //            (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
    //        }
    //        int qorid = -1;
    //        if (QORID != null && !string.IsNullOrEmpty(QORID) && int.TryParse(QORID, out qorid) && qorid > 0)
    //            emailBody.Append(" QORID : " + qorid + "<br />");
    //        else
    //            emailBody.Append(" QORID : N/A" + "<br />");
    //        emailBody.Append(" Date & Time : " + DateTime.Now + "<br />");
    //        emailBody.Append("<br /><br />");
    //        emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

    //        oAppEmail.EmailBody = emailBody.ToString();
    //        oAppEmail.AttachedFileName = String.Empty;
    //        oAppEmail.ReportFormat = String.Empty;
    //        if (oAppEmail.SendEMail() == "Success")

    //            return true;
    //        else
    //            return false;
    //    }
    //}
}
