﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DriverMobileApp.Models;
using System.Configuration;
using PR.Entities;
using AuditLog.DBStructure;
using System.Web.Script.Serialization;

namespace DriverMobileApp.Controllers
{
    public class TouchInfoController : BaseAuthenticatedController
    {
        EnDeCryption oEnDeCryption = new EnDeCryption();

        public ActionResult Index(string TouchType, int QORid, string StopType)
        {
            ViewBag.SlError = null;
            
            CurrentTouchType = TouchType.ToString().Trim();
            Response.Cookies["currentTouchType"].Expires = DateTime.Now.AddDays(1); // Validity of touch type is one day
            
            QORID = QORid.ToString().Trim();
            Response.Cookies["QORid"].Expires = DateTime.Now.AddDays(1); // Validity of touch type is one day

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();

            DateTime TouchOnDate = Convert.ToDateTime(TouchDate);

            DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(UserID), TouchOnDate, TouchType.ToString().Trim(), Convert.ToString(QORid), StopType.Trim());
            DriverTouchLocal oDriverTouchLocal = new DriverTouchLocal();
            if (oDriverTouch != null)
            {
                oDriverTouchLocal.AddressLine1 = oDriverTouch.AddressLine1;
                oDriverTouchLocal.AddressLine2 = oDriverTouch.AddressLine2;
                oDriverTouchLocal.BalanceDue = oDriverTouch.BalanceDue;
                oDriverTouchLocal.City = oDriverTouch.City;
                oDriverTouchLocal.ContainerNumber = oDriverTouch.ContainerNumber;
                oDriverTouchLocal.ContainerSize = oDriverTouch.ContainerSize;
                oDriverTouchLocal.CustomerName = oDriverTouch.CustomerName;
                oDriverTouchLocal.DestinationAddress = (oDriverTouch.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? oDriverTouch.DestinationAddress : oDriverTouch.WeightStationAddress);
                oDriverTouchLocal.IsCombinedTouch = oDriverTouch.IsCombinedTouch;
                oDriverTouchLocal.IsTrailerNeeded = oDriverTouch.IsTrailerNeeded;
                oDriverTouchLocal.OrderNum = oDriverTouch.OrderNum;
                oDriverTouchLocal.OriginAddress = oDriverTouch.OriginAddress;
                oDriverTouchLocal.PhoneNumber = oDriverTouch.PhoneNumber;
                oDriverTouchLocal.QORID = oDriverTouch.QORID;
                oDriverTouchLocal.ScheduledEndTime = oDriverTouch.ScheduledEndTime;
                oDriverTouchLocal.ScheduledStartTime = oDriverTouch.ScheduledStartTime;
                oDriverTouchLocal.EstimatedStartTime = oDriverTouch.EstimatedStartTime;
                oDriverTouchLocal.EstimatedEndTime = oDriverTouch.EstimatedEndTime;
                oDriverTouchLocal.SequenceNumber = oDriverTouch.SequenceNumber;
                oDriverTouchLocal.State = oDriverTouch.State;
                oDriverTouchLocal.Status = oDriverTouch.Status;
                oDriverTouchLocal.StopId = oDriverTouch.StopId;
                oDriverTouchLocal.StopType = oDriverTouch.StopType;
                oDriverTouchLocal.TouchType = oDriverTouch.TouchType;
                oDriverTouchLocal.TransiteCustRef = oDriverTouch.TransiteCustRef;
                oDriverTouchLocal.TransiteTouchType = oDriverTouch.TransiteTouchType;
                oDriverTouchLocal.TouchId = oDriverTouch.TouchId;
                oDriverTouchLocal.TouchStopId = oDriverTouch.TouchStopId;
                oDriverTouchLocal.FacilityName = oDriverTouch.FacilityName;
                oDriverTouchLocal.IsZippyShellMove = oDriverTouch.IsZippyShellMove;
               

                decimal starsid = 0;
                if (oDriverTouch.TransiteCustRef != null && !string.IsNullOrEmpty(oDriverTouch.TransiteCustRef.STARSID) && decimal.TryParse(oDriverTouch.TransiteCustRef.STARSID, out starsid))
                {
                    ViewBag.WTicket = oPRTransiteBusinessLogic.weightTicket(starsid);
                }
                else
                    ViewBag.WTicket = false;

                IsExceptionTouch = Convert.ToString(oDriverTouch.IsExceptionalTouch);
                oDriverTouchLocal.Zip = oDriverTouch.Zip;
                ViewBag.VerifiedTouchInfo_ReadyToStart = string.IsNullOrEmpty(oDriverTouch.ActualStartTime) ? false : true;
            }

            DriverTouchInfo model = new DriverTouchInfo();

            try
            {
                if (oDriverTouchLocal.DestinationAddress.AddressType != PR.UtilityLibrary.PREnums.AddressType.Warehouse)
                {
                    model.TouchInfoUpdatedBySL = oPRTransiteBusinessLogic.RetieveTouchSLUpdates(oDriverTouchLocal);

                    if (model.TouchInfoUpdatedBySL != null && (model.TouchInfoUpdatedBySL.SLUpdatedStatusType == SLUpdateType.Sync || model.TouchInfoUpdatedBySL.SLUpdatedStatusType == SLUpdateType.Nochanges))
                    {
                        model.TouchInfoUpdatedBySL = null;
                    }

                    //PENDING-TEST
                    //SFERP-TODO-CTUPD 
                    model.POSData = oPRbusinesslogic.GetPOSitem(QORid);
                    List<TouchData> lstModel = oPRbusinesslogic.GetTouchInfo(TouchType.ToString().Trim(), QORid);
                    model.TouchData = (lstModel.Count > 0 && lstModel[0] != null) ? lstModel[0] : new TouchData();

                    ////Need to make sure about the method call based on order type required or not.
                    //if (oDriverTouch.StarsUnitId > 0)//LDM
                    //{
                    //    model.POSData = oPRbusinesslogic.GetPOSitemFromStarsDB(oDriverTouch.StarsUnitId);
                    //    List<TouchData> lstModel = oPRbusinesslogic.GetLDMTouchInfo(TouchType.ToString().Trim(), QORid);
                    //    model.TouchData = (lstModel.Count > 0 && lstModel[0] != null) ? lstModel[0] : new TouchData();
                    //}
                    //else
                    //{
                    //    model.POSData = oPRbusinesslogic.GetPOSitem(QORid);
                    //    List<TouchData> lstModel = oPRbusinesslogic.GetTouchInfo(TouchType.ToString().Trim(), QORid);
                    //    model.TouchData = (lstModel.Count > 0 && lstModel[0] != null) ? lstModel[0] : new TouchData();
                    //}

                    model.SiteLinkCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QORid);
                }
                else
                {
                    model.POSData = null;
                    model.TouchData = null;
                    model.SiteLinkCustomerInfo = null;
                }
            }
            catch (Exception ex)
            {
                ViewBag.SlError = ex.Message + "<br/>" + ex.StackTrace;
            }
            model.DriverTouch = oDriverTouchLocal;


            // Save Driver info in  local database for further referance

            AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();
            AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster();
            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();

            oAuditLogMaster.ApplicationName = ConfigurationManager.AppSettings["AppName"];
            oAuditLogMaster.DriverID = Convert.ToInt64(oEnDeCryption.Decrypt(UserID));
            oAuditLogMaster.Qorid = QORid.ToString();
            oAuditLogMaster.TransiteTouchType = TouchType;
            oAuditLogMaster.ContainerNo = (model.DriverTouch != null ? model.DriverTouch.ContainerNumber : "");
            if (oDriverTouchLocal.TouchStopId > 0)
                oAuditLogMaster.RefID = Convert.ToString(oDriverTouchLocal.TouchStopId);

            oAuditLogMaster.OrderNo = (model.DriverTouch != null ? model.DriverTouch.OrderNum : "");
            oAuditLogMaster.StopType = (model.DriverTouch != null ? model.DriverTouch.StopType : "");
            if (oDriverTouchLocal.DestinationAddress.AddressType != PR.UtilityLibrary.PREnums.AddressType.Warehouse)
            {
                if (model.TouchData != null)
                    oAuditLogMaster.EditIdentifier = Convert.ToString(model.TouchData.EditIdentifier);
                else
                    oAuditLogMaster.EditIdentifier = string.Empty;
            }
            else
            {
                oAuditLogMaster.EditIdentifier = string.Empty;
            }

            TouchInfoLog oTouchInfoLog = new TouchInfoLog();

            string _LogID = oAuditLog.AddActivityMaster(oAuditLogMaster);
            if (_LogID != "Error")
            {

                LogID = _LogID;
                Response.Cookies["LogID"].Expires = DateTime.Now.AddDays(1);

                oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                {
                    oTouchInfoLog = new TouchInfoLog();
                    oTouchInfoLog.LogID = _LogID;
                    oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);
                }

            }
            model.TouchInfoLog = oTouchInfoLog;
            //End save Driver info in  local database for further referance

            return View(model);
        }

        //public ActionResult SubmitUnitNo(string UnitNo, Int32 QORid, string TouchType)    commenting this line as this ActionResult no more required. System will not validate UnitNo
        public bool SubmitUnitNo(string UnitNo, Int32 QORid, string TouchType)
        {

            try
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                bool valid = false;
                if (TouchType == PR_TouchType_Constant.DE)
                    valid = true; //oTransiteBusinessLogic.ValidateContainer(QORid, UnitNo);
                else if (Convert.ToString(oPRbusinesslogic.GetUnitName(QORid)) == UnitNo.Trim().ToUpper())
                    valid = true;

                if (valid)
                {
                    ///Store Login info in AuditLog DB
                    ///populating values in Auditlog class
                    AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();


                    string _LogID = LogID;
                    oAuditLogDetail.LogID = LogID;
                    oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    oAuditLogDetail.Description = LoginName + " has confirmed POS/ visual inspection at " + System.DateTime.Now.ToString() + " for the QORid" + QORid.ToString();
                    oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.POS_Inspection.ToString();

                    ///Storing log values in Log DB
                    AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
                    oActivityLog.AddActivityDetail(oAuditLogDetail);

                    //Update Touch info log table
                    AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                    TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                    if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                    {
                        oTouchInfoLog = new TouchInfoLog();
                        oTouchInfoLog.LogID = _LogID;
                    }
                    oTouchInfoLog.VerifiedTouchInfo_Chkbox = true;

                    oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);
                    ///End store Login info in AuditLog DB
                    ///

                    //return Content("true");
                    return true;
                }
                else
                    //return Content("false");
                    return false;
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                //return Content("false");
                return false;
            }
        }

        //public ActionResult PosConfirm(string UnitNo) Commenting this line as POS confirmation is no more needed
        public bool PosConfirm(string UnitNo)
        {
            ///Store Login info in AuditLog DB
            ///populating values in Auditlog class
            AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();

            string _LogID = LogID;
            oAuditLogDetail.LogID = _LogID;
            oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            oAuditLogDetail.Description = LoginName + " has reconfirmed POS/ visual inspection at " + System.DateTime.Now.ToString();
            oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.Reconfirm_POS.ToString();

            ///Storing log values in Log DB
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
            oActivityLog.AddActivityDetail(oAuditLogDetail);

            ///End store Login info in AuditLog DB
            ///

            //return Content("true");
            return true;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ReadyToStartConfirm(Int32 QORid, string TouchType, string UnitNo, string StopType, Int32 intTouchId, Int32 intTouchStopId)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                PRTransiteBusinessLogic oPRT = new PRTransiteBusinessLogic();

                //oPRT.UpdateIsTouchStartedOrder(true, OrderNo, StopType, Convert.ToInt32(oEnDeCryption.Decrypt(Request.Cookies["UserID"].Value)));
                oPRT.UpdateIsTouchStartedOrder(intTouchId, intTouchStopId);

                AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog(); // Ctearing instance of Audit log



                bool retUnitVal = SubmitUnitNo(UnitNo, QORid, TouchType);   //Calling this method to add info in log table
                bool retPOSVal = PosConfirm(UnitNo);        // Calling this method to add info in log table

                AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster(); // Creation instant for AuditLogMaster for adding Start Date And Time
                oAuditLogMaster.LogID = LogID; //Passing existing LogID
                oAuditLogMaster.RefID = intTouchStopId.ToString();
                oAuditLogMaster.ScheduledStartTime = System.DateTime.Now.ToString(); // Adding current date and Time
                string _LogID = oActivityLog.AddActivityMaster(oAuditLogMaster);


                ///Store Login info in AuditLog DB
                ///populating values in Auditlog class

                AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();

                //string _LogID = Request.Cookies["LogID"].Value;
                oAuditLogDetail.LogID = LogID;
                oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                oAuditLogDetail.Description = LoginName + " has confirmed start time at " + System.DateTime.Now.ToString();
                oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.ReadyToStart.ToString();

                ///Storing log values in Log DB
                oActivityLog.AddActivityDetail(oAuditLogDetail);

                //Update Touch info log table
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                {
                    oTouchInfoLog = new TouchInfoLog();
                    oTouchInfoLog.LogID = _LogID;
                }
                oTouchInfoLog.VerifiedTouchInfo_ReadyToStart = true;

                oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

                ///End store Login info in AuditLog DB
                ///
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
            }

            return Content(js.Serialize(new { success = flag, msg = msg }));
        }

        public ActionResult ContinerLocked()
        {
            ///Store Login info in AuditLog DB
            ///populating values in Auditlog class
            AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();


            oAuditLogDetail.LogID = LogID;
            oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            oAuditLogDetail.Description = LoginName + " Container is locked propertly at " + System.DateTime.Now.ToString();
            oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.Reconfirm_POS.ToString();

            ///Storing log values in Log DB
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
            oActivityLog.AddActivityDetail(oAuditLogDetail);

            //Update Touch info log table
            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
            TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(oAuditLogDetail.LogID);
            if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
            {
                oTouchInfoLog = new TouchInfoLog();
                oTouchInfoLog.LogID = oAuditLogDetail.LogID;
            }
            oTouchInfoLog.VerifiedTouchInfo_Chkbox = true;

            oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

            ///End store Login info in AuditLog DB
            ///

            return Content("true");
        }

        public ActionResult UpdateVerifiedTouchInfoChkbox()
        {
            try
            {
                string sLogID = LogID;

                //Update Touch info log table
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(sLogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                {
                    oTouchInfoLog = new TouchInfoLog();
                    oTouchInfoLog.LogID = sLogID;
                }
                oTouchInfoLog.VerifiedTouchInfo_Chkbox = true;

                oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

                ///End store Login info in AuditLog DB
                ///
                return Content("true");
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                return Content("false");
            }
        }

    }
}