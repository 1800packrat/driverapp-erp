﻿using DriverMobileApp.Helper;
using PR.Entities;
using System;
using System.Web;
using System.Web.Mvc;

namespace DriverMobileApp.Controllers
{
    [DecryptActionParameter]
    public class BaseController : Controller
    {
        #region Const Keys

        private const string sTouchDate = "TouchDate";
        private const string sStoreNumber = "StoreNumber";
        private const string sLoginName = "LoginName";
        private const string sLastLogin = "LastLogin";
        private const string sUserID = "UserID";
        private const string slocalDateTime = "localDateTime";
        //private const string sVNumber = "VNumber";
        private const string sLoadId = "LoadId";

        private const string sQorID = "QORid";
        private const string scurrentTouchType = "currentTouchType";
        private const string sLogID = "LogID";
        private const string sJustCompletedStopId = "JustCompletedStopId";
        private const string sJustCompletedTouchId = "JustCompletedTouchId";

        private string sSiteInfo = "SiteInfo";
        private string sIsExceptionTouch = "IsExceptionTouch";

        #endregion

        #region Constructor

        public BaseController()
        {
        }

        #endregion

        #region Cookies properties

        public void SetCookiesExpire()
        {
            //Response.Cookies[sTouchDate].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sLoginName].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sLastLogin].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sUserID].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sVNumber].Expires = DateTime.Now.AddDays(1);

            //Response.Cookies[sQorID].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[scurrentTouchType].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sJustCompletedTouchId].Expires = DateTime.Now.AddDays(1);
            //Response.Cookies[sJustCompletedStopId].Expires = DateTime.Now.AddDays(1);
        }

        public void RemoveCookies()
        {
            Response.Cookies[sTouchDate].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[sLoginName].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[sLastLogin].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[sUserID].Expires = DateTime.Now.AddDays(-1);
            //Response.Cookies[sVNumber].Expires = DateTime.Now.AddDays(-1);

            Response.Cookies[sQorID].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[scurrentTouchType].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[sJustCompletedTouchId].Expires = DateTime.Now.AddDays(-1);
            Response.Cookies[sJustCompletedStopId].Expires = DateTime.Now.AddDays(-1);
        }

        public DateTime dtTouchDate
        {
            get { return Convert.ToDateTime(TouchDate); }
        }

        //public string VNumber
        //{
        //    get { return Request.Cookies[sVNumber].Value; }
        //    set
        //    {
        //        Response.Cookies.Add(new HttpCookie(sVNumber, value) { HttpOnly = true });
        //        Response.Cookies[sVNumber].Expires = DateTime.Now.AddDays(1);
        //        //Response.Cookies[sVNumber].HttpOnly = true;
        //    }
        //}

        public string TouchDate
        {
            get { return Request.Cookies[sTouchDate].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sTouchDate, value) { HttpOnly = true });
                Response.Cookies[sTouchDate].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string StoreNumber
        {
            get { return Request.Cookies[sStoreNumber].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sStoreNumber, value) { HttpOnly = true });
                Response.Cookies[sStoreNumber].Expires = DateTime.Now.AddDays(1);
            }
        }

        public bool boolIsExceptionTouch
        {
            get { return Convert.ToBoolean(IsExceptionTouch); }
        }

        public string IsExceptionTouch
        {
            get { return Request.Cookies[sIsExceptionTouch].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sIsExceptionTouch, value) { HttpOnly = true });
                Response.Cookies[sIsExceptionTouch].Expires = DateTime.Now.AddDays(1);
            }
        }

        public int IntLoadId
        {
            get { return Convert.ToInt32(LoadId); }
        }

        public string LoadId
        {
            get { return Request.Cookies[sLoadId].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sLoadId, value) { HttpOnly = true });
                Response.Cookies[sLoadId].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string LoginName
        {
            get { return Request.Cookies[sLoginName].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sLoginName, value) { HttpOnly = true });
                Response.Cookies[sLoginName].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string LastLogin
        {
            get { return Request.Cookies[sLastLogin].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sLastLogin, value) { HttpOnly = true });
                Response.Cookies[sLastLogin].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string UserID
        {
            get { return Request.Cookies[sUserID].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sUserID, value) { HttpOnly = true });
                Response.Cookies[sUserID].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string StrLocalDateTime
        {
            get { return Convert.ToString(Session[slocalDateTime]); }
            set
            {
                Session[slocalDateTime] = value;
                Response.Cookies[slocalDateTime].Expires = DateTime.Now.AddDays(1);
                Response.Cookies[slocalDateTime].HttpOnly = true;
            }
        }

        public DateTime LocalDateTime
        {
            get { return Convert.ToDateTime(StrLocalDateTime); }
        }

        public string QORID
        {
            get { return Request.Cookies[sQorID].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sQorID, value) { HttpOnly = true });
                Response.Cookies[sQorID].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string CurrentTouchType
        {
            get { return Request.Cookies[scurrentTouchType].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(scurrentTouchType, value) { HttpOnly = true });
                Response.Cookies[scurrentTouchType].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string LogID
        {
            get { return Request.Cookies[sLogID].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sLogID, value) { HttpOnly = true });
                Response.Cookies[sLogID].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string JustCompletedStopId
        {
            get { return Request.Cookies[sJustCompletedStopId].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sJustCompletedStopId, value) { HttpOnly = true });
                Response.Cookies[sJustCompletedStopId].Expires = DateTime.Now.AddDays(1);
            }
        }

        public string JustCompletedTouchId
        {
            get { return Request.Cookies[sJustCompletedTouchId].Value; }
            set
            {
                Response.Cookies.Add(new HttpCookie(sJustCompletedTouchId, value) { HttpOnly = true });
                Response.Cookies[sJustCompletedTouchId].Expires = DateTime.Now.AddDays(1);
            }
        }

        #endregion

        public SiteInfo CurrentSiteInfo
        {
            get { return (SiteInfo)Session[sSiteInfo]; }
            set { Session[sSiteInfo] = value; }
        }
    }
}