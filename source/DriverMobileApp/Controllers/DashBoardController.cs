﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DriverMobileApp.Models;
using DriverMobileApp.Helper; 

namespace DriverMobileApp.Controllers
{
    public class DashBoardController : BaseAuthenticatedController
    {
  
        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //[HandleError(View = "Error", ExceptionType = typeof(NotImplementedException))]
        [CustomActionFilter]
        public ActionResult Index()
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();

            PRTransiteBusinessLogic oPRT = new PRTransiteBusinessLogic();
            DashBoardTouchinfo model = new DashBoardTouchinfo();

            try
            {
                if (LoginName == null)
                    return RedirectToAction("Index", "Login");
                else
                {
                    PR.BusinessLogic.LocalComponent lc = new PR.BusinessLogic.LocalComponent();
                    DriverMobileApp.Models.DBclass.RouteOptimzation rp = new Models.DBclass.RouteOptimzation();
                    //---------------------- Pending Touches --------------------------------------------
                    var TouchRemainingList = (from _TouchList in oPRT.RetievePendingTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate))
                                              select _TouchList).DefaultIfEmpty().ToList();

                    List<DriverTouchLocal> lstDriverTouchRemaining = new List<DriverTouchLocal>();
                    foreach (var chdTouchRemainingList in TouchRemainingList)
                    {
                        if (chdTouchRemainingList != null)
                        {

                            var Etasettings = lc.GetETASetting(Convert.ToInt32(chdTouchRemainingList.QORID), chdTouchRemainingList.TouchType, chdTouchRemainingList.SLSeqNo);
                            lstDriverTouchRemaining.Add
                                (
                                    new DriverTouchLocal
                                    {
                                        QORID = chdTouchRemainingList.QORID,
                                        Company = chdTouchRemainingList.Company,
                                        CustomerName = chdTouchRemainingList.CustomerName,
                                        DestinationAddress = (chdTouchRemainingList.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? chdTouchRemainingList.DestinationAddress : chdTouchRemainingList.WeightStationAddress),
                                        OriginAddress = chdTouchRemainingList.OriginAddress,
                                        ContainerNumber = chdTouchRemainingList.ContainerNumber,
                                        ContainerSize = chdTouchRemainingList.ContainerSize,
                                        ScheduledStartTime = chdTouchRemainingList.ScheduledStartTime,
                                        ScheduledEndTime = chdTouchRemainingList.ScheduledEndTime,
                                        EstimatedStartTime = chdTouchRemainingList.EstimatedStartTime,
                                        EstimatedEndTime = chdTouchRemainingList.EstimatedEndTime,
                                        SequenceNumber = chdTouchRemainingList.SequenceNumber,
                                        TouchType = chdTouchRemainingList.TouchType,
                                        BalanceDue = chdTouchRemainingList.BalanceDue,
                                        Status = chdTouchRemainingList.Status,
                                        OrderNum = chdTouchRemainingList.OrderNum,
                                        StopType = chdTouchRemainingList.StopType,
                                        IsCombinedTouch = chdTouchRemainingList.IsCombinedTouch,
                                        IsTrailerNeeded = chdTouchRemainingList.IsTrailerNeeded,
                                        TouchId = chdTouchRemainingList.TouchId,
                                        TouchStopId = chdTouchRemainingList.TouchStopId,
                                        SkipTouch = chdTouchRemainingList.SkipTouch,
                                        FacilityName = chdTouchRemainingList.FacilityName,
                                        ETASettings = Etasettings,
                                        IsZippyShellMove = chdTouchRemainingList.IsZippyShellMove
                                    }
                                );
                        }
                    }
                    model.RemainingTouchData = lstDriverTouchRemaining;
                    //---------------------- End Pending Touches --------------------------------------------

                    //---------------------- Skipped Touches --------------------------------------------
                    var TouchSkippedList = (from _TouchList in oPRT.RetieveSkippedTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate))
                                            select _TouchList).DefaultIfEmpty().ToList();

                    List<DriverTouchLocal> lstDriverTouchSkipped = new List<DriverTouchLocal>();
                    foreach (var chdTouchSkippedList in TouchSkippedList)
                    {
                        if (chdTouchSkippedList != null)
                        {
                            lstDriverTouchSkipped.Add
                                (
                                    new DriverTouchLocal
                                    {
                                        QORID = chdTouchSkippedList.QORID,
                                        CustomerName = chdTouchSkippedList.CustomerName,
                                        Company = chdTouchSkippedList.Company,
                                        DestinationAddress = (chdTouchSkippedList.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? chdTouchSkippedList.DestinationAddress : chdTouchSkippedList.WeightStationAddress),
                                        OriginAddress = chdTouchSkippedList.OriginAddress,
                                        ContainerNumber = chdTouchSkippedList.ContainerNumber,
                                        ContainerSize = chdTouchSkippedList.ContainerSize,
                                        ScheduledStartTime = chdTouchSkippedList.ScheduledStartTime,
                                        ScheduledEndTime = chdTouchSkippedList.ScheduledEndTime,
                                        EstimatedStartTime = chdTouchSkippedList.EstimatedStartTime,
                                        EstimatedEndTime = chdTouchSkippedList.EstimatedEndTime,
                                        SequenceNumber = chdTouchSkippedList.SequenceNumber,
                                        TouchType = chdTouchSkippedList.TouchType,
                                        BalanceDue = chdTouchSkippedList.BalanceDue,
                                        Status = chdTouchSkippedList.Status,
                                        OrderNum = chdTouchSkippedList.OrderNum,
                                        StopType = chdTouchSkippedList.StopType,
                                        IsCombinedTouch = chdTouchSkippedList.IsCombinedTouch,
                                        IsTrailerNeeded = chdTouchSkippedList.IsTrailerNeeded,
                                        TouchId = chdTouchSkippedList.TouchId,
                                        TouchStopId = chdTouchSkippedList.TouchStopId,
                                        SkipTouch = chdTouchSkippedList.SkipTouch,
                                        FacilityName = chdTouchSkippedList.FacilityName,
                                        IsZippyShellMove = chdTouchSkippedList.IsZippyShellMove
                                    }
                                );
                        }
                    }
                    model.SkippedTouchData = lstDriverTouchSkipped;
                    //---------------------- End Skipped Touches --------------------------------------------


                    //---------------------- Completed Touches --------------------------------------------

                    var TouchCompletedList = (from _TouchList in oPRT.RetieveCompletedTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate))
                                              select _TouchList).DefaultIfEmpty().ToList();

                    List<DriverTouchLocal> lstDriverTouchCompleted = new List<DriverTouchLocal>();
                    foreach (var chdTouchCompletedList in TouchCompletedList)
                    {
                        if (chdTouchCompletedList != null)
                        {
                            lstDriverTouchCompleted.Add
                                (
                                    new DriverTouchLocal
                                    {
                                        QORID = chdTouchCompletedList.QORID,
                                        Company = chdTouchCompletedList.Company,
                                        CustomerName = chdTouchCompletedList.CustomerName,
                                        DestinationAddress = (chdTouchCompletedList.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? chdTouchCompletedList.DestinationAddress : chdTouchCompletedList.WeightStationAddress),
                                        OriginAddress = chdTouchCompletedList.OriginAddress,
                                        ContainerNumber = chdTouchCompletedList.ContainerNumber,
                                        ContainerSize = chdTouchCompletedList.ContainerSize,
                                        ScheduledStartTime = chdTouchCompletedList.ScheduledStartTime,
                                        ScheduledEndTime = chdTouchCompletedList.ScheduledEndTime,
                                        EstimatedStartTime = chdTouchCompletedList.EstimatedStartTime,
                                        EstimatedEndTime = chdTouchCompletedList.EstimatedEndTime,
                                        ActualStartTime = chdTouchCompletedList.ActualStartTime,
                                        ActualEndTime = chdTouchCompletedList.ActualEndTime,
                                        SequenceNumber = chdTouchCompletedList.SequenceNumber,
                                        TouchType = chdTouchCompletedList.TouchType,
                                        BalanceDue = chdTouchCompletedList.BalanceDue,
                                        Status = chdTouchCompletedList.Status,
                                        OrderNum = chdTouchCompletedList.OrderNum,
                                        StopType = chdTouchCompletedList.StopType,
                                        IsCombinedTouch = chdTouchCompletedList.IsCombinedTouch,
                                        IsTrailerNeeded = chdTouchCompletedList.IsTrailerNeeded,
                                        TouchId = chdTouchCompletedList.TouchId,
                                        TouchStopId = chdTouchCompletedList.TouchStopId,
                                        FacilityName = chdTouchCompletedList.FacilityName,
                                        IsZippyShellMove = chdTouchCompletedList.IsZippyShellMove
                                    }
                                );
                        }
                    }
                    model.CompletedTouchData = lstDriverTouchCompleted;
                    //---------------------- End Completed Touches --------------------------------------------


                    //---------------------- Next Touches --------------------------------------------

                    PR.Entities.DriverTouch oDriverTouch = oPRT.RetieveNextTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate));
                    DriverTouchLocal oDriverTouchNext = new DriverTouchLocal();
                    if (oDriverTouch != null)
                    {
                        oDriverTouchNext.Company = oDriverTouch.Company;
                        oDriverTouchNext.CustomerName = oDriverTouch.CustomerName;
                        oDriverTouchNext.AddressLine1 = oDriverTouch.AddressLine1;
                        oDriverTouchNext.AddressLine2 = oDriverTouch.AddressLine2;
                        oDriverTouchNext.BalanceDue = oDriverTouch.BalanceDue;
                        oDriverTouchNext.City = oDriverTouch.City;
                        oDriverTouchNext.ContainerNumber = oDriverTouch.ContainerNumber;
                        oDriverTouchNext.ContainerSize = oDriverTouch.ContainerSize;
                        oDriverTouchNext.CustomerName = oDriverTouch.CustomerName;
                        oDriverTouchNext.DestinationAddress = (oDriverTouch.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? oDriverTouch.DestinationAddress : oDriverTouch.WeightStationAddress);
                        oDriverTouchNext.IsCombinedTouch = oDriverTouch.IsCombinedTouch;
                        oDriverTouchNext.IsTrailerNeeded = oDriverTouch.IsTrailerNeeded;
                        oDriverTouchNext.OrderNum = oDriverTouch.OrderNum;
                        oDriverTouchNext.OriginAddress = oDriverTouch.OriginAddress;
                        oDriverTouchNext.PhoneNumber = oDriverTouch.PhoneNumber;
                        oDriverTouchNext.QORID = oDriverTouch.QORID;

                        oDriverTouchNext.ScheduledEndTime = oDriverTouch.ScheduledEndTime;
                        oDriverTouchNext.ScheduledStartTime = oDriverTouch.ScheduledStartTime;
                        oDriverTouchNext.EstimatedStartTime = oDriverTouch.EstimatedStartTime;
                        oDriverTouchNext.EstimatedEndTime = oDriverTouch.EstimatedEndTime;
                        oDriverTouchNext.SequenceNumber = oDriverTouch.SequenceNumber;
                        oDriverTouchNext.State = oDriverTouch.State;
                        oDriverTouchNext.Status = oDriverTouch.Status;
                        oDriverTouchNext.StopId = oDriverTouch.StopId;
                        oDriverTouchNext.StopType = oDriverTouch.StopType;
                        oDriverTouchNext.TouchType = oDriverTouch.TouchType;
                        oDriverTouchNext.Zip = oDriverTouch.Zip;
                        oDriverTouchNext.TouchId = oDriverTouch.TouchId;
                        oDriverTouchNext.TouchStopId = oDriverTouch.TouchStopId;
                        oDriverTouchNext.FacilityName = oDriverTouch.FacilityName;
                        oDriverTouchNext.IsZippyShellMove = oDriverTouch.IsZippyShellMove;
                    }
                    else
                        oDriverTouch = null;
                    model.NextTouchData = (oDriverTouch != null ? oDriverTouchNext : null);

                    //---------------------- End Next Touches --------------------------------------------
                    //---------------------- Current Touches --------------------------------------------                    

                    oDriverTouch = null;
                    oDriverTouch = oPRT.RetieveCurrentTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate));
                    DriverTouchLocal oDriverTouchCurrent = new DriverTouchLocal();

                    if (oDriverTouch != null)
                    {
                        oDriverTouchCurrent.Company = oDriverTouch.Company;
                        oDriverTouchCurrent.CustomerName = oDriverTouch.CustomerName;
                        oDriverTouchCurrent.AddressLine1 = oDriverTouch.AddressLine1 == null ? "" : oDriverTouch.AddressLine1;
                        oDriverTouchCurrent.AddressLine2 = oDriverTouch.AddressLine2 == null ? "" : oDriverTouch.AddressLine2;
                        oDriverTouchCurrent.BalanceDue = oDriverTouch.BalanceDue;
                        oDriverTouchCurrent.City = oDriverTouch.City;
                        oDriverTouchCurrent.ContainerNumber = oDriverTouch.ContainerNumber;
                        oDriverTouchCurrent.ContainerSize = oDriverTouch.ContainerSize;
                        oDriverTouchCurrent.CustomerName = oDriverTouch.CustomerName;
                        oDriverTouchCurrent.DestinationAddress = (oDriverTouch.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? oDriverTouch.DestinationAddress : oDriverTouch.WeightStationAddress);
                        oDriverTouchCurrent.IsCombinedTouch = oDriverTouch.IsCombinedTouch;
                        oDriverTouchCurrent.IsTrailerNeeded = oDriverTouch.IsTrailerNeeded;
                        oDriverTouchCurrent.OrderNum = oDriverTouch.OrderNum;
                        oDriverTouchCurrent.OriginAddress = oDriverTouch.OriginAddress;
                        oDriverTouchCurrent.PhoneNumber = oDriverTouch.PhoneNumber;
                        oDriverTouchCurrent.QORID = oDriverTouch.QORID;
                        oDriverTouchCurrent.ScheduledEndTime = oDriverTouch.ScheduledEndTime;
                        oDriverTouchCurrent.ScheduledStartTime = oDriverTouch.ScheduledStartTime;
                        oDriverTouchCurrent.EstimatedStartTime = oDriverTouch.EstimatedStartTime;
                        oDriverTouchCurrent.EstimatedEndTime = oDriverTouch.EstimatedEndTime;
                        oDriverTouchCurrent.SequenceNumber = oDriverTouch.SequenceNumber;
                        oDriverTouchCurrent.State = oDriverTouch.State;
                        oDriverTouchCurrent.Status = oDriverTouch.Status;
                        oDriverTouchCurrent.StopId = oDriverTouch.StopId;
                        oDriverTouchCurrent.StopType = oDriverTouch.StopType;
                        oDriverTouchCurrent.TouchType = oDriverTouch.TouchType;
                        oDriverTouchCurrent.Zip = oDriverTouch.Zip;
                        oDriverTouchCurrent.TouchId = oDriverTouch.TouchId;
                        oDriverTouchCurrent.TouchStopId = oDriverTouch.TouchStopId;
                        oDriverTouchCurrent.FacilityName = oDriverTouch.FacilityName;
                        oDriverTouchCurrent.IsZippyShellMove = oDriverTouch.IsZippyShellMove;

                        oDriverTouchCurrent.ETASettings = lc.GetETASetting(Convert.ToInt32(oDriverTouch.QORID), oDriverTouch.TouchType, oDriverTouch.SLSeqNo);
                    }
                    else
                        oDriverTouch = null;
                    model.CurrentTouchData = (oDriverTouch != null ? oDriverTouchCurrent : null);
                    //---------------------- End Current Touches --------------------------------------------

                    var RemCount = (from _TransiteTouchData in oPRT.RetievePendingTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate)) select _TransiteTouchData).DefaultIfEmpty().ToList();
                    if (RemCount[0] != null)
                        model.RemainingTouchCount = RemCount.Count();
                    else
                        model.RemainingTouchCount = 0;


                    var SkipCount = (from _TransiteTouchData in oPRT.RetieveSkippedTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate)) select _TransiteTouchData).DefaultIfEmpty().ToList();
                    if (SkipCount[0] != null)
                        model.SkippedTouchCount = SkipCount.Count();
                    else
                        model.SkippedTouchCount = 0;



                    var ComCount = (from _TransiteTouchData in oPRT.RetieveCompletedTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate)) select _TransiteTouchData).DefaultIfEmpty().ToList();
                    if (ComCount[0] != null)
                        model.CompletedTouchCount = ComCount.Count();
                    else
                        model.CompletedTouchCount = 0;

                    //Making local class variable null
                    oDriverTouchNext = null;
                    oDriverTouchCurrent = null;

                    ViewBag.Error = string.Empty;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                ViewBag.Error = ex.Message;
                return View(model);
            }
        }

        public ActionResult ErrorOnRouteLock()
        {
            DashBoardTouchinfo model = new DashBoardTouchinfo();
            ViewBag.Error = "Your route is not locked in for today. Contact your OM.";
            return View("Index", model);
        }

        public ActionResult ClearTouchList()
        {
            Session["TouchList"] = null; 

            return RedirectToAction("Index");
        }
    }
}
