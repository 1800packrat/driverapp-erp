﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DriverMobileApp.Models;

namespace DriverMobileApp.Controllers
{
    public class CustomerInfoController : BaseAuthenticatedController
    { 
        public ActionResult Index(string TouchType, Int32 QORid, string StopType)
        {
            //Heading of Touch time populates based on following criteria
            switch (TouchType.ToString().Trim())
            {
                case PR_TouchType_Constant.DE:
                case PR_TouchType_Constant.DF:
                    ViewBag.TimeText = "Delivery Time";
                    ViewBag.CustomerText = "Customer Name";
                    ViewBag.AddressText = "Customer Address";
                    break;
                case PR_TouchType_Constant.RF:
                case PR_TouchType_Constant.RE:
                case PR_TouchType_Constant.RD:
                    ViewBag.TimeText = "Pickup Time";
                    ViewBag.CustomerText = "Facility Name";
                    ViewBag.AddressText = "Facility Address";
                    break;

                default:
                    ViewBag.TimeText = "Delivery Time";
                    ViewBag.CustomerText = "Customer Name";
                    ViewBag.AddressText = "Customer Address";
                    break;
            }

            PRTransiteBusinessLogic oPRT = new PRTransiteBusinessLogic();
            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            EnDeCryption oEnDeCryption = new EnDeCryption();
            RemainingToucheCustinfo model = new RemainingToucheCustinfo();


            PR.Entities.DriverTouch oDriverTouch = oPRT.RetieveToucheByQorid(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate), TouchType, Convert.ToString(QORid), StopType.Trim());
            DriverTouchLocal oDriverTouchLocal = new DriverTouchLocal();
            if (oDriverTouch != null)
            {
                  oDriverTouchLocal.CustomerName = oDriverTouch.CustomerName;
                oDriverTouchLocal.AddressLine1 = oDriverTouch.AddressLine1;
                oDriverTouchLocal.AddressLine2 = oDriverTouch.AddressLine2;
                oDriverTouchLocal.BalanceDue = oDriverTouch.BalanceDue;
                oDriverTouchLocal.City = oDriverTouch.City;
                oDriverTouchLocal.ContainerNumber = oDriverTouch.ContainerNumber;
                oDriverTouchLocal.ContainerSize = oDriverTouch.ContainerSize;
                oDriverTouchLocal.CustomerName = oDriverTouch.CustomerName;
                oDriverTouchLocal.DestinationAddress = oDriverTouch.StopType != DriverMobileApp.Models.PR_Action_Constant.WS ? oDriverTouch.DestinationAddress : oDriverTouch.WeightStationAddress;
                oDriverTouchLocal.IsCombinedTouch = oDriverTouch.IsCombinedTouch;
                oDriverTouchLocal.IsTrailerNeeded = oDriverTouch.IsTrailerNeeded;
                oDriverTouchLocal.OrderNum = oDriverTouch.OrderNum;
                oDriverTouchLocal.OriginAddress = oDriverTouch.OriginAddress;
                oDriverTouchLocal.PhoneNumber = oDriverTouch.PhoneNumber;
                oDriverTouchLocal.QORID = oDriverTouch.QORID;
                oDriverTouchLocal.ScheduledEndTime = oDriverTouch.ScheduledEndTime;
                oDriverTouchLocal.ScheduledStartTime = oDriverTouch.ScheduledStartTime;
                oDriverTouchLocal.EstimatedStartTime = oDriverTouch.EstimatedStartTime;
                oDriverTouchLocal.EstimatedEndTime = oDriverTouch.EstimatedEndTime;
                oDriverTouchLocal.SequenceNumber = oDriverTouch.SequenceNumber;
                oDriverTouchLocal.State = oDriverTouch.State;
                oDriverTouchLocal.Status = oDriverTouch.Status;
                oDriverTouchLocal.StopId = oDriverTouch.StopId;
                oDriverTouchLocal.StopType = oDriverTouch.StopType;
                oDriverTouchLocal.TouchType = oDriverTouch.TouchType;
                oDriverTouchLocal.TransiteCustRef = oDriverTouch.TransiteCustRef;
                oDriverTouchLocal.TransiteTouchType = oDriverTouch.TransiteTouchType;
                oDriverTouchLocal.Zip = oDriverTouch.Zip;
                oDriverTouchLocal.SkipTouch = oDriverTouch.SkipTouch;
                oDriverTouchLocal.FacilityName = oDriverTouch.FacilityName;
                oDriverTouchLocal.IsZippyShellMove = oDriverTouch.IsZippyShellMove;

                model.CustomerInfo = oDriverTouchLocal;

                if (oDriverTouchLocal.DestinationAddress.AddressType != PR.UtilityLibrary.PREnums.AddressType.Warehouse)
                {
                    List<TouchData> lstModel = oPRbusinesslogic.GetTouchInfo(TouchType.ToString().Trim(), Convert.ToInt32(model.CustomerInfo.QORID));
                    model.TouchData = lstModel.Count > 0 ? lstModel[0] : new TouchData();

                    if (TouchType == PR_TouchType_Constant.DE)
                        model.POSData = oPRbusinesslogic.GetPOSitem(Convert.ToInt32(model.CustomerInfo.QORID));
                    else
                        model.POSData = null;
                }
                else
                {
                    model.POSData = null;
                }

            }
            model.CustomerInfo = oDriverTouchLocal;

            return View(model);
        }
 
    }
}
