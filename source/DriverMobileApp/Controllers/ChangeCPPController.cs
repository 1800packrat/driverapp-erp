﻿using System;
using System.Linq;
using System.Web.Mvc;
using DriverMobileApp.Models;
using System.Web.Script.Serialization;
using DriverMobileApp.Models.Email;
using System.Configuration;
using System.Text;
using PR.BusinessLogic;
using System.Data;
using PR.ValidationHandler;

namespace DriverMobileApp.Controllers
{
    public class ChangeCPPController : BaseAuthenticatedController
    {
        //
        // GET: /ChangeCPP/
        public ActionResult Index(string QORid, string StopType, string TouchType, string TouchStopId)
        {
            ViewBag.QORid = QORid;
            ViewBag.StopType = StopType;
            ViewBag.TouchType = TouchType;
            ViewBag.TouchStopId = TouchStopId;

            ChangeCPP oChangeCPP = new ChangeCPP();
            ChangeCppInfo oChangeCppInfo = new ChangeCppInfo();

            oChangeCppInfo.SelectedCpp = "-";
            oChangeCppInfo.CppList = oChangeCPP.GetCPPitems(Convert.ToInt32(QORid));
            oChangeCppInfo.Lock = 0;
            oChangeCppInfo.Blankets = 0;
            oChangeCppInfo.Comments = string.Empty;

            return View(oChangeCppInfo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeCppRequest(string cppLevel, string lockNo, string blanketNo, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = string.Empty;

            try
            {
                if (lockNo.AllowIntegersOnly() && blanketNo.AllowIntegersOnly() && (cppLevel.AllowDecimals() || cppLevel.OnlyAlpha()))
                {
                    EnDeCryption oEnDeCryption = new EnDeCryption();

                    ChangeCPP oChangeCPP = new ChangeCPP();
                    flag = oChangeCPP.AddCppItem(TouchStopId, oEnDeCryption.Decrypt(UserID), qorid, TouchType, StopType, cppLevel, lockNo, blanketNo, comments);
                    msg = flag ? "CPP added successfully" : "Error occurred while inserting CPP";

                    if (flag)
                    {
                        SendEmail(cppLevel, lockNo, blanketNo, comments, qorid, TouchStopId, TouchType, StopType);
                    }
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                flag = false;
                msg = ex.Message;
            }

            return Content(js.Serialize(new { success = flag, msg = msg }));
        }


        /// <summary>
        /// Method to send email for ChangeCPP operation
        /// </summary>
        /// <param name="cppLevel"></param>
        /// <param name="lockNo"></param>
        /// <param name="blanketNo"></param>
        /// <param name="comments"></param>
        /// <param name="qorid"></param>
        /// <param name="TouchStopId"></param>
        /// <param name="TouchType"></param>
        /// <param name="StopType"></param>
        /// <returns></returns>
        private void SendEmail(string cppLevel, string lockNo, string blanketNo, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EmailLogic objEmail = new Models.Email.EmailLogic();
            objEmail.ChangeCPP_SendEmail(cppLevel, lockNo, blanketNo, comments, qorid, TouchStopId, TouchType, StopType, UserID, LoginName);
        }

        #region This method has been moved to Models/Email/EmailLogic.cs

        ///// <summary>
        ///// Method to send email for ChangeCPP operation - HomeGrown
        ///// </summary>
        ///// <param name="cppLevel"></param>
        ///// <param name="lockNo"></param>
        ///// <param name="blanketNo"></param>
        ///// <param name="comments"></param>
        ///// <param name="qorid"></param>
        ///// <param name="TouchStopId"></param>
        ///// <param name="TouchType"></param>
        ///// <param name="StopType"></param>
        ///// <returns></returns>
        //private bool ChangeCPP_SendEmail(string cppLevel, string lockNo, string blanketNo, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        //{

        //    AppEmail oAppEmail = new AppEmail();
        //    EnDeCryption oEnDeCryption = new EnDeCryption();

        //    //IF cppLevel == "-" means, user did not select any cpp. so in this case we need not look for Database for description
        //    string strCppText = ((cppLevel == "-") ? "-" : (cppLevel == "RemoveCPP" ? "Remove CPP" : GetSelectedCPPTextForEmail(qorid, cppLevel)));

        //    oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
        //    oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
        //    oAppEmail.Subject = "Changed CPP items";

        //    StringBuilder emailBody = new StringBuilder();

        //    emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //    emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //    emailBody.Append("<head><title>1800PACKRAT.COM | New CPP items</title></head>");
        //    emailBody.Append("<body>");

        //    emailBody.Append("<br />Changed CPP information<br /><br />");
        //    emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID/ Name </td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oEnDeCryption.Decrypt(UserID) + " - " + LoginName + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Changed CPP item</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + strCppText + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Lock</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + lockNo + "<br />");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Blankets</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + blanketNo + "<br />");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>QORID</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + qorid + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Touch Type</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + TouchType + " - " + StopType + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver's comments</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + comments + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("</table><br /><br />");

        //    emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

        //    oAppEmail.EmailBody = emailBody.ToString();

        //    //Inserting Email Log
        //    Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
        //    el.ApplicationName = "Driver App";
        //    el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //    el.User = oEnDeCryption.Decrypt(UserID);
        //    el.StartTime = DateTime.Now;
        //    el.Subject = "Changed CPP items";
        //    el.ToEmailIDs = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
        //    el.FromEmailID = ConfigurationManager.AppSettings["mailFrom"].ToString();
        //    el.CCEmailIDs = null;
        //    el.MethodName = "ChangeCPP";
        //    el.EmailTypeID = 3;
        //    el.QORID = Convert.ToInt32(qorid);
        //    el.TouchType = TouchType;
        //    el.SLSeqNo = Convert.ToInt32(TouchStopId);

        //    var LogId = LogEmails.InsertLog(el);
        //    if (oAppEmail.SendEMail() == "Success")
        //    {
        //        //updating endtime in email log
        //        Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
        //        eml.PK_ID = LogId;
        //        eml.EndTime = DateTime.Now;
        //        LogEmails.UpdateLog(eml);
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        ///// <summary>
        ///// PENDING-TEST
        ///// Method to get CPP item text based on QORID and cppLevel selected for the Unit/QOR
        ///// </summary>
        ///// <param name="qorId"></param>
        ///// <param name="cppLevel"></param>
        ///// <returns></returns>
        //private string GetSelectedCPPTextForEmail(string qorId, string cppLevel)
        //{
        //    #region SFERP-TODO-ESBMTD

        //    SMDBusinessLogic smd = new SMDBusinessLogic();
        //    System.Collections.Generic.List<PR.Entities.CPPItem> cppItems = smd.GetAppliedCppItems(qorId, cppLevel);
        //    var cppText = cppItems.Where(c => c.Price == cppLevel).SingleOrDefault().Description;

        //    return cppText;

        //    #endregion

        //    //if (IsLocalQuote(Convert.ToInt32(qorId)))
        //    //{
        //    //    LocalComponent lc = new LocalComponent();
        //    //    var cppResult = lc.GetCPPInfo().Tables[0].AsEnumerable().Where(s => !s.Field<bool>("IsWeekly") && s.Field<decimal>("Price").ToString() == cppLevel).SingleOrDefault();

        //    //    return cppResult != null ? (cppResult.Field<decimal>("Price").ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us")) + " - " + cppResult.Field<string>("CPPDescription").ToString()) : cppLevel;
        //    //}
        //    //else
        //    //{
        //    //    using (var oStarDB = new DriverMobileApp.Models.DBclass.STARSDBEntities())
        //    //    {
        //    //        var cppResult = (from _cppResult in oStarDB.T66_Contentprotection
        //    //                         where _cppResult.T66_SlabID == (oStarDB.T66_Contentprotection.Select(t => t.T66_SlabID).Max()) &&
        //    //                         _cppResult.T66_DeclaredValue == cppLevel && _cppResult.T66_Special != "Special"
        //    //                         select _cppResult).SingleOrDefault();

        //    //        return cppResult != null ? (((decimal)cppResult.T66_MonthlyPremium).ToString("C", System.Globalization.CultureInfo.GetCultureInfo("en-us")) + " - " + cppResult.T66_DeclaredValue) : cppLevel;
        //    //    }
        //    //}
        //}

        #endregion

        //private bool IsLocalQuote(int Qorid)
        //{
        //    bool isLocal = true;

        //    using (var oDBRoute = new DriverMobileApp.Models.DBclass.RouteOptimzation())
        //    {
        //        oDBRoute.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

        //        var TouchInfo = (from _TouchInfo in oDBRoute.TouchInformations
        //                         where _TouchInfo.QORId == Qorid //&& _TouchInfo.StarsUnitId > 0
        //                         select _TouchInfo).FirstOrDefault();
        //        if (TouchInfo.StarsUnitId > 0)
        //        {
        //            isLocal = false;
        //        }
        //    }

        //    return isLocal;
        //}



    }
}
