﻿using System;
using DriverMobileApp.Models.Signature;
using System.Web.Mvc;
using DriverMobileApp.Models;
using PR.Entities;
using AuditLog.DBStructure;
using System.IO;


namespace DriverMobileApp.Controllers
{  
    public class BillDetailController : BaseAuthenticatedController
    {
        //// Commented by Sohan: Assumed not in Use. 

        //    public ActionResult Index()
        //    {
        //        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //        BillDetailInfo oBillDetailInfo = new BillDetailInfo();
        //        oBillDetailInfo.TransportationItems = oPRbusinesslogic.GetTransportationItems(Convert.ToInt32(QORID));
        //        oBillDetailInfo.POSItems = oPRbusinesslogic.GetPOSItemWithPrice(Convert.ToInt32(QORID));
        //        oBillDetailInfo.RecurringItems = oPRbusinesslogic.GetRecurringItems(Convert.ToInt32(QORID));
        //        oBillDetailInfo.UnitInfo = oPRbusinesslogic.GetUnitInfo(Convert.ToInt32(QORID));
        //        oBillDetailInfo.PricingInfo = oPRbusinesslogic.GetPricing(Convert.ToInt32(QORID));
        //        CustomerInfo oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(Convert.ToInt32(QORID));
        //        ViewBag.CustomerEmailID = String.IsNullOrWhiteSpace(oCustomerInfo.EmailID) ? "" : oCustomerInfo.EmailID;
        //        //oBillDetailInfo.UnitInfo = oPRbusinesslogic.GetUnitInfo(Convert.ToInt32(Request.Cookies["QORid"].Value));

        //        SignatureData oSignatureData = new SignatureData();
        //        CustomerSignature oCustomerSignature = new CustomerSignature();

        //        oSignatureData = oCustomerSignature.GetSignature(Convert.ToInt32(QORID));

        //        ViewBag.SigCaptured = (oSignatureData == null ? false : true);

        //        oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(Convert.ToInt32(QORID));

        //        ViewBag.CustomerName = oCustomerInfo.FirstName + " " + oCustomerInfo.LName;
        //        ViewBag.SigText = System.Configuration.ConfigurationManager.AppSettings["SigBelowLine"].ToString();


        //        ViewBag.QORid = Request.QueryString["QORid"];
        //        ViewBag.StopType = Request.QueryString["StopType"];
        //        ViewBag.TouchType = Request.QueryString["TouchType"];
        //        ViewBag.TouchStopId = Request.QueryString["TouchStopId"];

        //        return View(oBillDetailInfo);
        //    }


        //    [HttpPost]
        //    public ActionResult GenerateImage(string json, string CustomerName, string Remark, bool SameAsPayee)
        //    {
        //        CustomerSignature oCustomerSignature = new CustomerSignature();

        //        AuditLogMaster oAuditLogMaster = new AuditLogMaster();
        //        AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

        //        oAuditLogMaster = oActivityLog.RetLogMaster(LogID);


        //        SignatureToImage sigImg = new SignatureToImage();
        //        System.Drawing.Bitmap bp = sigImg.SigJsonToImage(json);


        //        MemoryStream ms = new MemoryStream();
        //        bp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //        byte[] byteImage = ms.ToArray();
        //        Convert.ToBase64String(byteImage); //here you should get a base64 string



        //        SignatureData oSignatureData = new SignatureData();
        //        oSignatureData.QORid = Convert.ToInt32(oAuditLogMaster.Qorid);
        //        oSignatureData.TouchType = oAuditLogMaster.TransiteTouchType;
        //        oSignatureData.Comments = (SameAsPayee) ? "Signed by " + CustomerName : Remark;
        //        oSignatureData.SignatureImage = Convert.ToBase64String(byteImage).ToString();

        //        oCustomerSignature.addSignatureInfo(oSignatureData);

        //        ViewBag.SigCaptured = true;
        //        return Content("true");
        //    }


        //    [HttpPost]
        //    public ActionResult getCustomerEmailID(string CustomerEmail)
        //    {
        //        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //        BillDetailInfo oBillDetailInfo = new BillDetailInfo();
        //        oBillDetailInfo.TransportationItems = oPRbusinesslogic.GetTransportationItems(Convert.ToInt32(QORID));
        //        oBillDetailInfo.POSItems = oPRbusinesslogic.GetPOSItemWithPrice(Convert.ToInt32(QORID));
        //        oBillDetailInfo.UnitInfo = oPRbusinesslogic.GetUnitInfo(Convert.ToInt32(QORID));
        //        oBillDetailInfo.RecurringItems = oPRbusinesslogic.GetRecurringItems(Convert.ToInt32(QORID));
        //        CustomerInfo oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(Convert.ToInt32(QORID));
        //        oBillDetailInfo.PricingInfo = oPRbusinesslogic.GetPricing(Convert.ToInt32(QORID));
        //        SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfo(Convert.ToInt32(QORID));

        //        bool retVal = oPRbusinesslogic.EmailBillInfo(CustomerEmail, oBillDetailInfo, oCustomerInfo, oSiteInfo, Convert.ToInt32(QORID));

        //        if (retVal)
        //            return Content("true");
        //        else
        //            return Content("false");
        //    }

    }
}
