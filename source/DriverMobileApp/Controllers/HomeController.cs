﻿using System;
using System.Web;
using System.Web.Mvc;
using DriverMobileApp.Models;
using AuditLog.DBStructure;
using System.Globalization;
using DriverMobileApp.Helper;
using PR.LocalLogisticsSolution.Interfaces;

namespace DriverMobileApp.Controllers
{
    public class HomeController : BaseController
    {
        EnDeCryption oEnDeCryption = new EnDeCryption();
        public IUserService _userService { get; set; }

        public HomeController(IUserService userService)
        {
            _userService = userService;
        }


        public ActionResult Index()
        {
            ViewBag.Message = "1-800-PACK-RAT Driver Application";
           
            //VNumber = Utility.AppVersion;//Display Application version

            SetCookiesExpire();

            return View();
        }

        [HttpPost]
        [HandleError(View = "NotImplErrorView", ExceptionType = typeof(NotImplementedException))]
        public ActionResult Index(UserLogin oUserLogin)
        {
            PRTransiteBusinessLogic oPRT = new PRTransiteBusinessLogic();
            Login oLogin = new Login();

            var LoginStatus = (dynamic)null;           

           // ExpCookie(); //Cleaning cookies if exists
            RemoveCookies();
            
            LoginStatus = oLogin.ValidateUser(oUserLogin, _userService);
            ViewBag.Error = null;

            if (LoginStatus != null)
            {
                if (LoginStatus.Status == true)
                {
                    // Save Driver info in  local database for further referance
                    AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();
                    AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();

                    oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    oAuditLogDetail.DateTime = System.DateTime.Now.Date;
                    oAuditLogDetail.DriverID = Convert.ToInt64(LoginStatus.UserID);
                    oAuditLogDetail.Description = "Driver's login time has successfully added";
                    oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.Login.ToString();

                    AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
                    oAuditLog.AddActivityDetail(oAuditLogDetail);

                    // End save Driver info in  local database for further referance
                    //Going forward we need to handle cookies from base class
                    LoginName = LoginStatus.FirstName + " " + LoginStatus.LastName;
                    LastLogin = LoginStatus.LastLoginDate.ToString();  //we are not getting this property from middle tier
                    UserID = oEnDeCryption.Encrypt(LoginStatus.UserID.ToString());
                    StoreNumber = LoginStatus.StoreNumber;
                    LoadId = Convert.ToString(LoginStatus.LoadId);

                    Session["localDateTime"] = Request.Params["localDateTime"];
                    StrLocalDateTime = Request.Params["localDateTime"];
                   
                    TouchDate = System.DateTime.Now.Date.ToString();
                    //Response.Cookies.Add(new HttpCookie("TouchDate", "01/26/2016")); // This is for testing purpose. must be commented before production deployment
                    //TouchDate = "02/05/2016";

                    //SetCookiesExpire();
                    Session["TouchList"] = null; // Making Touch list empty;

                    ////Added by Sohan
                    ////Need to send to ESB update method calls.
                    Session["LoginName"] = oUserLogin.UserName; // UserName
                    //Session["LoginName"] = LoginStatus.FirstName + " " + LoginStatus.LastName; // Display Name


                    //This is for TESTING
                    //string storeNum = Convert.ToString(LoginStatus.StoreNumber);                    
                    //var whto = oPRT.SearchTouch("dfdf", DateTime.Now, storeNum);


                    // DriverMobileApp.Models.PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                    ViewBag.Error = null;
                    return RedirectToAction("Index", "DashBoard");
                }
            }
            else
            {
                ViewBag.Error = "Invalid Login/ Password";
            }
            return View();
        }

        public ActionResult Logout()
        {
            ///Store Login info in AuditLog DB
            ///populating values in Auditlog class
            AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();


            //oAuditLogDetail.LogID = Request.Cookies["LogID"].Value;
            oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            oAuditLogDetail.Description = "Driver's logout has successfully added";
            oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.Logout.ToString();

            ///Storing log values in Log DB
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
            oActivityLog.AddActivityDetail(oAuditLogDetail);

            RemoveCookies(); //Cleaning cookies
            //ExpCookie(); 

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult GetLocalTime(string localDate, string localTime)
        {
            string[] localTimeCorrection = localTime.Split(':');
            if (localTimeCorrection[0].Trim() == "0")
            {
                localTimeCorrection[0] = "00";
            }
            if (localTimeCorrection[0].Trim().Length == 1)
            {
                localTimeCorrection[0] = "0" + localTimeCorrection[0].Trim();
            }
            if (localTimeCorrection[1].Trim().Length == 1)
            {
                localTime = localTimeCorrection[0] + ":0" + localTimeCorrection[1];
            }
            else
            {
                localTime = localTimeCorrection[0] + ":" + localTimeCorrection[1];
            }
            //System.Net.WebClient webClient = new System.Net.WebClient();

            //string getDateAndTime = webClient.DownloadString("http://www.worldweatheronline.com/feed/tz.ashx?key=570d7b5c5b110642132901&q=" + latitude + "," + longtitude + "&format=XML");

            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.LoadXml(getDateAndTime);
            //XmlNodeList parentNode = xmlDoc.GetElementsByTagName("time_zone");
            //XmlNode child = parentNode.Item(0);
            //string stDateTime = child.ChildNodes[0].InnerText;

            //string stDateTime = System.DateTime.Now.Date.ToString("MM/dd/yy") + " " + System.DateTime.Now.ToString("HH:mm");
            string stDateTime = localDate + " " + localTime;
            Session["localDateTime"] = Convert.ToDateTime(stDateTime);
            Session["latitude"] = string.Empty;
            Session["longtitude"] = string.Empty;
            stDateTime = localDate + " " + DateTime.ParseExact(localTime.Replace(":", ""), "HHmm", CultureInfo.CurrentCulture).ToString("h:mm tt");
            return Json(stDateTime, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Error(string strError)
        {
            StackTraceLog oStackTraceLog = new StackTraceLog();
            oStackTraceLog.ErrorMsg = strError;
            oStackTraceLog.StackTraceInfo = (string)System.Web.HttpContext.Current.Session["StackTrace"];
            System.Web.HttpContext.Current.Session["Err"] = null; //Making session value null

            bool cookieUserIDExists = UserID != null;

            if (cookieUserIDExists)
                oStackTraceLog.DriverID = oEnDeCryption.Decrypt(UserID);
            else
                oStackTraceLog.DriverID = null;

            bool cookieQORidExists = QORID != null;

            if (cookieQORidExists)
                oStackTraceLog.QorID = QORID;
            else
                oStackTraceLog.QorID = null;

            oStackTraceLog.IpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
            bool blResult = oAuditLog.addStackTrace(oStackTraceLog);

            return Json(JsonRequestBehavior.AllowGet);
        }

        public void ExpCookie()
        {

            bool cookieQORidExists = System.Web.HttpContext.Current.Request.Cookies["QORid"] != null;
            bool cookieLoginNameExists = System.Web.HttpContext.Current.Request.Cookies["LoginName"] != null;
            bool cookieLastLoginExists = System.Web.HttpContext.Current.Request.Cookies["LastLogin"] != null;
            bool cookieUserIDExists = System.Web.HttpContext.Current.Request.Cookies["UserID"] != null;
            bool cookiecurrentTouchTypeExists = System.Web.HttpContext.Current.Request.Cookies["currentTouchType"] != null;

            if (cookieQORidExists)
            {
                var expcurrentQORid = new HttpCookie("QORid");
                expcurrentQORid.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(expcurrentQORid);
            }
            if (cookieLoginNameExists)
            {
                var expcurrentLoginName = new HttpCookie("LoginName");
                expcurrentLoginName.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(expcurrentLoginName);
            }
            if (cookieLastLoginExists)
            {
                var expcurrentLastLogin = new HttpCookie("LastLogin");
                expcurrentLastLogin.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(expcurrentLastLogin);
            }
            if (cookieUserIDExists)
            {
                var expcurrentUserID = new HttpCookie("UserID");
                expcurrentUserID.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(expcurrentUserID);
            }
            if (cookiecurrentTouchTypeExists)
            {
                var expcurrentcurrentTouchType = new HttpCookie("currentTouchType");
                expcurrentcurrentTouchType.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(expcurrentcurrentTouchType);
            }
        }

        public ActionResult ErrorOnUrlTampar()
        {
            var ex = new Exception("Looks like you have updated URL, Please submit your request without updating URL.");
            HandleErrorInfo oHandleErrorInfo = new HandleErrorInfo(ex, "Home", "Index");
            return View("Error", oHandleErrorInfo);
        }
    }
}
