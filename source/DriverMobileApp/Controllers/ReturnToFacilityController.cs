﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DriverMobileApp.Models;

namespace DriverMobileApp.Controllers
{
    public class ReturnToFacilityController : BaseAuthenticatedController
    {
        //
        // GET: /ReturnToFacility/
        EnDeCryption oEnDeCryption = new EnDeCryption();


        public ActionResult Index()
        {
            string TouchType = CurrentTouchType;
            Int32 QORid = Convert.ToInt32(QORID);

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            List<TouchData> lstModel = oPRbusinesslogic.GetTouchInfo(TouchType.ToString().Trim(), QORid);
            TouchDataWithCustomerInfo model = new TouchDataWithCustomerInfo();
            model.TouchData = lstModel.Count > 0 ? lstModel[0] : new TouchData();
            model.CustomerInfo = oPRbusinesslogic.GetCustomerInfo(QORid);
            model.POSData = oPRbusinesslogic.GetPOSitem(QORid);

            ViewBag.TouchType = TouchType;

            return View(model);
        }

        public ActionResult SubmitUnitNo(string UnitNo, Int32 QORid)
        {

            try
            {
                PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                if (Convert.ToString(oPRbusinesslogic.GetUnitName(QORid)) == UnitNo.Trim())
                {
                    ///Store Login info in AuditLog DB
                    ///populating values in Auditlog class
                    AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();


                    oAuditLogDetail.LogID = LogID;
                    oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                    oAuditLogDetail.Description = LoginName + " has confirmed POS/ visual inspection at " + System.DateTime.Now.ToString() + " for the QORid" + QORid.ToString();
                    oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.POS_Inspection.ToString();

                    ///Storing log values in Log DB
                    AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
                    oActivityLog.AddActivityDetail(oAuditLogDetail);

                    ///End store Login info in AuditLog DB
                    ///
                    return Content("true");
                }
                else
                    return Content("false");
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return Content("false");
            }
        }

        [HttpPost]
        public ActionResult ReadyToStartConfirm()
        {
            ///Store Login info in AuditLog DB
            ///populating values in Auditlog class
            AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();


            oAuditLogDetail.LogID = LogID;
            oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
            oAuditLogDetail.Description = LoginName + " has confirmed start time at " + System.DateTime.Now.ToString();
            oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.ReadyToStart.ToString();

            ///Storing log values in Log DB
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
            oActivityLog.AddActivityDetail(oAuditLogDetail);

            ///End store Login info in AuditLog DB
            ///

            return Content("true");
        }

    }
}
