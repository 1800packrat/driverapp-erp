﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DriverMobileApp.Models;
using System.Configuration;
using System.Web.Script.Serialization;
using PR.Entities;
using DriverMobileApp.Models.Email;
using AuditLog.DBStructure;
using System.Text;
using PR.ValidationHandler;

namespace DriverMobileApp.Controllers
{
    public class TouchCompleteController : BaseAuthenticatedController
    {
        EnDeCryption oEnDeCryption = new EnDeCryption();
        AuditLogMaster oAuditLogMaster = new AuditLogMaster();

        public ActionResult Index(string QORid, string StopType, string TouchType, string TouchStopId)
        {
            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
            oAuditLogMaster = oAuditLog.RetLogMaster(LogID);
            ViewBag.StopType = oAuditLogMaster.StopType.Trim();

            ViewBag.QORid = QORid;
            ViewBag.StopType = StopType;
            ViewBag.TouchType = TouchType;
            ViewBag.TouchStopId = TouchStopId;
            return View();
        }

        [ValidateAntiForgeryToken]
        public ActionResult CompleteTouch(string unitNo, string remark)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
            EnDeCryption oEnDeCryption = new EnDeCryption();

            try
            {
                if (unitNo.AllowAlphaNumeric())
                {
                    AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                    AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster()
                    {
                        LoginName = Session["LoginName"].ToString()  // Added For ESB method calls.
                    };
                    oAuditLogMaster = oAuditLog.RetLogMaster(LogID);

                    PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                    bool valid = false;
                    int qorid = -1;
                    int.TryParse(oAuditLogMaster.Qorid, out qorid);

                    bool unitNoValidButNotMovedIn = false;

                    bool isThisTruckFromDiferentFacilityForDE = false;
                    string unitFacilityName = string.Empty;

                    if (oAuditLogMaster.StopType == "WeighStati" || oAuditLogMaster.StopType == "WeighStation")
                        valid = true;
                    else if (oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.DE)
                    {
                        PRTransiteBusinessLogic oPRTransiteBL = new PRTransiteBusinessLogic();
                        int touchStopID = 0;
                        int.TryParse(oAuditLogMaster.RefID, out touchStopID);
                        DriverTouch currentStop = new DriverTouch();
                        if (touchStopID > 0)
                        {
                            currentStop = oPRTransiteBL.RetieveCurrentTouchByStopId(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate), touchStopID);
                            valid = oPRbusinesslogic.GetUnitByUnitName(qorid, unitNo.Trim(), out isThisTruckFromDiferentFacilityForDE, currentStop.OriginAddress.SLLocCode) > 0;

                            if (isThisTruckFromDiferentFacilityForDE && !string.IsNullOrEmpty(currentStop.OriginAddress.SLLocCode))
                            {
                                unitFacilityName = oPRbusinesslogic.GetFacilityNameByLocCode(currentStop.OriginAddress.SLLocCode.ToString());
                            }
                        }
                    }
                    else
                    {
                        if (oTransiteBusinessLogic.IsQuoteMovedIn(qorid))
                        {
                            //Rental
                            if (Convert.ToString(oPRbusinesslogic.GetUnitName(qorid)) == unitNo.Trim().ToUpper())
                                valid = true;
                            else
                                valid = false;
                        }
                        else
                        {
                            //Quote
                            using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
                            {
                                oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                                var TouchInfo = (from _TouchInfo in oDB.TouchInformations
                                                 where _TouchInfo.QORId == qorid && _TouchInfo.TouchType == "DE"
                                                 select _TouchInfo).FirstOrDefault();

                                if (TouchInfo == null)
                                {
                                    throw new Exception("This quote DE is not yet completed in Driver App / SiteLink, Before you complete this touch please movein.");
                                }
                                else if (unitNo == TouchInfo.ContainerNo)//check unit no with DE
                                {
                                    unitNoValidButNotMovedIn = true;
                                    valid = true;
                                }
                                else
                                    valid = false;
                            }
                        }
                    }


                    if (valid)
                    {
                        // Ctearing instant for AuditLogMaster for adding Start Date And Time
                        AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();
                        oAuditLogMaster.LogID = LogID; //Passing existing LogID
                        oAuditLogMaster.ScheduledEndTime = System.DateTime.Now.ToString(); // Adding current date and Time
                        string _LogID = oActivityLog.AddActivityMaster(oAuditLogMaster);


                        ///Store Login info in AuditLog DB
                        ///populating values in Auditlog class

                        AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();

                        oAuditLogDetail.LogID = LogID;
                        oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                        oAuditLogMaster.DriverID = long.Parse(oEnDeCryption.Decrypt(UserID));
                        oAuditLogDetail.Description = LoginName + " has confirmed end time at " + System.DateTime.Now.ToString();
                        oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.CompleteTouch.ToString();

                        ///Storing log values in Log DB
                        oActivityLog.AddActivityDetail(oAuditLogDetail);

                        // Fetching order type Local/LDM -------------------------------------------------------------------------------------
                        using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
                        {
                            oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                            int intQorId = Convert.ToInt32(oAuditLogMaster.Qorid);

                            var TouchInfo = (from _TouchInfo in oDB.TouchInformations
                                             where _TouchInfo.QORId == intQorId && _TouchInfo.TouchType == oAuditLogMaster.TransiteTouchType
                                             select _TouchInfo).FirstOrDefault();
                            // End fetching order type Local/LDM -------------------------------------------------------------------------------------

                            // Same for LDM and Local now.
                            if (oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.DE || oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.RE)
                            {
                                msg = oTransiteBusinessLogic.CompleteDE_RE_touch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate, remark);
                            }
                            else
                            {
                                msg = oTransiteBusinessLogic.CompleteTouch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate, unitNoValidButNotMovedIn, remark);
                            }

                            //if (!(TouchInfo.StarsUnitId > 0))
                            //{
                            //    //Local order
                            //    if (oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.DE || oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.RE)
                            //    {
                            //        msg = oTransiteBusinessLogic.CompleteDE_RE_touch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate);
                            //    }
                            //    else
                            //    {
                            //        msg = oTransiteBusinessLogic.CompleteTouch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate, unitNoValidButNotMovedIn);
                            //    }
                            //}
                            //else
                            //{
                            //    //LDM order
                            //    //TBD - Rajib Basu
                            //    if (CurrentTouchType == PR_TouchType_Constant.DE || CurrentTouchType == PR_TouchType_Constant.RE)
                            //    {
                            //        msg = oTransiteBusinessLogic.CompleteDE_RE_touch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate);
                            //    }
                            //    else
                            //    {
                            //        msg = oTransiteBusinessLogic.CompleteTouch(oAuditLogMaster, unitNo, LogID, oEnDeCryption.Decrypt(UserID), TouchDate, unitNoValidButNotMovedIn);
                            //    }
                            //}

                            //saving remarks to db
                            if (remark != string.Empty)
                            {
                                TouchInfo.Remarks = remark;
                                oDB.SaveChanges();
                            }

                            //Since driver is providing container number we have to save that info and use it for future use
                            //We are executing this method not only for DE but also for UnitNo Valid but not moved in.
                            if ((oAuditLogMaster.TransiteTouchType == PR_TouchType_Constant.DE || unitNoValidButNotMovedIn) && oAuditLogMaster.StopType == "Pickup")
                                oTransiteBusinessLogic.UpdateUnitNumberForDE(oAuditLogMaster, unitNo);

                            bool isTouchCompleted = oDB.TouchInformations.Any(t => t.QORId == intQorId && oAuditLogMaster.TransiteTouchType == t.TouchType && t.Status == "Completed");
                            if (isTouchCompleted == true)
                            {
                                //As per Kelly request: For now we are sending this email to all touches but not required for all touches, this is required only for DE & RE
                                string strEmailMsg = SendEmail(Convert.ToInt32(QORID), oAuditLogMaster, unitNo, isThisTruckFromDiferentFacilityForDE, unitFacilityName, TouchInfo.Remarks);

                                if (strEmailMsg != "Success")
                                {
                                    flag = false;
                                    msg = strEmailMsg;
                                }
                            }
                        }

                        if (msg == "" || msg == "Success")
                        {
                            flag = true;

                            if (Request.Cookies["LogID"] != null)
                            {
                                HttpCookie LogIDCookie = new HttpCookie("LogID");
                                HttpCookie QORidCookie = new HttpCookie("QORid");
                                HttpCookie currentTouchType = new HttpCookie("currentTouchType");
                                LogIDCookie.Expires = DateTime.Now.AddDays(-1d);
                                QORidCookie.Expires = DateTime.Now.AddDays(-1d);
                                currentTouchType.Expires = DateTime.Now.AddDays(-1d);
                                Response.Cookies.Add(LogIDCookie);
                                Response.Cookies.Add(QORidCookie);
                                Response.Cookies.Add(currentTouchType);
                            }
                        }
                        else
                            flag = false;
                        //return Content("true");
                    }
                    else
                    {
                        flag = false;
                        msg = "InvalidUnitNumber";
                        //return Content("false");
                    }
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                bool retVal = oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                flag = false;
                msg = ex.Message;
                //return Content("true");
            }

            return Content(js.Serialize(new
            {
                success = flag,
                msg = msg
            }));
        }

        [ValidateAntiForgeryToken]
        private bool checkunitNoValidButNotMovedIn(int qorid, string unitNo)
        {
            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            bool unitNoValidButNotMovedIn = false;
            if (!oTransiteBusinessLogic.IsQuoteMovedIn(qorid))
            {
                //Quote
                using (var oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
                {
                    oDB.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    var TouchInfo = (from _TouchInfo in oDB.TouchInformations
                                     where _TouchInfo.QORId == qorid && _TouchInfo.TouchType == "DE"
                                     select _TouchInfo).FirstOrDefault();

                    if (unitNo == TouchInfo.ContainerNo)//check unit no with DE
                    {
                        unitNoValidButNotMovedIn = true;
                    }
                }
            }
            return unitNoValidButNotMovedIn;
        }


        public string SendEmail(Int32 QorID, AuditLogMaster oAuditLogMaster, string unitNo, bool isThisTruckFromDiferentFacilityForDE, string unitFacilityName, string remark)
        {
            return TouchComplete_SendEmail(QorID, oAuditLogMaster, unitNo, oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate), CurrentTouchType, Session["localDateTime"].ToString(), LoginName,
                                    isThisTruckFromDiferentFacilityForDE, unitFacilityName, remark);
        }

        #region This method has been moved to Models/Email/EmailLogic.cs

        ///// <summary>
        ///// Method to send email for touch complete operation - HomeGrown
        ///// </summary>
        ///// <param name="QorID"></param>
        ///// <param name="oAuditLogMaster"></param>
        ///// <param name="unitNo"></param>
        ///// <param name="UserID"></param>
        ///// <param name="TouchDate"></param>
        ///// <param name="currentTouchType"></param>
        ///// <param name="localDateTime"></param>
        ///// <param name="driverName"></param>
        ///// <param name="isThisTruckFromDiferentFacilityForDE"></param>
        ///// <param name="unitFacilityName"></param>
        ///// <param name="remark"></param>
        ///// <returns></returns>
        //public string TouchComplete_SendEmail(Int32 QorID, AuditLogMaster oAuditLogMaster, string unitNo, string UserID, DateTime TouchDate, string currentTouchType, string localDateTime, string driverName, bool isThisTruckFromDiferentFacilityForDE, string unitFacilityName, string remark)
        //{
        //    try
        //    {
        //        AppEmail oAppEmail = new AppEmail();
        //        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //        PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
        //        CustomerInfo oCustomerInfo = null;
        //        try
        //        {
        //            oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(QorID);
        //        }
        //        catch (Exception ex)
        //        {
        //            (new PRTransiteBusinessLogic()).Error(ex.Message.ToString(), ex.StackTrace.ToString());
        //        }

        //        UnitInfo oUnitInfo = null;

        //        try
        //        {
        //            oUnitInfo = oPRbusinesslogic.GetUnitInfo(QorID);
        //        }
        //        catch { }

        //        SiteInfo oSiteInfo = null;

        //        try { oSiteInfo = oPRbusinesslogic.GetFacilityInfo(QorID); }
        //        catch { }

        //        DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(UserID, TouchDate, currentTouchType, QorID.ToString(), oAuditLogMaster.StopType);

        //        string strfacilityEmailId = oPRbusinesslogic.GetFacilityEmailId(Convert.ToInt32(QorID));
        //        string touchcompleteEmailIds = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" + strfacilityEmailId;

        //        //oAppEmail.EmailTo = "gurupratap.chilakala@spectraforce.com";
        //        //oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();

        //        //// Added by Sohan
        //        //// below 2 lines are Commented for testing purpose. 
        //        //oAppEmail.EmailTo = touchcompleteEmailIds;
        //        // if (oSiteInfo != null) oAppEmail.EmailFrom = oSiteInfo.LegalName.ToString().Replace(" ", "_") + "@1800packrat.com";

        //        oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString(); //Testing only
        //        oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString(); //Testing only


        //        if (oSiteInfo != null) oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Touch Complete Confirmation";

        //        // Added by Sohan
        //        // below 3 lines are Commented for testing purpose. 
        //        oAppEmail.Attachment = GetTouchPDF(oAuditLogMaster.LogID);
        //        oAppEmail.ReportFormat = "pdf";
        //        oAppEmail.AttachedFileName = "TouchTicket";

        //        oAppEmail.EmailCc = "";

        //        StringBuilder emailBody = new StringBuilder();
        //        emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //        emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //        emailBody.Append("<head><title>1800PACKRAT.COM | Touch Complete Confirmation</title></head>");
        //        emailBody.Append("<body>");

        //        emailBody.Append("<br />Touch Complete Information<br /><br />");
        //        if (isThisTruckFromDiferentFacilityForDE && !string.IsNullOrEmpty(unitFacilityName))
        //        {
        //            oAppEmail.EmailTo = touchcompleteEmailIds + ";" + ConfigurationManager.AppSettings["inventoryEmail"].ToString(); //Added InventoryEmail on 19-10-2016 as per LL-303 bug id
        //            emailBody.Append("<br /><div style='color: red;'>Inventory Alert:  " + unitFacilityName + " Unit delivered to " + oSiteInfo.LegalName.ToString() + " customer</div><br /><br />");
        //        }

        //        if (checkunitNoValidButNotMovedIn(QorID, unitNo) && currentTouchType != "DE")
        //        {
        //            emailBody.Append("<br /><div style='color: red;'>Not Auto Completed. DE touch not moved in.</div><br /><br />");
        //        }

        //        emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

        //        if (oCustomerInfo != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.FirstName + " " + oCustomerInfo.LName + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Billing Address</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Addr1 + "<br />" +
        //                        oCustomerInfo.Addr2 + "<br />" + oCustomerInfo.City + (String.IsNullOrWhiteSpace(oCustomerInfo.City) ? "" : ", ") + oCustomerInfo.Region + (String.IsNullOrWhiteSpace(oCustomerInfo.Region) ? "" : ", ") + "<br />" + oCustomerInfo.Country + " - " + oCustomerInfo.PostalCode + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Phone No</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oCustomerInfo.Phone + ", Alt Phone No " + oCustomerInfo.PhoneAlt + "</td>");
        //            emailBody.Append("</tr>");
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID / Name</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + UserID + " " + driverName + "</td>");
        //        emailBody.Append("</tr>");

        //        string purchaseOrder = oPRbusinesslogic.GetPRGQuoteOrderLogData(QorID);
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Purchase Order</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (String.IsNullOrWhiteSpace(purchaseOrder) ? "N/A" : purchaseOrder) + "</td>");
        //        emailBody.Append("</tr>");

        //        if (oDriverTouch != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Origin Address</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.OriginAddress.AddressLine1 + "<br />" +
        //            oDriverTouch.OriginAddress.AddressLine2 + "<br />" + oDriverTouch.OriginAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.City) ? "" : ", ") + oDriverTouch.OriginAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.OriginAddress.State) ? "" : ", ") + " - " + oDriverTouch.OriginAddress.Zip + "</td>");

        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Destination Address</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.DestinationAddress.AddressLine1 + "<br />" +
        //            oDriverTouch.DestinationAddress.AddressLine2 + "<br />" + oDriverTouch.DestinationAddress.City + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.City) ? "" : ", ") + oDriverTouch.DestinationAddress.State + (String.IsNullOrWhiteSpace(oDriverTouch.DestinationAddress.State) ? "" : ", ") + " - " + oDriverTouch.DestinationAddress.Zip + "</td>");

        //            emailBody.Append("</tr>");
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>QORID</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + QorID.ToString() + "</td>");
        //        emailBody.Append("</tr>");

        //        string tmpUnitNo = unitNo;
        //        if (oUnitInfo != null && oUnitInfo.Tables.Count > 0 && oUnitInfo.Tables[0].Rows.Count > 0)
        //        {
        //            tmpUnitNo = (string.IsNullOrWhiteSpace(unitNo) ? oUnitInfo.Tables[0].Rows[0]["UnitName"].ToString() : unitNo);
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Delivered Unit Number</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + tmpUnitNo + "</td>");
        //        emailBody.Append("</tr>");

        //        if (oUnitInfo != null && oUnitInfo.Tables.Count > 0 && oUnitInfo.Tables[0].Rows.Count > 0)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Unit Description</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oUnitInfo.Tables[0].Rows[0]["UnitDescription"].ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }
        //        if (oSiteInfo != null)
        //        {
        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Facility Name</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LegalName.ToString() + "</td>");
        //            emailBody.Append("</tr>");

        //            emailBody.Append("<tr>");
        //            emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Location Code</td>");
        //            emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + oSiteInfo.LocationCode.ToString() + "</td>");
        //            emailBody.Append("</tr>");
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Touch Type</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + currentTouchType + "</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Date</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + localDateTime + "</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-right:#2296ca solid 1px;'>Comments</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-right:#ccc solid 1px;'>" + remark + "</td>");
        //        emailBody.Append("</tr>");
        //        emailBody.Append("</table><br /><br />");

        //        emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

        //        oAppEmail.EmailBody = emailBody.ToString();

        //        //Inserting Email Log
        //        Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
        //        el.ApplicationName = "Driver App";
        //        el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //        el.User = UserID;
        //        el.StartTime = DateTime.Now;
        //        el.Subject = oSiteInfo.LegalName.ToString() + " | Touch Complete Confirmation"; ;
        //        el.ToEmailIDs = touchcompleteEmailIds;
        //        el.FromEmailID = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" + strfacilityEmailId;
        //        el.CCEmailIDs = null;
        //        el.MethodName = "CompleteTouch";
        //        el.EmailTypeID = 1;
        //        el.QORID = QorID;
        //        el.TouchType = currentTouchType;
        //        el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

        //        var LogId = LogEmails.InsertLog(el);
        //        if (oAppEmail.SendEMailWithAttachment() == "Success")
        //        {
        //            //updating endtime in email log
        //            Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
        //            eml.PK_ID = LogId;
        //            eml.EndTime = DateTime.Now;
        //            LogEmails.UpdateLog(eml);
        //            return "Success";
        //        }
        //        else
        //            return "Unable to send Email";
        //    }
        //    catch (Exception ex)
        //    {
        //        PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
        //        oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
        //        return ex.Message + " , " + "Unable to send Email";
        //    }
        //}

        //public Byte[] GetTouchPDF(string LogID = null)
        //{
        //    AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

        //    oAuditLogMaster = oActivityLog.RetLogMaster(LogID);

        //    DriverMobileApp.Models.CustomerSignature oCustomerSignature = new Models.CustomerSignature();

        //    string en64PDF = string.Empty;
        //    PR.UtilityLibrary.ERP_Enums.eQTType qtType = new PR.UtilityLibrary.ERP_Enums.eQTType();
        //    switch (oAuditLogMaster.TransiteTouchType)
        //    {
        //        case "CC":
        //            qtType = PR.UtilityLibrary.ERP_Enums.eQTType.CurbToCurb;
        //            break;
        //        case "RF":
        //            qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseFull;
        //            break;
        //        case "RE":
        //            qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseEmpty;
        //            break;
        //        case "DE":
        //            qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbEmpty;
        //            break;
        //        case "DF":
        //            qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbFull;
        //            break;
        //    }

        //    en64PDF = oCustomerSignature.GetTouchInfo(Convert.ToInt32(oAuditLogMaster.Qorid), qtType);

        //    byte[] bytPDF = System.Convert.FromBase64String(en64PDF);

        //    return bytPDF;
        //    //return File(bytPDF, "application/pdf", "TouchData.pdf");

        //}

        #endregion

        /// <summary>
        /// Method to send email for touch complete operation
        /// </summary>
        /// <param name="mainReasonType"></param>
        /// <param name="drpOption"></param>
        /// <param name="drpCustReq"></param>
        /// <param name="oSkipTouchInfo"></param>
        /// <param name="qorid"></param>
        /// <param name="TouchStopId"></param>
        /// <param name="TouchType"></param>
        /// <param name="StopType"></param>
        /// <param name="MethodName"></param>
        public string TouchComplete_SendEmail(Int32 QorID, AuditLogMaster oAuditLogMaster, string unitNo, string UserID, DateTime TouchDate, string currentTouchType, string localDateTime, string driverName, bool isThisTruckFromDiferentFacilityForDE, string unitFacilityName, string remark)
        {
            bool isUnitNoValidButNotMovedIn = checkunitNoValidButNotMovedIn(QorID, unitNo);

            EmailLogic objEmail = new Models.Email.EmailLogic();
            return objEmail.TouchComplete_SendEmail(QorID, oAuditLogMaster, unitNo, UserID, TouchDate, currentTouchType, localDateTime, driverName, isThisTruckFromDiferentFacilityForDE, unitFacilityName, remark, isUnitNoValidButNotMovedIn);
        }
    }
}
