﻿using DriverMobileApp.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using PR.LocalLogisticsSolution.Model;
using PR.LocalLogisticsSolution.Interfaces;
using PR.ValidationHandler;

namespace DriverMobileApp.Controllers
{
    public class ExceptionTouchController : BaseAuthenticatedController
    {
        PRTransiteBusinessLogicExceptionTouch oPRT;
        PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        EnDeCryption oEnDeCryption = new EnDeCryption();
        PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();

        public ExceptionTouchController(ISitelinkRepository ISiteLinkrepository, ITouchService ITouchService)
        {
            oPRT = new PRTransiteBusinessLogicExceptionTouch(ISiteLinkrepository, ITouchService);
        }

        public ActionResult Index()
        {
            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            CurrentSiteInfo = oPRbusinesslogic.GetFacilityInfoByStoreNumber(StoreNumber);
            return View();
        }

        //
        // GET: /Search/
        [HttpGet]
       
        public PartialViewResult GetAllTouches(string SearchTerm)
        {
            List<Touch> searchResult = new List<Touch>();
            ViewBag.Error = string.Empty;

            try
            {
                SearchTerm = !string.IsNullOrWhiteSpace(SearchTerm) ? SearchTerm.ToLower() : string.Empty;

                if (SearchTerm.AllowAlphaNumForExTouchSeardh())
                {
                    searchResult = oPRT.SearchTouch(SearchTerm, dtTouchDate, CurrentSiteInfo);
                    Response.Cookies.Add(new HttpCookie("SearchTerm", SearchTerm));
                    Session["SearchTouchList"] = searchResult;
                }
                else
                {
                    ViewBag.Error = "Please enter valid  Unit #, Name, Phone # or Address to search.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                ViewBag.Error = ex.Message;
            }

            return PartialView("_TouchDetails", searchResult);
        }

        public ActionResult TouchInformation(string TouchType, Int32 QORid)
        {
            ViewBag.SlError = null;

            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();

            Touch oTouch = oPRT.RetrieveSearchedToucheByQorid(dtTouchDate, IntLoadId, StoreNumber, QORid, CurrentSiteInfo, TouchType);

            return View(oTouch);
        }

        [ValidateAntiForgeryToken]
        public JsonResult OptimizeTouch(int? TouchId, int QorId, string TouchType)
        {
            Touch exceptionTouch = new PR.LocalLogisticsSolution.Model.Touch();
            bool success = true;
            string msg = string.Empty;
            bool canHeProceedWithExceptionTouch = false;
            try
            {
                //IF next stop is Warehouse access stop then, driver should not proceed with exception touch.
                canHeProceedWithExceptionTouch = oTransiteBusinessLogic.IsNextTouchWareHouseTouch(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate)) == true ? false : true;

                if (canHeProceedWithExceptionTouch)
                {
                    /*
                     * 1. Segregate the Touch into stops 
                     * 2. Get the Origin and destination addresses and get timings by using google map api
                     * 3. Here the origin and destinations will changes accordingly where we are impending this new touch with stops
                     * 4. Save the time and stops data the db
                     * 
                     */
                    Touch oTouch = oPRT.RetrieveSearchedToucheByQorid(dtTouchDate, IntLoadId, StoreNumber, QorId, CurrentSiteInfo, TouchType);

                    if (oTouch.TouchId <= 0)
                    {
                        oTouch.ScheduledDate = LocalDateTime;
                    }

                    //This touch should be exceptional touch and make IsExceptionalTouch flag to true
                    oTouch.IsExceptionalTouch = true;

                    //SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfoByStoreNumber(StoreNumber);

                    //oPRT.AddExceptionalTouchToExitingRoute(LoadId, -1, -1, oTouch, oSiteInfo);
                    exceptionTouch = oPRT.AddExceptionalTouchToExitingRoute(IntLoadId, 0, 0, oTouch, CurrentSiteInfo);

                    //Here need to make it null other wise, in the next page this list is not updating and it cause for an null exception
                    Session["TouchList"] = null;
                }
                //else
                //{
                //    msg = "He can not start this touch because, you are in middle of other touch. Please skip or complete Warehouse access touch and then proceed with this touch.";
                //}
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                success = false;
                msg = "Error Occurred while starting this exceptional touch. Please contact OM and then continue or Skip this touch and then continue. <br/>Error: " + ex.Message.ToString();
            }

            var rtTouch = new
            {
                success = success,
                canHeProceedWithExceptionTouch = canHeProceedWithExceptionTouch,
                errorMsg = msg,
                TouchId = exceptionTouch.TouchId,
                TouchType = exceptionTouch.TouchType,
                StopId = exceptionTouch.PickupStop != null ? exceptionTouch.PickupStop.TouchStopId : 0,
                StopType = "Pickup",
                RedirectURL = DriverMobileApp.Helper.UrlHelperExtensions.EncryptAction(null, "Index", "TouchInfo", new { TouchType = exceptionTouch.TouchType, QORid = QorId, StopType = "Pickup" })
            };
            //We are redirecing throuh TouchInfo page due to some issues. In future we may enable this to save user clicks
            //saveDetailstoAudit(exceptionTouch.TouchType, QorId, "Pickup", exceptionTouch);

            return Json(rtTouch, JsonRequestBehavior.AllowGet);
            //add exception handling mechanism
        }

        //public JsonResult CanDriverStartThisExceptionTouch(Touch exceptionTouch)
        //{
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    bool flag = true;
        //    string msg = string.Empty;
        //    bool canHeProceedWithExceptionTouch = false;

        //    try
        //    {
        //        //IF next stop is Warehouse then, driver should not proceed with exce stop.
        //        canHeProceedWithExceptionTouch = oTransiteBusinessLogic.IsNextTouchWareHouseTouch(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate)) == true ? false : true;
        //    }
        //    catch (Exception ex)
        //    {
        //        oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
        //        flag = false;
        //        msg = ex.Message;
        //    }

        //    return Json(new { success = flag, canHeProceedWithExceptionTouch = canHeProceedWithExceptionTouch, errmsg = msg });
        //}

        //private void saveDetailstoAudit(string TouchType, int QorId, string StopType, Touch exceptionTouch)
        //{
        //    // Save Driver info in  local database for further referance

        //    AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();
        //    AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster();
        //    AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();

        //    oAuditLogMaster.ApplicationName = ConfigurationManager.AppSettings["AppName"];
        //    oAuditLogMaster.DriverID = Convert.ToInt64(oEnDeCryption.Decrypt(UserID));
        //    oAuditLogMaster.Qorid = QorId.ToString();
        //    oAuditLogMaster.TransiteTouchType = TouchType;
        //    oAuditLogMaster.ContainerNo = (exceptionTouch != null ? exceptionTouch.UnitNumber : "");
        //    if (exceptionTouch.PickupStop.TouchStopId > 0)
        //        oAuditLogMaster.RefID = Convert.ToString(exceptionTouch.PickupStop.TouchStopId);
        //    //oAuditLogMaster.ScheduledStartTime = Convert.ToString(model.DriverTouch.ScheduledStartTime);
        //    //oAuditLogMaster.ScheduledEndTime = Convert.ToString(model.DriverTouch.ScheduledEndTime);
        //    oAuditLogMaster.OrderNo = (exceptionTouch != null ? exceptionTouch.OrderNumber : "");
        //    oAuditLogMaster.StopType = (exceptionTouch != null ? StopType : "");
        //    if (exceptionTouch.DestAddress.AddressType != PR.UtilityLibrary.PREnums.AddressType.Warehouse)
        //    {
        //        //Need to add it later
        //        //if (exceptionTouch.TouchData != null)
        //        //    oAuditLogMaster.EditIdentifier = Convert.ToString(model.TouchData.EditIdentifier);
        //        //else
        //            oAuditLogMaster.EditIdentifier = string.Empty;
        //    }
        //    else
        //    {
        //        oAuditLogMaster.EditIdentifier = string.Empty;
        //    }

        //    TouchInfoLog oTouchInfoLog = new TouchInfoLog();

        //    string _LogID = oAuditLog.AddActivityMaster(oAuditLogMaster);
        //    if (_LogID != "Error")
        //    {
        //        //Response.Cookies.Add(new HttpCookie("LogID", _LogID));
        //        LogID = _LogID;
        //        Response.Cookies["LogID"].Expires = DateTime.Now.AddDays(1);

        //        oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
        //        if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
        //        {
        //            oTouchInfoLog = new TouchInfoLog();
        //            oTouchInfoLog.LogID = _LogID;
        //            oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);
        //        }

        //    }
        //}
    }
}
