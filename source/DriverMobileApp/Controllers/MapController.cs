﻿using System;
using System.Web.Mvc;
using AuditLog.DBStructure;
using DriverMobileApp.Models;

namespace DriverMobileApp.Controllers
{
    public class MapController : BaseAuthenticatedController
    {
        //
        // GET: /Map/

        public ActionResult Index(string latitude, string longitude, string toaddress)
        {
            ViewBag.Latitude = latitude;
            ViewBag.Longitude = longitude;
            ViewBag.ToAddress = toaddress;

            ViewBag.TouchType = CurrentTouchType;

            string _LogID = LogID;

            //Update Touch info log table
            AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
            TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
            if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                oTouchInfoLog = new TouchInfoLog();
            else
                oTouchInfoLog.LogID = _LogID;

            ViewBag.TouchInfoLog = oTouchInfoLog;

            return View();
        }

        public ActionResult ReachedDestination()
        {
            try
            {
                string _LogID = LogID;

                //Update Touch info log table
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                TouchInfoLog oTouchInfoLog = oAuditLog.retTouchActionStatus(_LogID);
                if (oTouchInfoLog == null || (oTouchInfoLog != null && string.IsNullOrEmpty(oTouchInfoLog.LogID)))
                    oTouchInfoLog = new TouchInfoLog();
                else
                    oTouchInfoLog.LogID = _LogID;

                oTouchInfoLog.ReachedDestination = true;

                oAuditLog.UpdateTouchInfoLog(oTouchInfoLog);

                return Content("true");
            }
            catch(Exception ex) {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                return Content("false");
            }
        }
    }
}
