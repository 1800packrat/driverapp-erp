﻿using System;
using System.Web.Mvc;
using DriverMobileApp.Models.Signature;
using DriverMobileApp.Models;
using System.IO;
using AuditLog.DBStructure;
using System.Drawing;


namespace DriverMobileApp.Controllers
{
    public class SignatureController : BaseAuthenticatedController
    {  
        public ActionResult Index()
        {
            PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
            CustomerInfo oCustomerInfo = new CustomerInfo();

            AuditLogMaster oAuditLogMaster = new AuditLogMaster();
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

            oAuditLogMaster = oActivityLog.RetLogMaster(LogID);




            Int32 intQORid = Convert.ToInt32(oAuditLogMaster.Qorid);
            oCustomerInfo = oPRbusinesslogic.GetCustomerInfo(intQORid);

            ViewBag.CustomerName = oCustomerInfo.FirstName + " " + oCustomerInfo.LName;

            return View();
        }

        [HttpPost]
        public ActionResult GenerateImage(string json, string CustomerName, string Remark, bool SameAsPayee)
        {
            CustomerSignature oCustomerSignature = new CustomerSignature();

            AuditLogMaster oAuditLogMaster = new AuditLogMaster();
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

            oAuditLogMaster = oActivityLog.RetLogMaster(LogID);


            SignatureToImage sigImg = new SignatureToImage();
            System.Drawing.Bitmap bp = sigImg.SigJsonToImage(json);


            MemoryStream ms = new MemoryStream();
            bp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            byte[] byteImage = ms.ToArray();
            Convert.ToBase64String(byteImage); //here you should get a base64 string 

            SignatureData oSignatureData = new SignatureData();
            oSignatureData.QORid = Convert.ToInt32(oAuditLogMaster.Qorid);
            oSignatureData.TouchType = oAuditLogMaster.TransiteTouchType;
            oSignatureData.Comments = (SameAsPayee) ? "Signed by " + CustomerName : Remark;
            oSignatureData.SignatureImage = Convert.ToBase64String(byteImage).ToString();

            oCustomerSignature.addSignatureInfo(oSignatureData);

            byte[] imageBytes = Convert.FromBase64String(oSignatureData.SignatureImage);
            MemoryStream ms1 = new MemoryStream(imageBytes);
            Image img = Image.FromStream(ms1);
            img.Save(@"d:\asd.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
            return Content("true");

        }

        public ActionResult GetTouchPDF()
        {
            AuditLogMaster oAuditLogMaster = new AuditLogMaster();
            AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog();

            oAuditLogMaster = oActivityLog.RetLogMaster(LogID); 

            DriverMobileApp.Models.CustomerSignature oCustomerSignature = new Models.CustomerSignature();

            string en64PDF = string.Empty;

            PR.UtilityLibrary.ERP_Enums.eQTType qtType = new PR.UtilityLibrary.ERP_Enums.eQTType();
            switch (oAuditLogMaster.TransiteTouchType)
            {
                case "CC":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.CurbToCurb;
                    break;
                case "RF":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseFull;
                    break;
                case "RE":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.ReturnToWarehouseEmpty;
                    break;
                case "DE":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbEmpty;
                    break;
                case "DF":
                    qtType = PR.UtilityLibrary.ERP_Enums.eQTType.WarehouseToCurbFull;
                    break;
            }

            en64PDF = oCustomerSignature.GetTouchInfo(Convert.ToInt32(oAuditLogMaster.Qorid), qtType);

            byte[] bytPDF = System.Convert.FromBase64String(en64PDF);

            return File(bytPDF, "application/pdf", "TouchData.pdf");
        }


    }
}
