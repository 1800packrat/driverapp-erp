﻿using System;
using System.Linq;
using System.Web.Mvc;
using DriverMobileApp.Models;
using DriverMobileApp.Models.Email;
using System.Configuration;
using System.Web.Script.Serialization;
using PR.Entities;
using System.Text;
using PR.ValidationHandler;

namespace DriverMobileApp.Controllers
{
    public class SkipTouchController : BaseAuthenticatedController
    {
        //
        // GET: /SkipTouch/
        PRTransiteBusinessLogic oPRTransiteBusinessLogic = new PRTransiteBusinessLogic();
        public ActionResult Index(string TouchType, string QORid, string StopType, string TouchStopId, string pg)
        {
            ViewBag.QORid = QORid;
            ViewBag.StopType = StopType;
            ViewBag.TouchType = TouchType;
            ViewBag.TouchStopId = TouchStopId;
            ViewBag.Pg = pg;


            SkipTouchInfo model = new SkipTouchInfo();
            SkipTouch oSkipTouch = new SkipTouch();
            model.mainReasonType = oSkipTouch.GetMainReasons();
            model.cNotAccessible = oSkipTouch.GetCnotAccessible();
            model.custRequest = oSkipTouch.GetCustRequest();
            model.comments = string.Empty;
            model.wContainer = false;
            model.containerNo = string.Empty;
            model.byVehicle = false;
            model.other = false;
            model.rain = false;
            model.mud = false;
            model.snow = false;
            model.byCustRequest = false;
            model.containerLocked = false;
            model.cOverWeight = oSkipTouch.GetCOverWeight();

            return View(model);
        }

        public ActionResult SkipCurrentWarehousetouch()
        {
            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
            EnDeCryption oEnDeCryption = new EnDeCryption();
            DriverTouch dt = oTransiteBusinessLogic.RetieveCurrentTouches(oEnDeCryption.Decrypt(UserID), Convert.ToDateTime(TouchDate));

            if (dt.DestinationAddress.AddressType == DriverMobileApp.Models.PR_AddressType.AddressType.Warehouse)
            {
                ViewBag.QORid = dt.QORID;
                ViewBag.StopType = dt.StopType;
                ViewBag.TouchType = dt.TouchType;
                ViewBag.TouchStopId = dt.TouchStopId;
                ViewBag.Pg = "exceptiontouch";


                SkipTouchInfo model = new SkipTouchInfo();
                SkipTouch oSkipTouch = new SkipTouch();
                model.mainReasonType = oSkipTouch.GetMainReasons();
                model.cNotAccessible = oSkipTouch.GetCnotAccessible();
                model.custRequest = oSkipTouch.GetCustRequest();
                model.comments = string.Empty;
                model.wContainer = false;
                model.containerNo = string.Empty;
                model.byVehicle = false;
                model.other = false;
                model.rain = false;
                model.mud = false;
                model.snow = false;
                model.byCustRequest = false;
                model.containerLocked = false;
                model.cOverWeight = oSkipTouch.GetCOverWeight();

                return View("Index", model);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WrongContainer(string mainReasonType, bool wContainer, string containerNo, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (containerNo.AllowAlphaNumeric() && mainReasonType.AllowIntegersOnly())
                {
                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.wContainer = wContainer;
                    oSkipTouchInfo.containerNo = containerNo;
                    oSkipTouchInfo.comments = comments;

                    flag = oSkipTouch.WCskipTouch(oEnDeCryption.Decrypt(UserID), mainReasonType, wContainer, containerNo, comments, qorid, TouchStopId, TouchType, StopType);
                    AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster();
                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, null, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "WrongContainer");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContainerNotAccessible(string mainReasonType, string CNAreason, bool byVehicle, bool rain, bool mud, bool snow, bool other1, bool other2, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (mainReasonType.AllowIntegersOnly() && CNAreason.AllowIntegersOnly())
                {
                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.byVehicle = byVehicle;
                    oSkipTouchInfo.other = (CNAreason == "1" ? other1 : other2);
                    oSkipTouchInfo.rain = rain;
                    oSkipTouchInfo.mud = mud;
                    oSkipTouchInfo.snow = snow;
                    oSkipTouchInfo.comments = comments;

                    flag = oSkipTouch.ContainerNotAccessible(oEnDeCryption.Decrypt(UserID), mainReasonType, CNAreason, byVehicle, rain, mud, snow, (CNAreason == "1" ? other1 : other2), comments, qorid, TouchStopId, TouchType, StopType);

                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, CNAreason, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "ContainerNotAccessible");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustNotReady(string mainReasonType, bool byCustRequest, bool containerLocked, string custRequest, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (mainReasonType.AllowIntegersOnly() && custRequest.AllowIntegersOnly())
                {

                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.byCustRequest = byCustRequest;
                    oSkipTouchInfo.containerLocked = containerLocked;
                    oSkipTouchInfo.comments = comments;


                    flag = oSkipTouch.CustNotReady(oEnDeCryption.Decrypt(UserID), mainReasonType, byCustRequest, containerLocked, custRequest, comments, qorid, TouchStopId, TouchType, StopType);
                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, null, custRequest, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "CustNotReady");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContainerOverWeight(string mainReasonType, string weight, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (mainReasonType.AllowIntegersOnly() && weight.AllowIntegersOnly())
                {
                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.comments = comments;


                    flag = oSkipTouch.ContainerOverWeight(oEnDeCryption.Decrypt(UserID), mainReasonType, weight, comments, qorid, TouchStopId, TouchType, StopType);
                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, weight, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "ContainerOverWeight");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());
                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CouldNotReachCustomer(string mainReasonType, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (mainReasonType.AllowIntegersOnly())
                {
                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.comments = comments;


                    flag = oSkipTouch.CouldntReachCust(oEnDeCryption.Decrypt(UserID), mainReasonType, comments, qorid, TouchStopId, TouchType, StopType);
                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, null, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "CouldNotReachCustomer");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OtherReason(string mainReasonType, string comments, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                if (mainReasonType.AllowIntegersOnly())
                {

                    SkipTouch oSkipTouch = new SkipTouch();
                    SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                    oSkipTouchInfo.comments = comments;


                    flag = oSkipTouch.OtherReason(oEnDeCryption.Decrypt(UserID), mainReasonType, comments, qorid, TouchStopId, TouchType, StopType, false, null);
                    bool blCompleteStop = CompleteTouch(TouchStopId);

                    if (flag && blCompleteStop)
                        SendEmail(mainReasonType, null, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "OtherReason");
                }
                else
                {
                    flag = false;
                    msg = "Please enter valid inputs, special characters are not allowed.";
                }
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }
            return Content(js.Serialize(new { success = flag, msg = msg }));

        }

        public RedirectToRouteResult CancelTouchForCancledOrRescheduledTouch(string TouchId, string qorid, string TouchStopId, string TouchType, string StopType)
        {
            EnDeCryption oEnDeCryption = new EnDeCryption();
            JavaScriptSerializer js = new JavaScriptSerializer();
            bool flag = true;
            string msg = "";

            try
            {
                SkipTouch oSkipTouch = new SkipTouch();
                SkipTouchInfo oSkipTouchInfo = new SkipTouchInfo();
                string comments = "This touch has been removed from the day";
                oSkipTouchInfo.comments = comments;
                string mainReasonType = "6";

                flag = oSkipTouch.OtherReason(oEnDeCryption.Decrypt(UserID), mainReasonType, comments, qorid, TouchStopId, TouchType, StopType, true, Convert.ToInt32(TouchId));
                bool blCompleteStop = CompleteTouch(TouchStopId, true, Convert.ToInt32(TouchId));

                if (flag && blCompleteStop)
                    SendEmail(mainReasonType, null, null, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, "CancelTouchForCancledOrRescheduledTouch");
            }
            catch (Exception ex)
            {
                PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                flag = false;
                msg = ex.Message;
                //return Content("true");
            }

            return RedirectToAction("Index", "Dashboard");
        }

        public bool CompleteTouch(string TouchStopId, bool? cancleTouch = null, int? touchId = null)
        {

            JavaScriptSerializer js = new JavaScriptSerializer();

            PRTransiteBusinessLogic oTransiteBusinessLogic = new PRTransiteBusinessLogic();
            EnDeCryption oEnDeCryption = new EnDeCryption();

            try
            {
                AuditLog.ActivityLog oAuditLog = new AuditLog.ActivityLog();
                AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster();
                oAuditLogMaster = oAuditLog.RetLogMaster(LogID);

                PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
                int qorid = -1;
                int.TryParse(oAuditLogMaster.Qorid, out qorid);

                AuditLog.ActivityLog oActivityLog = new AuditLog.ActivityLog(); // Ctearing instance of Audit log

                //AuditLog.DBStructure.AuditLogMaster oAuditLogMaster = new AuditLog.DBStructure.AuditLogMaster(); // Creation instant for AuditLogMaster for adding Start Date And Time
                oAuditLogMaster.LogID = LogID; //Passing existing LogID
                oAuditLogMaster.ScheduledEndTime = System.DateTime.Now.ToString(); // Adding current date and Time
                oAuditLogMaster.RefID = TouchStopId;
                string _LogID = oActivityLog.AddActivityMaster(oAuditLogMaster);


                ///Store Login info in AuditLog DB
                ///populating values in Auditlog class

                AuditLog.DBStructure.AuditLogDetail oAuditLogDetail = new AuditLog.DBStructure.AuditLogDetail();

                //string _LogID = Request.Cookies["LogID"].Value;
                oAuditLogDetail.LogID = LogID;
                oAuditLogDetail.IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                oAuditLogDetail.Description = LoginName + " has confirmed end time at " + System.DateTime.Now.ToString();
                oAuditLogDetail.Activity = AuditLog.AppConstant.ActivityType.Skip_Touch.ToString();

                ///Storing log values in Log DB
                oActivityLog.AddActivityDetail(oAuditLogDetail);

                if (cancleTouch != null && cancleTouch == true && touchId.HasValue)
                {
                    oPRTransiteBusinessLogic.CompleteTouchStopsForCancelTouches(oAuditLogMaster, TouchStatus.Skipped.ToString(), touchId.Value); // Completing stop
                }
                else
                    oPRTransiteBusinessLogic.CompleteStop(oAuditLogMaster, TouchStatus.Skipped.ToString()); // Completing stop
                return true;
            }
            catch (Exception ex)
            {
                oTransiteBusinessLogic.Error(ex.Message.ToString(), ex.StackTrace.ToString());

                return false;
            }
        }

        #region This method has been moved to Models/Email/EmailLogic.cs

        //Moved to Models/Email/EmailLogic.cs
        ///// <summary>
        ///// Method to send email for skip touch operation - HomeGrown
        ///// </summary>
        ///// <param name="mainReasonType"></param>
        ///// <param name="drpOption"></param>
        ///// <param name="drpCustReq"></param>
        ///// <param name="oSkipTouchInfo"></param>
        ///// <param name="qorid"></param>
        ///// <param name="TouchStopId"></param>
        ///// <param name="TouchType"></param>
        ///// <param name="StopType"></param>
        ///// <param name="MethodName"></param>
        ///// <returns></returns>
        //private bool SkipTouch_SendEmail(string mainReasonType, string drpOption, string drpCustReq, SkipTouchInfo oSkipTouchInfo, string qorid, string TouchStopId, string TouchType, string StopType, string MethodName)
        //{

        //    AppEmail oAppEmail = new AppEmail();
        //    EnDeCryption oEnDeCryption = new EnDeCryption();

        //    SkipTouch oSkipTouch = new SkipTouch();
        //    var reason = (from _reason in oSkipTouch.GetMainReasons() where _reason.Value == mainReasonType select _reason).SingleOrDefault();

        //    //SiteInfo oSiteInfo = (new PRbusinesslogic()).GetFacilityInfo(Convert.ToInt32(qorid));

        //    DateTime TouchOnDate = Convert.ToDateTime(TouchDate);
        //    DriverTouch oDriverTouch = oPRTransiteBusinessLogic.RetieveToucheByQorid(oEnDeCryption.Decrypt(UserID), TouchOnDate, TouchType.ToString().Trim(), Convert.ToString(qorid), StopType.Trim(), TouchStatus.Skipped.ToString());

        //    PRbusinesslogic oPRbusinesslogic = new PRbusinesslogic();
        //    string strfacilityEmailId = oPRbusinesslogic.GetFacilityEmailId(Convert.ToInt32(qorid));

        //    string skipToEmailids = ConfigurationManager.AppSettings["mailFrom"].ToString() + ";" +
        //                           ConfigurationManager.AppSettings["LocalLogistics"].ToString() + ";" +
        //                           strfacilityEmailId;

        //    SiteInfo oSiteInfo = oPRbusinesslogic.GetFacilityInfo(Convert.ToInt32(qorid));

        //    oAppEmail.EmailTo = skipToEmailids;
        //    //oAppEmail.EmailTo = ConfigurationManager.AppSettings["PackRatFEmailID"].ToString();
        //    //oAppEmail.EmailCc = strfacilityEmailId;
        //    //oAppEmail.EmailFrom = ConfigurationManager.AppSettings["mailFrom"].ToString();
        //    oAppEmail.EmailFrom = strfacilityEmailId;
        //    //oAppEmail.Subject = "Skip Touch";
        //    oAppEmail.Subject = oSiteInfo.LegalName.ToString() + " | Skip Touch";

        //    StringBuilder emailBody = new StringBuilder();

        //    emailBody.Append("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>");
        //    emailBody.Append("<html xmlns='http://www.w3.org/1999/xhtml'>");
        //    emailBody.Append("<head><title>1800PACKRAT.COM | Skip Touch information</title></head>");
        //    emailBody.Append("<body>");

        //    emailBody.Append("<br />Skip Touch information<br /><br />");
        //    emailBody.Append("<table width='100%' cellpadding='8' cellspacing='0'  style='font-family:Arial, Helvetica, sans-serif; font-size:13px; color:black;' bgcolor='#FFFFFF'>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver ID / Name </td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oEnDeCryption.Decrypt(UserID) + " - " + LoginName + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Reason</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + reason.Text + "</td>");
        //    emailBody.Append("</tr>");

        //    if (mainReasonType == "1")
        //    {
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Did you pick up a container</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (oSkipTouchInfo.wContainer ? "Yes" : "No") + "<br />");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Container No</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oSkipTouchInfo.containerNo + "</td>");
        //        emailBody.Append("</tr>");
        //    }
        //    if (mainReasonType == "2")
        //    {
        //        var CNA_reason = (from _reason in oSkipTouch.GetCnotAccessible() where _reason.Value == drpOption select _reason).SingleOrDefault();


        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'></td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + CNA_reason.Text + "<br />");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Reason</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (oSkipTouchInfo.byVehicle ? "By Vehicle" : ""));
        //        emailBody.Append((oSkipTouchInfo.rain ? "Rain" : "") + "<Br/>");
        //        emailBody.Append((oSkipTouchInfo.mud ? "Mud" : "") + "<Br/>");
        //        emailBody.Append((oSkipTouchInfo.snow ? "Snow" : "") + "<Br/>");
        //        emailBody.Append((oSkipTouchInfo.other ? "Other" : "") + "<Br/>");
        //        emailBody.Append("</td>");
        //        emailBody.Append("</tr>");
        //    }
        //    if (mainReasonType == "3")
        //    {
        //        string concString = string.Empty;
        //        if (oSkipTouchInfo.byCustRequest)
        //        {
        //            var custReq = (from _custReq in oSkipTouch.GetCustRequest() where _custReq.Value == drpCustReq select _custReq).SingleOrDefault();
        //            concString = " - " + "By Customer request" + "<br />";
        //            concString = concString + custReq.Text.Trim() + "<br /><br />";
        //        }
        //        if (oSkipTouchInfo.containerLocked)
        //        {
        //            concString = concString + "Container locked" + "<br />";
        //        }

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer not Ready</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + concString);
        //        emailBody.Append("</tr>");
        //    }
        //    if (mainReasonType == "4")
        //    {
        //        var oWeight = (from _reason in oSkipTouch.GetCOverWeight() where _reason.Value == drpOption select _reason).SingleOrDefault();
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Weight</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oWeight.Text.Trim() + "<br />");
        //        emailBody.Append("</tr>");
        //    }

        //    if (oDriverTouch != null)
        //    {
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Customer Name</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.CustomerName + "</td>");
        //        emailBody.Append("</tr>");

        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Company</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + (string.IsNullOrWhiteSpace(oDriverTouch.Company) ? "" : oDriverTouch.Company) + "</td>");
        //        emailBody.Append("</tr>");
        //    }

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>QORID</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + qorid + "</td>");
        //    emailBody.Append("</tr>");

        //    string unitNumber = string.Empty;
        //    if (oDriverTouch != null)
        //    {
        //        unitNumber = oDriverTouch.ContainerNumber;
        //    }
        //    else
        //    {
        //        try
        //        {
        //            using (DriverMobileApp.Models.DBclass.RouteOptimzation oDB = new DriverMobileApp.Models.DBclass.RouteOptimzation())
        //            {
        //                var stopinfo = oDB.StopInformations.Where(s => s.Pk_Touch_StopId == Convert.ToInt32(TouchStopId)).FirstOrDefault();
        //                unitNumber = stopinfo.TouchInformation.ContainerNo;
        //            }
        //        }
        //        catch { }
        //    }
        //    if (string.IsNullOrEmpty(unitNumber) == false)
        //    {
        //        emailBody.Append("<tr>");
        //        emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Unit Number</td>");
        //        emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oDriverTouch.ContainerNumber + "</td>");
        //        emailBody.Append("</tr>");
        //    }

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Touch Type</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + TouchType + " - " + StopType + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("<tr>");
        //    emailBody.Append("<td align='left' valign='top' bgcolor='#00628E' style='font-size:14px; color:white; border-top: #ccc solid 1px; border-right:#2296ca solid 1px;'>Driver's comments</td>");
        //    emailBody.Append("<td align='left' valign='top' style='border-left:#ccc solid 1px; border-bottom:#ccc solid 1px; border-top: #ccc solid 1px; border-right:#ccc solid 1px;'>" + oSkipTouchInfo.comments + "</td>");
        //    emailBody.Append("</tr>");

        //    emailBody.Append("</table><br /><br />");

        //    emailBody.Append("Regards, <br />Driverapp.1800PACKRAT.com");

        //    oAppEmail.EmailBody = emailBody.ToString();

        //    //Inserting Email Log
        //    Models.DBclass.EmailLog el = new Models.DBclass.EmailLog();
        //    el.ApplicationName = "Driver App";
        //    el.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        //    el.User = oEnDeCryption.Decrypt(UserID);
        //    el.StartTime = DateTime.Now;
        //    el.Subject = oSiteInfo.LegalName.ToString() + " | Skip Touch";
        //    el.ToEmailIDs = skipToEmailids;
        //    el.FromEmailID = strfacilityEmailId;
        //    el.CCEmailIDs = null;
        //    el.MethodName = MethodName;
        //    el.EmailTypeID = 2;
        //    el.QORID = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.QORID);
        //    el.TouchType = oDriverTouch == null ? "" : oDriverTouch.TouchType;
        //    el.SLSeqNo = oDriverTouch == null ? 0 : Convert.ToInt32(oDriverTouch.SLSeqNo);

        //    var LogId = LogEmails.InsertLog(el);
        //    if (oAppEmail.SendEMail() == "Success")
        //    {
        //        //updating endtime in email log
        //        Models.DBclass.EmailLog eml = new Models.DBclass.EmailLog();
        //        eml.PK_ID = LogId;
        //        eml.EndTime = DateTime.Now;
        //        LogEmails.UpdateLog(eml);
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        #endregion

        /// <summary>
        /// Method to send email for skip touch operation
        /// </summary>
        /// <param name="mainReasonType"></param>
        /// <param name="drpOption"></param>
        /// <param name="drpCustReq"></param>
        /// <param name="oSkipTouchInfo"></param>
        /// <param name="qorid"></param>
        /// <param name="TouchStopId"></param>
        /// <param name="TouchType"></param>
        /// <param name="StopType"></param>
        /// <param name="MethodName"></param>
        private void SendEmail(string mainReasonType, string drpOption, string drpCustReq, SkipTouchInfo oSkipTouchInfo, string qorid, string TouchStopId, string TouchType, string StopType, string MethodName)
        {
            EmailLogic objEmail = new Models.Email.EmailLogic();
            objEmail.SkipTouch_SendEmail(mainReasonType, drpOption, drpCustReq, oSkipTouchInfo, qorid, TouchStopId, TouchType, StopType, MethodName, UserID, TouchDate, LoginName);
        }
    }
}
