﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AuditLog.DBStructure;
namespace AuditLog
{
    public class ActivityLog
    {
        //AuditLog.Data.AuditLogDBDataContext oDataContext = new Data.AuditLogDBDataContext();
        AuditLog.Data.RouteOptimzation oDataContext = new Data.RouteOptimzation();
        /// <summary>
        /// Saves necessary info about a touch
        /// </summary>
        /// <param name="oAuditLogMaster"></param>
        /// <returns>Int64 LogID</returns>
        public string AddActivityMaster(AuditLogMaster oAuditLogMaster)
        {
            var retVal = (from _retVal in oDataContext.AuditLogMaster_AddUpdate(oAuditLogMaster.LogID, oAuditLogMaster.ApplicationName, oAuditLogMaster.RefID, oAuditLogMaster.DriverID,
                          oAuditLogMaster.Qorid, oAuditLogMaster.TransiteTouchType, oAuditLogMaster.ContainerNo, oAuditLogMaster.ScheduledStartTime,
                          oAuditLogMaster.ScheduledEndTime, oAuditLogMaster.OrderNo, oAuditLogMaster.StopType, oAuditLogMaster.EditIdentifier)
                          select _retVal.Result).ToList().SingleOrDefault();

            return retVal;
        }


        /// <summary>
        /// Saves detailed log entries
        /// </summary>
        /// <param name="oAuditLogDetail"></param>
        /// <returns>true is success, false in case of failure </returns>
        public bool AddActivityDetail(AuditLogDetail oAuditLogDetail)
        {
            var retVal = (from _retVal in oDataContext.AuditLogDetail_Add(oAuditLogDetail.LogID, oAuditLogDetail.IPAddress,
                              oAuditLogDetail.Description, oAuditLogDetail.DriverID, oAuditLogDetail.Activity)
                          select _retVal.Result).ToList().SingleOrDefault();

            if (retVal == 0)
                return true;
            return false;

        }

        public AuditLogMaster RetLogMaster(string LogID)
        {
            //var retVal = (from _retVal in oDataContext.AuditLogMasters
            //              where _retVal.LogID == LogID
            //              select new AuditLogMaster
            //              {
            //                  LogID = LogID,
            //                  ApplicationName = _retVal.ApplicationName,
            //                  RefID = _retVal.RefID,
            //                  DriverID = Convert.ToInt64(_retVal.DriverID),
            //                  Qorid = _retVal.Qorid,
            //                  TransiteTouchType = _retVal.TransiteTouchType,
            //                  ContainerNo = _retVal.ContainerNo,
            //                  ScheduledStartTime = _retVal.ScheduledStartTime,
            //                  ScheduledEndTime = _retVal.ScheduledEndTime,
            //                  OrderNo = _retVal.OrderNo,
            //                  StopType = _retVal.StopType,
            //                  EditIdentifier = _retVal.EditIdentifier
            //              }).ToList().SingleOrDefault();
            //return retVal;

            oDataContext.ExecuteStoreCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
            var retVal = oDataContext.AuditLogMasters.Where(t => t.LogID == LogID).FirstOrDefault();
            AuditLogMaster oAuditLogMaster = new AuditLogMaster();

            if (retVal != null)
            {
                oAuditLogMaster.LogID = retVal.LogID;
                oAuditLogMaster.ApplicationName = retVal.ApplicationName;
                oAuditLogMaster.RefID = retVal.RefID;
                oAuditLogMaster.DriverID = Convert.ToInt64(retVal.DriverID);
                oAuditLogMaster.Qorid = retVal.Qorid;
                oAuditLogMaster.TransiteTouchType = retVal.TransiteTouchType;
                oAuditLogMaster.ContainerNo = retVal.ContainerNo;
                oAuditLogMaster.ScheduledStartTime = retVal.ScheduledStartTime;
                oAuditLogMaster.ScheduledEndTime = retVal.ScheduledEndTime;
                oAuditLogMaster.OrderNo = retVal.OrderNo;
                oAuditLogMaster.StopType = retVal.StopType;
                oAuditLogMaster.EditIdentifier = retVal.EditIdentifier;
            }

            return oAuditLogMaster;
        }

        public bool UpdateTouchInfoLog(TouchInfoLog oTouchInfoLog)
        {
            var retVal = (from _retVal in oDataContext.TouchInfo_AddUpdate(oTouchInfoLog.LogID, oTouchInfoLog.VerifiedTouchInfo_Chkbox,
                             oTouchInfoLog.VerifiedTouchInfo_ReadyToStart, oTouchInfoLog.CustomerInfoVerified, oTouchInfoLog.ReachedDestination,
                             oTouchInfoLog.CapturedCreditcardInfo, oTouchInfoLog.CapturedSignation, oTouchInfoLog.TouchCompleted)
                          select _retVal.Result).ToList().SingleOrDefault();
            if (retVal == 0)
                return false;
            else
                return true;
        }

        public TouchInfoLog retTouchActionStatus(string LogID)
        {
            var retVal = (from _retVal in oDataContext.TouchInfoLogs
                          where _retVal.LogID == LogID
                          select new TouchInfoLog
                          {
                              PageTransactionID = _retVal.PageTransactionID,
                              LogID = _retVal.LogID,
                              VerifiedTouchInfo_Chkbox = (bool)(_retVal.VerifiedTouchInfo_Chkbox == null ? false : _retVal.VerifiedTouchInfo_Chkbox),
                              VerifiedTouchInfo_ReadyToStart = (bool)(_retVal.VerifiedTouchInfo_ReadyToStart == null ? false : _retVal.VerifiedTouchInfo_ReadyToStart),
                              CustomerInfoVerified = (bool)(_retVal.CustomerInfoVerified == null ? false : _retVal.CustomerInfoVerified),
                              ReachedDestination = (bool)(_retVal.ReachedDestination == null ? false : _retVal.ReachedDestination),
                              CapturedCreditcardInfo = (bool)(_retVal.CapturedCreditcardInfo == null ? false : _retVal.CapturedCreditcardInfo),
                              CapturedSignation = (bool)(_retVal.CapturedSignation == null ? false : _retVal.CapturedSignation),
                              TouchCompleted = (bool)(_retVal.TouchCompleted == null ? false : _retVal.TouchCompleted)
                          }).ToList().SingleOrDefault();

            return retVal;
        }

        public bool addStackTrace(StackTraceLog oStackTraceLog)
        {
            var retVal = (from _retVal in oDataContext.StackTraceLogs_Add(oStackTraceLog.StackTraceInfo,
                              oStackTraceLog.ErrorMsg, oStackTraceLog.DriverID, oStackTraceLog.QorID, oStackTraceLog.IpAddress)
                          select _retVal.Result).ToList().SingleOrDefault();
            if (retVal == 0)
                return true;
            else
                return false;
        }
    }
}
