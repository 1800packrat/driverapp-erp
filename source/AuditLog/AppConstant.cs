﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuditLog
{
    public class AppConstant
    {
        public enum ActivityType
        {
            Login = 1,
            Insert = 2,
            Update = 3,
            Delete = 4,
            Logout = 5,
            POS_Inspection = 6,
            Reconfirm_POS = 7,
            ReadyToStart = 8,
            ReachedDestination = 9,
            CustomerSignature = 10,
            CustomerCardInfo = 11,
            CompleteTouch = 12,
            AlertSent = 13,
            Skip_Touch = 14
        }

    }
}
