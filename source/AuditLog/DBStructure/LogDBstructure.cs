﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AuditLog.DBStructure
{
    public class AuditLogMaster
    {
        public string LogID { get; set; }
        public string ApplicationName { get; set; }
        public string RefID { get; set; }
        public Int64 DriverID { get; set; }
        public string Qorid { get; set; }
        public string TransiteTouchType { get; set; }
        public string ContainerNo { get; set; }
        public string ScheduledStartTime { get; set; }
        public string ScheduledEndTime { get; set; }
        public string OrderNo { get; set; }
        public string StopType { get; set; }
        public string EditIdentifier { get; set; }

        public string LoginName { get; set; }//Added by Sohan for ESB 
    }

    public class AuditLogDetail
    {
        public string LogDetID { get; set; }
        public string LogID { get; set; }
        public string IPAddress { get; set; }
        public DateTime DateTime { get; set; }
        public string Description { get; set; }
        public string Activity { get; set; }
        public Int64 DriverID { get; set; }
    }

    public class TouchInfoLog
    {
        public string PageTransactionID { get; set; }
        public string LogID { get; set; }
        public bool VerifiedTouchInfo_Chkbox { get; set; }
        public bool VerifiedTouchInfo_ReadyToStart { get; set; }
        public bool CustomerInfoVerified { get; set; }
        public bool ReachedDestination { get; set; }
        public bool CapturedCreditcardInfo { get; set; }
        public bool CapturedSignation { get; set; }
        public bool TouchCompleted { get; set; }
    }

    public class StackTraceLog
    {
        public string StackTraceInfo { get; set; }
        public string ErrorMsg { get; set; }
        public string DriverID { get; set; }
        public string QorID { get; set; }
        public string IpAddress { get; set; }
    }

}
