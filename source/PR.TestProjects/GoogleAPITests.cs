﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PR.DataHandler;
using PR.Entities;
using PR.ExternalInterfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.ExternalInterfaces.Tests
{
    [TestClass()]
    public class GoogleAPITests
    {
        [TestMethod()]
        public void ValidateAddressTest()
        {

            try
            {
                GoogleAPI gapi = new GoogleAPI();

                //Address oAddress = new Address
                //{
                //    City = "Cliffwood",
                //    State = "NJ",
                //    Zip = "07721"
                //};

                Address oAddress = new Address
                {
                    //AddressLine1 = "1138 sweet spot circle",
                    //AddressLine2 = "sweet spot circle",
                    //State = "NC",
                    //City = "Morrisville",
                    //City = "Manhattan",
                    //State = "NY",
                    Zip = "29672"
                };
                //SoHo, Manhattan, New York, NY

                var a = gapi.ValidateAddress(oAddress);

            }
            catch (Exception ex)
            {

            }

            Assert.Fail();
        }

        [TestMethod()]
        public void ValidateAddressByZipTest()
        {

            try
            {
                GoogleAPI gapi = new GoogleAPI();

                //Address oAddress = new Address
                //{
                //    City = "Cliffwood",
                //    State = "NJ",
                //    Zip = "07721"
                //};

                Address oAddress = new Address
                {
                    //AddressLine1 = "1138 sweet spot circle",
                    //AddressLine2 = "sweet spot circle",
                    //State = "NC",
                    //City = "Morrisville",
                    //City = "Manhattan",
                    //State = "NY",
                    Zip = "12065"
                };
                //SoHo, Manhattan, New York, NY

                //gapi.GenerateLatLongBasedOnZip(ref oAddress);

            }
            catch (Exception ex)
            {

            }

            Assert.Fail();
        }

        [TestMethod()]
        public void GetDistanceTest()
        {

            try
            {
                float mynum = 1.345698f;
                double r = Math.Ceiling(mynum * 1000) / 1000;

                GoogleAPI gapi = new GoogleAPI();

                Address oAddressf = new Address
                {
                    Zip = "32920"
                };

                Address oAddresst = new Address
                {
                    AddressLine1 = "40 Down St",
                    City = "Rochester",
                    Zip = "32837"
                };

                var a = gapi.GetDistance(oAddressf, oAddresst);

                var a1 = gapi.GetDistance(oAddressf, oAddresst);

            }
            catch (Exception ex)
            {

            }

            Assert.Fail();
        }

        [TestMethod()]
        public void GetDistanceTest1()
        {
            Assert.Fail();
        }

/*        [TestMethod()]
        public void PopulateGooglelatlongToPostalCodes()
        {
            try
            {
                LocalDataHandler ld = new LocalDataHandler();
                int start = 0;
                int numberofzipsprocess = 40930;
                GoogleAPI gapi = new GoogleAPI();

                var allPostalCodes = ld.GetPostalCodes().Skip(start).Take(numberofzipsprocess);
                StringBuilder sb = new StringBuilder();

                Address oAddresst;
                //Address oAddresst = new Address
                //{
                //    AddressLine1 = "15640 Knollcliff",
                //    City = "San Antonio",
                //    State = "TX",
                //    Zip = "78247"
                //};

                //var validatedAddress = gapi.ValidateAddress(oAddresst);
                int index = 0;
                using (var file = new StreamWriter("c:\\googleapilog\\googleapilog.txt", true))
                {

                    foreach (var ps in allPostalCodes)
                    {
                        try
                        {
                            index++;
                            oAddresst = new Address
                            {
                                Zip = ps.Zipcode
                            };

                            var validatedAddress = gapi.ValidateAddress(oAddresst);

                            //ld.InsertGoogleLatLongValuesToPostalCodes(validatedAddress.Zip, validatedAddress.City, validatedAddress.State, validatedAddress.Latitude, validatedAddress.Longitude);
                        }
                        catch (Exception ex)
                        {
                            sb.Append($"Zip: {ps.Zipcode}; Error: {ex.Message}");
                            file.WriteLine($"Zip: {ps.Zipcode}; Error: {ex.Message}");
                            //file.Close();
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            Assert.Fail();
        }*/
    }
}