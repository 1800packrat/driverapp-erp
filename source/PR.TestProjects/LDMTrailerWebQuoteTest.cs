﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PR.Entities;
using PR.BusinessLogic;
using PR.UtilityLibrary;
namespace PR.TestProjects
{
    [TestClass]
    public class LDMTrailerWebQuoteTest
    {
        ///// <summary>
        ///// 
        ///// </summary>
        //[TestMethod]
        //public void CreateLDMTrailerQuote_Test()
        //{

        //    Random randomd = new Random();
            
        //    LDMTrailerServiceQuoteRequest requestQuoteObj = new LDMTrailerServiceQuoteRequest() 
        //                                                                        { 
        //                                                                         VendorName="Web",
        //                                                                         VendorCode = "v0000006",
        //                                                                         VendorId = "VWebSub",
        //                                                                         VendorPass = "2k8E9p",
        //                                                                         EntryDate =  "11/25/2014",
        //                                                                         FirstName = "Web",
        //                                                                         LastName = "Prospect",
        //                                                                         AddressLine1 = "",
        //                                                                         AddressLine2= "",
        //                                                                         City = "",
        //                                                                         State = "",
        //                                                                         ZipCode = "27587",
        //                                                                         Phone = "9192564"+  randomd.Next(100, 999).ToString(),
        //                                                                         Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //                                                                         UnitCalcMethod = "1",
        //                                                                         UnitCalcvalue = "0",
        //                                                                         OriginZipCode = "27606",
        //                                                                         DestinationZipCode = "29580",
        //                                                                         DryVan = "0",
        //                                                                         EstInitialDelivery = DateTime.Now.AddDays(10).ToShortDateString(),  //"10/20/2014"
        //                                                                         EstDateAtDestination=DateTime.Now.AddDays(15).ToShortDateString(),//"11/30/2014",  
        //                                                                         EstMosStorage="1",  
        //                                                                         Guarrantee="0",  
        //                                                                         Promo="",  
        //                                                                         SendEmail="F",  
        //                                                                         CppValue="0",  
        //                                                                         AddEmails="",
        //                                                                         WebUnitCalcMethod = "U",
        //                                                                         WebUnitCalcValue = "1",
        //                                                                         WebSourceReferralCodes = "",
        //                                                                         QuoteCreatedBy = "WUser",
        //                                                                         ConfirmationScreenName = ""

                                                                                
        //                                                                        };
        //    LDMTrailerWebQuote LDMTWebQuote=new LDMTrailerWebQuote();
        //    var response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));

     

        //}




        ///// <summary>
        ///// 
        ///// </summary>
        //[TestMethod]
        //public void MoreTest()
        //{

        //    var randomd = new Random();
        //    var requestQuoteObj = new LDMTrailerServiceQuoteRequest()
        //    {
        //        VendorName = "Web",
        //        VendorCode = "v0000006",
        //        VendorId = "VWebSub",
        //        VendorPass = "2k8E9p",
        //        EntryDate = DateTime.Now.ToShortDateString(), //"3/25/2014",
        //        FirstName = "Web",
        //        LastName = "Prospect",
        //        AddressLine1 = "",
        //        AddressLine2 = "",
        //        City = "",
        //        State = "",
        //        ZipCode = "27587",
        //        Phone = "9192564" + randomd.Next(100, 999).ToString(),
        //        Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //        UnitCalcMethod = "1",
        //        UnitCalcvalue = "0",
        //        OriginZipCode = "60118",
        //        DestinationZipCode = "94506",
        //        DryVan = "0",
        //        EstInitialDelivery = "10/20/2014",//DateTime.Now.AddDays(15).ToShortDateString(),  //"10/20/2014"
        //        EstDateAtDestination = DateTime.Now.AddDays(45).ToShortDateString(),//"11/30/2014",  
        //        EstMosStorage = "1",
        //        Guarrantee = "0",
        //        Promo = "",
        //        SendEmail = "F",
        //        CppValue = "0",
        //        AddEmails = "",
        //        WebUnitCalcMethod = "U",
        //        WebUnitCalcValue = "1",
        //        WebSourceReferralCodes = "",
        //        QuoteCreatedBy = "WUser",
        //        ConfirmationScreenName = ""


        //    };
        //    var LDMTWebQuote = new LDMTrailerWebQuote();
        //    var response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));

        //    Console.WriteLine("_________________________________________________________________________________________________________________________________________________________________");

        //    requestQuoteObj = new LDMTrailerServiceQuoteRequest()
        //    {
        //        VendorName = "Web",
        //        VendorCode = "v0000006",
        //        VendorId = "VWebSub",
        //        VendorPass = "2k8E9p",
        //        EntryDate = DateTime.Now.ToShortDateString(), //"3/25/2014",
        //        FirstName = "Web",
        //        LastName = "Prospect",
        //        AddressLine1 = "",
        //        AddressLine2 = "",
        //        City = "",
        //        State = "",
        //        ZipCode = "27587",
        //        Phone = "9192564" + randomd.Next(100, 999).ToString(),
        //        Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //        UnitCalcMethod = "1",
        //        UnitCalcvalue = "0",
        //        OriginZipCode = "46403",
        //        DestinationZipCode = "92101",
        //        DryVan = "0",
        //        EstInitialDelivery = "10/16/2014",//DateTime.Now.AddDays(15).ToShortDateString(),  //"10/20/2014"
        //        EstDateAtDestination = DateTime.Now.AddDays(45).ToShortDateString(),//"11/30/2014",  
        //        EstMosStorage = "1",
        //        Guarrantee = "0",
        //        Promo = "",
        //        SendEmail = "F",
        //        CppValue = "0",
        //        AddEmails = "",
        //        WebUnitCalcMethod = "U",
        //        WebUnitCalcValue = "1",
        //        WebSourceReferralCodes = "",
        //        QuoteCreatedBy = "WUser",
        //        ConfirmationScreenName = ""


        //    };
        //    LDMTWebQuote = new LDMTrailerWebQuote();
        //    response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));

        //    Console.WriteLine("_________________________________________________________________________________________________________________________________________________________________");


        //    requestQuoteObj = new LDMTrailerServiceQuoteRequest()
        //    {
        //        VendorName = "Web",
        //        VendorCode = "v0000006",
        //        VendorId = "VWebSub",
        //        VendorPass = "2k8E9p",
        //        EntryDate = DateTime.Now.ToShortDateString(), //"3/25/2014",
        //        FirstName = "Web",
        //        LastName = "Prospect",
        //        AddressLine1 = "",
        //        AddressLine2 = "",
        //        City = "",
        //        State = "",
        //        ZipCode = "27587",
        //        Phone = "9192564" + randomd.Next(100, 999).ToString(),
        //        Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //        UnitCalcMethod = "1",
        //        UnitCalcvalue = "0",
        //        OriginZipCode = "45241",
        //        DestinationZipCode = "91709",
        //        DryVan = "0",
        //        EstInitialDelivery = "10/17/2014",//DateTime.Now.AddDays(15).ToShortDateString(),  //"10/20/2014"
        //        EstDateAtDestination = DateTime.Now.AddDays(45).ToShortDateString(),//"11/30/2014",  
        //        EstMosStorage = "1",
        //        Guarrantee = "0",
        //        Promo = "",
        //        SendEmail = "F",
        //        CppValue = "0",
        //        AddEmails = "",
        //        WebUnitCalcMethod = "U",
        //        WebUnitCalcValue = "1",
        //        WebSourceReferralCodes = "",
        //        QuoteCreatedBy = "WUser",
        //        ConfirmationScreenName = ""


        //    };
        //    LDMTWebQuote = new LDMTrailerWebQuote();
        //    response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));

        //    Console.WriteLine("_________________________________________________________________________________________________________________________________________________________________");



        //    requestQuoteObj = new LDMTrailerServiceQuoteRequest()
        //    {
        //        VendorName = "Web",
        //        VendorCode = "v0000006",
        //        VendorId = "VWebSub",
        //        VendorPass = "2k8E9p",
        //        EntryDate = DateTime.Now.ToShortDateString(), //"3/25/2014",
        //        FirstName = "Web",
        //        LastName = "Prospect",
        //        AddressLine1 = "",
        //        AddressLine2 = "",
        //        City = "",
        //        State = "",
        //        ZipCode = "27587",
        //        Phone = "9192564" + randomd.Next(100, 999).ToString(),
        //        Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //        UnitCalcMethod = "1",
        //        UnitCalcvalue = "0",
        //        OriginZipCode = "60622",
        //        DestinationZipCode = "90069",
        //        DryVan = "0",
        //        EstInitialDelivery = "10/21/2014",//DateTime.Now.AddDays(15).ToShortDateString(),  //"10/20/2014"
        //        EstDateAtDestination = DateTime.Now.AddDays(45).ToShortDateString(),//"11/30/2014",  
        //        EstMosStorage = "1",
        //        Guarrantee = "0",
        //        Promo = "",
        //        SendEmail = "F",
        //        CppValue = "0",
        //        AddEmails = "",
        //        WebUnitCalcMethod = "U",
        //        WebUnitCalcValue = "1",
        //        WebSourceReferralCodes = "",
        //        QuoteCreatedBy = "WUser",
        //        ConfirmationScreenName = ""


        //    };
        //    LDMTWebQuote = new LDMTrailerWebQuote();
        //    response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));

        //    Console.WriteLine("_________________________________________________________________________________________________________________________________________________________________");


        
        //}





        ///// <summary>
        ///// 
        ///// </summary>
        //[TestMethod]
        //public void CreateLDMTrailerQuote_Test_With_Hari()
        //{

        //    Random randomd = new Random();

        //    LDMTrailerServiceQuoteRequest requestQuoteObj = new LDMTrailerServiceQuoteRequest()
        //    {
        //        VendorName = "Web",
        //        VendorCode = "v0000006",
        //        VendorId = "VWebSub",
        //        VendorPass = "2k8E9p",
        //        EntryDate = "11/30/2014", //"3/25/2014",
        //        FirstName = "Web",
        //        LastName = "Prospect",
        //        AddressLine1 = "",
        //        AddressLine2 = "",
        //        City = "",
        //        State = "",
        //        ZipCode = "48306",
        //        Phone = "1111111111",
        //        Email = "ldmtest" + randomd.Next(100, 999).ToString() + "@email.com",
        //        UnitCalcMethod = "1",
        //        UnitCalcvalue = "0",
        //        OriginZipCode = "48306",
        //        DestinationZipCode = "95661",
        //        DryVan = "0",
        //        EstInitialDelivery = "11/31/2014",//DateTime.Now.AddDays(15).ToShortDateString(),  //"10/20/2014"
        //        EstDateAtDestination = "12/10/2014",//"11/30/2014",  
        //        EstMosStorage = "1",
        //        Guarrantee = "0",
        //        Promo = "",
        //        SendEmail = "F",
        //        CppValue = "0",
        //        AddEmails = "",
        //        WebUnitCalcMethod = "U",
        //        WebUnitCalcValue = "1",
        //        WebSourceReferralCodes = "",
        //        QuoteCreatedBy = "WUser",
        //        ConfirmationScreenName = ""


        //    };
        //    LDMTrailerWebQuote LDMTWebQuote = new LDMTrailerWebQuote();
        //    var response = LDMTWebQuote.CreateLDMTrailerQuote(requestQuoteObj);

        //    Assert.IsNotNull(response, "Response object is Null.");
        //    Assert.IsNotNull(response.LDMWebQuoteResponse, "LDM Quote Response is Null.");
        //    Assert.IsNotNull(response.TrailerWebQuoteResponse, "Trailer Quote Response is Null.");
        //    Assert.IsFalse(!response.LDMWebQuoteResponse.IsQuoteSuccess, "LDM Quote fails  .");
        //    Assert.IsFalse(!response.TrailerWebQuoteResponse.IsQuoteSuccess, "Trailer Quote fails  .");
        //    Console.WriteLine(string.Format("Uss Cust Id : {0}  \n DE Date: {11} \n LDM Quote No: {1} \n  LDM Total Price : {9} \n LDM Unbundled Price : {10} \n Cust Phone: {2}  \n Cust Email: {3} \n Trailer OD Ref : {4}  \n Origin ZipCode: {5}  \n Destination ZipCode: {6} \n OD Customer Price: {7} \n OD Quote No: {8}", response.LDMWebQuoteResponse.USS_CustId, response.LDMWebQuoteResponse.QuoteId, requestQuoteObj.Phone, requestQuoteObj.Email, response.TrailerWebQuoteResponse.ODReference, requestQuoteObj.OriginZipCode, requestQuoteObj.DestinationZipCode, response.TrailerWebQuoteResponse.CustPrice.ToLeadingMinusCurrency(), response.TrailerWebQuoteResponse.QuoteNumber, response.LDMWebQuoteResponse.TotalPrice.ToLeadingMinusCurrency(), response.LDMWebQuoteResponse.UnBundledTotal.ToLeadingMinusCurrency(), requestQuoteObj.EstInitialDelivery));



        //}


   

    }
}
