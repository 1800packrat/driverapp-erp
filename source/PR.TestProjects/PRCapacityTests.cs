﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;

namespace PR.BusinessLogic.Tests
{
    [TestClass()]
    public class ERP_TouchTests
    {

        string corpcode = ConfigurationManager.AppSettings["corpCode"];
        string username = ConfigurationManager.AppSettings["SMDUsername"];
        string password = ConfigurationManager.AppSettings["SMDPassword"];

        [TestMethod()]
        public void CheckPassword()
        {
            //try
            //{
            //    var EnDeKey = "P@ckr@tFac!l1tyApp"; //from config file

            //    var encPwd = "7OaTyyB9aHs=";
            //    var decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);

            //    //for Testing Purpose only
            //    encPwd = PR.UtilityLibrary.CommonUtility.Encrypt("tglass@123", EnDeKey);
            //    decPwd = PR.UtilityLibrary.CommonUtility.Decrypt(encPwd, EnDeKey);
            //}
            //catch (Exception ex)
            //{
            //    string msg = ex.Message;
            //}
        } 

        [TestMethod()]
        public void GetFacilityCapacityTest()
        {
            try
            {
                SMDBusinessLogic smd = new SMDBusinessLogic();

                DateTime startDate = Convert.ToDateTime("12/20/2019");
                DateTime endDate = Convert.ToDateTime("12/27/2019");

                var ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L157");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L167");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L160");
                ds = smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, "L001");

                Assert.Fail();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }
    }
}