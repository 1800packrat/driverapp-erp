﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class MoveSection
    {
        public MoveSection()
        {
            Units = new List<Model.Unit>();
        }

        public List<Unit> Units;

        public MailerAddress CustomerAddress { get; set; }

        public MailerAddress FacilityAddress { get; set; }

        public string DeliveryObstacles { get; set; }

        public string LocationCode { get; set; }

    }
}
