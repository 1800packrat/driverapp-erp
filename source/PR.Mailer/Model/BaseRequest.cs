﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    [DataContract]
    public class BaseRequest
    {
        [DataMember]
        public string LoginFullName { get; set; }

        [DataMember]
        public string LoginUserName { get; set; }
        [DataMember]
        public string LoginId { get; set; }

        [DataMember]
        public int LoginStarsEmployeID { get; set; }

        [DataMember]
        public int UssCustID { get; set; }

        [DataMember]
        public int StarsCustID { get; set; }
        [DataMember]
        public int QuoteId { get; set; }
        [DataMember]
        public int QORID { get; set; }

        [DataMember]
        public string TrailerTODRef { get; set; }

        [DataMember]
        public string MoveType { get; set; }
        [DataMember]
        public string ApplicationType { get; set; }
    }
}
