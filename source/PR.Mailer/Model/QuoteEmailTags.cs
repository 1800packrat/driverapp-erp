﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.Mailer.Model
{
    public class LDMQuoteEmailTags
    {
        //Quote ID	{QuoteID}
        //Quote Date	{QuoteDate}
        //Quote Expiration Date	{QuoteExpirationDate}
        //Name	{FirstName} {LastName}
        //Moving From	{MovingFromAddress}
        //Moving To	{MovingToAddress}
        //No of Containers	{NoOfContainers}
        //Container Size	{ContainerSize}
        //Total Estimate	{TotalEstimate}
        //Content Protection	{ContentProtection}
        //Origin Rent	{OriginRent}
        //Destination Rent	{DestinationRent}

        public int LDMQuoteID { get; set; } // "{{LDMQuoteID}}";
        public string LDMQuoteNo { get; set; } // "{{LDMQuoteNo}}";
        public string QuoteCreatedDate { get; set; } // "{{QuoteCreatedDate}}";
        public string ExpirationDate { get; set; } // "{{ExpirationDate}}";
        public string FirstName { get; set; } // "{{FirstName}}";
        public string LastName { get; set; } // "{{LastName}}";
        public string UnitSize { get; set; } // "{{UnitSize}}";
        public string TotalEstimateCost { get; set; } //TotalEstimateCost

        public string NoOfContainers { get; set; } //NoOfContainers

        //public string NoOfContainers { get { return (Total16FtContainer + (Total8FtContainer / 2)).ToString(); } private set { } } //NoOfContainers

        public int Total16FtContainer { get; set; } //Total16FtContainer
        public int Total8FtContainer { get; set; } //Total8FtContainer
        public bool Is16FtContainerExists { get { return Total16FtContainer > 0; } private set { } } //Is16FtContainerExists
        public bool Is8FtContainerExists { get { return Total8FtContainer > 0; } private set { } } //Is8FtContainerExists
        public bool IsNewQuote { get; set; } //IsNewQuote
        public bool IsOldQuote { get; set; } //IsNewQuote

        public string Note { get; set; }

        public string ContentProtection { get; set; } //ContentProtection
        public string OriginRent { get; set; } //OriginRent
        public string DestinationRent { get; set; }//DestinationRent

        public int NoOfLocks { get; set; }
        public int NoOfBlankets { get; set; }
        public string DeclaredvalueCPP { get; set; }
        public bool IsUnBundledPrice { get; set; }
        public bool IfStandardBlanketExists { get { return NoOfBlankets > 0; } private set { } }
        public bool IsLocksExists { get { return NoOfLocks > 0; } private set { } }
        public bool IsCPPDeclaredValueDW { get; set; }

        public MailerAddress MovingFromAddress { get; set; }

        public MailerAddress MovingToAddress { get; set; }

        public string PackratPhoneNumber { get; set; }

        public string PackratLink { get; set; }
    }
}
