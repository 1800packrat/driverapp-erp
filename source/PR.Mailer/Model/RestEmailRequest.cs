﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PR.Mailer.Model
{
    internal class Personalization<T> where T : new()
    {
        public Personalization()
        {
            to = new List<EmailAddressRequest>();
            bcc = new List<EmailAddressRequest>();
            cc = new List<EmailAddressRequest>();
            dynamic_template_data = new T();
        }

      //  public string subject { get; set; }
        public List<EmailAddressRequest> to { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<EmailAddressRequest> cc { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public List<EmailAddressRequest> bcc { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T dynamic_template_data { get; set; }
    }

    internal class From
    {
        public string email { get; set; }
        public string name { get; set; }
    }

    internal class RestEmailRequest<T> where T : new()
    {
        public RestEmailRequest()
        {
            personalizations = new List<Personalization<T>>();
        }

        public List<Personalization<T>> personalizations { get; set; }
        public From from { get; set; }
        public string template_id { get; set; }
        public string subject { get; set; }
        public List<string> categories { get; set; }
    }
}
