﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PR.Mailer.Model;
using System.Collections;
using PR.BusinessLogic;
using PR.Entities;
using RestSharp;

namespace PR.Mailer.SendGrid
{
    public class SendGridManager<T> where T : new()
    {
        public static MailResponse Execute(MailBaseRequest<T> mailRequest)
        {
            LocalComponent lc = new LocalComponent();
            try
            {
                var client = new RestClient(mailRequest.RestClientURL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + mailRequest.apiKey);

                Personalization<T> transactionData = new Personalization<T>();

                transactionData.to = mailRequest.TOs;

                //If no cc email ids, then assing null to Personalization.cc. So that json serialize will igone it.
                if (mailRequest.CCs != null && mailRequest.CCs.Count > 0)
                    transactionData.cc = mailRequest.CCs;
                else
                    transactionData.cc = null;

                //If no bcc email ids, then assing null to Personalization.bcc. So that json serialize will igone it.
                if (mailRequest.BCCs != null && mailRequest.BCCs.Count > 0)
                    transactionData.bcc = mailRequest.BCCs;
                else
                    transactionData.bcc = null;

                transactionData.dynamic_template_data = mailRequest.TransactionData;

                RestEmailRequest<T> objEmailRequest = new RestEmailRequest<T>
                {
                    from = new From { email = mailRequest.From.Email },
                    subject = mailRequest.Subject,
                    template_id = mailRequest.TemplateId,
                    categories = mailRequest.GetSendGridCategories //Added for filter (category stats) in Sendgrid web portal
                }; 

                objEmailRequest.personalizations.Add(transactionData);

                var mailRequestJsonContent = Newtonsoft.Json.JsonConvert.SerializeObject(objEmailRequest);

                request.AddParameter("application/json", mailRequestJsonContent, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                MailResponse mailResponse = new MailResponse();
                mailResponse.StatusCode = response.StatusCode;

                if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
                    lc.InsertPRErrorLog(DateTime.Now, mailRequest.LoginId, "Mail Sent Error", response.StatusCode.ToString(), string.Empty,
                       (mailRequest.QORID > 0 ? mailRequest.QORID : mailRequest.QuoteId), string.Empty, string.Empty);

                return mailResponse;
            }
            catch (Exception ex)
            {
                lc.InsertPRErrorLog(DateTime.Now, mailRequest.LoginId, "Mail Sent Error", ex.Message.ToString(), ex.StackTrace.ToString(),
                    (mailRequest.QORID > 0 ? mailRequest.QORID : mailRequest.QuoteId), string.Empty, string.Empty);
                return null;
            }
        }
    }

    public class SendGridManager
    {
        public static async Task<MailResponse> Execute(MailBaseRequest request)
        {
            LocalComponent lc = new LocalComponent();
            try
            {
                //string apiKey = "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";
                var client = new SendGridClient(request.apiKey);

                var msg = new SendGridMessage();
                msg.SetFrom(new EmailAddress(request.From.Email));

                var emails = new List<EmailAddress>();
                foreach (PR.Mailer.Model.EmailAddressRequest add in request.TOs)
                {
                    if (!string.IsNullOrWhiteSpace(add.Email) && !emails.Any(e => e.Email == add.Email))
                        emails.Add(new EmailAddress(add.Email));

                }
                msg.AddTos(emails);

                var ccemails = new List<EmailAddress>();
                foreach (EmailAddressRequest add in request.CCs)
                {
                    // ccemails.Add(new EmailAddress(add.Email));
                    if (!string.IsNullOrWhiteSpace(add.Email) && !ccemails.Any(e => e.Email == add.Email))
                        msg.AddCc(new EmailAddress(add.Email));

                }
                //msg.AddCcs(ccemails);

                var bccemails = new List<EmailAddress>();
                foreach (EmailAddressRequest add in request.BCCs)
                {
                    //bccemails.Add(new EmailAddress(add.Email));
                    if (!string.IsNullOrWhiteSpace(add.Email) && !bccemails.Any(e => e.Email == add.Email))
                        msg.AddBcc(new EmailAddress(add.Email));

                }
                //msg.AddBccs(bccemails);
                //DictionaryEntry entry in hashtable
                foreach (DictionaryEntry tag in request.Tags)
                {
                    msg.AddSubstitution(tag.Key.ToString(), tag.Value == null ? "" : Convert.ToString(tag.Value));
                }

                msg.SetTemplateId(request.TemplateId);

                if (request.GetSendGridCategories != null)
                    msg.AddCategories(request.GetSendGridCategories);

                var response = await client.SendEmailAsync(msg);
                MailResponse mailResponse = new MailResponse();
                mailResponse.StatusCode = response.StatusCode;
                mailResponse.Headers = response.Headers;
                mailResponse.Body = response.Body;

                if (response.StatusCode != System.Net.HttpStatusCode.Accepted)
                    lc.InsertPRErrorLog(DateTime.Now, request.LoginId, "Mail Sent Error", response.StatusCode.ToString(), string.Empty, request.QORID, string.Empty, string.Empty);

                return mailResponse;
            }
            catch (Exception ex)
            {
                lc.InsertPRErrorLog(DateTime.Now, request.LoginId, "Mail Sent Error", ex.Message.ToString(), ex.StackTrace.ToString(), request.QORID, string.Empty, string.Empty);
                return null;
            }
        }
    }
}
