using System;
using System.Data;
using PR.Entities;
using PR.DataHandler;


namespace PR.ExternalInterfaces
{
    //public class PCMInterface
    class PCMInterface
    {
        public PCMilerReurnClass GetDistance(Address originationAddress, Address destinationAddress)
        {

            PCMService.LoginType login = new PCMService.LoginType();
            PCMService.LocationInputType tripOrigin = new PCMService.LocationInputType();
            PCMService.LocationInputType tripDestination = new PCMService.LocationInputType();
            PCMService.LocationInputType currentLocation = new PCMService.LocationInputType();
            PCMService.OptionsType tripOption = new PCMService.OptionsType();

            PCMService.ReportResponse response = new PCMService.ReportResponse();
            PCMService.Service service = new PCMService.Service();

            PCMilerReurnClass returnClass = new PCMilerReurnClass();
            LocalDataHandler dh = new LocalDataHandler();

            //set the login values
            login.UserID = "PackRat";
            login.Password = "PackRat88152";
            login.Account = "1800PackRat";

            if (!((originationAddress.Zip == String.Empty || originationAddress.Zip == null) && (destinationAddress.Zip == String.Empty || destinationAddress.Zip == null)))
            {
                if (originationAddress.Zip.IndexOf('-') > 0)
                {
                    if (originationAddress.Zip.Length > 5)
                    {
                        originationAddress.Zip = originationAddress.Zip.Substring(0, 5);
                    }
                }
                if ((destinationAddress.Zip.IndexOf('-') > 0))
                {
                    if (destinationAddress.Zip.Length > 5)
                    {
                        destinationAddress.Zip = destinationAddress.Zip.Substring(0, 5);
                    }
                }

                //Populate the origination address
                if ((originationAddress.Latitude != String.Empty) && (originationAddress.Longitude != String.Empty))
                {
                    tripOrigin.Lat = originationAddress.Latitude;
                    tripOrigin.Long = originationAddress.Longitude;
                }
                else
                {
                    tripOrigin.Address1 = originationAddress.AddressLine1;
                    tripOrigin.Address2 = originationAddress.AddressLine2;
                    tripOrigin.City = originationAddress.City;
                    tripOrigin.State = originationAddress.State;
                    tripOrigin.Zip = originationAddress.Zip;
                }

                //Populate the destination address
                if ((destinationAddress.Latitude != String.Empty) && (destinationAddress.Longitude != String.Empty))
                {
                    tripDestination.Lat = destinationAddress.Latitude;
                    tripDestination.Long = destinationAddress.Longitude;
                }
                else
                {
                    tripDestination.Address1 = destinationAddress.AddressLine1;
                    tripDestination.Address2 = destinationAddress.AddressLine2;
                    tripDestination.City = destinationAddress.City;
                    tripDestination.State = destinationAddress.State;
                    tripDestination.Zip = destinationAddress.Zip;
                }

                tripOrigin.CountryPostalCode = "US";
                tripDestination.CountryPostalCode = "US";

                currentLocation = tripOrigin;
                PCMService.LocationInputType[] currentLocations = new PCMService.LocationInputType[1] { currentLocation };

                //Call PC Miler
                try
                {
                    response = service.PMWSGetETAOutOfRoute(login, tripOrigin, tripDestination, currentLocations, null, null);

                    if (response == null)
                    {
                        dh.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, "Response null from PMWSGetETAOutOfRoute web service.", String.Empty, -1);
                        returnClass.ErrorMessage = "Response null from PMWSGetETAOutOfRoute web service.";
                        returnClass.Distance = GetStraightLineDistance(originationAddress, destinationAddress);
                        returnClass.OriginationAddress.Zip = originationAddress.Zip;
                        returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                        returnClass.IsStraightLineDistance = true;

                    }
                    else if (response.ErrorList != null)
                    {
                        if (response.ErrorList.Length > 0)
                        {
                            if (response.ErrorList[0].ErrorCode == "460")
                            {
                                throw new Exception(response.ErrorList[0].ErrorDesc);
                            }
                            returnClass.ErrorMessage = response.ErrorList[0].ErrorDesc;
                            dh.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, response.ErrorList[0].ErrorDesc, String.Empty, -1);
                        }
                        else
                        {
                            dh.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, "Empty response from PMWSGetETAOutOfRoute web service.", String.Empty, -1);
                        }

                        returnClass.Distance = GetStraightLineDistance(originationAddress, destinationAddress);
                        returnClass.OriginationAddress.Zip = originationAddress.Zip;
                        returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                        returnClass.IsStraightLineDistance = true;
                    }
                    else
                    {
                        //Set the origination and destination address
                        if (originationAddress.AddressLine1 == String.Empty)
                            originationAddress.AddressLine1 = response.Report.Origin.Address1;
                        if (originationAddress.AddressLine2 == String.Empty)
                            originationAddress.AddressLine2 = response.Report.Origin.Address2;
                        if (originationAddress.City == String.Empty)
                            originationAddress.City = response.Report.Origin.City;
                        if (originationAddress.State == String.Empty)
                            originationAddress.State = response.Report.Origin.State;
                        if (originationAddress.Zip == String.Empty)
                            originationAddress.Zip = response.Report.Origin.Zip;
                        if (originationAddress.Latitude == String.Empty)
                            originationAddress.Latitude = Math.Round(Convert.ToDouble(response.Report.Origin.Lat) / 1000000, 6).ToString();
                        if (originationAddress.Longitude == String.Empty)
                            originationAddress.Longitude = Math.Round(Convert.ToDouble(response.Report.Origin.Long) / 1000000, 6).ToString();

                        if (destinationAddress.AddressLine1 == String.Empty)
                            destinationAddress.AddressLine1 = response.Report.Destination.Address1;
                        if (destinationAddress.AddressLine2 == String.Empty)
                            destinationAddress.AddressLine2 = response.Report.Destination.Address2;
                        if (destinationAddress.City == String.Empty)
                            destinationAddress.City = response.Report.Destination.City;
                        if (destinationAddress.State == String.Empty)
                            destinationAddress.State = response.Report.Destination.State;
                        if (destinationAddress.Zip == String.Empty)
                            destinationAddress.Zip = response.Report.Destination.Zip;
                        if (destinationAddress.Latitude == String.Empty)
                            destinationAddress.Latitude = Math.Round(Convert.ToDouble(response.Report.Destination.Lat) / 1000000, 6).ToString();
                        if (destinationAddress.Longitude == String.Empty)
                            destinationAddress.Longitude = Math.Round(Convert.ToDouble(response.Report.Destination.Long) / 1000000, 6).ToString();

                        returnClass.OriginationAddress = originationAddress;
                        returnClass.DestinationAddress = destinationAddress;
                        //Set the distance value
                        returnClass.Distance = Math.Round(Convert.ToDecimal(response.Report.EORReport.OORLineType[0].TMiles), 2);
                    }
                }
                catch (Exception ex)
                {
                    dh.InsertPRErrorLog(DateTime.Now, String.Empty, ex.GetType().ToString(), ex.Message, ex.StackTrace, -1);
                    returnClass.ErrorMessage = "Exception thrown from PMWSGetETAOutOfRoute web service.";
                    returnClass.Distance = GetStraightLineDistance(originationAddress, destinationAddress);
                    returnClass.OriginationAddress.Zip = originationAddress.Zip;
                    returnClass.DestinationAddress.Zip = destinationAddress.Zip;
                    returnClass.IsStraightLineDistance = true;
                }
            }
            return returnClass;
        }


        private decimal GetDistanceFromGeoCodes(double decLatitude1, double decLongitude1, double decLatitude2, double decLongitude2)
        {

            double dblRadius = 3981.875D;
            //6371D / (1.6) 
            double dblDiffLatitude = 0;
            double dblDiffLongitude = 0;
            double dblAngleFactor = 0;
            double declatitude1A = 0;
            double declatitude2A = 0;
            double dblCurvature = 0;
            decimal decDistance = 0;


            dblDiffLatitude = DegreesToRadians(decLatitude2 - decLatitude1);
            dblDiffLongitude = DegreesToRadians(decLongitude2 - decLongitude1);

            declatitude1A = DegreesToRadians(decLatitude1);
            declatitude2A = DegreesToRadians(decLatitude2);

            dblAngleFactor = Math.Sin(dblDiffLatitude / 2) * Math.Sin(dblDiffLatitude / 2) + Math.Cos(declatitude1A) * Math.Cos(declatitude2A) * Math.Sin(dblDiffLongitude / 2) * Math.Sin(dblDiffLongitude / 2);

            dblCurvature = 2 * Math.Atan2(Math.Sqrt(dblAngleFactor), Math.Sqrt(1 - dblAngleFactor));

            decDistance = (decimal)(dblRadius * dblCurvature);

            //decDistance = CType(((10 - decDistance * (0.01)) * (0.01) * decDistance) + decDistance, Decimal) 


            return decDistance;
        }

        private double DegreesToRadians(double dblDegrees)
        {
            return 2 * Math.PI * dblDegrees / 360.0;
        }

        private DataSet GetGeoCode(string zip)
        {
            LocalDataHandler dh = new LocalDataHandler();
            return dh.GetGeocode(zip);
        }

        public decimal GetStraightLineDistance(Address originationAddress, Address destinationAddress)
        {
            double origLat = 0D;
            double origLong = 0D;
            double destLat = 0D;
            double destLong = 0D;

            if ((originationAddress.Latitude != String.Empty) && (originationAddress.Longitude != String.Empty))
            {
                origLat = Convert.ToDouble(originationAddress.Latitude);
                origLong = Convert.ToDouble(originationAddress.Longitude);
            }
            else if (originationAddress.Zip != String.Empty)
            {
                DataSet dsGeoCode = GetGeoCode(originationAddress.Zip);
                if ((dsGeoCode != null) && (dsGeoCode.Tables.Count > 0) && (dsGeoCode.Tables[0].Rows.Count > 0))
                {
                    origLat = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Latitude"]);
                    origLong = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Longitude"]);
                }
            }

            if ((destinationAddress.Latitude != String.Empty) && (destinationAddress.Longitude != String.Empty))
            {
                destLat = Convert.ToDouble(destinationAddress.Latitude);
                destLong = Convert.ToDouble(destinationAddress.Longitude);
            }
            else if (destinationAddress.Zip != String.Empty)
            {
                DataSet dsGeoCode = GetGeoCode(destinationAddress.Zip);
                if ((dsGeoCode != null) && (dsGeoCode.Tables.Count > 0) && (dsGeoCode.Tables[0].Rows.Count > 0))
                {
                    destLat = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Latitude"]);
                    destLong = Convert.ToDouble(dsGeoCode.Tables[0].Rows[0]["Longitude"]);
                }
            }

            //Check if any of the geo codes are zero.
            if (origLat == 0 && origLong == 0)
            {
                throw new Exception("The origination geo code is (0,0), distance cannot be calculated for the touch.");
            }
            else if (destLat == 0 && destLong == 0)
            {
                throw new Exception("The origination geo code is (0,0), distance cannot be calculated for the touch.");
            }

            return Math.Round(GetDistanceFromGeoCodes(origLat, origLong, destLat, destLong), 2);
        }

        public PCMilerGeoinfo GetLatitudeLongitude(Address address)
        {
            //set the login values
            PCMService.LoginType login = new PCMService.LoginType();
            login.UserID = "PackRat";
            login.Password = "PackRat88152";
            login.Account = "1800PackRat";

            PCMilerGeoinfo geoinfo = new PCMilerGeoinfo();

            // Address information
            PCMService.GeoLocType pcmGeoLocType = new PCMService.GeoLocType();
            PCMService.LocationInputType pcmLocationInputType = new PCMService.LocationInputType();


            pcmLocationInputType.Address1 = address.AddressLine1;
            pcmLocationInputType.Address2 = address.AddressLine2;
            pcmLocationInputType.City = address.City;
            pcmLocationInputType.State = address.State;
            pcmLocationInputType.Zip = address.Zip;

            pcmGeoLocType.GeoAddress = pcmLocationInputType;
            pcmGeoLocType.GeoType = "G";
            pcmGeoLocType.GeoList = "Yes";


            PCMService.GeoLocType[] pcmGeoLocTypes = { pcmGeoLocType };

            PCMService.Service pcmServiceClient = new PCMService.Service();

            PCMService.ReportResponse[] pcmResponses = pcmServiceClient.PMWSGeoCode(login, pcmGeoLocTypes, "Current", "");
            bool ignoreError = true;
            if (pcmResponses[0].ErrorList != null)
            {

                foreach (var item in pcmResponses[0].ErrorList)
                {
                    if (item.ErrorCode != "1000")
                    {
                        ignoreError = false;

                    }

                    geoinfo.Error = item.ErrorDesc;
                }
            }

            if (ignoreError)
            {
                if (pcmResponses[0].Report.Geocode != null)
                {
                    foreach (var item in pcmResponses[0].Report.Geocode)
                    {
                        geoinfo.Latitude = Math.Round(Convert.ToDouble(item.Lat) / 1000000, 6).ToString();
                        geoinfo.Longitude = Math.Round(Convert.ToDouble(item.Long) / 1000000, 6).ToString();

                    }
                }
            }
            return geoinfo;
        }

        public ValidatedAddress ValidateAddress(Address address)
        {
            PCMService.LoginType login = new PCMService.LoginType();
            login.UserID = "PackRat";
            login.Password = "PackRat88152";
            login.Account = "1800PackRat";

            PCMilerGeoinfo geoinfo = new PCMilerGeoinfo();

            // Address information
            PCMService.GeoLocType pcmGeoLocType = new PCMService.GeoLocType();
            PCMService.LocationInputType pcmLocationInputType = new PCMService.LocationInputType();

            pcmLocationInputType.Address1 = address.AddressLine1;
            pcmLocationInputType.Address2 = address.AddressLine2;
            pcmLocationInputType.City = address.City;
            pcmLocationInputType.State = address.State;
            pcmLocationInputType.Zip = address.Zip;

            pcmGeoLocType.GeoAddress = pcmLocationInputType;
            pcmGeoLocType.GeoType = "G";
            pcmGeoLocType.GeoList = "Yes";

            PCMService.GeoLocType[] pcmGeoLocTypes = { pcmGeoLocType };

            // Validate Address with PC Miler.
            PCMService.Service pcmServiceClient = new PCMService.Service();

            PCMService.ReportResponse[] pcmResponses = pcmServiceClient.PMWSGeoCode(login, pcmGeoLocTypes, "Current", "");
            ValidatedAddress vAddress = new ValidatedAddress();

            if (pcmResponses[0].ErrorList != null)
            {

                foreach (var item in pcmResponses[0].ErrorList)
                {
                    vAddress.Error += item.ErrorDesc + " - ";
                    // vAddress.Error += item.ErrorDesc + "<br/>";
                }

                //if (pcmResponses[0].Report.Geocode != null)
                //{
                //    if (pcmResponses[0].Report.Geocode.Length > 1)
                //    {
                //        vAddress.Error = "Multiple streets found!";

                //    }

                //}
            }
            else
            {
                vAddress.IsAddressValid = true;
                vAddress.AddressLine1 = pcmResponses[0].Report.Geocode[0].Address1;
                vAddress.AddressLine2 = pcmResponses[0].Report.Geocode[0].Address2;
                vAddress.City = pcmResponses[0].Report.Geocode[0].City;
                vAddress.State = pcmResponses[0].Report.Geocode[0].State;
                vAddress.Zip = pcmResponses[0].Report.Geocode[0].Zip;

            }

            return vAddress;
        }

    }
}
