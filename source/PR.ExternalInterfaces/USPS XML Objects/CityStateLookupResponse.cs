﻿using System.Collections.Generic;

namespace PR.ExternalInterfaces.USPS_XML_Objects
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CityStateLookupResponse
    {

        private CityStateLookupResponseZipCode[] zipCodeField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ZipCode")]
        public CityStateLookupResponseZipCode[] ZipCode
        {
            get
            {
                return this.zipCodeField;
            }
            set
            {
                this.zipCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CityStateLookupResponseZipCode
    {

        private string zip5Field;

        private string cityField;

        private string stateField;

        private byte idField;

        /// <remarks/>
        public string Zip5
        {
            get
            {
                return this.zip5Field;
            }
            set
            {
                this.zip5Field = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string State
        {
            get
            {
                return this.stateField;
            }
            set
            {
                this.stateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }


}
