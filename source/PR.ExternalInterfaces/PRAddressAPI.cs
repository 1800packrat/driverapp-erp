﻿// -----------------------------------------------------------------------
// <copyright file="PRAddressAPI.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

using System.Collections.Generic;

namespace PR.ExternalInterfaces
{
    using PR.Entities;

    /// <summary>
    /// PRAddressAPI validates Address, returns Geocode, Distance between two addresses
    /// </summary>
    public class PRAddressAPI
    {
        GoogleAPI oGoogleAPI = new GoogleAPI();
        
        public AddressValidator GetDistance(Address originationAddress, Address destinationAddress)
        {
            oGoogleAPI.recursionCounter = 0;    // before calling GetDistance setting recursionCounter 0
            return oGoogleAPI.GetDistance(originationAddress, destinationAddress);
        }
        public decimal GetStraightLineDistance(Address originationAddress, Address destinationAddress)
        {
            return oGoogleAPI.GetStraightLineDistance(originationAddress, destinationAddress);
        }
        public GeoInfo GetLatitudeLongitude(Address address)
        {
            return oGoogleAPI.GetLatitudeLongitude(address);
        }

        public ValidatedAddress ValidateAddress(Address address)
        {
            return oGoogleAPI.ValidateAddress(address);
        }


        /// <summary>
        /// wraper too GoogleAPI GetDistanceByLatitudeLongitude
        /// </summary>
        /// <param name="originLatitude"></param>
        /// <param name="originLongitude"></param>
        /// <param name="destinationLatitude"></param>
        /// <param name="destinationLongitude"></param>
        /// <returns></returns>
        public decimal GetDistanceByLatitudeLongitude(double originLatitude, double originLongitude, double destinationLatitude, double destinationLongitude)
        {
            return oGoogleAPI.GetDistanceByLatitudeLongitude( originLatitude,  originLongitude,  destinationLatitude,  destinationLongitude);
        }

        public Address UsAddressLookUpByZipcode(string usZipcode)
        {
            return new UspsApi().UsAddressLookUpByZipcode(usZipcode);
        }
        public IEnumerable<Address> UsAddressLookUpByOrgDestZipcodes(string usOrgZipcode, string usDestZipcode)
        {
            return new UspsApi().UsAddressLookUpByOrgDestZipcodes(usOrgZipcode, usDestZipcode);
        }
    }
}
