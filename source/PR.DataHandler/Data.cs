using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml;


namespace PR.DataHandler
{
    public static class Data
    {

        static Dictionary<string, string> _connectionStrings = new Dictionary<string, string>();
        //static string _connStringSLNet = @"Persist Security Info=False;User ID=PackRatUser28;Password=R@t$4N0w;Initial Catalog=SLNetLogon;Data Source=Server012\SQLDev";
        //static string _connStringPR = @"Persist Security Info=False;User ID=PackRatUser28;Password=R@t$4N0w;Initial Catalog=PRSLLocal;Data Source=Server012\SQLDev";

        public static string _connStringSLNet = String.Empty; //= PR.DataHandler.Properties.Settings.Default.SLNet;
        public static string _connStringPR = String.Empty;//= PR.DataHandler.Properties.Settings.Default.PRSLocal;
        public static string _connStringSTARS = String.Empty;
        //static Dictionary<string, string> _dbConnections = new Dictionary<string, string>();

        public static string _connStringERPDB = String.Empty;


        #region "Methods"
        public static SqlConnection GetSLNetConnection()
        {
            SqlConnection conn = new SqlConnection(_connStringSLNet);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        public static SqlConnection GetConnection(string connString)
        {
            SqlConnection conn = new SqlConnection(connString);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        public static SqlConnection GetPRConnection()
        {
            SqlConnection conn = new SqlConnection(_connStringPR);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        //public static SqlConnection GetSTARSConnection()
        //{
        //    SqlConnection conn = new SqlConnection(_connStringSTARS);
        //    if (conn.State != ConnectionState.Open)
        //        conn.Open();
        //    return conn;
        //}

        public static SqlConnection GetSFERPConnection()
        {
            SqlConnection conn = new SqlConnection(_connStringERPDB);
            if (conn.State != ConnectionState.Open)
                conn.Open();
            return conn;
        }

        public static bool CloseConnection(SqlConnection conn)
        {
            try
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                    conn.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        
        public static void LoadSettings()
        {
            if ((_connStringPR == String.Empty) || (_connStringSLNet == String.Empty) || (_connStringERPDB == string.Empty))
            {
                // this will support for different web app on same server with different SQL server

                string strFilepath = ConfigurationManager.AppSettings["PRSettingFilePath"];
                if (string.IsNullOrWhiteSpace(strFilepath))
                    strFilepath = @"C:\PRSettings\PR.xml";

                XmlTextReader reader = new XmlTextReader(strFilepath);
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.HasAttributes)
                        {
                            reader.MoveToAttribute("key");
                            switch (reader.Value)
                            {
                                case "PRSLocal":
                                    reader.MoveToAttribute("value");
                                    _connStringPR = reader.Value;
                                    break;
                                case "SLNet":
                                    reader.MoveToAttribute("value");
                                    _connStringSLNet = reader.Value;
                                    break;
                                case "STARS":
                                    reader.MoveToAttribute("value");
                                    _connStringSTARS = reader.Value;
                                    break;
                                case "SFERPDB":
                                    reader.MoveToAttribute("value");
                                    _connStringERPDB = reader.Value;
                                    break;
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Gets the connection string to the database corresponding to thhe Location Code and Corp Code.
        /// </summary>
        /// <param name="locationCode"></param>
        /// <param name="corpCode"></param>
        /// <returns></returns>
        public static SqlConnection GetDBConnection(string locationCode, string corpCode)
        {
            string connString = _connectionStrings[locationCode + "," + corpCode].ToString();
            return GetConnection(connString);
        }
        #endregion

        #region Properties
        //public static Dictionary<string, string> DBConnections  //readonly
        //{
        //    get
        //    {
        //        return _dbConnections;
        //    }
        //}

        #endregion
    }
}
