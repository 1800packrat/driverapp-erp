using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PR.Entities;
using PR.ExceptionTypes;
using System.Web;

namespace PR.DataHandler
{
    public class ERP_DataHandler : BaseDataHandler
    {
        public ERP_DataHandler()
        {
            Data.LoadSettings();
        }

        #region Methods to get Capacity and save data DB for TG-450 

        public void TG450_LogCapacityCalendarTouchData(DataTable cAPTouchLogDetailDataTable, string locationCode)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetSFERPConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter() { ParameterName = "@CAPTouchLogDetailDataTable", Value = cAPTouchLogDetailDataTable };
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "TG450_SP_AddCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }

        public void TG450_GetTouchScheduleAvailableData(DateTime startDate, string locationCode)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter() { ParameterName = "@CAPDate", Value = startDate }; //.ToString("MM/dd/yyyy")
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "TG450_SP_GetCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }

        #endregion
         
    }

}
