using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using PR.Entities;
using PR.ExceptionTypes;
using System.Web;
using PR.Entities.CCPaymentProcess;

namespace PR.DataHandler
{
    public class LocalDataHandler : BaseDataHandler
    {
        public LocalDataHandler()
        {
            Data.LoadSettings();
        }

        public DataSet GetUnitInfoForStubMethod()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_SL_UnitDetails");
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public bool IsOldDominionZip(string zip)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sqlText = string.Format("select ZipCode from ODZipCodes where ZipCode=@ZipCode and IsDeleted=0", zip);

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@ZipCode";
                param[0].Value = zip;

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText, param);
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    return true;
                else
                    return false;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetUnitByUnitName(string locationCode, string unitName)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[0].ParameterName = "@LocationCode";
                param[0].Value = locationCode;
                param[1].ParameterName = "@UnitName";
                param[1].Value = unitName;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetUnitInfo", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetZipCodeInfoFromGlobalView(string zip)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string sql = "Select ZipCodeOwnerID, SiteID, sZipCode, bOwned, bServicedBy, bLDMServiced, bESATServiced, MarketId, DBName from VW_ZipCodeowners where sZipCode=@zip";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@zip";
                param[0].Value = zip;

                conn = Data.GetPRConnection();
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetCorpCodeAndLocationCode(int SiteID, string connstring)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                StringBuilder sqlText = new StringBuilder();
                sqlText.Append("SELECT Sites.sLocationCode, Owners.sCorpCode ");
                sqlText.Append("FROM Sites INNER JOIN ");
                sqlText.Append("Owners ON Sites.OwnerID = Owners.OwnerID ");
                sqlText.Append("WHERE Sites.SiteID = @SiteID");

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@SiteID";
                param[0].Value = SiteID;

                conn = Data.GetConnection(connstring);
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sqlText.ToString(), param);
                return ds;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet GetLoginInfo(string firstName, string lastName, string password)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsLogin = new DataSet();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();

                param[0].ParameterName = "@firstName";
                param[0].Value = firstName;
                param[1].ParameterName = "@lastName";
                param[1].Value = lastName;
                param[2].ParameterName = "@password";
                param[2].Value = password;

                dsLogin = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_LogIn", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsLogin;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="dtm"></param>
        /// <param name="siteId"></param>
        /// <param name="qorid"></param>
        /// <param name="tenantId"></param>
        /// <param name="activity"></param>
        public void InsertActivityLog(string userId, DateTime dtm, int siteId, int qorid, int tenantId, string activity)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@UserID";
                param[0].Value = userId;
                param[1].ParameterName = "@DTM";
                param[1].Value = dtm;
                param[2].ParameterName = "@SiteID";
                param[2].Value = siteId;
                param[3].ParameterName = "@QORID";
                param[3].Value = qorid;
                param[4].ParameterName = "@TenantID";
                param[4].Value = tenantId;
                param[5].ParameterName = "@Activity";
                param[5].Value = activity;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGActivityLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGErrorLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid, string strAppURL, string DataDump)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[8];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();
                param[6] = new SqlParameter();
                param[7] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;
                param[6].ParameterName = "@AppURL";
                param[6].Value = strAppURL;
                param[7].ParameterName = "@DataDump";
                param[7].Value = DataDump;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRGErrorLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public int InsertActivityLog(int qorid, string activityType, string activityText, string activityBy)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORID";
                param[0].Value = qorid;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ActivityType";
                param[1].Value = activityType;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityText";
                param[2].Value = activityText;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityBy";
                param[3].Value = activityBy;

                param[4] = new SqlParameter();
                param[4].ParameterName = "@ApplicationName";
                param[4].Value = GetApplicationName();

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ins_PRActivityLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        private string GetApplicationName()
        {
            ///Since we are adding below code now added exception block because, dont know how many places this method beeing used. 
            ///If this method hits by any job then we may not get host name, that the reason why added exception block here.
            string applicationName = string.Empty;
            try
            {
                var request = HttpContext.Current.Request;
                applicationName = request.ServerVariables["HTTP_HOST"];

                if (!string.IsNullOrEmpty(applicationName))
                {
                    applicationName = applicationName.ToUpper();
                    if (applicationName.EndsWith(".1800PACKRAT.COM"))
                        applicationName = applicationName.Replace(".1800PACKRAT.COM", "");
                    if (applicationName.Length > 20)
                        applicationName = applicationName.Substring(0, 19); //Since database field name lenght 20 charectors then we are stripping out to 20 characters
                }
            }
            catch { }

            return applicationName;
        }

        public int InsertActivityLog(int quoteId, int qorId, string activityTypeName, string activityText, string activityBy, string tODRef, int USSCustId)
        {
            SqlConnection conn = Data.GetPRConnection();
            int retValue = 0;
            try
            {
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@STARSID";
                param[0].Value = quoteId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@QORID";
                param[1].Value = qorId;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@ActivityTypeName";
                param[2].Value = activityTypeName;

                param[3] = new SqlParameter();
                param[3].ParameterName = "@ActivityText";
                param[3].Value = activityText;

                param[4] = new SqlParameter();
                param[4].ParameterName = "@ActivityBy";
                param[4].Value = activityBy;

                param[5] = new SqlParameter();
                param[5].ParameterName = "@ActivityDateTime";
                param[5].Value = DateTime.Now;

                param[6] = new SqlParameter();
                param[6].ParameterName = "@ApplicationName";
                param[6].Value = GetApplicationName();

                param[7] = new SqlParameter();
                param[7].ParameterName = "@TODRef";
                param[7].Value = tODRef;//TODO

                param[8] = new SqlParameter();
                param[8].ParameterName = "@USSCustId";
                param[8].Value = USSCustId;

                retValue = SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "USS_Insert_ActivityLogBYActivityName", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return retValue;
        }

        public DataSet GetGeocode(string zip)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsZipCode = new DataSet();
            try
            {
                string sql = "Select Latitude, longitude from USPSZipCodeList where ZipCode= @zip";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@zip", Value = zip };

                dsZipCode = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsZipCode;
        }

        public DataSet GetActivePackRatFacilitiesInfo()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsFacilities = new DataSet();

            try
            {
                dsFacilities = SqlHelper.ExecuteDataset(conn, CommandType.Text, "Select * from RDFacility where statusrowid=70 and storetype>0");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsFacilities;
        }

        public DataSet GetRDFacilityData(string locCode)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where slloccode='" + locCode + "'");

                string query = @"select * from RDfacility where slloccode = @slloccode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@slloccode", Value = locCode };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityDataByStoreNumber(string storeNumber)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where StoreNo ='" + storeNumber + "'");
                string query = @"select * from RDfacility where statusrowid>=65 and storetype > 0 and  StoreNo = @StoreNo";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@StoreNo", Value = storeNumber };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilityData()
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility");
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetRDFacilitiesDataByMarket(int marketId)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where fk_marketId=" + marketId);
                string query = "select * from RDfacility where fk_marketId= @marketId";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@marketId", Value = marketId };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetPRGOrderContractLogData(int qorid)
        {
            DataSet dsOrderContractLog = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsOrderContractLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from PRGQuoteorderLog where QORID=" + qorid);
                string query = "select * from PRGQuoteorderLog where QORID=@qorid";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@qorid", Value = qorid };

                dsOrderContractLog = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsOrderContractLog;
        }

        //SPERP-TODO-CTRMV , Remove after done with ESB
        //public DataSet GetTripDetails(int T26ContainerId)
        //{
        //    DataSet dsTenant = new DataSet();
        //    SqlConnection conn = Data.GetSTARSConnection();
        //    try
        //    {
        //        SqlParameter[] param = new SqlParameter[1];
        //        param[0] = new SqlParameter();
        //        param[0].ParameterName = "@ContainerId";
        //        param[0].Value = T26ContainerId;

        //        dsTenant = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_FAWT_GetTripDetails", param);
        //    }
        //    finally
        //    {
        //        Data.CloseConnection(conn);
        //    }
        //    return dsTenant;
        //}

        public DataSet GetLoadDriverDetails(int QorId, string TouchType, int SLSeqNumber)
        {
            DataSet dsTenant = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QorId";
                param[0].Value = QorId;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@TouchType";
                param[1].Value = TouchType;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@SLSeqNumber";
                param[2].Value = SLSeqNumber;

                dsTenant = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_FAWT_GetDriverDetailsForQuote", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsTenant;
        }

        public DataSet GetCallBlastData(string locationCode)
        {
            DataSet dsCallBlast = new DataSet();
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("select u.StoreRowid, u.StoreNumber, u.City, u.Level1Name, u.Level1Phone, u.Level2Name, u.Level2Phone, u.Level3Name, u.level3Phone, ltrim(rtrim(u.Email1)) as Email1, ltrim(rtrim(u.Email2)) as Email2 from UniqueCallBlast u inner join RDFacility f on u.StoreRowId = f.StoreRowId where f.slLocCode = '{0}'", locationCode);
                string sql = @"select u.StoreRowid, u.StoreNumber, u.City, u.Level1Name, u.Level1Phone, u.Level2Name, u.Level2Phone, u.Level3Name, u.level3Phone, ltrim(rtrim(u.Email1)) as Email1, ltrim(rtrim(u.Email2)) as Email2 
from UniqueCallBlast u 
inner join RDFacility f on u.StoreRowId = f.StoreRowId 
where f.slLocCode = @locationCode";

                var param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@locationCode", Value = locationCode };

                dsCallBlast = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
            }
            finally
            {
                //conn.Close();
                Data.CloseConnection(conn);
            }
            return dsCallBlast;
        }

        public DataSet GetAllPackratFacilities()
        {
            DataSet dsFacilites = new DataSet();
            string sql = "select * from RdFacility";
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                dsFacilites = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql);
            }
            finally
            {
                conn.Close();
            }

            return dsFacilites;
        }

        public List<PostalCodes> GetPostalCodes()
        {
            SqlConnection conn = Data.GetPRConnection();
            List<PostalCodes> oPostalCodes = null;
            List<string> failedZipCodes = new List<string>();

            try
            {
                var dsPostalCode = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetPostalCodes");

                if (dsPostalCode?.Tables?.Count > 0 && dsPostalCode.Tables[0]?.Rows?.Count > 0)
                {
                    oPostalCodes = new List<PostalCodes>();
                    foreach (DataRow dr in dsPostalCode.Tables[0]?.Rows)
                    {
                        try
                        {
                            oPostalCodes.Add(new PostalCodes
                            {
                                City = dr["City"] != DBNull.Value ? Convert.ToString(dr["City"]) : string.Empty,
                                State = dr["State"] != DBNull.Value ? Convert.ToString(dr["State"]) : string.Empty,
                                County = dr["County"] != DBNull.Value ? Convert.ToString(dr["County"]) : string.Empty,
                                Zipcode = Convert.ToString(dr["Zipcode"]),
                                Latitude = dr["Latitude"] != DBNull.Value ? Convert.ToDecimal(dr["Latitude"]) : 0,
                                Longitude = dr["Longitude"] != DBNull.Value ? Convert.ToDecimal(dr["Longitude"]) : 0
                                //,Status = (PostalCodeValidationStatus)Enum.Parse(typeof(PostalCodeValidationStatus), Convert.ToString(dr["Status"]))
                            });
                        }
                        catch
                        {
                            string zipcode = dr["Zipcode"] != DBNull.Value ? Convert.ToString(dr["Zipcode"]) : "NoZipCode";
                            failedZipCodes.Add(zipcode);
                        }
                    }
                }
            }
            finally
            {
                Data.CloseConnection(conn);

                if (failedZipCodes.Count > 0)
                {
                    try
                    {
                        LocalDataHandler local = new LocalDataHandler();
                        local.InsertPRErrorLog(DateTime.Now, "", "Postal Code Read Error", "Error Reading Zipcode Data", "GetPostalCodes Method: " + String.Join(", ", failedZipCodes.ToArray()), -1);
                    }
                    catch
                    { //Ignore it 
                    }
                }
            }
            return oPostalCodes;
        }

        public void UpdatePostalCodeStatus(string Zipcode, PostalCodeValidationStatus Status)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[2];
                param[0] = new SqlParameter { ParameterName = "@Zipcode", Value = Zipcode };
                param[1] = new SqlParameter { ParameterName = "@Status", Value = Status.ToString() };

                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_UpdatePostalCodes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void InsertPostalCode(PostalCodes postalCode)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[6];
                param[0] = new SqlParameter { ParameterName = "@Zipcode", Value = postalCode.Zipcode };
                param[1] = new SqlParameter { ParameterName = "@City", Value = postalCode.City };
                param[2] = new SqlParameter { ParameterName = "@State", Value = postalCode.State };
                param[3] = new SqlParameter { ParameterName = "@Latitude", Value = postalCode.Latitude };
                param[4] = new SqlParameter { ParameterName = "@Longitude", Value = postalCode.Longitude };
                param[5] = new SqlParameter { ParameterName = "@Status", Value = postalCode.Status.ToString() };

                SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "USP_InsertPostalCodes", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void LogGAPIRequestResponse(GoogleMapAuditLog googleMapAuditLog)
        {
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                var param = new SqlParameter[7];
                param[0] = new SqlParameter { ParameterName = "@Request", Value = googleMapAuditLog.Request };
                param[1] = new SqlParameter { ParameterName = "@Response", Value = googleMapAuditLog.Response };
                param[2] = new SqlParameter { ParameterName = "@StartTime", Value = googleMapAuditLog.StartTime };
                param[3] = new SqlParameter { ParameterName = "@EndTime", Value = googleMapAuditLog.EndTime };
                param[4] = new SqlParameter { ParameterName = "@MethodName", Value = googleMapAuditLog.MethodName };
                param[5] = new SqlParameter { ParameterName = "@ApplicationName", Value = googleMapAuditLog.ApplicationName };
                param[6] = new SqlParameter { ParameterName = "@ServerName", Value = googleMapAuditLog.ServerName };

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_InsertGoogleMapAuditLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void LogUspsRequestResponse(string request, string response, DateTime dateTime, string methodName)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter
                {
                    ParameterName = "@Request",
                    Value = request
                };

                param[1] = new SqlParameter
                {
                    ParameterName = "@Response",
                    Value = response
                };

                param[2] = new SqlParameter
                {
                    ParameterName = "@DateTime",
                    Value = dateTime
                };

                param[3] = new SqlParameter
                {
                    ParameterName = "@methodName",
                    Value = methodName
                };

                SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_USPS_INS_Log", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        // Added to get catpacity
        public DataSet GetCapacity(DateTime startDate, DateTime endDate, string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCapacity = new DataSet();
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@StartDate";
                param[0].Value = startDate;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@EndDate";
                param[1].Value = endDate;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@LocationCode";
                param[2].Value = locationCode;

                dsCapacity = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCapacity;
        }

        public DataSet GetCapCategoryTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategoryTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetTouchMilesByTypeByFacilityByDateRange(int touchTypeId, string locationCode, DateTime startDate, DateTime endDate)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@TouchTypeId";
            param[0].Value = touchTypeId;

            param[1] = new SqlParameter();
            param[1].ParameterName = "@locationCode";
            param[1].Value = locationCode;

            param[2] = new SqlParameter();
            param[2].ParameterName = "@StartDate";
            param[2].Value = startDate;

            param[3] = new SqlParameter();
            param[3].ParameterName = "@EndDate";
            param[3].Value = endDate;

            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetTouchMilesByTypeByFacilityBYDateRange", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetAllCapTouchTypes()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetAllTouchTypes");
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetCapacityColorActionsByRole(int? roleLevelId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@RoleLevelId";
            param[0].Value = roleLevelId;


            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure,
                    "USP_CAP_GetCapacityColorActionsByRole", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public String GetCapCategoryByCategoryId(int categoryId)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@CategoryId";
            param[0].Value = categoryId;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetCapCategory", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            if (dsResult != null && dsResult.Tables.Count > 0 && dsResult.Tables[0].Rows.Count > 0)
            {
                if (dsResult.Tables[0].Rows[0][0] != DBNull.Value)
                    return Convert.ToString(dsResult.Tables[0].Rows[0][0]);
                else
                    return null;
            }
            else
                return null;
        }

        public DataSet GetFacilitiesWithLimitedCapacities(string locationCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter();
            param[0].ParameterName = "@LocationCode";
            param[0].Value = locationCode;
            DataSet dsResult = new DataSet();
            try
            {
                dsResult = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_CAP_GetFacilitiesWithLimitedCapacity", param);
            }
            catch
            {
                throw;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsResult;
        }

        public DataSet GetETASetting(int QorID, string TouchType, int SLSeqNo)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QorID";
                param[0].Value = QorID;

                param[1] = new SqlParameter();
                param[1].ParameterName = "@TouchType";
                param[1].Value = TouchType;

                param[2] = new SqlParameter();
                param[2].ParameterName = "@SLSeqNo";
                param[2].Value = SLSeqNo;
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetETASettings", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        public DataSet GetETASettingList(string uniqueIdsToGetETASettings)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@TouchUniqueKeys";
                param[0].Value = uniqueIdsToGetETASettings;

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_GetETASettings_MultipleTouches", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return ds;
        }

        #region Methods to get Capacity and save data DB for TG-450 

        public void TG450_LogCapacityCalendarTouchData(DataTable cAPTouchLogDetailDataTable, string locationCode)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[2];
                param[0] = new SqlParameter() { ParameterName = "@CAPTouchLogDetailDataTable", Value = cAPTouchLogDetailDataTable };
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "TG450_SP_AddCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public DataSet TG450_GetTouchScheduleAvailableData(string locationCode, DateTime startDate, int numberOfDays)
        {
            Data.LoadSettings();
            DataSet dsCapacity = new DataSet();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[3];
                param[0] = new SqlParameter() { ParameterName = "@CAPDate", Value = startDate }; //.ToString("MM/dd/yyyy")
                param[1] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };
                param[2] = new SqlParameter() { ParameterName = "@NumberOfDays", Value = numberOfDays };

                dsCapacity = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "TG450_SP_GetCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCapacity;
        }

        public DataSet Get_TG733_GetFacilityCapacityData(DateTime startDate, DateTime endDate, string locationCode)
        {
            Data.LoadSettings();
            DataSet dsCapacity = new DataSet();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[3];
                param[0] = new SqlParameter() { ParameterName = "@LocationCode", Value = locationCode };
                param[1] = new SqlParameter() { ParameterName = "@StartDate", Value = startDate };
                param[2] = new SqlParameter() { ParameterName = "@EndDate", Value = endDate };

                dsCapacity = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "TG733_SP_GetFacilityCapacityData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsCapacity;

        }

        //Added by Sohan, Get end Point config. 
        public DataSet GetEsbEndPintConfig()
        {
            //// Get from database later once finalized.
            //return new EsbEndPointConfig
            //{
            //    Url = "http://esb.1800packrat.com:40020/",
            //    Username = "Packrat2019",
            //    Password = "SanFran2005!"
            //};

            Data.LoadSettings();
            DataSet dsConfig = new DataSet();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                dsConfig = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "ERP_GetEndPointConfig");
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsConfig;

        }

        #endregion

        #region Added below method for Driver App to work/build

        /// <summary>
        /// Gets all the CPP available for today.
        /// </summary>
        /// <returns></returns>
        public DataSet GetCPPInfo()
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet dsCPP = new DataSet();
            try
            {
                dsCPP = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_sel_PRGCPPInfo");
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return dsCPP;
        }

        public int InsertSLAuditLog(string methodName, string applicationName, string userName, DateTime startDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@MethodName";
                param[0].Value = methodName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ApplicationName";
                param[1].Value = applicationName;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserName";
                param[2].Value = userName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ServerName";
                param[3].Value = Environment.MachineName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@StartTime";
                param[4].Value = startDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_SLCallAuditLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return auditLogId;
        }

        public int InsertSLAuditLog(string methodName, string applicationName, string userName, int qorid, DateTime startDateTime)
        {
            int auditLogId = 0;
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                SqlParameter[] param = new SqlParameter[6];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@MethodName";
                param[0].Value = methodName;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@ApplicationName";
                param[1].Value = applicationName;
                param[2] = new SqlParameter();
                param[2].ParameterName = "@UserName";
                param[2].Value = userName;
                param[3] = new SqlParameter();
                param[3].ParameterName = "@ServerName";
                param[3].Value = Environment.MachineName;
                param[4] = new SqlParameter();
                param[4].ParameterName = "@QORID";
                param[4].Value = qorid;
                param[5] = new SqlParameter();
                param[5].ParameterName = "@StartTime";
                param[5].Value = startDateTime;

                auditLogId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ins_SLCallAuditLog", param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return auditLogId;
        }

        public void UpdateSLAuditLog(int auditLogId, DateTime endTime)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@AuditLogId";
                param[0].Value = auditLogId;
                param[1] = new SqlParameter();
                param[1].ParameterName = "@EndTime";
                param[1].Value = endTime;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_upd_SLCallAuditLog", param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public void UpdateSLAuditLog(int auditLogId, int QORID, DateTime endTime)
        {
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                //string sql = String.Format("update SLCallAuditLog set EndTime='{0}', QORID={1} where AuditLogId={2}", endTime, QORID, auditLogId);
                string sql = "update SLCallAuditLog set EndTime=@endTime, QORID=@QORID where AuditLogId=@auditLogId";

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter { ParameterName = "@endTime", Value = endTime };
                param[1] = new SqlParameter { ParameterName = "@QORID", Value = QORID };
                param[2] = new SqlParameter { ParameterName = "@auditLogId", Value = auditLogId };

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sql, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }


        public int[] GetArchivedQorIDs(string QorIDs)
        {
            SqlConnection conn = Data.GetPRConnection();
            DataSet ds = new DataSet();

            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@QORIDs";
                param[0].Value = QorIDs;

                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_sel_GetArchivedQorIds", param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows.Cast<DataRow>().Select(row => Convert.ToInt32(row["QORID"].ToString())).ToArray();
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return null;
        }

        public DataSet GetRDFacilityDataByLocationCode(string SLLocCode)
        {
            DataSet dsRDfacility = new DataSet();
            SqlConnection conn = Data.GetPRConnection();

            try
            {
                //dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select * from RDfacility where SLLocCode='" + SLLocCode + "'");

                string query = "select * from RDfacility where SLLocCode = @SLLocCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter { ParameterName = "@SLLocCode", Value = SLLocCode };

                dsRDfacility = SqlHelper.ExecuteDataset(conn, CommandType.Text, query, param);
            }
            finally
            {
                Data.CloseConnection(conn);
            }

            return dsRDfacility;
        }

        public DataSet GetConcessionPlan(int promoglobalnum, string chgDescription, string locationCode)
        {
            SqlConnection conn = new SqlConnection();
            try
            {
                string connString = String.Empty;
                DataSet dsConcessionPlan = new DataSet();
                int siteId = 0;
                connString = GetSLDBConnStringByLocCode(locationCode);
                if (connString != String.Empty)
                {
                    siteId = GetSiteID(locationCode);
                    conn = Data.GetConnection(connString);
                    //                string sql = string.Format(@"select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount 
                    //from concessionplans cp 
                    //inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum 
                    //inner join promotions p on p.promoofferid = poi.promoofferid 
                    //inner join chargedesc cd on cp.chargedescid = cd.chargedescid 
                    //where cd.schgdesc='{0}' and p.ipromoglobalnum='{1}' and cp.siteid={2}", chgDescription, promoglobalnum, siteId);
                    string sql = @"select cp.iConcessionGlobalNum, cp.sPlanName, cp.bNeverExpires, cp.iExpirMonths, cp.dcChgAmt, cp.dcFixedDiscount, cp.dcPCDiscount 
                                    from concessionplans cp 
                                    inner join promoofferitems poi on cp.iConcessionGlobalNum = poi.iConcessionGlobalNum 
                                    inner join promotions p on p.promoofferid = poi.promoofferid 
                                    inner join chargedesc cd on cp.chargedescid = cd.chargedescid 
                                    where cd.schgdesc=@chgDescription and p.ipromoglobalnum=@promoglobalnum and cp.siteid=@siteId";

                    SqlParameter[] param = new SqlParameter[3];
                    param[0] = new SqlParameter { ParameterName = "@chgDescription", Value = chgDescription };
                    param[1] = new SqlParameter { ParameterName = "@promoglobalnum", Value = promoglobalnum };
                    param[2] = new SqlParameter { ParameterName = "@siteId", Value = siteId };

                    dsConcessionPlan = SqlHelper.ExecuteDataset(conn, CommandType.Text, sql, param);
                }
                return dsConcessionPlan;
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        }

        public string GetSLDBConnStringByLocCode(string slLocCode)
        {
            SqlConnection conn = Data.GetPRConnection();
            string slDBName = String.Empty;
            string slDBConnString = String.Empty;
            try
            {
                SqlCommand cmd = new SqlCommand();
                string sql = "Select SLDBName from RDFacility where SLLocCode = @slLocCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@slLocCode";
                param[0].Value = slLocCode;

                slDBName = SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param).ToString();
                Data.CloseConnection(conn);
                if (Data._connStringPR.IndexOf("PRSLLocalTest") > 0)
                    slDBConnString = Data._connStringPR.Replace("PRSLLocalTest", slDBName);
                else
                    slDBConnString = Data._connStringPR.Replace("PRSLLocal", slDBName);
            }
            catch (Exception ex)
            {
                try
                {
                    LocalDataHandler local = new LocalDataHandler();
                    local.InsertPRErrorLog(DateTime.Now, "", "Connection String LocationCode: " + slLocCode, ex.Message, ex.StackTrace.ToString(), -1);
                }
                catch
                {
                    throw ex;
                }
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return slDBConnString;
        }

        public int GetSiteID(string locationCode)
        {
            int siteId = 0;
            SqlConnection conn = Data.GetPRConnection();
            try
            {
                string sql = "select slsiteid from rdfacility where slloccode= @locationCode";

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter();
                param[0].ParameterName = "@locationCode";
                param[0].Value = locationCode;

                siteId = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, sql, param));
            }
            finally
            {
                Data.CloseConnection(conn);
            }
            return siteId;
        }


        #endregion

    }

}
