﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace PR.DataHandler
{
    public abstract class BaseDataHandler
    {
        /// <summary>
        /// error log insert for passport application 
        /// </summary>
        /// <param name="errorDateTime"></param>
        /// <param name="userId"></param>
        /// <param name="exceptionType"></param>
        /// <param name="exceptionMsg"></param>
        /// <param name="stackTrace"></param>
        /// <param name="qrid"></param>
        /// <param name="strAppURL"></param>
        /// <param name="DataDump"></param>
        /// <param name="lDMQuoteID"></param>
        /// <param name="trailerQuoteNo"></param>
        public static void InsertPRErrorLog(DateTime errorDateTime, string userId, string exceptionType, string exceptionMsg, string stackTrace, int qrid, string strAppURL, string DataDump, int lDMQuoteID, int trailerQuoteNo)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[10];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
                param[5] = new SqlParameter();
                param[6] = new SqlParameter();
                param[7] = new SqlParameter();
                param[8] = new SqlParameter();
                param[9] = new SqlParameter();

                param[0].ParameterName = "@ErrorDTM";
                param[0].Value = errorDateTime;
                param[1].ParameterName = "@UserId";
                param[1].Value = userId;
                param[2].ParameterName = "@ExceptionType";
                param[2].Value = exceptionType;
                param[3].ParameterName = "@ExceptionMsg";
                param[3].Value = exceptionMsg;
                param[4].ParameterName = "@StackTrace";
                param[4].Value = stackTrace;
                param[5].ParameterName = "@QRID";
                param[5].Value = qrid;
                param[6].ParameterName = "@AppURL";
                param[6].Value = strAppURL;
                param[7].ParameterName = "@DataDump";
                param[7].Value = DataDump;
                param[8].ParameterName = "@LDMQuoteID";
                param[8].Value = lDMQuoteID;
                param[9].ParameterName = "@TrailerQuoteNo";
                param[9].Value = trailerQuoteNo;

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_insertPRGErrorLogPassport", param);
            }
            catch(Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }
        } 

        //by Sohan - We may need this in future. Leave it.
        public static void LogTouchCapacityCalendar(DataTable cAPTouchLoggingDataTable, DataTable cAPTouchLogDetailDataTable, string IsAction = null, string touchActionOnTouchType = null, int? touchActionOnTouchSequence = null)
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[5];
                param[0] = new SqlParameter();
                param[1] = new SqlParameter();
                param[2] = new SqlParameter();
                param[3] = new SqlParameter();
                param[4] = new SqlParameter();
               

                param[0].ParameterName = "@CAPTouchLoggingDataTable";
                param[0].Value = cAPTouchLoggingDataTable;
                param[1].ParameterName = "@CAPTouchLogDetailDataTable";
                param[1].Value = cAPTouchLogDetailDataTable;
                param[2].ParameterName = "@IsAction";
                param[2].Value = IsAction;
                param[3].ParameterName = "@TouchActionOnTouchType";
                param[3].Value = touchActionOnTouchType;
                param[4].ParameterName = "@TouchActionOnTouchSequence";
                param[4].Value = touchActionOnTouchSequence;
               
              

                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "LogCapacityCalendarTouchData", param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }  

        public static void InsertDataSetToDB(string spName ,DataTable dataSetObject )
        {
            Data.LoadSettings();

            SqlConnection conn = Data.GetPRConnection();
            try
            {
                SqlParameter[] param;
                param = new SqlParameter[1];
                param[0] = new SqlParameter();



                param[0].ParameterName = "@dataset";
                param[0].Value = dataSetObject;



                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, spName, param);
            }
            catch (Exception ex)
            {
                // as its an error log no need to throw exception
            }
            finally
            {
                Data.CloseConnection(conn);
            }

        }
    }
}
