﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PR.UtilityLibrary
{
    public class ERP_Enums
    { 

        public enum eQTStatus
        {
            Open = 1,
            Completed = 2,
            OnHold = 3,
            Cancelled = 4,
            Unscheduled = 5
        }

        public enum eQTType
        {
            SiteInspection = 1,             //SI - SiteInspection
            WarehouseToCurbEmpty = 2,       //DE - DeliverEmpty
            WarehouseToCurbFull = 3,        //DF - DeliverFull
            CurbToCurb = 4,                 //CC - CurbToCurb
            ReturnToWarehouseEmpty = 5,     //RE - ReturnEmpty
            ReturnToWarehouseFull = 6,      //RF - ReturnFull
            WarehouseAccess = 7,            //QA - WarehouseAccess
            InByOwner = 8,                  //IBO - InByOwner
            OutByOwner = 9,                 //OBO - OutByOwner
            LDMTransferIn = 10,             //TI - LDMTransferIn
            LDMTransferOut = 11             //TO - LDMTransferOut
        }
         
        public enum eQTRentalTypes
        {
            Quote = 1,
            Order = 2,
            Rental = 3,
            Appointment = 4
        }

        public enum eQTRentalStatuses
        {
            Completed = 1,
            Cancelled = 2,
            Active = 3,
            On_Hold = 4,
            Initiated = 5,
            NotYetDefined6 = 6,
            NotYetDefined7 = 7,
            NotYetDefined8 = 8,
            Entry = 9
        }
    }
}
