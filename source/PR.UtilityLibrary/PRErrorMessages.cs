namespace PR.UtilityLibrary
{
    public class EsbMethod
    {

        // Facility App - Get ESB Methods
        public const string GetWareHouseTouches = "GetWarehouseTouches";
        public const string GetTransportationTouches = "GetTransportationTouches";
        public const string GetTouchAvailableSchedule = "GetTouchAvailableSchedule";
        public const string GetUnitInfo = "GetUnitInfo";
        public const string GetTouchStatus = "GetTouchStatus";
        public const string GetBrandInfo = "GetBrandInfo";
        public const string GetUnassignedTouches = "GetUnassignedTouches";
        public const string GetStagingTouches = "GetStagingTouches";

        // Facility App - Update ESB Methods
        public const string UpdateTouchStatus = "UpdateTouchStatus";
        public const string UpdateTouchSchedule = "UpdateTouchSchedule";
        public const string CompleteTransferInMoveIn = "CompleteTransferInMoveIn";
        public const string CompleteTransferOutMoveOut = "CompleteTransferOutMoveOut";
        public const string CompleteCCTouch = "CompleteCCTouch";
        public const string UpdateTouchData = "UpdateTouchData";
        public const string UpdateTouchAssignment = "UpdateTouchAssignment";

        // Driver App - Get ESB Methods
        public const string GetOrderInfo = "GetOrderInfo";
        public const string GetCustomerInfo = "GetCustomerInfo";
        //public const string GetQORDetails = "GetQORDetails";
        public const string GetQuoteInfo = "GetQuoteInfo";
        public const string GetAppliedRecurringItems = "GetAppliedRecurringItems";
        public const string GetAppliedPOSItems = "GetAppliedPOSItems";
        public const string GetPricingInfo = "GetPricingInfo";
        public const string GetAppliedTransportationCharges = "GetAppliedTransportationCharges";
        public const string GetAppliedTouches = "GetAppliedTouches";
        public const string GetAppliedCppItems = "GetAppliedCppItems";
        public const string GetCppItems = "GetCPPInfoMasterData";
        

    }

    public class PRErrorMessages
    {
        public const string Zip_Code_Not_Exist = "The zip code does not exists";
        public const string Zip_Code_Not_Serviced = "The zip code is not locally serviced or LDM serviced";
        public const string Move_Not_Supported = "This kind of move is not supported by the system.";

    }

    public class PRErrorCode
    {
        public const string Zip_Code_Not_Exist = "100";
        public const string Zip_Code_Not_Serviced = "101";
        public const string Move_Not_Supported = "102";
    }
}
