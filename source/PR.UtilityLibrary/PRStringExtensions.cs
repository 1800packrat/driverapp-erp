﻿
using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace PR.UtilityLibrary
{
    public static class PRStringExtensions
    {


        /// <summary>
        /// This Extension will convert string to decimal and has optional format style;
        /// default is format is Currency.
        /// </summary>
        /// <param name="targetString">if null returns null decimal</param>
        /// <param name="style">Default to Currency </param>
        /// <returns>nullable Decimal</returns>
        public static decimal? ToDecimalNullable(this string targetString, NumberStyles style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol)
        {
            decimal decimalformated;
            if (targetString == null || String.IsNullOrEmpty(targetString))
            {
                return null;
            }
            if (!(decimal.TryParse(targetString, out decimalformated)))
            {
                decimalformated = NumericValue(targetString, style);
                return decimalformated;

            }

            return decimalformated;
        }

        /// <summary>
        /// Convert string to decimal if invalid 
        /// </summary>
        /// <param name="targetString"></param>
        /// <param name="style"></param>
        /// <returns></returns>
        public static decimal ToDecimal(this string targetString)
        {
            decimal decimalformated;
            if (targetString == null || String.IsNullOrEmpty(targetString))
            {
                return 0.00m;
            }
            if (!(decimal.TryParse(targetString, out decimalformated)))
            {

                throw new Exception(string.Format("Unable to parse {0} to decimal.", targetString));
            }

            return decimalformated;
        }


        /// <summary>
        /// Returns a string as currency with a leading minus sign if the number is negative. 
        /// </summary>
        /// <param name="source">A decimal number</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string ToLeadingMinusCurrency(this string targetString)
        {
            decimal decimalformated;

            if (string.IsNullOrEmpty(targetString))
            {
                return targetString;
            }

            if (!(decimal.TryParse(targetString, out decimalformated)))
            {
                throw new Exception(string.Format("Unable to parse {0} to decimal.", targetString));
            }
            CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
            culture.NumberFormat.CurrencyNegativePattern = 1;

            return decimalformated.ToString("C2", culture);
        }

        /// <summary>
        /// get last no. of tail_length provided from string
        /// </summary>
        /// <param name="source"></param>
        /// <param name="tail_length"> length of string </param>
        /// <returns></returns>
        public static string Last(this string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        /// <summary>
        /// internal method used for string to given decimal style.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="styles"></param>
        /// <returns></returns>
        private static decimal NumericValue(string value, NumberStyles styles)
        {
            decimal number;
            try
            {
                number = Decimal.Parse(value, styles);

            }
            catch (FormatException)
            {
                throw new Exception(string.Format("Unable to parse {0} with styles {1}.", value, styles.ToString()));
            }
            return number;
        }


        /// <summary>
        /// Converts a string into a Int32 
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <returns>An Int32</returns>
        /// <remarks></remarks>
        public static int ToInt(this string source)
        {
            return Int32.Parse(source);
        }


        /// <summary>
        /// Converts a string into a Int32 
        /// </summary>
        /// <param name="source">The source string.</param>
        /// <returns>An Int32</returns>
        /// <remarks></remarks>
        public static bool IsInt(this string source)
        {
            int outPutInt;
            return Int32.TryParse(source, out outPutInt);
        }


        /// <summary>
        /// Takes only numric from string
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ExtractOnlyNumbers(this string source)
        {
            Regex regexObj = new Regex(@"[^\d]");

            return regexObj.Replace(source, "");

        }

        /// <summary>
        /// Convert string to string in format mm/dd/yyyy
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToStandardFormatedDateString(this string source)
        {

            DateTime dt;
            if (DateTime.TryParseExact(source, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            {
                source = dt.ToString(("MM/dd/yyyy"));
            }
            return source;
        }


        /// <summary>
        /// Convert string format(yyyyMMddHHmmss) to date in format mm/dd/yyyy
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTime ToStandardFormatedDateTime(this string source)
        {

            DateTime dt;
            if (!DateTime.TryParseExact(source, "yyyyMMddHHmmss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dt))
            {
                throw new Exception(string.Format("Unable to parse {0} to date time.", source));
            }
            return dt;
        }

        /// <summary>
        /// Formats phone no. as (###)###-#### if valid 10 digit string else return original string
        /// </summary>
        /// <param name="targetString"></param>
        /// <returns></returns>
        public static string FormatPhoneNumber(this string targetString)
        {
            long resultParse;
            if ((!String.IsNullOrEmpty(targetString)) && targetString.Length == 10 && long.TryParse(targetString, out resultParse))
            {
                if (resultParse.ToString().Length != 10)
                {
                    return targetString;
                }
                return String.Format("{0:(###)###-####}", resultParse);


            }
            return targetString;

        }

        /// <summary>
        /// return data time if valid string 
        /// </summary>
        /// <param name="targetString"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this string targetString)
        {
            DateTime dt;
            if (!DateTime.TryParse(targetString, out dt))
            {
                if (String.IsNullOrEmpty(targetString)) return dt;
                throw new Exception("Invalid string to convert to datetime.");
            }
            return dt;
        }





        /// <summary>
        /// Gets last provided no. of charachter from string 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="tail_length">no. of last  character from string </param>
        /// <returns></returns>
        public static string GetLast(this string source, int numberOfLastChar)
        {
            if (numberOfLastChar >= source.Length)
                return source;
            return source.Substring(source.Length - numberOfLastChar);
        }



        /// <summary>
        /// return string with first letter as upper 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="tail_length"> </param>
        /// <returns>string with first letter as upper case</returns>
        public static string UppercaseFirstChar(this string source)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(source))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(source[0]) + source.Substring(1);
        }


        /// <summary>
        /// Returns a Hash of the string. 
        /// </summary>
        /// <param name="targetString">The target string.</param>
        /// <param name="salt">An option Guid that will be appended to the string before the hash is computed.</param>
        /// <returns>A hashed string that is always 64 characters long</returns>
        /// <remarks>This extension uses a SHA256 hash with UTF8 encoding.</remarks>
        public static string Hashed(this string targetString, Nullable<Guid> salt = null)
        {
            if (salt.HasValue)
            {
                return CommonUtility.GetHash(targetString + salt.Value.ToString(), PREnums.HashType.SHA256_UTF8);
            }

            return CommonUtility.GetHash(targetString, PREnums.HashType.SHA256_UTF8);
        }

        /// <summary>
        /// Return custom instruction by removing auto added  instruction
        /// mostly used in LDM orders
        /// </summary>
        public static string ExtractCustomInstruction(this string targetString)
        {
            if (string.IsNullOrEmpty(targetString))
            {
                return targetString;
            }
            if (!string.IsNullOrWhiteSpace(targetString) && targetString.Contains("|"))
            {
                var splitInstrution = targetString.Split('|');
                return splitInstrution[1];
            }
            targetString = targetString?.Replace("Empty Weight Required", "");
            targetString = targetString?.Replace("Full Weight Required", "");
            targetString = targetString?.Replace("Weight Ticket Required", "");
            targetString = targetString?.Replace("Full Weight Ticket needed", "");

            if (targetString?.IndexOf("Lock(s).") != -1)
            {
                targetString = targetString?.Substring(targetString.IndexOf("Lock(s).")).Remove(0, 8);

            }

            return targetString;

        }
    }
}


