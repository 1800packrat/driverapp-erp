﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
//using PR.DataHandler;

namespace PR.UtilityLibrary
{
    public class PRHelper
    {
        public static SortedDictionary<string, string> GetUSStates()
        {

            SortedDictionary<string, string> dicUSList = new SortedDictionary<string, string>();

            dicUSList.Add("AL", "Alabama");

            dicUSList.Add("AK", "Alaska");

            dicUSList.Add("AZ", "Arizona");

            dicUSList.Add("AR", "Arkansas");

            dicUSList.Add("CA", "California");

            dicUSList.Add("CO", "Colorado");

            dicUSList.Add("CT", "Connecticut");

            dicUSList.Add("DE", "Delaware");

            dicUSList.Add("DC", "District of Columbia");

            dicUSList.Add("FL", "Florida");

            dicUSList.Add("GA", "Georgia");

            dicUSList.Add("HI", "Hawaii");

            dicUSList.Add("ID", "Idaho");

            dicUSList.Add("IL", "Illinois");

            dicUSList.Add("IN", "Indiana");

            dicUSList.Add("IA", "Iowa");

            dicUSList.Add("KS", "Kansas");

            dicUSList.Add("KY", "Kentucky");

            dicUSList.Add("LA", "Louisiana");

            dicUSList.Add("ME", "Maine");

            dicUSList.Add("MD", "Maryland");

            dicUSList.Add("MA", "Massachusetts");

            dicUSList.Add("MI", "Michigan");

            dicUSList.Add("MN", "Minnesota");

            dicUSList.Add("MS", "Mississippi");

            dicUSList.Add("MO", "Missouri ");

            dicUSList.Add("MT", "Montana");

            dicUSList.Add("NE", "Nebraska");

            dicUSList.Add("NV", "Nevada");

            dicUSList.Add("NH", "New Hampshire");

            dicUSList.Add("NJ", "New Jersey");

            dicUSList.Add("NM", "New Mexico");

            dicUSList.Add("NY", "New York");

            dicUSList.Add("NC", "North Carolina");

            dicUSList.Add("ND", "North Dakota");

            dicUSList.Add("OH", "Ohio");

            dicUSList.Add("OK", "Oklahoma");

            dicUSList.Add("OR", "Oregon");

            dicUSList.Add("PA", "Pennsylvania");

            dicUSList.Add("RI", "Rhode Island");

            dicUSList.Add("SC", "South Carolina");

            dicUSList.Add("SD", "South Dakota");

            dicUSList.Add("TN", "Tennessee");

            dicUSList.Add("TX", "Texas");

            dicUSList.Add("UT", "Utah");

            dicUSList.Add("VT", "Vermont");

            dicUSList.Add("VA", "Virginia");

            dicUSList.Add("WA", "Washington");

            dicUSList.Add("WV", "West Virginia");

            dicUSList.Add("WI", "Wisconsin");

            dicUSList.Add("WY", "Wyoming");

            return dicUSList;
        }

        public static string GetLDMXMLForPosting(string vendorName, string vendorCode, string vendorId, string vendorPass, string entryDate,
       string fname, string lname, string addr1, string addr2, string city, string state, string zipCode, string phone, string email,
       string unitCalcMethod, string unitCalcvalue, string originZipCode, string destinationZipCode, string dryVan, string estInitialDelivery,
       string estDateAtDestination, string estMosStorage, string guarrantee, string promo, string sendEmail, int cppValue, string addEmails,
       string webUnitCalcMethod, string webUnitCalcValue, string webSourceReferralCodes, string quoteCreatedBy)
        {
            //Get the stars and sitelink promo codes to pass
            //string[] promos = new string[2] { String.Empty, String.Empty };
            //if (promo != String.Empty)
            //{
            //    LocalDataHandler dh = new LocalDataHandler();
            //    promos = dh.GetPromoCodesFromSTARS(promo);
            //}

            StringBuilder sb = new StringBuilder();

            //Vendor login section
            sb.Append("<PackRatXMLImport Version = \"1.0\" System = \"STARS\" >");
            sb.Append("<VendorLogin>");
            sb.Append("<element name=\"VendorName\" value=");
            sb.Append("\"" + vendorName + "\" />");
            sb.Append("<element name=\"VendorCode\" value=");
            sb.Append("\"" + vendorCode + "\" />");
            sb.Append("<element name=\"VendorID\" value=");
            sb.Append("\"" + vendorId + "\" />");
            sb.Append("<element name=\"VendorPass\" value=");
            sb.Append("\"" + vendorPass + "\" />");
            sb.Append("</VendorLogin>");

            //Set the right values for the parameters.
            if (dryVan == String.Empty) dryVan = "0";
            if (guarrantee == String.Empty) guarrantee = "0";
            if (estMosStorage == String.Empty) estMosStorage = "1";
            if (sendEmail == String.Empty) sendEmail = "F";


            //Quote section
            sb.Append("<Quote>");
            sb.Append("<element name=\"EntryDate\" value=");
            sb.Append("\"" + entryDate + "\" />");
            sb.Append("<element name=\"FirstName\" value=");
            sb.Append("\"" + fname + "\" />");
            sb.Append("<element name=\"LastName\" value=");
            sb.Append("\"" + lname + "\" />");
            sb.Append("<element name=\"Address1\" value=");
            sb.Append("\"" + addr1 + "\" />");
            sb.Append("<element name=\"Address2\" value=");
            sb.Append("\"" + addr2 + "\" />");
            sb.Append("<element name=\"City\" value=");
            sb.Append("\"" + city + "\" />");
            sb.Append("<element name=\"State\" value=");
            sb.Append("\"" + state + "\" />");
            sb.Append("<element name=\"ZipCode\" value=");
            sb.Append("\"" + zipCode + "\" />");
            sb.Append("<element name=\"Phone\" value=");
            sb.Append("\"" + phone + "\" />");
            sb.Append("<element name=\"EMail\" value=");
            sb.Append("\"" + email + "\" />");
            sb.Append("<element name=\"UnitCalcMethod\" value=");
            sb.Append("\"" + unitCalcMethod + "\" />");
            sb.Append("<element name=\"UnitCalcValue\" value=");
            sb.Append("\"" + unitCalcvalue + "\" />");
            sb.Append("<element name=\"OriginCity\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"OriginState\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"OriginZipCode\" value=");
            sb.Append("\"" + originZipCode + "\" />");
            sb.Append("<element name=\"DestinationCity\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"DestinationState\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"DestinationZipCode\" value=");
            sb.Append("\"" + destinationZipCode + "\" />");
            sb.Append("<element name=\"DryVan\" value=");
            sb.Append("\"" + dryVan + "\" />");
            sb.Append("<element name=\"EstInitialDelivery\" value=");
            sb.Append("\"" + estInitialDelivery + "\" />");
            sb.Append("<element name=\"EstDateAtDestination\" value=");
            sb.Append("\"" + estDateAtDestination + "\" />");
            sb.Append("<element name=\"EstMosStorage\" value=");
            sb.Append("\"" + estMosStorage + "\" />");
            sb.Append("<element name=\"Guarantee\" value=");
            sb.Append("\"" + guarrantee + "\" />");
            sb.Append("<element name=\"PromoCode\" value=");
            sb.Append("\"" + promo + "\" />");
            sb.Append("<element name=\"SLPromo\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"SendEMail\" value=");
            sb.Append("\"" + sendEmail + "\" />");
            sb.Append("<element name=\"orderid\" value=");
            sb.Append("\" \" />");
            sb.Append("<element name=\"CPPLevel\" value=");
            sb.Append("\"" + cppValue + "\" />");
            sb.Append("<element name=\"AddEmails\" value=");
            sb.Append("\"" + addEmails + "\" />");
            sb.Append("<element name=\"WebUnitCalcMethod\" value=");
            sb.Append("\"" + webUnitCalcMethod + "\" />");
            sb.Append("<element name=\"WebUnitCalcValue\" value=");
            sb.Append("\"" + webUnitCalcValue + "\" />");
            sb.Append("<element name=\"WebSourceReferralCodes\" value=");
            sb.Append("\"" + webSourceReferralCodes + "\" />");
            sb.Append("<element name=\"QuoteCreatedBy\" value=");
            sb.Append("\"" + quoteCreatedBy + "\" />");
            sb.Append("</Quote>");
            sb.Append("</PackRatXMLImport>");

            return sb.ToString();
        }

        public static string ConvertDateTimeToDateInString(DateTime dtTime)
        {
            return dtTime.Month + "/" + dtTime.Day + "/" + dtTime.Year;
        }

        public static int GetSLReturnCode(DataSet ds)
        {
            if (Assert(ds) && ds.Tables.Contains("RT") && Assert(ds.Tables["RT"]) && ds.Tables["RT"].Columns.Contains("Ret_Code") && Assert(ds.Tables["RT"].Rows[0]["Ret_Code"]))
            {
                return (int)ds.Tables["RT"].Rows[0]["Ret_Code"];
            }
            else
            {
                return int.MinValue;
            }
        }

        public static int GetRowCount(DataSet ds)
        {
            if (Assert(ds) && Assert(ds.Tables[0]))
            {
                return ds.Tables[0].Rows.Count;
            }
            else
            {
                return 0;
            }
        }

        public static bool IsVoid(object obj)
        {
            if (obj == null || object.ReferenceEquals(obj, System.Convert.DBNull)) return true;
            if (obj is string)
            {
                string strTest = (string)obj;
                if (strTest.Length == 0 || strTest.Trim().Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if (obj is Array)
            {
                return ((Array)obj).Length == 0;
            }
            else if (obj is DataTable)
            {
                return ((DataTable)obj).Rows == null || ((DataTable)obj).Rows.Count == 0;
            }
            else if (obj is DataSet)
            {
                return ((DataSet)obj).Tables == null || ((DataSet)obj).Tables.Count == 0;
            }
            else
            {
                return false;
            }
        }

        public static bool Assert(object obj)
        {
            return !IsVoid(obj);
        }

        /// <summary>
        /// Returns the dictionary obect with touch types. This is used for activity logging.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<int, string> GetSMDTouchTypeDictionary()
        {
            Dictionary<int, string> dicTouchType = new Dictionary<int, string>();
            dicTouchType.Add(1, "SiteInspection");
            dicTouchType.Add(2, "DeliverEmpty");
            dicTouchType.Add(3, "DeliverFull");
            dicTouchType.Add(4, "CurbToCurb");
            dicTouchType.Add(5, "ReturnEmpty");
            dicTouchType.Add(6, "ReturnFull");
            dicTouchType.Add(7, "WarehouseAccess");
            dicTouchType.Add(8, "InByOwner");
            dicTouchType.Add(9, "OutByOwner");
            dicTouchType.Add(10, "LDMTransferIn");
            dicTouchType.Add(11, "LDMTransferOut");

            return dicTouchType;
        }

        /// <summary>
        /// Returns the dictionary obect with touch types. This is used for activity logging.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, int> GetTouchTypeIdByName()
        {
            Dictionary<string, int> dicTouchType = new Dictionary<string, int>();
            dicTouchType.Add("SiteInspection", 1);
            dicTouchType.Add("DeliverEmpty", 2);
            dicTouchType.Add("DeliverFull", 3);
            dicTouchType.Add("CurbToCurb", 4);
            dicTouchType.Add("ReturnEmpty", 5);
            dicTouchType.Add("ReturnFull", 6);
            dicTouchType.Add("WarehouseAccess", 7);
            dicTouchType.Add("InByOwner", 8);
            dicTouchType.Add("OutByOwner", 9);
            dicTouchType.Add("LDMTransferIn", 10);
            dicTouchType.Add("LDMTransferOut", 11);

            return dicTouchType;
        }


        /// <summary>
        /// Returns the dictionary obect with touch types. This is used for activity logging.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, int> GetTouchTypeIdByShortName()
        {
            Dictionary<string, int> dicTouchType = new Dictionary<string, int>();
            dicTouchType.Add("SI", 1);
            dicTouchType.Add("DE", 2);
            dicTouchType.Add("DF", 3);
            dicTouchType.Add("CC", 4);
            dicTouchType.Add("RE", 5);
            dicTouchType.Add("RF", 6);
            dicTouchType.Add("WA", 7);
            dicTouchType.Add("IBO", 8);
            dicTouchType.Add("OBO", 9);
            dicTouchType.Add("TI", 10);
            dicTouchType.Add("TO", 11);

            return dicTouchType;
        }

        public static decimal GetDecimalValue(object obj)
        {
            if (obj == null)
            {
                return 0;
            }
            else if (obj == DBNull.Value)
            {
                return 0;
            }
            else
            {
                return Convert.ToDecimal(obj);
            }
        }

        public static String SerializeToString(object obj)
        {
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());

            using (System.IO.StringWriter writer = new System.IO.StringWriter())
            {
                serializer.Serialize(writer, obj);

                return writer.ToString();
            }
        }

        public static SqlParameter SqlParameterObj(string paramName, object val)
        {
            return new SqlParameter { ParameterName = paramName, Value = val ?? DBNull.Value };
        }
    }
}