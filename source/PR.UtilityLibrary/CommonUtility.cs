﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;
using System.Xml.Serialization;

namespace PR.UtilityLibrary
{
    public static class CommonUtility
    {

        /// <summary>
        /// Generates the hash of a text.
        /// </summary>
        /// <param name="strPlain">The text of which to generate a hash of.</param>
        /// <param name="hshType">The hash function to use.</param>
        /// <returns>The hash as a hexadecimal string.</returns>
        /// <remarks></remarks>
        public static string GetHash(string strPlain, PR.UtilityLibrary.PREnums.HashType hshType)
        {
            string strRet;
            switch (hshType)
            {
                case PR.UtilityLibrary.PREnums.HashType.MD5: strRet = GetMD5(strPlain); break;
                case PR.UtilityLibrary.PREnums.HashType.SHA1: strRet = GetSHA1(strPlain); break;
                case PR.UtilityLibrary.PREnums.HashType.SHA256: strRet = GetSHA256(strPlain); break;
                case PR.UtilityLibrary.PREnums.HashType.SHA256_UTF8: strRet = GetSHA256ForUTF8Encoding(strPlain); break;
                case PR.UtilityLibrary.PREnums.HashType.SHA384: strRet = GetSHA384(strPlain); break;
                case PR.UtilityLibrary.PREnums.HashType.SHA512: strRet = GetSHA512(strPlain); break;
                default: strRet = "Invalid HashType"; break;
            }
            return strRet;
        } /* GetHash */


        /// <summary>
        /// Returns a Hash of the string. 
        /// </summary>
        /// <param name="targetString">The target string.</param>
        /// <param name="salt">An option Guid that will be appended to the string before the hash is computed.</param>
        /// <returns>A hashed string that is always 64 characters long</returns>
        /// <remarks>This extension uses a SHA256 hash with UTF8 encoding.</remarks>
        public static string GetHash(this string targetString, Nullable<Guid> salt = null)
        {
            if (salt.HasValue)
            {
                return GetHash(targetString + salt.Value.ToString(), PR.UtilityLibrary.PREnums.HashType.SHA256_UTF8);
            }

            return GetHash(targetString, PR.UtilityLibrary.PREnums.HashType.SHA256_UTF8);
        }





        #region Hashers
        /// <summary>
        /// Gets the M d5.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetMD5(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            MD5 md5 = new MD5CryptoServiceProvider();
            string strHex = "";

            HashValue = md5.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetMD5 */

        /// <summary>
        /// Gets the SH a1.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetSHA1(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA1Managed SHhash = new SHA1Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetSHA1 */

        /// <summary>
        /// Gets the SH a256.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetSHA256(string strPlain)
        {

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA256Managed SHhash = new SHA256Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetSHA256 */

        /// <summary>
        /// Gets the SH a256 for UT f8 encoding.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetSHA256ForUTF8Encoding(string strPlain)
        {

            UTF8Encoding UE = new UTF8Encoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA256Managed SHhash = new SHA256Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetSHA256 */

        /// <summary>
        /// Gets the SH a384.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetSHA384(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA384Managed SHhash = new SHA384Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetSHA384 */

        /// <summary>
        /// Gets the SH a512.
        /// </summary>
        /// <param name="strPlain">The STR plain.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string GetSHA512(string strPlain)
        {
            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] HashValue, MessageBytes = UE.GetBytes(strPlain);
            SHA512Managed SHhash = new SHA512Managed();
            string strHex = "";

            HashValue = SHhash.ComputeHash(MessageBytes);
            foreach (byte b in HashValue)
            {
                strHex += String.Format("{0:x2}", b);
            }
            return strHex;
        } /* GetSHA512 */
        #endregion




        /// <summary>
        /// Encrypts the specified data.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="key">The key to be used to encrypt the data.</param>
        /// <returns>The encrypted data.</returns>
        /// <remarks>If the key is not specified, the machinekey is used.</remarks>
        public static string Encrypt(string data, string key = null)
        {

            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(data);

            if (key == null)
            {
                return MachineKey.Encode(toEncryptArray, MachineKeyProtection.Encryption);
            }

            return EncodeWithTDes(toEncryptArray, key);

        }

        /// <summary>
        /// Decrypts the specified encrypted data.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="key">The key used to encrypt the data.</param>
        /// <returns>The decrypted data.</returns>
        /// <remarks>If the key is not specified, the machinekey is used.</remarks>
        public static string Decrypt(string encryptedData, string key = null)
        {

            if (key == null)
            {
                return UTF8Encoding.UTF8.GetString(MachineKey.Decode(encryptedData, MachineKeyProtection.Encryption));
            }

            return DecodeWithTDes(encryptedData, key);

        }


        /// <summary>
        /// Encrypts the data with TripleDES.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="key">The key to be used to encrypt the data.</param>
        /// <returns>The encrypted data.</returns>
        /// <remarks></remarks>
        private static string EncodeWithTDes(byte[] data, string key)
        {
            TripleDESCryptoServiceProvider tDes = GetTDes(key);
            ICryptoTransform cTransform = tDes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(data, 0, data.Length);
            tDes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Decrypts the data with TripleDES.
        /// </summary>
        /// <param name="encryptedData">The encrypted data.</param>
        /// <param name="key">The key used to encrypt the data.</param>
        /// <returns>The decrypted data.</returns>
        /// <remarks></remarks>
        private static string DecodeWithTDes(string encryptedData, string key)
        {
            byte[] toDecryptArray = Convert.FromBase64String(encryptedData);
            TripleDESCryptoServiceProvider tDes = GetTDes(key);

            ICryptoTransform cTransform = tDes.CreateDecryptor();

            byte[] resultArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);
            tDes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray, 0, resultArray.Length);

        }

        /// <summary>
        /// Creates a TripleDES Crypto Service Provider.
        /// </summary>
        /// <param name="key">The key to be used to encrypt the data.</param>
        /// <returns>A TripleDES Crypto Service Provider.</returns>
        /// <remarks></remarks>
        private static TripleDESCryptoServiceProvider GetTDes(string key)
        {
            byte[] keyArray;
            TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider();
            tDes.Mode = CipherMode.ECB;
            tDes.Padding = PaddingMode.PKCS7;
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            hashmd5.Clear();
            tDes.Key = keyArray;
            return tDes;
        }


        /// <summary>
        /// returns xml string of Serialize object 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        public static string SerializeObjectToXML<T>(T toSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        /// <summary>
        /// save xml file to provided location 
        /// </summary>
        /// <typeparam name="T">object to Serialize in xml </typeparam>
        /// <param name="toSerialize"></param>
        public static void SerializeObjectToXMLFile<T>(T toSerialize, string xmlFilePath)
        {
            //var xmlFilePath = Path.Combine(ConfigurationManager.AppSettings["XMLFeedSharePath"].ToString(), "SpiritXMLFeed_" + string.Format("{0:yyyy-MM-dd}", DateTime.Now) + ".xml");
            XmlSerializer xmlSerializer = new XmlSerializer(toSerialize.GetType());
            using (FileStream fs = new FileStream(xmlFilePath, FileMode.Create))
            {

                xmlSerializer.Serialize(fs, toSerialize);

            }

        }

        /// <summary>
        /// save json file to provided location 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize">object to Serialize in xml</param>
        /// <param name="jsonFilePath"></param>
        public static void SerializeObjectToJSONFile<T>(T toSerialize, string jsonFilePath)
        {
            //var jsonFilePath = Path.Combine(ConfigurationManager.AppSettings["XMLFeedSharePath"].ToString(), "SpiritXMLFeed_" + string.Format("{0:yyyy-MM-dd}", DateTime.Now) + ".Json");

            if (!File.Exists(jsonFilePath))
            {
                // Create a file to write to.

                File.WriteAllText(jsonFilePath, JsonConvert.SerializeObject(toSerialize));
            }




        }

        /// <summary>
        /// return dynamic query for search.
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source">IQueryable object </param>
        /// <param name="parameter">Compare parameter</param>
        /// <param name="predicate">lambda expressions</param>
        /// <returns>if Compare parameter is not null then where clause is attache to IQueryable object returns predicate </returns>
        /// <remarks>object.DynamicWhere(someObject.id,x=>x.id==someObject.id) </remarks> 
        public static IQueryable<TSource> DynamicWhere<TSource>(this IQueryable<TSource> source, dynamic parameter, Expression<Func<TSource, bool>> predicate)
        {
            IQueryable<TSource> returnQuery = source;
            if (parameter != null)
            {
                // handle if the parameter is of type string
                if ((parameter.GetType() == typeof(String)))
                {
                    if (!string.IsNullOrWhiteSpace(parameter))
                        returnQuery = returnQuery.Where(predicate);
                }
                else
                {
                    returnQuery = returnQuery.Where(predicate);
                }

            }
            return returnQuery;
        }
        /// <summary>
        /// get property name 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TProp"></typeparam>
        /// <param name="o"></param>
        /// <param name="propertySelector"></param>
        /// <returns></returns>
        public static string Name<T, TProp>(this T o, Expression<Func<T, TProp>> propertySelector)
        {
            MemberExpression body = (MemberExpression)propertySelector.Body;
            return body.Member.Name;
        }

        public static string HttpGet(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        /// <summary>
        /// get item from name value with type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static T Get<T>(this NameValueCollection collection, string key, T defaultValue)
        {
            var value = collection[key];
            var converter = TypeDescriptor.GetConverter(typeof(T));
            if (string.IsNullOrWhiteSpace(value) || !converter.IsValid(value))
            {
                return defaultValue;
            }

            return (T)(converter.ConvertFromInvariantString(value));
        }

        public static string ReadFromFile(string filePath)
        {
            string line = "";
            try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(filePath);

                ////Read the first line of text
                //line = sr.ReadLine();

                ////Continue to read until you reach end of file
                //while (line != null)
                //{
                //    //write the lie to console window
                //    Console.WriteLine(line);
                //    //Read the next line
                //    line = sr.ReadLine();
                //}

                //Read whole text
                line = sr.ReadToEnd();

                //close the file
                sr.Close();
                //Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
            return line;
        }

        public static void WriteToFile(string text, string filePath)
        {
            try
            {

                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(filePath);

                ////Write a line of text
                //sw.WriteLine("Hello World!!");

                ////Write a second line of text
                //sw.WriteLine("From the StreamWriter class");

                sw.WriteLine(text);

                //Close the file
                sw.Close();
            }
            catch (Exception e)
            {
                ; // Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                ; // Console.WriteLine("Executing finally block.");
            }
        }

        public static void WriteLogFile(string text, string directoryPath)
        {
            try
            {
                directoryPath = Path.Combine(directoryPath, DateTime.Now.ToString("yyyy"),
                                DateTime.Now.ToString("MMMM"), DateTime.Now.ToString("MM-dd-yyyy"));

                if (!System.IO.Directory.Exists(directoryPath))
                    System.IO.Directory.CreateDirectory(directoryPath);

                var filePath = Path.Combine(directoryPath, $"{DateTime.Now.ToString("yyyyMMdd hhmm")}.log");

                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    writer.WriteLine("Time: " + DateTime.Now.ToString("hh:mm:ss:tt"));
                    writer.WriteLine(text + Environment.NewLine);
                }
            }
            catch (Exception e)
            {
                ; //Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                ; // Console.WriteLine("Executing finally block.");
            }
        }

        public static DataSet ToDataSet<T>(this IList<T> list)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (T item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }

        public static string GetUnitSize(string unitSize)
        {
            switch (unitSize)
            {
                case "16":
                case "16feet":
                case "16 feet":
                    return unitSize.Replace("feet", "").Replace(" ", "");
                case "12":
                case "12feet":
                case "12 feet":
                    return unitSize.Replace("feet", "").Replace(" ", "");
                case "8":
                case "8feet":
                case "8 feet":
                    return unitSize.Replace("feet", "").Replace(" ", "");
                default:
                    return ((Int32)PREnums.UnitOrContainerSize.SixteenFeet).ToString();
            }
        }

        public static string GetUnitNameOrNumber(string unitNumber, string unitSize = "")
        {
            if (!string.IsNullOrWhiteSpace(unitNumber)) return unitNumber;

            var uSize = (!string.IsNullOrWhiteSpace(unitSize))
                ? (PREnums.UnitOrContainerSize)Convert.ToInt32(unitSize)
                : PREnums.UnitOrContainerSize.SixteenFeet;

            switch (uSize)
            {
                case PREnums.UnitOrContainerSize.SixteenFeet:
                    return ConfigurationManager.AppSettings["16FootUName"].ToString();
                case PREnums.UnitOrContainerSize.TweleveFeet:
                    return ConfigurationManager.AppSettings["12FootUName"].ToString();
                case PREnums.UnitOrContainerSize.EightFeet:
                    return ConfigurationManager.AppSettings["8FootUName"].ToString();
                default:
                    return ConfigurationManager.AppSettings["16FootUName"].ToString();
            }
        }

    }
}
