using System;

namespace PR.UtilityLibrary
{
    public class PREnums
    {
        public enum UnitOrContainerSize
        {
            SixteenFeet = 16,
            TweleveFeet = 12,
            EightFeet = 8
        }

        public enum MoveType
        {
            NoService = 0,
            Local = 1,
            ESAT = 2,
            LDM = 3,
            TransferFacility = 4,
            OD = 5,
            ABF = 6
        }

        public enum ServiceType
        {
            OnsiteStorage = 0,
            WarehouseStorage = 1,
            PointAtoPointBMove = 2,
            LDMOrigination = 3,
            LDMDestination = 4

        }

        public enum TouchStatus
        {
            Open = 0,
            Completed = 1,
            OnHold = 2,
            Cancelled = 3
        }

        public enum AutoBillType
        {
            None = 0,
            CreditCard = 1,
            ACH = 2
        }

        public enum BillingCycle
        {
            Weekly = 2,
            Monthly = 3
        }

        public enum CreditCardType
        {
            MasterCard = 0,
            VISA = 1,
            AmEX = 2,
            Discover = 4
        }


        public enum PaymentTypesSiteLinkEnums
        {
            Cash = 69,
            Check = 70,
            MasterCard = 72,
            Visa = 73,
            AmericanExpress = 74,
            Discover = 75,
            MoneyOrder = 83

        }

        public enum TouchType
        {
            DeliverEmpty = 0,
            DeliverFull = 1,
            ReturnEmpty = 2,
            ReturnFull = 3,
            CurbToCurb = 4,
            LDMTransferIn = 5,
            LDMTransferOut = 6,
            WareHouseAccess,
            OutByOwner,
            InByOwner
        }

        /// SiteInspection = 1,
        /// WarehouseToCurbEmpty = 2,
        /// WarehouseToCurbFull = 3,
        /// CurbToCurb = 4,
        /// ReturnToWarehouseEmpty = 5,
        /// ReturnToWarehouseFull = 6,
        /// WarehouseAccess = 7,
        /// InByOwner = 8,
        /// OutByOwner = 9,
        /// LDMTransferIn = 10,
        /// LDMTransferOut = 11
        public enum TouchTypeShort
        {
            DE = 1,
            DF,
            CC,
            RE,
            RF,
            WA,
            IBO,
            OBO,
            TI,
            TO
        }

        public enum UnitIndex
        {
            FT16 = 0,
            FT12 = 1,
            FT8 = 2
        }

        public enum RDFee
        {
            LocalAdminFee = 1,
            LDMAdminFee = 2,

        }

        public enum CreditCardPaymentType
        {
            Mastercard = 5,
            VISA = 6,
            AmericanExpress = 7,
            Discover = 8,
            DinnersClub = 9
        }

        public enum SourceOfPayment
        {
            CallCenter = 3,
            WebSite = 10
        }
        [Serializable]
        public enum AddressType
        {
            Warehouse = 1,
            Customer = 2
        }

        public enum ChargeType
        {
            Recurring = 1,
            POS = 2,
            Transportation = 3
        }

        public enum MoveInStatus
        {
            AllreadyMoveIn,
            InvalidUnitName,
            InvalidServiceType,
            GeneralError,
            Success
        }

        public enum MoveOutStatus
        {
            AllreadyMoveOut,
            InvalidServiceType,
            GeneralError,
            Success
        }

        public enum ColorCodes
        {
            Green = 0,
            Red = 1,
            Blue = 2,
            Black = 3,
            Orange = 4,
            Purple = 5,
            White = 6
        }


        public enum HashType : int
        {
            /// <summary>MD5 Hashing</summary>
            MD5,
            /// <summary>SHA1 Hashing</summary>
            SHA1,
            /// <summary>SHA256 Hashing</summary>
            SHA256,
            /// <summary>SHA256 Hashing using UTF8 encoding</summary>
            SHA256_UTF8,
            /// <summary>SHA384 Hashing</summary>
            SHA384,
            /// <summary>SHA512 Hashing</summary>
            SHA512
        } /* HashType */

        public enum CommunicationLevel
        {
            Customer = 1,
            Order = 2,
            Touch = 3
        }
        public enum CommunicationMethod
        {
            Email = 1,
            Text = 2,
            Phone = 3,
            AutomatedPhone = 4
        }

        public enum ETAGlobalSettings
        {
            ETANotProvided_BackGroundColor,
            ETANotProvided_TextColor,
            ETAChanged_BackGroundColor,
            ETAChanged_TextColor,
            ETARisk_BackGroundColor,
            ETARisk_TextColor,
            ETAValid_BackGroundColor,
            ETAValid_TextColor
        }

        [Serializable]
        public enum TrailerFailedCriteria
        {
            None =0,
            MinimumDistanceNotMet,
            OneOfTheZipcodesBelongsToMexico,
            BothZipcodesBelongsToCanada,
            InvalidOriginZipcode,
            InvalidDestinationZipcode,
            IntraStateMove
        }
        [Serializable]
        public enum TrailerProviders
        {
            Abf = 1,
            Od
        }

        public enum AccessLevel
        {
            Any = 0, //This option to show button to enter into particular page
            None = 1,
            ReadOnly = 2,
            ReadAndWrite = 3,
            PrimaryFacility = 4,
            SecondaryFacility = 5
        }

        public enum Application
        {
            FacilityApp = 1,
            DriverApp = 2
        }

        public enum UserStatus
        {

            Active = 1,
            Inactive = 2,
            In_Training = 3
        }

        public enum DriverDayOfWork
        {
            S, //Scheduled
            C, //Off
            O  //On-Call
        }

        public enum FADALoginStatus
        {
            Success,
            InvalidCredentials,
            InactiveUser,
            InactiveRole,
            NoApplicationAccessToYourRole
        }

        public enum DriverRouteStausForADay
        {
            NoRouteExists,
            RouteExistsAndLocked,
            RouteExistsAndNonLocked

        }
    }
}
