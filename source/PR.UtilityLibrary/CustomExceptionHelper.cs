﻿using System;
using System.Data;
using PR.ExceptionTypes;

namespace PR.UtilityLibrary
{
    public static class CustomExceptionHelper
    {
        public static void ThrowException<T>(DataSet siteLinkDataset = null, string optionalMessage="") where T: Exception
        {
            var sitelinkErrorCode = string.Empty;

            if (siteLinkDataset?.Tables["RT"].Rows?[0]?["Ret_Code"] != null)
            {
                sitelinkErrorCode = $"Sitelink Error Code : {siteLinkDataset?.Tables["RT"].Rows?[0]?["Ret_Code"]}";
            }

            if (typeof(T) == typeof(CustomerMovedOutInactiveLedgerException))
            {
                throw new CustomerMovedOutInactiveLedgerException($"The customer is moved out or does not have an active ledger entry. {optionalMessage} {sitelinkErrorCode}");
            }
            else if (typeof(T) == typeof(UnitAlreadyExistsException))
            {
                throw new UnitAlreadyExistsException($"This unit already exists in this QOR. {optionalMessage} {sitelinkErrorCode}");
            }
            else if (typeof(T) == typeof(InvalidUnitException))
            {
                throw new InvalidUnitException($"Invalid unit id. {optionalMessage} {sitelinkErrorCode}");
            }
            else if (typeof(T) == typeof(OnlinePaymentsDisabledException))
            {
                throw new OnlinePaymentsDisabledException($"Tenant is too many days past due. Online payments disabled. {optionalMessage} {sitelinkErrorCode}");
            }
            else if (typeof(T) == typeof(InvalidQorListException))
            {
                throw new InvalidQorListException($"QOR list empty. {optionalMessage} {sitelinkErrorCode}");
            }
            else if (typeof(T) == typeof(ValidateQuoteException))
            {
                throw new ValidateQuoteException($"Unable to validate quote. {optionalMessage} {sitelinkErrorCode}");
            }
            else 
            {
                throw new Exception($"General exception. {optionalMessage} {sitelinkErrorCode}");
            }
        }
    }
}
