﻿using PR.Entities;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;
using static PR.UtilityLibrary.PREnums;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface IUserService
    {
        LogisticsUser Login(string username, string password, Application application);

        FADALoginStatus Login(string username, string password, string IPAddress, Application application, ref LogisticsUser user);
        
        //List<PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date);
        List<PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date, int? markertId);

        DriverSchedule GetDriverSchedule(DateTime date, int driverId);

        List<Trucks> GetTruckStatus(string storeNumber);

        //List<LogisticsUser> GetUserList();

        string FetchSuggestedDriverUserName(string firstName, string LastName, string facilityNumber);

        void CreateDriverLogin(PRDriver driver);

        List<SiteInfo> GetAllFacilities();

        List<SiteInfo> GetMarketFacilities(int marketId);

        Entities.SiteInfo AddMarketFacilities(Entities.SiteInfo site);

        Model.LogisticsUser GetDriverById(int ID);

        List<LogisticsUser> GetAllDrivers(string Name, int StoreNo, bool IsActive);

        int SaveDriverDetails(LogisticsUser driver);

        bool SetDriverStatus(LogisticsUser driver);

        List<DriverScheduleHourChangeAndStatus> CheckUpdateDriverScheduleHours(int driverId, List<DateTime> ListScheduleDates);

        bool CheckLoadExistForGivenDateRange(int driverId, DateTime StartDate, DateTime EndDate);

        List<DateTime> GetDriverLoadDates(int driverId, DateTime StartDate);
    }
}
