﻿using PR.Entities;
using System;
using System.Collections.Generic;
using PR.LocalLogisticsSolution.Model;
using System.Data;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public interface ISitelinkRepository
    {
        List<Touch> GetUnassignedTouches(SiteInfo site, DateTime date);

        List<Touch> GetStagingTouches(SiteInfo site, DateTime date);

        List<SiteInfo> GetFaciltyInformation();

        List<SiteInfo> GetFaciltyInformationWithMarkets();

        SiteInfo GetFaciltyInformationWithMarkets(string storeNum);

        void CompletTOAndMoveOutTOTouch(int originQorid, string activityBy, string unitName);

        void CompleteWarehouseTouch(int billingQorid, string activityBy);

        void CompletTIAndMoveInTITouch(int destQorid, string unitName, string activityBy, int unitId, int starsId);

        void CompleteCCTouchForBillingQor(int billingQorid, string activityBy);

        bool CheckIsUnitValidForThisQor(string unitName, int originQorId, string activityBy, out string suggestedUnitName);

        bool IsLDMOrderSettled(int dummyBillingQorId);

        void CompleteIBOWHOBHExtraTouches(int Qorid, string touchType, int sequenceNum, string activityBy, string unitNo);

        //SEFRP-TODO-RMVNICD
        ScheduleCalendar GetCapacity(DateTime StartDate, DateTime EndDate, string LocationCode);

        List<Touch> GetTransportationTouches(Entities.SiteInfo marketSite, DateTime date, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime date, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime startdate, DateTime enddate, bool isGetLiveSLData);

        List<Touch> GetTransportationTouches(CriteriaSitelinkTouches criteriaSitelinkTouches, List<SiteInfo> lstSiteInfo);

        void RemovedCachedAddress(Entities.Address address);
        ScheduleCalendar GetFacilityCapacity(DateTime startDate, DateTime endDate, string LocationCode);

        bool UpdateTouchAssignment(DateTime date, Touch touch, int driverID, string activityBy);
    }
}
