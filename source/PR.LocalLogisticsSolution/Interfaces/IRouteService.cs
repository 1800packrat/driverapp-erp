﻿using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Interfaces
{
    public  interface IRouteService
    {
        List<Stops> GetAllTouchesByDriver(int driverNumber, DateTime touchDate);
        List<Stops> GetAllTouchesByLoad(int loadId);
    }
}
