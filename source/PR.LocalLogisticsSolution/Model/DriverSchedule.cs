﻿using System;

namespace PR.LocalLogisticsSolution.Model
{
    public class DriverSchedule
    {
        public int Hours { get { return (ScheduleEndTime - ScheduleStartTime).Hours; } }
        
        public TimeSpan ScheduleStartTime { get; set; }

        public TimeSpan ScheduleEndTime { get; set; }

        public DateTime ScheduleDate { get; set; }

        public string DayOfWeek { get; set; }

        public string StartTime { get { return new DateTime().Add(ScheduleStartTime).ToShortTimeString(); } }

        public string EndTime { get { return new DateTime().Add(ScheduleEndTime).ToShortTimeString(); } }

        public UtilityLibrary.PREnums.DriverDayOfWork DayWork { set; get; }
    }
}
