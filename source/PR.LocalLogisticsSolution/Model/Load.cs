﻿// -----------------------------------------------------------------------
// <copyright file="Load.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Load
    {
        #region Public Properties

        public int LoadId { get; private set; }
        public List<Touch> Touches { get; private set; }

        public List<Touch> DisplayTouches
        {
            get
            {
                return Touches != null ? Touches.Where(t => LoadHelper.ExcudeTouchTyps.ToList().Any(ex => ex.ToUpper().Contains(t.TouchType.ToUpper())) == false).OrderBy(ls => ls.RouteSequence).ToList() : Touches;
                //return Touches.Where(t => LoadHelper.ExcudeTouchTyps.ToList().Any(ex=>ex.ToUpper().Contains(t.TouchType.ToUpper()))).ToList();
            }
        }

        public List<Touch> BusyTouches
        {
            get
            {
                return Touches.Where(t => LoadHelper.ExcudeTouchTyps.ToList().Any(ex => ex.ToUpper().Contains(t.TouchType.ToUpper()))).ToList();
                //return Touches.Where(t => LoadHelper.ExcudeTouchTyps.ToList().Any(ex=>ex.ToUpper().Contains(t.TouchType.ToUpper()))).ToList();
            }
        }

        //public List<Stops> Stops { get; private set; }
        public int DriverId { get; private set; }
        public string GlobalSiteNumber { get; private set; }
        public int? TruckID { get; private set; }
        public int? TrailerID { get; private set; }

        public string DriverNameWithFacilityCode { get { return (DriverName + (string.IsNullOrEmpty(GlobalSiteNumber) ? ("") : (" - " + GlobalSiteNumber))); } }
        public string FacCode { get; set; }
        public string DriverName { get; set; }
        public string Date { get; set; }
        public bool CanDeleteLoad { get; set; }
        public bool IsTrailerRequired { get; set; }

        public string TruckNumber { get; set; }

        public bool IsLocked { get; set; }

        public bool IsOptimized { get; set; }

        public string TrailerNumber { get; set; }

        public DateTime? ScheduleStartDateTime { get; set; }

        public DateTime? ScheduleEndDateTime { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public DriverSchedule DriverSchedule { get; set; }

        public bool HasSLUpdates
        {
            get
            {
                return DisplayTouches != null && DisplayTouches.Any(t => t.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated ||
              t.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated ||
              t.SLUpdatedStatusType == SLUpdateType.OnlyInfoUpdated);
            }
        }

        public bool HasDeletedTouchs
        {
            get
            {
                return DisplayTouches != null && DisplayTouches.Any(t => t.SLUpdatedStatusType == SLUpdateType.Canceled);
            }
        }


        public bool IsDriverStarted
        {
            get
            {
                if (Touches != null)
                    return Touches.Any(x => (x.Status == "Completed" || x.Status == "InProgress") ? true : false);
                else
                    return false;
            }
        }

        public string LoadEndTime
        {
            get
            {
                if (Touches != null)
                    return Touches.Last().Stops.Last().ScheduledEndTime.ToString("hh:mm tt");
                else
                    return string.Empty;
            }
        }

        public string LoadStartTime
        {
            get
            {
                if (Touches != null)
                    return Touches.First().Stops.First().ScheduledStartTime.ToString("hh:mm tt");
                else
                    return string.Empty;
            }
        }
        #endregion

        #region Constructor

        public Load() { }
        public Load(int loadID)
        {
            this.LoadId = loadID;
        }
        public Load(int loadID, int DriverID, string GlobalStoreNumber, int? TruckID, bool? IsTrailerReq, bool? IsOptimized, bool? IsLocked, DateTime CreatedDateTime, DateTime? ScheduleStartDateTime, DateTime? ScheduleEndDateTime = null, string driverName = null)
        {
            this.LoadId = loadID;
            this.DriverId = DriverID;
            this.GlobalSiteNumber = GlobalStoreNumber;
            this.TruckID = TruckID;
            this.IsLocked = (IsLocked != null ? IsLocked.Value : false);
            this.IsTrailerRequired = IsTrailerReq.HasValue ? IsTrailerReq.Value : false;
            this.IsOptimized = IsOptimized != null ? Convert.ToBoolean(IsOptimized) : false;
            this.CreatedDateTime = CreatedDateTime;
            this.ScheduleStartDateTime = ScheduleStartDateTime;
            this.ScheduleEndDateTime = ScheduleEndDateTime;
            this.DriverName = driverName;
            //this.ScheduleStartDateTime = (ScheduleStartDateTime != null ? ScheduleStartDateTime.Value : DateTime.Now);
        }

        public void AddTouches(List<Touch> touches)
        {
            this.Touches = touches;
        }

        //public void AddStops(List<Stops> stops)
        //{
        //    this.Stops = stops;
        //}
        #endregion

        #region Public methods

        public void AddTouch(Touch touchInfo)
        {
            if (this.Touches == null)
                this.Touches = new List<Touch>();

            this.Touches.Add(touchInfo);
        }

        public IEnumerable<Touch> GetGroupTouches(Guid? guid)
        {
            return this.Touches.Where(t => t.GroupGUID == guid).OrderBy(t => t.RouteSequence);
        }

        public string GetGroupTouchesSequence(Guid? guid)
        {
            string seq = string.Empty;

            foreach (var touch in GetGroupTouches(guid))
            {
                seq = seq + (seq.Length > 0 ? " > " : "") + touch.TouchTypeAcronym;
            }

            return seq;
        }

        public bool TrailerHookupHere(int touchId)
        {
            //    int trailerid = 0;
            int requiredTouchIndex = -1;
            for (int index = 0; index < this.DisplayTouches.Count(); index++)
            {
                if (touchId == this.DisplayTouches[index].TouchId)
                {
                    requiredTouchIndex = index;
                    break;
                }
            }

            bool pickupLeg = false;

            if (requiredTouchIndex == 0)
            {
                if (this.DisplayTouches[requiredTouchIndex].TrailerId > 0)
                {
                    pickupLeg = true;
                }
            }
            //else if(requiredTouchIndex == this.Touches.Count())
            //{
            //    pickupLeg = false;
            //}
            else
            {
                if (this.DisplayTouches[requiredTouchIndex - 1].TrailerId != this.DisplayTouches[requiredTouchIndex].TrailerId && this.DisplayTouches[requiredTouchIndex].TrailerId > 0)
                {
                    pickupLeg = true;
                }
            }

            return pickupLeg;
        }

        public bool TrailerUnHookHere(int touchId)
        {
            int requiredTouchIndex = -1;
            bool dropoffLeg = false;
            Touch thisTouch = null;

            for (int index = 0; index < this.DisplayTouches.Count(); index++)
            {
                if (touchId == this.DisplayTouches[index].TouchId)
                {
                    requiredTouchIndex = index;
                    thisTouch = this.DisplayTouches[index];
                    break;
                }
            }

            //if (requiredTouchIndex == 0)
            //{
            //    dropoffLeg = false;
            //}
            //else 
            if (requiredTouchIndex == (this.DisplayTouches.Count() - 1))
            {
                if (this.DisplayTouches[requiredTouchIndex].TrailerId > 0)
                {
                    dropoffLeg = true;
                }
            }
            else
            {
                if (this.DisplayTouches[requiredTouchIndex].TrailerId > 0 &&
                    //this.Touches[requiredTouchIndex - 1].TrailerId == this.Touches[requiredTouchIndex].TrailerId &&
                    this.DisplayTouches[requiredTouchIndex].TrailerId != this.DisplayTouches[requiredTouchIndex + 1].TrailerId)
                {
                    dropoffLeg = true;
                }
            }

            if (dropoffLeg == false && thisTouch.GroupGUID != null)
            {
                for (int index = requiredTouchIndex + 1; index < this.DisplayTouches.Count(); index++)
                {
                    if (thisTouch.GroupGUID != this.DisplayTouches[index].GroupGUID &&
                       thisTouch.TrailerId > 0 &&
                        thisTouch.TrailerId != this.DisplayTouches[index].TrailerId)
                    {
                        dropoffLeg = true;
                        break;
                    }
                }
            }

            return dropoffLeg;
        }

        public List<Stops> GetThisLoadOptimizedStops
        {
            get
            {
                List<Stops> lstStops = new List<Stops>();

                this.Touches.ForEach(t => lstStops.AddRange(t.Stops));
                return lstStops.Where(l => l.IsRequired == true).OrderBy(ls => ls.StopSequence).ToList();

                //return lstStops;
            }
        }

        public bool StopsTrailerHookupHere(int stopId)
        {
            //int trailerid = 0;
            int requiredStopIndex = -1;
            for (int index = 0; index < this.GetThisLoadOptimizedStops.Count(); index++)
            {
                if (stopId == this.GetThisLoadOptimizedStops[index].TouchStopId)
                {
                    requiredStopIndex = index;
                    break;
                }
            }

            if (requiredStopIndex < 0)
                return false;

            bool pickupLeg = false;

            if (requiredStopIndex == 0)
            {
                if (this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId > 0)
                {
                    pickupLeg = true;
                }
            }
            //else if(requiredTouchIndex == this.Touches.Count())
            //{
            //    pickupLeg = false;
            //}
            else
            {
                if (this.GetThisLoadOptimizedStops[requiredStopIndex - 1].Touch.TrailerId != this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId &&
                    this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId > 0)
                {
                    pickupLeg = true;
                }
            }

            return pickupLeg;
        }

        public bool StopsTrailerUnHookHere(int stopId)
        {
            int requiredStopIndex = -1;
            bool dropoffLeg = false;
            Stops thisStop = null;

            for (int index = 0; index < this.GetThisLoadOptimizedStops.Count(); index++)
            {
                if (stopId == this.GetThisLoadOptimizedStops[index].TouchStopId)
                {
                    requiredStopIndex = index;
                    thisStop = this.GetThisLoadOptimizedStops[index];
                    break;
                }
            }

            if (requiredStopIndex < 0)
                return false;

            //if (requiredStopIndex <= 0)
            //{
            //    dropoffLeg = false;
            //}
            //else 
            if (requiredStopIndex == (this.GetThisLoadOptimizedStops.Count() - 1))
            {
                if (this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId > 0)
                {
                    dropoffLeg = true;
                }
            }
            else
            {
                if (this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId > 0 &&
                    //this.Touches[requiredTouchIndex - 1].TrailerId == this.Touches[requiredTouchIndex].TrailerId &&
                    this.GetThisLoadOptimizedStops[requiredStopIndex].Touch.TrailerId != this.GetThisLoadOptimizedStops[requiredStopIndex + 1].Touch.TrailerId)
                {
                    dropoffLeg = true;
                }
            }

            if (dropoffLeg == false && thisStop.Touch.GroupGUID != null)
            {
                for (int index = requiredStopIndex + 1; index < this.Touches.Count(); index++)
                {
                    if (thisStop.Touch.GroupGUID != this.GetThisLoadOptimizedStops[index].Touch.GroupGUID &&
                       thisStop.Touch.TrailerId > 0 &&
                        thisStop.Touch.TrailerId != this.GetThisLoadOptimizedStops[index].Touch.TrailerId)
                    {
                        dropoffLeg = true;
                        break;
                    }
                }
            }

            return dropoffLeg;
        }

        public IEnumerable<Stops> GetGroupedStops(Guid? guid)
        {
            //List<Stops> lstStops = new List<Stops>();

            //this.Touches.Where().ForEach(t => lstStops.AddRange(t.Stops));
            //lstStops.Where(l => l.IsRequired == true).OrderBy(ls => ls.StopSequence);

            return this.GetThisLoadOptimizedStops.Where(s => s.Touch.GroupGUID == guid);
        }
        #endregion


        /// <summary>
        /// This property will tells that user is already created for other shared facilities
        /// </summary>
        public bool IsAlreadyLoadCreatedForOtherFacilityForThisDriver { get; set; }

        public string StoreNoAlreadyLoadCreatedForOtherFacilityForThisDriver { get; set; }
    }
}
