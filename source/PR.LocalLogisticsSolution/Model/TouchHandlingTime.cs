﻿using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Model
{
    public class TouchHandlingTime
    {
        public int ID { get; set; }
        
        public string StoreNumber { get; set; }
        
        public string StopType { get; set; }

        public int HandlingTime { get; set; }

        public TouchHandlingTime() { }

        public TouchHandlingTime(int Id, string StoreNum, string StopType, int? HandlingTime)
        {
            this.ID = Id;
            this.StoreNumber = StoreNum.ToString();
            this.StopType = StopType;
            this.HandlingTime = Convert.ToInt32(HandlingTime);
        }
    }

    public class TouchStopHandlingTime
    {
        private TouchHandlingTime _mDEPU = new TouchHandlingTime();
        private TouchHandlingTime _mDEDF= new TouchHandlingTime();
        private TouchHandlingTime _mWeightStn= new TouchHandlingTime();
        private TouchHandlingTime _mTrailerHook= new TouchHandlingTime();
        private TouchHandlingTime _mTrailerUnHook= new TouchHandlingTime();
        private TouchHandlingTime _mREPU= new TouchHandlingTime();
        private TouchHandlingTime _mREDF= new TouchHandlingTime();
        private TouchHandlingTime _mDFPU= new TouchHandlingTime();
        private TouchHandlingTime _mDFDF= new TouchHandlingTime();
        private TouchHandlingTime _mRFPU= new TouchHandlingTime();
        private TouchHandlingTime _mRFDF= new TouchHandlingTime();
        private TouchHandlingTime _mCCPU= new TouchHandlingTime();
        private TouchHandlingTime _mCCDF= new TouchHandlingTime();

        public TouchHandlingTime DEPU { get { return _mDEPU; } set { _mDEPU = value; } }
        public TouchHandlingTime DEDF { get { return _mDEDF; } set { _mDEDF = value; } }
        public TouchHandlingTime WeightStn { get { return _mWeightStn; } set { _mWeightStn = value; } }
        public TouchHandlingTime TrailerHook { get { return _mTrailerHook; } set { _mTrailerHook = value; } }
        public TouchHandlingTime TrailerUnHook { get { return _mTrailerUnHook; } set { _mTrailerUnHook = value; } }
        public TouchHandlingTime REPU { get { return _mREPU; } set { _mREPU = value; } }
        public TouchHandlingTime REDF { get { return _mREDF; } set { _mREDF = value; } }
        public TouchHandlingTime DFPU { get { return _mDFPU; } set { _mDFPU = value; } }
        public TouchHandlingTime DFDF { get { return _mDFDF; } set { _mDFDF = value; } }
        public TouchHandlingTime RFPU { get { return _mRFPU; } set { _mRFPU = value; } }
        public TouchHandlingTime RFDF { get { return _mRFDF; } set { _mRFDF = value; } }
        public TouchHandlingTime CCPU { get { return _mCCPU; } set { _mCCPU = value; } }
        public TouchHandlingTime CCDF { get { return _mCCDF; } set { _mCCDF = value; } }

        public TouchStopHandlingTime() { }

        public TouchStopHandlingTime(List<TouchHandlingTime> _TouchHandlingTimes)
        {
            foreach (var touchHandlingTime in _TouchHandlingTimes)
            {
                switch (touchHandlingTime.StopType)
                {
                    case "DE-PU": DEPU = touchHandlingTime; break;
                    case "DE-DF": DEDF = touchHandlingTime; break;
                    case "WeightStn": WeightStn = touchHandlingTime; break;
                    case "TrailerHook": TrailerHook = touchHandlingTime; break;
                    case "TrailerUnHook": TrailerUnHook = touchHandlingTime; break;
                    case "RE-PU": REPU = touchHandlingTime; break;
                    case "RE-DF": REDF = touchHandlingTime; break;
                    case "DF-PU": DFPU = touchHandlingTime; break;
                    case "DF-DF": DFDF = touchHandlingTime; break;
                    case "RF-PU": RFPU = touchHandlingTime; break;
                    case "RF-DF": RFDF = touchHandlingTime; break;
                    case "CC-PU": CCPU = touchHandlingTime; break;
                    case "CC-DF": CCDF = touchHandlingTime; break;
                }
            }
        }
    }
}
