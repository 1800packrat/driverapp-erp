﻿using PR.Entities;
using System;

namespace PR.LocalLogisticsSolution.Model
{
    public class TouchSLUpdateOld
    {
        public int TouchSyncId { get; set; }

        public int TouchId { get; set; }

        public int? LoadId { get; set; }

        public string TouchType { get; set; }

        public bool? WeightStnReq { get; set; }

        public string Status { get; set; }

        public string CustomerName { get; set; }

        public string PhoneNumber { get; set; }

        public string StoreNO { get; set; }

        public string UnitNumber { get; set; }

        public string StartTime { get; set; }

        public string Unitsize { get; set; }

        public Address OriginAddress { get; set; }

        public Address DestAddress { get; set; }

        public Address WeighStopAddress { get; set; }

        public string OrderNumber { get; set; }

        public int QORId { get; set; }

        public string Instructions { get; set; }

        public bool DoorToFront { get; set; }
        
        public bool DoorToRear { get; set; }

        public string DoorPOS { get { return DoorToFront ? "Door Front" : "Door Rear"; } }

        public TouchSLUpdateOld(int TouchSyncId, int TouchId, int? LoadId, bool? WeightStnReq, string CustomerName, string PhoneNumber, string StartTime, int? Unitsize, string OrderNumber, int? QORId, string Instructions, bool? DoorToFront, bool? DoorToRear, string UnitNumber)
        {
            this.TouchSyncId = TouchSyncId;

            this.TouchId = TouchId;

            this.LoadId = LoadId;

            this.TouchType = TouchType;

            this.WeightStnReq = WeightStnReq;

            this.Status = Status;

            this.CustomerName = CustomerName;

            this.PhoneNumber = PhoneNumber;

            this.StartTime = StartTime;

            this.StoreNO = StoreNO;

            this.UnitNumber = UnitNumber;

            this.Unitsize = Unitsize.HasValue ? Unitsize.ToString() : string.Empty;

            this.OrderNumber = OrderNumber;

            this.QORId = QORId.HasValue ? QORId.Value : 0; 
            
            this.Instructions = Instructions;
            
            this.DoorToFront = DoorToFront != null ? Convert.ToBoolean(DoorToFront) : false;
            
            this.DoorToRear = DoorToRear != null ? Convert.ToBoolean(DoorToRear) : false;
        }
    }
}
