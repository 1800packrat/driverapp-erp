﻿using System;

namespace PR.LocalLogisticsSolution.Model
{
    public class BusyTime
    {
        public int BreakID { get; set; }

        public int StopID { get; set; }
        
        public Nullable<System.TimeSpan> BreakTime { get; set; }
        
        public string Comment { get; set; }
    }
}
