﻿// -----------------------------------------------------------------------
// <copyright file="StopInfo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Model
{
    using System;
    using PR.Entities;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Stops
    {
        #region Public Properties

        public int TouchStopId { get; private set; }
        public int TouchId { get; private set; }
        public int StopTypeId { get; private set; }
        //public StopType StopType { get; private set; }

        //public StopType StopType { get; private set; }
        //private string _qorid = String.Empty;
        //private string _touchType = String.Empty;
        //private string _sequenceno = String.Empty;

        private string _customerName = String.Empty;
        private string _addressLine1 = String.Empty;
        private string _addressLine2 = String.Empty;
        private string _city = String.Empty;
        private string _state = String.Empty;
        private string _zip = String.Empty;
        private string _phoneNumber = String.Empty;
        private string _containerNumber = String.Empty;
        private int _containerSize = 0;
        private string _status = String.Empty;
        private decimal _balanceDue = 0;
        private DateTime _scheduledstarttime;
        private DateTime _estimatedendtime;
        private DateTime _estimatedstarttime;
        private DateTime _scheduledendtime;
        private Address _originAddress = new Address();
        private Address _destinationAddress = new Address();
        private Address _weightstationAddress = new Address();
        private string _stopType = String.Empty;
        private string _orderNum = String.Empty;
        private int _stopId = 0;
        private bool _isCombinedTouch = false;
        private bool _isTrailerNeeded = false;
        private string _LoadId = String.Empty;
        private string _DriverId = String.Empty;
        private string _TruckId = String.Empty;
        private string _TruckNum = String.Empty;
        private string _TrailerNum = String.Empty;
        private string _TrailerId = String.Empty;
        private bool _isLockedIndicator = false;
        private int _stopsequence = 0;
        private decimal? _actualDistance;

        public string DestCountry { get; private set; }
        //public DateTime? ScheduledStartTime { get; private set; }
        //public DateTime? ScheduledEndTime { get; private set; }
        public DateTime? ActualStartTime { get; private set; }
        public DateTime? ActualEndTime { get; private set; }
        public string TouchStatus { get;  set; }
        public int? FK_OptStopInfoId { get; private set; }
        public string SkipTouchReason { get; set; }
        public string TouchStopTypeAcronym { get { return (Touch != null ? Touch.TouchTypeAcronym : "") + " " + StopType; } }

        public string TouchStopTypeFullAcronym { get { return (Touch != null ? Touch.TouchTypeAcronym : "") + GetStopType(StopType); } }

        private string GetStopType(string StopType)
        {
            if (PR.LocalLogisticsSolution.Model.StopType.PICKUP.ToString().ToLower() == StopType.ToLower())
            {
                return "PU";
            }
            if (PR.LocalLogisticsSolution.Model.StopType.DROPOFF.ToString().ToLower() == StopType.ToLower())
            {
                return "DF";
            }
            return StopType;
        }

        public Touch Touch { get; set; }

        public BusyTime BusyTime { get; set; }

        public Address SLUpdatedOriginAddress { get; set; }

        public Address SLUpdatedDestAddress { get; set; }

        #endregion

        #region Constructor

        public Stops() { }

        public Stops(int TouchStopId, int TouchId, StopType StopTypeId)
        {
            this.TouchStopId = TouchStopId;
            this.TouchId = TouchId;
            this.StopTypeId = (int)StopTypeId;
        }

        public Stops(int TouchStopId, int TouchId, string StopType, int StopTypeId,
            string CustomerName, bool? IsRequired,
            int OriginAddressId, string OriginAddressLn1, string OriginAddressLn2, string OriginZip, string OriginCity, string OriginState, string OriginCountry, string Olatitude, string Olongitude,PR.UtilityLibrary.PREnums.AddressType OAddressType, int? OFcailityStoreNo,
            int DestinationAddressId, string DestAddressLn1, string DestAddressLn2, string DestZip, string DestCity, string DestState, string DestCountry, string Dlatitude, string Dlongitude, PR.UtilityLibrary.PREnums.AddressType DAddressType, int? DFcailityStoreNo,
            int? Sequence,
            DateTime? ScheduledStartTime, DateTime? ScheduledEndTime,
             DateTime? EstimatedStartTime, DateTime? EstimatedEndTime,
            DateTime? ActualStartTime, DateTime? ActualEndTime,
            Touch touch,
            BusyTime busyTime,string status,
            decimal? actualDistance)
        {
            this.TouchStopId = TouchStopId;
            this.TouchId = TouchId;
            this.StopType = StopType;
            this.StopTypeId = StopTypeId;
            this.Touch = touch;
            this.BusyTime = busyTime;
            this.CustomerName = CustomerName;
            this.IsRequired = (IsRequired == null ? false : IsRequired.Value);
            if (Sequence != null)
            {
                this.StopSequence = Convert.ToInt32(Sequence);
            }
            else
            {
                this.StopSequence = (status == "Completed" || status == "Skipped" ? 0 : 9999);
            }

            ActualDistance = actualDistance;

            this.TouchStatus = status;
           // this.SkipTouchReason = skipTouchReason;
            AssignOriginDetails(OriginAddressId, OriginAddressLn1, OriginAddressLn2, OriginZip, OriginCity, OriginState, OriginCountry, Olatitude, Olongitude,OAddressType, OFcailityStoreNo);
            AssignDestDetails(DestinationAddressId, DestAddressLn1, DestAddressLn2, DestZip, DestCity, DestState, DestCountry, Dlatitude, Dlongitude, DAddressType, DFcailityStoreNo);
            //This needs to be updated -TODO-
            AssignScheduledTime(Convert.ToDateTime(ScheduledStartTime), Convert.ToDateTime(ScheduledEndTime));
            AssignEstimatedTime(EstimatedStartTime, EstimatedEndTime);
            AssignActualTime(ActualStartTime, ActualEndTime);
        }

        public void AssignEstimatedTime(DateTime? dateTime1, DateTime? dateTime2)
        {
            this._estimatedstarttime = dateTime1.HasValue ? dateTime1.Value : DateTime.MinValue;
            this._estimatedendtime = dateTime2.HasValue ? dateTime2.Value : DateTime.MinValue;
        }

        //public Stops(int TouchStopId, int TouchId, string StopType,
        // string CustomerName, Address OriginAddress, Address DestinationAddress,
        // int? Sequence,
        // DateTime? ScheduledStartTime, DateTime? ScheduledEndTime,
        // DateTime? ActualStartTime, DateTime? ActualEndTime)
        //{
        //    this.TouchStopId = TouchStopId;
        //    this.TouchId = TouchId;
        //    this.StopType = StopType;
        //    this.CustomerName = CustomerName;
        //    this.StopSequence = Sequence != null ? Convert.ToInt32(Sequence) : 0;
        //    this.OriginAddress = OriginAddress;
        //    this.DestinationAddress = DestinationAddress;
        //    //This needs to be updated -TODO-
        //    AssignScheduledTime(Convert.ToDateTime(ScheduledStartTime), Convert.ToDateTime(ScheduledEndTime));
        //    AssignActualTime(ActualStartTime, ActualEndTime);
        //}

        #endregion

        #region Publict methods
        public void AssignOriginDetails(Address orgAddress)
        {
            this.OriginAddress = orgAddress;
            // AssignDestDetails(orgAddress.AddressLine1, orgAddress.AddressLine2, orgAddress.Zip, orgAddress.City, orgAddress.State, orgAddress.Country);
        }

        public void AssignOriginDetails(int id, string OriginAddressLn1, string OriginAddressLn2, string OriginZip, string OriginCity, string OriginState, string OriginCountry, string latitude, string longitude,PR.UtilityLibrary.PREnums.AddressType OAddressType, int? OFacilityStoreNo)
        {
            this.OriginAddress.ID = id; this.OriginAddress.AddressLine1 = OriginAddressLn1; this.OriginAddress.AddressLine2 = OriginAddressLn2; this.OriginAddress.Zip = OriginZip; this.OriginAddress.City = OriginCity; this.OriginAddress.State = OriginState; this.OriginAddress.Country = OriginCountry; this.OriginAddress.Latitude = latitude; this.OriginAddress.Longitude = longitude; this.OriginAddress.AddressType = OAddressType;
            this.OriginAddress.FacilityStoreNo = OFacilityStoreNo;
        }

        public void AssignDestDetails(Address destAddress)
        {
            this.DestinationAddress = destAddress;
            //this.DestAddressLn1 = DestAddressLn1; this.DestAddressLn2 = DestAddressLn2; this.DestZip = DestZip; this.DestCity = DestCity; this.DestState = DestState; this.DestCountry = DestCountry;
            //  AssignDestDetails(destAddress.AddressLine1, destAddress.AddressLine2, destAddress.Zip, destAddress.City, destAddress.State, destAddress.Country);
        }

        public void AssignDestDetails(int id, string DestAddressLn1, string DestAddressLn2, string DestZip, string DestCity, string DestState, string DestCountry, string latitude, string longitude,PR.UtilityLibrary.PREnums.AddressType DAddressType, int? DFacilityStoreNo)
        {
            this.DestinationAddress.ID = id; this.DestinationAddress.AddressLine1 = DestAddressLn1; this.DestinationAddress.AddressLine2 = DestAddressLn2; this.DestinationAddress.Zip = DestZip; this.DestinationAddress.City = DestCity; this.DestinationAddress.State = DestState; this.DestinationAddress.Country = DestCountry; this.DestinationAddress.Latitude = latitude; this.DestinationAddress.Longitude = longitude; this.DestinationAddress.AddressType = DAddressType;
            this.DestinationAddress.FacilityStoreNo = DFacilityStoreNo;
        }

        public void AssignScheduledTime(DateTime ScheduledStartTime, DateTime ScheduledEndTime)
        {
            //this.ScheduledStartTime = TimeSpan.FromTicks(ScheduledStartTime.Ticks); this.ScheduledEndTime = TimeSpan.FromTicks(ScheduledEndTime.Ticks);
            this.ScheduledStartTime = ScheduledStartTime; this.ScheduledEndTime = ScheduledEndTime;
        }

        public void AssignActualTime(DateTime? ActualStartTime, DateTime? ActualEndTime)
        {
            //this.ActualStartTime = TimeSpan.FromTicks(ActualStartTime.Ticks); this.ActualEndTime = TimeSpan.FromTicks(ActualEndTime.Ticks);
            this.ActualStartTime = ActualStartTime; this.ActualEndTime = ActualEndTime;
        }

        //public void UpdateStatus(string TouchStatus) { this.TouchStatus = TouchStatus; }

        #endregion

        public string LoadId
        {
            get { return _LoadId; }
            set { _LoadId = value; }
        }
        public string DriverId
        {
            get { return _DriverId; }
            set { _DriverId = value; }
        }
        public string TruckId
        {
            get { return _TruckId; }
            set { _TruckId = value; }
        }
        public string TrailerId
        {
            get { return _TrailerId; }
            set { _TrailerId = value; }
        }
        public string TruckNumber
        {
            get { return _TruckNum; }
            set { _TruckNum = value; }
        }
        public string TrailerNumber
        {
            get { return _TrailerNum; }
            set { _TrailerNum = value; }
        }

        public bool IsLockedIndicator
        {
            get { return _isLockedIndicator; }
            set { _isLockedIndicator = value; }
        }

        public int StopSequence
        {
            get { return _stopsequence; }
            set { _stopsequence = value; }
        }

        public decimal? ActualDistance
        {
            get { return _actualDistance; }
            set { _actualDistance = value; }
        }


        //public string QORID
        //{
        //    get { return _qorid; }
        //    set { _qorid = value; }
        //}
        //public string TouchType
        //{
        //    get { return _touchType; }
        //    set { _touchType = value; }
        //}
        //public string SequenceNumber
        //{
        //    get { return _sequenceno; }
        //    set { _sequenceno = value; }
        //}
        public string CustomerName
        {
            get { return _customerName; }
            set { _customerName = value; }
        }
        public string AddressLine1
        {
            get { return _addressLine1; }
            set { _addressLine1 = value; }
        }
        public string AddressLine2
        {
            get { return _addressLine2; }
            set { _addressLine2 = value; }
        }
        public string City
        {
            get { return _city; }
            set { _city = value; }
        }
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        public string Zip
        {
            get { return _zip; }
            set { _zip = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public string ContainerNumber
        {
            get { return _containerNumber; }
            set { _containerNumber = value; }
        }
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public decimal BalanceDue
        {
            get { return _balanceDue; }
            set { _balanceDue = value; }
        }
        public DateTime ScheduledStartTime
        {
            get { return _scheduledstarttime; }
            set { _scheduledstarttime = value; }
        }
        public DateTime ScheduledEndTime
        {
            get { return _scheduledendtime; }
            set { _scheduledendtime = value; }
        }
        public int ContainerSize
        {
            get { return _containerSize; }
            set { _containerSize = value; }
        }

        public Address OriginAddress
        {
            get { return _originAddress; }
            set { _originAddress = value; }
        }

        public Address DestinationAddress
        {
            get { return _destinationAddress; }
            set { _destinationAddress = value; }
        }
        public Address WeightStationAddress
        {
            get { return _weightstationAddress; }
            set { _weightstationAddress = value; }
        }

        public string StopType
        {
            get { return _stopType; }
            set { _stopType = value; }
        }

        public string OrderNum
        {
            get { return _orderNum; }
            set { _orderNum = value; }
        }

        public int StopId
        {
            get { return _stopId; }
            set { _stopId = value; }
        }

        public bool IsCombinedTouch
        {
            get { return _isCombinedTouch; }
            set { _isCombinedTouch = value; }
        }

        public bool IsTrailerNeeded
        {
            get { return _isTrailerNeeded; }
            set { _isTrailerNeeded = value; }
        }

        public DateTime TouchDate { get; set; }

        public string ScheduleStartDateTime { get; set; }
        public string ScheduleEndDateTime { get; set; }

        public DateTime EstimatedStartDateTime { get { return _estimatedstarttime; } set { _estimatedstarttime = value; } }
        public DateTime EstimatedEndDateTime { get { return _estimatedendtime; } set { _estimatedendtime = value; } }

        public int OrderId { get; set; }

        public bool IgnoreConstraints { get; set; }

        public string Color { get; set; }

        public string TouchComments { get; set; }
        public bool IsFakeTouch { get; set; }
        public bool IsRequired { get; set; }

        //Below two properties are added for markets
        //If we get value to this property from UI then we are treating it as updated one
        public int? ChangedOrgAddressId { get; set; }
        public int? ChangedDestAddressId { get; set; }
    }
}
