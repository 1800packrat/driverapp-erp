﻿using System;

namespace PR.LocalLogisticsSolution.Model
{
    public class Trucks
    {
        public int TruckId { get; set; }
        public string Facility { get; set; }
        public string EnterdBy { get; set; }
        public DateTime? EntryDate { get; set; }
        public string TruckNumber { get; set; }
        public string FleetID { get; set; }
        public string TruckStatus { get; set; }
        public string Comments { get; set; }
        public string SiteNumber { get; set; }
        public string FacCode { get; set; }
        public string TruckNumberWithFacCode { get { return TruckNumber + (string.IsNullOrEmpty(SiteNumber) ? "" : " - " + SiteNumber); } }
        public bool IsAlreadyInLoad { get; set; }
    }
}
