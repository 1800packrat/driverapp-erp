﻿using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Model
{
    public class Market
    {
        private int _marketId;
        private string _marketName;

        public int MarketId
        {
            get { return _marketId; }
            set { _marketId = value; }
        }

        public string MarketName
        {
            get { return _marketName; }
            set { _marketName = value; }
        }

        private List<FacilityInfo> _Facilities = new List<FacilityInfo>();

        public List<FacilityInfo> Facilities
        {
            get { return _Facilities; }
            set { _Facilities = value; }
        }
    }

    public class FacilityInfo : Entities.SiteInfo
    {
        #region Private 

        private List<Touch> _touches;

        private List<Load> _loads;

        private List<Trucks> _Trucks;

        #endregion

        #region Public Properties

        public List<Touch> Touches
        {
            get { return _touches; }
            set { _touches = value; }
        }

        public List<Load> Loads
        {
            get { return _loads; }
            set { _loads = value; }
        }

        public List<Trucks> Trucks
        {
            get { return _Trucks; }
            set { _Trucks = value; }
        }

        #endregion

        #region Public Methods

        #endregion
    }
}
