﻿// -----------------------------------------------------------------------
// <copyright file="TouchInfo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using PR.Entities;
    using System.ComponentModel;
    using System.Reflection;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Touch : SLTouch
    {
        //private int _qorid;
        //private string _customerName = String.Empty;
        //private string _phoneNumber = String.Empty;
        //private DateTime _schDate = DateTime.MinValue;
        //private string _storeno = String.Empty;
        //private int _seqno;
        //private string _unitNumber = string.Empty;
        //private string _globalSiteNum = string.Empty;

        #region Public Properties

        public int TouchId { get; set; }

        public int? LoadId { get; set; }

        //public string TouchType { get; set; }

        public bool? WeightStnReq { get; set; }

        //public bool? TrailerReq { get; private set; }

        //public string Status { get; set; }

        public List<Stops> Stops { get; private set; }

        public List<Stops> OptimizedStops { get; private set; }

        //public string OrderNumber
        //{
        //    get { return GetTouchType(TouchType) + "-" + Qorid + "-" + SequenceNO; }
        //    //get { return GetTouchType(TouchType) + "-" + Qorid; }
        //}

        //public string OrderNumberDisp
        //{
        //    get { return GetTouchType(TouchType) + "-" + Qorid; }
        //}

        public string TouchNumberDisp
        {
            get
            {
                return GetTouchType(TouchType) + "-" + UnitNumber + " (" + (IsZippyShellQuote ? "ZM-" : string.Empty) + Unitsize + ")"
                   + (string.IsNullOrEmpty(GlobalSiteNum) ? string.Empty : " - " + GlobalSiteNum);
            }
        }

        public string TouchTypeDisp { get { return GetTouchType(this.TouchType); } }

        public string TouchTypeAcronym { get { return GetTouchType(TouchType); } }

        public int DisplayOrder//(string startTime)
        {
            get
            {
                switch (this.StartTime)
                {
                    case "AM":
                        return 1;
                    case "PM":
                        return 2;
                    case "Anytime":
                        return 3;
                    default:
                        return 4;
                }
            }
        }

        //public int Qorid
        //{
        //    get { return _qorid; }
        //    set { _qorid = value; }
        //}

        private SLUpdateType _SLStatusType;
        /// <summary>
        /// 1	Nochanges
        ////2	Sync
        ////3	Canceled
        ////4	OnlyAddressUpdated
        ////5	OnlyInfoUpdated
        ////6	AddressAndInfoUpdated
        /// </summary>
        public SLUpdateType SLUpdatedStatusType
        {
            get { return _SLStatusType; }
            set { _SLStatusType = value; }
        }

        public bool IsExceptionalTouch { get; set; }

        //public string CustomerName
        //{
        //    get { return _customerName; }
        //    set { _customerName = value; }
        //}

        public override string CustomerName { get { return PickupStop != null ? PickupStop.CustomerName : base.CustomerName; } }

        //public string PhoneNumber
        //{
        //    get { return _phoneNumber; }
        //    set { _phoneNumber = value; }
        //}

        //public string StoreNO
        //{
        //    get { return _storeno; }
        //    set { _storeno = value; }
        //}

        //public DateTime ScheduledDate
        //{
        //    get { return _schDate; }
        //    set { _schDate = value; }
        //}

        //public string ScheduledDateStr
        //{
        //    get { return (this.ScheduledDate != DateTime.MinValue) ? this.ScheduledDate.ToShortDateString() : ""; }
        //}

        public DateTime StopScheduledDateTime
        {
            get
            {
                if (Stops != null)
                {
                    var firstStp = Stops.Where(s => s.IsRequired == true).OrderBy(s => s.StopSequence).FirstOrDefault();

                    if (firstStp != null && firstStp.TouchStopId > 0)
                    {
                        return firstStp.ScheduledStartTime;
                    }
                    else
                    {
                        return ScheduledDate;
                    }
                }
                else
                    return ScheduledDate;
            }
        }

        //public int SequenceNO
        //{
        //    get { return _seqno; }
        //    set { _seqno = value; }
        //}

        //public string UnitNumber
        //{
        //    get { return _unitNumber; }
        //    set { _unitNumber = value; }
        //}

        //public string Unitsize { get; set; }

        //public string StarsID { get; set; }

        public decimal LDMQuoteID { get; set; }

        public decimal LDMTripId { get; set; }

        public string LDMTripNumber { get; set; }

        public decimal LDMOrgQORId { get; set; }

        public decimal LDMDestQORId { get; set; }

        public decimal LDMBillingQORId { get; set; }

        public decimal LDMDummyBillingQORId { get; set; }

        public string LDMArrivalDepartureTime { get; set; }

        public string LDMOrigLocationCode { get; set; }

        public byte? LDMContainerType { get; set; }

        public string LDMDestLocationCode { get; set; }

        public string Confirmed { get; set; }

        public string StartTimeZone
        {
            get
            {
                string timeZone = StartTime;
                try
                {
                    switch (StartTime.ToLower())
                    {
                        case "am":
                        case "pm":
                        case "anytime":
                            timeZone = StartTime;
                            break;
                        default:
                            TimeSpan ts = TimeSpan.Parse(StartTime);
                            TimeSpan amTs = TimeSpan.Parse("12:00");
                            if (ts <= amTs)
                            {
                                timeZone = "AM";
                            }
                            else if (ts > amTs)
                            {
                                timeZone = "PM";
                            }
                            break;
                    }
                }
                catch { }

                return timeZone;
            }
        }

        //public string StartTime { get; set; }

        //public string EndTime { get; set; }

        //public string Instructions { get; set; }

        public int OrderId { get; set; }

        //public bool IsWeightTicket { get; set; }

        //public string Obstacles { get; set; }

        //public string DriveWay { get; set; }

        //public string DoorPOS { get { return DoorToFront ? "Door Front" : "Door Rear"; } }

        //public bool DoorToFront { get; set; }

        //public bool DoorToRear { get; set; }

        //public bool doorposSLupdated { get; set; }

        // public Address OriginAddress { get { if (this._originAddress != null) return GetOriginAddress(); else return this._originAddress; } set { _originAddress = value; } }

        // public Address DestAddress { get { if (this._destinationAddress != null) return GetDestinationAddress(); else return this._destinationAddress; } set { _destinationAddress = value; } }

        //public Address OriginAddress { get; set; }
        //public Address DestAddress { get; set; }

        public Address TouchOriginAddressWithOutUpdation { get; set; }
        public Address TouchDestAddressWithOutUpdation { get; set; }

        //public Address WeighStopAddress { get; set; }

        public String Color { get; set; }

        //public string mOrderNumber { get; set; }

        //public DateTime mCannotDeliverBefore { get; set; }

        //public DateTime mMustDeliverBy { get; set; }

        //public DateTime mCannotShipBefore { get; set; }

        //public DateTime mMustShipBy { get; set; }

        public int RouteSequence { get; set; }

        public int TouchGroupId { get; set; }

        public Guid? GroupGUID { get; set; }

        public bool IsGrouped { get; set; }

        public int TrailerId { get; set; }

        public Trailers Trailer { get; set; }

        //public int StarsUnitId { get; set; }

        //public decimal SiteLinkMileage { get; set; }

        public Stops PickupStop
        {
            get
            {
                return Stops == null ? null : Stops.Where(s => s.StopType.ToLower() == StopType.PICKUP.ToString().ToLower()).FirstOrDefault();
            }
        }

        public Stops DropoffStop
        {
            get
            {
                return (Stops == null) ? null : Stops.Where(s => s.StopType.ToLower() == StopType.DROPOFF.ToString().ToLower()).FirstOrDefault();
            }
        }

        public Stops ToFacilityStop
        {
            get
            {
                return (Stops == null) ? null : Stops.Where(s => s.StopType.ToLower() == StopType.ToFacility.ToString().ToLower()).FirstOrDefault();
            }
        }

        public string GlobalSiteNum { get; set; }


        //following 3 properties are related to load details
        public bool? IsLocked { get; set; }

        public int? TruckId { get; set; }

        public string DriverName { get; set; }

        public int? DriverId { get; set; }

        //private string _facCode;

        //public string FacCode
        //{
        //    get { return _facCode; }
        //    set { _facCode = value; }
        //}

        //public ETASettings ProvidedETASettings { get; set; }
        #endregion

        #region Constructor

        public Touch()
        { }

        public Touch(int TouchId, int LoadId, bool? WeightStnReq, int trailerId, string Status, string StartTime, string TouchType, int? QORId, int? SequenceNO, DateTime? scheduleDate,
            string phoneNumber, string instructions, bool? DoorToFront, bool? DoorToRear, bool isgrouped, int fkgroupid, int? TouchGroupId, Guid? guid, Trailers trailer,
            int? starsUnitId, string containerNo, int? routeSequence, int unitSize, decimal? SiteLinkMileage, int? SLStatus, bool? IsExceptionalTouch,
            bool? IsLocked, int? TruckID, string DriverName, int? DriverId, bool? IsZippyShellMove, string storeNumber = null)
        {
            this.TouchId = TouchId;
            this.LoadId = LoadId;
            this.WeightStnReq = WeightStnReq != null ? WeightStnReq : false;
            this.TrailerId = trailerId;
            this.Trailer = trailer;
            this.TouchGroupId = TouchGroupId == null ? 0 : Convert.ToInt32(TouchGroupId);
            this.Status = Status;
            this.StartTime = StartTime;
            this.TouchType = TouchType;
            if (scheduleDate != null) this.ScheduledDate = Convert.ToDateTime(scheduleDate);
            this.Instructions = instructions;
            this.DoorToFront = DoorToFront != null ? Convert.ToBoolean(DoorToFront) : false;
            this.DoorToRear = DoorToRear != null ? Convert.ToBoolean(DoorToRear) : false;
            this.PhoneNumber = phoneNumber;
            this.IsGrouped = isgrouped;
            this.GroupGUID = guid;
            this.StarsUnitId = (starsUnitId != null ? Convert.ToInt32(starsUnitId) : 0);
            this.UnitNumber = containerNo;
            this.RouteSequence = routeSequence ?? 0;
            this.Unitsize = unitSize.ToString();
            int tmp = 0;

            int.TryParse(Convert.ToString(SequenceNO), out tmp);
            this.SequenceNO = tmp;
            this.GlobalSiteNum = storeNumber;

            int.TryParse(Convert.ToString(QORId), out tmp);
            this.Qorid = tmp;
            this.SLUpdatedStatusType = (SLStatus.HasValue ? (SLUpdateType)SLStatus : SLUpdateType.Nochanges);

            this.IsExceptionalTouch = IsExceptionalTouch ?? false;

            this.SiteLinkMileage = (SiteLinkMileage == null ? 0 : SiteLinkMileage.Value);

            this.IsLocked = IsLocked;
            this.TruckId = TruckID;
            this.DriverName = DriverName;
            this.DriverId = DriverId;

            this.IsZippyShellQuote = (bool)IsZippyShellMove;
        }

        #endregion

        #region Public methods

        public void AddStop(Stops stop)
        {
            if (this.Stops == null)
                this.Stops = new List<Stops>();

            this.Stops.Add(stop);
        }

        public void AddStops(List<Stops> stop)
        {
            if (this.Stops == null)
                this.Stops = new List<Stops>();

            this.Stops.AddRange(stop);
        }

        public void AddOptimizedStop(Stops stop)
        {
            if (this.OptimizedStops == null)
                this.OptimizedStops = new List<Stops>();

            this.OptimizedStops.Add(stop);
        }

        public void AddOptimizedStops(List<Stops> stop)
        {
            if (this.OptimizedStops == null)
                this.OptimizedStops = new List<Stops>();

            this.OptimizedStops.AddRange(stop);
        }

        #endregion

        public TouchSLUpdateOld SLUpdatedTouch { get; set; }

        private Address GetOriginAddress()
        {
            switch (this.TouchTypeAcronym)
            {
                case "DE":
                case "DF":
                    return this.Stops[0].OriginAddress;

                case "CC":
                    return this.Stops[0].DestinationAddress;

                case "RE":
                case "RF":
                    return this.Stops[0].DestinationAddress;
                default:
                    break;
            }
            return null;
        }

        private Address GetDestinationAddress()
        {
            switch (this.TouchTypeAcronym)
            {
                case "DE":
                case "DF":
                    return this.Stops[1].DestinationAddress;

                case "CC":
                    return this.Stops[1].DestinationAddress;

                case "RE":
                case "RF":
                    return this.Stops[1].OriginAddress;
                default:

                    break;
            }
            return null;
        }

        // copy base class properties.
        public Touch(SLTouch slTouch)
        {
            foreach (PropertyInfo prop in slTouch.GetType().GetProperties())
            {
                if (prop.CanWrite)
                {
                    PropertyInfo prop2 = slTouch.GetType().GetProperty(prop.Name);
                    prop2.SetValue(this, prop.GetValue(slTouch, null), null);
                }
            }
        }
    }
}
