﻿using PR.Entities;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;
using static PR.UtilityLibrary.PREnums;

namespace PR.LocalLogisticsSolution.Service.Implementation
{
    public class UserService : IUserService
    {
        IDBRepository _dbRepository = null;
        IRouteRepository _routeRepository = null;
        public UserService(IDBRepository dbRepository, IRouteRepository routeRepository)
        {
            _dbRepository = dbRepository;
            _routeRepository = routeRepository;
        }

        public Model.LogisticsUser Login(string username, string password, Application application)
        {
            LogisticsUser user = new LogisticsUser();
            _dbRepository.Login(username, password, string.Empty, application, ref user);

            return user;
        }
        
        public FADALoginStatus Login(string username, string password, string IPAddress, Application application, ref LogisticsUser user)
        {
            return _dbRepository.Login(username, password, IPAddress, application, ref user);
        }

        //public List<Model.LogisticsUser> GetUserList()
        //{
        //    return _dbRepository.GetUserList();
        //}


        public List<Entities.PRDriver> GetAllDriversByFacility(string faciltyNumber, DateTime date, int? marketId = null)
        {
            return _dbRepository.GetAllDriversByFacility(faciltyNumber, date, marketId);
        }

        public Model.DriverSchedule GetDriverSchedule(DateTime date, int driverId)
        {
            return _dbRepository.GetDriverSchedule(date, driverId);
        }


        public List<Model.Trucks> GetTruckStatus(string storeNumber)
        {
            return _dbRepository.GetTruckStatus(storeNumber);
        }


        public string FetchSuggestedDriverUserName(string firstName, string lastName, string facilityNumber)
        {
            string usernm = firstName[0] + lastName + "-" + facilityNumber;
            if (!_routeRepository.CheckIfUserNameExsists(usernm))
            {
                return usernm;
            }
            else
            {
                Random random = new Random();

                return firstName[0] + lastName + random.Next(01, 10).ToString("00") + "-" + facilityNumber;
            }
        }


        public void CreateDriverLogin(Entities.PRDriver driver)
        {
            if (!_routeRepository.CheckIfUserNameExsists(driver.UserName))
            {
                driver.Password = "Packrat!1";
                _routeRepository.CreateDriverLogin(driver);
            }
            else
            {
                throw new Exception("Username already exisits");
            }
        }


        public List<Entities.SiteInfo> GetAllFacilities()
        {
            return _dbRepository.GetAllFacilities();
        }

        public List<Entities.SiteInfo> GetMarketFacilities(int marketId)
        {
            return _routeRepository.GetMarketFacilities(marketId);
        }

        public Entities.SiteInfo AddMarketFacilities(Entities.SiteInfo site)
        {
            return _routeRepository.AddMarketFacilities(site);
        }

        public List<Model.LogisticsUser> GetAllDrivers(string Name, int StoreNo, bool IsActive)
        {
            return _dbRepository.GetAllDrivers(Name, StoreNo, IsActive);
        }

        public Model.LogisticsUser GetDriverById(int ID)
        {
            return _dbRepository.GetDriverById(ID);
        }

        public int SaveDriverDetails(Model.LogisticsUser driver)
        {
            return _dbRepository.SaveDriverDetails(driver);
        }

        public bool SetDriverStatus(Model.LogisticsUser driver)
        {
            return _dbRepository.SetDriverStatus(driver);
        }

        public List<DriverScheduleHourChangeAndStatus> CheckUpdateDriverScheduleHours(int driverId, List<DateTime> ListScheduleDates)
        {
            return _dbRepository.CheckUpdateDriverScheduleHours(driverId, ListScheduleDates);
        }

        public bool CheckLoadExistForGivenDateRange(int driverId, DateTime StartDate, DateTime EndDate)
        {
            return _dbRepository.CheckLoadExistForGivenDateRange(driverId, StartDate, EndDate);
        }

        public List<DateTime> GetDriverLoadDates(int driverId, DateTime StartDate)
        {
            return _dbRepository.GetDriverLoadDates(driverId, StartDate);
        }
    }
}
