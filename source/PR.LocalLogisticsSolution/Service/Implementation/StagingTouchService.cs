﻿using PR.Entities;
using PR.LocalLogisticsSolution.Interfaces;
using PR.LocalLogisticsSolution.Model;
using System;
using System.Collections.Generic;

namespace PR.LocalLogisticsSolution.Service.Implementation
{
    public class StagingTouchService : IStagingTouchService
    {
        private readonly IRouteRepository _routeRepository;
        private readonly ISitelinkRepository _sitelinkRepository;
        private readonly IUserService _userService;
        private readonly IDBRepository _dbRepository;
        private readonly IStarsDbRepository _starsRepository;

        public StagingTouchService(IRouteRepository routeRepository, ISitelinkRepository iSitelinkRepository, IUserService userService, IDBRepository dbRepository, IStarsDbRepository starsRepository)
        {
            _routeRepository = routeRepository;
            _sitelinkRepository = iSitelinkRepository;
            _userService = userService;
            _dbRepository = dbRepository;
            _starsRepository = starsRepository;
        }

        public List<Touch> GetStagingTouches(SiteInfo site, DateTime StartDate, DateTime EndDate)
        {
            List<Touch> lstTouches = new List<Touch>();

            foreach (DateTime date in EachDay(StartDate, EndDate))
            {
                lstTouches.AddRange(_sitelinkRepository.GetStagingTouches(site, date));
            }

            return lstTouches;
        }

        private IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public void CompleteIBOTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo)
        {
            _sitelinkRepository.CompleteIBOWHOBHExtraTouches(QORId, "InByOwner", sequenceNum, activityBy, unitNo);
            _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.TouchStatusUpdated, "Updated IBO touch status to complete. QOR: " + QORId, activityBy, 0, StarsId, applicationName);
        }

        public void CompleteWHTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo)
        {
            _sitelinkRepository.CompleteIBOWHOBHExtraTouches(QORId, "WarehouseAccess", sequenceNum, activityBy, unitNo);
            _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.TouchStatusUpdated, "Updated WarehouseAccess touch status to complete. QOR: " + QORId, activityBy, 0, StarsId, applicationName);
        }

        public void CompleteOBOTouch(int QORId, int sequenceNum, int StarsId, string applicationName, string activityBy, string unitNo)
        {
            _sitelinkRepository.CompleteIBOWHOBHExtraTouches(QORId, "OutByOwner", sequenceNum, activityBy, unitNo);
            _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.TouchStatusUpdated, "Updated OBO touch status to complete. QOR: " + QORId, activityBy, 0, StarsId, applicationName);
        }

        /// <summary>
        /// Below is the flow to complete Transfer Out touch
        /// </summary>
        /// <param name="QORId"></param>
        /// <param name="dummyBillingQORId"></param>
        /// <param name="billingQORId"></param>
        /// <param name="TripId"></param>
        /// <param name="StarsId"></param>
        /// <param name="applicationName"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool TransferOut(int QORId, int dummyBillingQORId, int billingQORId, int TripId, int StarsId, string applicationName, string activityBy)
        {
            bool success = true;
            string suggestedUnitName = string.Empty;

            if (_sitelinkRepository.IsLDMOrderSettled(QORId) == false)  //if (_sitelinkRepository.IsLDMOrderSettled(dummyBillingQORId) == false)
            {
                _sitelinkRepository.CompleteWarehouseTouch(QORId, activityBy);
                _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.TouchStatusUpdated, "Completed warehouse touch, qorid: " + QORId, activityBy, 0, StarsId, applicationName);
            }

            _sitelinkRepository.CompletTOAndMoveOutTOTouch(QORId, activityBy, "");
            _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.MoveOut, "Completed Transferout touch and moving out from origin: " + QORId, activityBy, 0, StarsId, applicationName);

            ////IsOrderSettled() method we are checking in StarsDB but not sure Stars application already setted this flag to true with out settling the order
            //if (IsOrderSettled(StarsId) == false || _sitelinkRepository.IsLDMOrderSettled(dummyBillingQORId) == false)
            //{   
            //    //Need to update start and end date for non-recurring items
            //    _sitelinkRepository.UpdateNonRecItemsDateForBillingQOR(dummyBillingQORId, StarsId, Guid.NewGuid().ToString());
            //    _dbRepository.InsertActivityLog(billingQORId, PRStagingActivityType.NonRecurringItemUpdated, "Updated non recurring item date for billing qor. QOR: " + QORId + "; UnitBillingQOR: " + billingQORId, activityBy, 0, StarsId, applicationName);

            //    _starsRepository.MakeOrderSettle(StarsId);
            //    _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.SettleOrder, "Settle order for StarsId: " + QORId, activityBy, 0, StarsId, applicationName);

            //    //Complete WH touches
            //    _sitelinkRepository.CompleteWarehouseTouch(dummyBillingQORId);
            //    _dbRepository.InsertActivityLog(QORId, PRStagingActivityType.TouchStatusUpdated, "Completed warehouse touche in dummy billing qorid: " + dummyBillingQORId, activityBy, 0, StarsId, applicationName);
            //}

            //_sitelinkRepository.CompletTOAndMoveOutTOTouch(QORId);
            //_dbRepository.InsertActivityLog(QORId, PRStagingActivityType.MoveOut, "Completed Transferout touche and moving out from origin: " + QORId, activityBy, 0, StarsId, applicationName);

            return success;
        }

        /// <summary>
        /// Below is the flow to complete Transfer In touch
        /// </summary>
        /// <param name="unitName"></param>
        /// <param name="UnitId"></param>
        /// <param name="orgQORId"></param>
        /// <param name="destQORId"></param>
        /// <param name="billingQORId"></param>
        /// <param name="TripId"></param>
        /// <param name="StarsId"></param>
        /// <param name="orgLocationCode"></param>
        /// <param name="destLocationCode"></param>
        /// <param name="sessionId"></param>
        /// <param name="ContainerType"></param>
        /// <param name="applicationName"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool TransferIn(string unitName, int UnitId, int orgQORId, int destQORId, int billingQORId, int TripId, int StarsId, string orgLocationCode, string destLocationCode, string sessionId, int ContainerType, string applicationName, string activityBy)
        {
            bool success = true;
            string suggestedUnitName = string.Empty;
            if (_sitelinkRepository.CheckIsUnitValidForThisQor(unitName, orgQORId, activityBy, out suggestedUnitName) == true)
            {
                _sitelinkRepository.CompletTIAndMoveInTITouch(destQORId, unitName, activityBy, UnitId, StarsId);
                _dbRepository.InsertActivityLog(destQORId, PRStagingActivityType.TouchStatusUpdated, "Complete Transfer In touch and transfer in destination qor: " + destQORId + ", with Unitname: " + unitName, activityBy, 0, StarsId, applicationName);

                ////As per discusison with Sidda, its not required for Salesforce - 03/04/2020.
                //_sitelinkRepository.CompleteCCTouchForBillingQor(billingQORId, activityBy);
                //_dbRepository.InsertActivityLog(billingQORId, PRStagingActivityType.TouchStatusUpdated, "Complete CC touch in billing qor: " + billingQORId, activityBy, 0, StarsId, applicationName);
            }
            else
            {
                throw new Exception("You have entered invalid UnitName. Please reenter valid UnitName. Please");
            }

            //// Commented for TG-705
            //if (_sitelinkRepository.CheckIsUnitValidForThisQor(unitName, orgQORId, sessionId, out suggestedUnitName) == true)
            //{
            //    _sitelinkRepository.TransferUnitFromOrigToDestForLDM(orgLocationCode, destLocationCode, unitName, (ContainerType == 1 ? "000016" : "000008"), false);
            //    _dbRepository.InsertActivityLog(orgQORId, PRStagingActivityType.LDMUnitTransfer, "Transfer Unit from Origin to Destination: " + orgQORId + " to " + destQORId, activityBy, 0, StarsId, applicationName);

            //    //TODO this thing needs to discuss with Amit or Sushil or JD
            //    //Update the rental tate to destination rent
            //    decimal unitprice = _starsRepository.GetUnitRent(StarsId, ContainerType == 1); //If ContainerType is one means it should be 16Ft container
            //    _sitelinkRepository.UpdateLDMUnitRentToSiteLink(billingQORId, unitprice);

            //    _sitelinkRepository.CompletTIAndMoveInTITouch(destQORId, unitName, sessionId);
            //    _dbRepository.InsertActivityLog(destQORId, PRStagingActivityType.TouchStatusUpdated, "Complete Transfer In touch and transfer in destination qor: " + destQORId + ", with Unitname: " + unitName, activityBy, 0, StarsId, applicationName);

            //    _sitelinkRepository.CompleteCCTouchForBillingQor(billingQORId, sessionId);
            //    _dbRepository.InsertActivityLog(billingQORId, PRStagingActivityType.TouchStatusUpdated, "Complete CC touch in billing qor: " + billingQORId, activityBy, 0, StarsId, applicationName);
            //}
            //else
            //{
            //    throw new Exception("You have entered invalid UnitName. Please reenter valid UnitName. Please");
            //}

            return success;
        }

        //private bool IsOrderSettled(int OrderId)
        //{
        //    return _starsRepository.IsQuoteSettled(OrderId);
        //}

    }
}
