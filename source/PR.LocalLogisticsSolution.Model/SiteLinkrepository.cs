﻿using PR.LocalLogisticsSolution.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using PR.BusinessLogic;
using System.Data;
using PR.Entities;
using PR.ExternalInterfaces;
using PR.UtilityLibrary;
using static PR.UtilityLibrary.ERP_Enums;
using Newtonsoft.Json;
using System.IO;
using PR.Entities.EsbEntities;

namespace PR.LocalLogisticsSolution.Model
{
    public class SiteLinkrepository : ISitelinkRepository
    {

        private static List<Entities.Address> CachedAddresses = new List<Entities.Address>();
        string TransportationTouchTypeIds = $"{(int)eQTType.WarehouseToCurbEmpty},{(int)eQTType.WarehouseToCurbFull},{(int)eQTType.CurbToCurb},{(int)eQTType.ReturnToWarehouseEmpty},{(int)eQTType.ReturnToWarehouseFull}";
        string WarehouseTouchTypeIds = $"{(int)eQTType.WarehouseAccess},{(int)eQTType.InByOwner},{(int)eQTType.OutByOwner},{(int)eQTType.LDMTransferIn},{(int)eQTType.LDMTransferOut}";
        string StatusIDs_OpenCompleted = $"{(int)eQTStatus.Open},{(int)eQTStatus.Completed}";
        string RentalStatusIDs_ActiveCompleted = $"{(int)eQTRentalStatuses.Active},{(int)eQTRentalStatuses.Completed}";
        string RentalTypeIDs_OrderRental = $"{(int)eQTRentalTypes.Order},{(int)eQTRentalTypes.Rental}";

        public SiteLinkrepository()
        {
        }

        public List<Touch> GetStagingTouches(Entities.SiteInfo site, DateTime date)
        {
            List<Touch> touchList = new List<Touch>();

            #region SFERP-TODO-ESBMT 

            var touchRequest = new StagingUnassignTouchRequest
            {
                locationCode = site.LocationCode,
                touchDate = date.ToString("MM/dd/yyyy"),
                touchTypeIDs = WarehouseTouchTypeIds,
                touchStatusIDs = "1,2,3,4,5"  //  (includeAllTouchStatus ? string.Empty : " and QTStatusID not in (3) ");
            };

            SMDBusinessLogic smd = new SMDBusinessLogic();
            PR.Entities.EsbEntities.TouchStage.RootObject stageTouchList = smd.GetStagingTouches(touchRequest);

            touchList = GetStagingTouches(stageTouchList);

            ////Read json data to table, comment above 5 lines of code after getting data to JSON file. 
            //var jsonData = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetStagingTouches-{site.LocationCode}.txt"));
            //touchList = (List<Touch>)JsonConvert.DeserializeObject(jsonData, (typeof(List<Touch>)));

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SMDBusinessLogic smd = new SMDBusinessLogic("LocalLogistics");
            //DataSet whDataSet = smd.GetWareHouseTouchesByFacility(corpcode, site.LocationCode, username, password, date, 0, 0);
            //PRAddressAPI prApi = new PRAddressAPI();
            //RouteRepository oRouteRepository = new RouteRepository();
            //List<Entities.Address> cachedAddresses = new List<Entities.Address>();

            //if (whDataSet.Tables != null && whDataSet.Tables.Count > 0 && whDataSet.Tables["QTs"] != null && whDataSet.Tables["QTs"].Rows != null && whDataSet.Tables["QTs"].Rows.Count > 0)
            //{
            //    DataRow[] dRowsStagingTouches = whDataSet.Tables["QTs"].Select("QTTypeID in (7, 8, 9, 10,11)");
            //    using (var starsDbContext = new StarsdbEntities())
            //    {
            //        Touch model = null;
            //        foreach (DataRow dr in dRowsStagingTouches)
            //        {
            //            model = new Touch();
            //            model.CustomerName = dr["Customer Name"].ToString();

            //            model.Status = dr["Status"].ToString(); ;
            //            model.UnitNumber = dr["sUnitName"].ToString();

            //            model.TouchType = dr["Touch"].ToString();
            //            model.Qorid = Convert.ToInt32(dr["iQRID_GlobalNum"]);
            //            model.SequenceNO = Convert.ToInt32(dr["iSequenceNum"]);

            //            model.Instructions = Convert.ToString(dr["sInstructions"]);
            //            model.DoorToFront = Convert.ToBoolean(dr["bDoorToFront"]);
            //            model.DoorToRear = Convert.ToBoolean(dr["bDoorToRear"]);
            //            model.PhoneNumber = Convert.ToString(dr["sPhone"]);
            //            model.Unitsize = Convert.ToInt32(dr["dcLength"]).ToString();
            //            model.SiteLinkMileage = Convert.ToDecimal(dr["dcMileage"]);
            //            model.StarsUnitId = string.IsNullOrEmpty(dr["StarsUnitId"].ToString()) ? 0 : Convert.ToInt32(dr["StarsUnitId"]);

            //            model.ScheduledDate = Convert.ToDateTime(dr["dTouch"]);
            //            model.StartTime = dr["QTTimeStrt_sTimeDesc"].ToString();

            //            var StarsTouchDetails = (from t27 in starsDbContext.T27_tbltouch
            //                                     join t26 in starsDbContext.T26_tblContainer on t27.T27_26_Container_Id equals t26.T26_Container_Id
            //                                     where (t27.T27_Flow_Flag == 11 || t27.T27_Flow_Flag == 51) && t27.T27_RATS_QRId == model.Qorid
            //                                     select new
            //                                     {
            //                                         t27.T27_T25_Quote_Id,
            //                                         t27.T27_Flow_Flag,
            //                                         t27.T27_Touch_Id,
            //                                         t26.T26_Container_Type,
            //                                         t26.T26_Container_Id,
            //                                         t26.T26_BillingQORID
            //                                     }).FirstOrDefault();

            //            if (StarsTouchDetails != null)
            //            {
            //                model.LDMBillingQORId = StarsTouchDetails.T26_BillingQORID.HasValue ? StarsTouchDetails.T26_BillingQORID.Value : 0;
            //                model.LDMContainerType = StarsTouchDetails.T26_Container_Type;
            //                model.LDMQuoteID = StarsTouchDetails.T27_T25_Quote_Id;
            //                var t25Qoute = starsDbContext.T25_tblQuote.Where(t => t.T25_Quote_Id == StarsTouchDetails.T27_T25_Quote_Id).FirstOrDefault();

            //                #region Get LDM Origin and Destination QorID

            //                var containerTouches = (from t27 in starsDbContext.T27_tbltouch
            //                                        where t27.T27_26_Container_Id == StarsTouchDetails.T26_Container_Id
            //                                        select new { t27.T27_RATS_QRId, t27.T27_Flow_Flag }).AsEnumerable();

            //                if (containerTouches != null)
            //                {
            //                    model.LDMOrgQORId = containerTouches.Where(t => t.T27_Flow_Flag == 11).FirstOrDefault().T27_RATS_QRId.Value;
            //                    model.LDMDestQORId = containerTouches.Where(t => t.T27_Flow_Flag == 51).FirstOrDefault().T27_RATS_QRId.Value;
            //                }

            //                #endregion

            //                if (t25Qoute != null)
            //                {
            //                    model.LDMOrigLocationCode = t25Qoute.T25_OrigLocationCode;
            //                    model.LDMDestLocationCode = t25Qoute.T25_DestLocationCode;
            //                }

            //                var TripDetails = (from t72 in starsDbContext.T72_tblTripdetails
            //                                   join t71 in starsDbContext.T71_tblTrips on t72.T72_T71_TripId equals t71.T71_TripId.ToString()
            //                                   where t72.T72_T25_OrderId == StarsTouchDetails.T27_T25_Quote_Id
            //                                   && t72.T72_T26_ContainerId == StarsTouchDetails.T26_Container_Id
            //                                   select new
            //                                   {
            //                                       t71.T71_TripId,
            //                                       t71.T71_Trip,
            //                                       t71.T71_From_Date,
            //                                       t71.T71_From_Time,
            //                                       t71.T71_To_Date,
            //                                       t71.T71_To_Time
            //                                   }).FirstOrDefault();

            //                if (TripDetails != null)
            //                {
            //                    model.LDMTripId = TripDetails.T71_TripId;
            //                    model.LDMTripNumber = TripDetails.T71_Trip;
            //                    model.LDMArrivalDepartureTime = TripDetails.T71_To_Time; //TODO this field needs to update
            //                }

            //                var order = starsDbContext.T25_tblQuote.Find(StarsTouchDetails.T27_T25_Quote_Id);
            //                if (order != null && order.T25_BillingQORID.HasValue == true)
            //                    model.LDMDummyBillingQORId = starsDbContext.T25_tblQuote.Find(StarsTouchDetails.T27_T25_Quote_Id).T25_BillingQORID.Value;
            //            }

            //            touchList.Add(model);
            //        }
            //    }
            //}
            #endregion

            return touchList;
        }

        /// <summary>
        /// Method to get Touch object list from ESB method object list.
        /// </summary>
        /// <param name="touchList"></param>
        /// <returns></returns>
        private List<Touch> GetStagingTouches(Entities.EsbEntities.TouchStage.RootObject touchList)
        {
            List<Touch> finalTouchList = new List<Touch>();

            if (touchList != null)
            {
                foreach (var sfTouch in touchList.SF_StagingTouch)
                {
                    var objTouch = new Touch()
                    {
                        Qorid = string.IsNullOrWhiteSpace(sfTouch.QORId) ? 0 : Convert.ToInt32(sfTouch.QORId),
                        CustomerName = string.IsNullOrWhiteSpace(sfTouch.Customer_Name) ? "" : sfTouch.Customer_Name,
                        Status = string.IsNullOrWhiteSpace(sfTouch.TouchStatus) ? "" : sfTouch.TouchStatus,
                        UnitNumber = string.IsNullOrWhiteSpace(sfTouch.UnitName) ? "" : sfTouch.UnitName,
                        TouchType = string.IsNullOrWhiteSpace(sfTouch.TouchTypeShort) ? "" : sfTouch.TouchTypeShort,
                        SequenceNO = string.IsNullOrWhiteSpace(sfTouch.SequenceNo) ? 0 : Convert.ToInt32(sfTouch.SequenceNo),
                        Instructions = string.IsNullOrWhiteSpace(sfTouch.Instructions) ? "" : sfTouch.Instructions,
                        DoorToFront = string.IsNullOrWhiteSpace(sfTouch.DoorToFront) ? false : Convert.ToBoolean(sfTouch.DoorToFront),
                        DoorToRear = string.IsNullOrWhiteSpace(sfTouch.DoorToRear) ? false : Convert.ToBoolean(sfTouch.DoorToRear),
                        PhoneNumber = string.IsNullOrWhiteSpace(sfTouch.PhoneNo) ? "" : sfTouch.PhoneNo,
                        Unitsize = string.IsNullOrWhiteSpace(sfTouch.UnitSize) ? "" : sfTouch.UnitSize,
                        SiteLinkMileage = string.IsNullOrWhiteSpace(sfTouch.TouchMiles) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.TouchMiles),
                        StarsUnitId = string.IsNullOrWhiteSpace(sfTouch.UnitId) ? 0 : Convert.ToInt32(sfTouch.UnitId),

                        ScheduledDate = string.IsNullOrWhiteSpace(sfTouch.ScheduledDate) ? DateTime.MinValue : Convert.ToDateTime(sfTouch.ScheduledDate),
                        StartTime = string.IsNullOrWhiteSpace(sfTouch.TouchTime) ? "Anytime" : sfTouch.TouchTime,

                        LDMBillingQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Billing_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_Billing_QORID),
                        LDMContainerType = string.IsNullOrWhiteSpace(sfTouch.UnitTypeId) ? Convert.ToByte(0) : Convert.ToByte(sfTouch.UnitTypeId),
                        LDMQuoteID = string.IsNullOrWhiteSpace(sfTouch.LDM_QuoteID) ? 0 : Convert.ToDecimal(sfTouch.LDM_QuoteID),
                        LDMOrgQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Origin_QORID) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.LDM_Origin_QORID),
                        LDMDestQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Destination_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_Destination_QORID),
                        LDMOrigLocationCode = string.IsNullOrWhiteSpace(sfTouch.LDM_Origin_LocationCode) ? "" : Convert.ToString(sfTouch.LDM_Origin_LocationCode),
                        LDMDestLocationCode = string.IsNullOrWhiteSpace(sfTouch.LDM_Destination_LocationCode) ? "" : Convert.ToString(sfTouch.LDM_Destination_LocationCode),
                        LDMTripId = string.IsNullOrWhiteSpace(sfTouch.LDM_TripID) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.LDM_TripID),
                        LDMTripNumber = string.IsNullOrWhiteSpace(sfTouch.LDM_TripNumber) ? "" : sfTouch.LDM_TripNumber,
                        LDMArrivalDepartureTime = string.IsNullOrWhiteSpace(sfTouch.LDM_ArrivalDepartureTime) ? "" : sfTouch.LDM_ArrivalDepartureTime,
                        LDMDummyBillingQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_DummyBilling_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_DummyBilling_QORID)

                    };

                    finalTouchList.Add(objTouch);
                }
            }
            else
            {
                throw new Exception("EsbMethod.GetStagingTouches - No touch data available.");
            }

            return finalTouchList;
        }

        // TG-717 , ticket created for ESB endpoint 
        public List<Touch> GetUnassignedTouches(Entities.SiteInfo site, DateTime date)
        {
            List<Touch> touchList = new List<Touch>();
            RouteRepository oRouteRepository = new RouteRepository();

            #region SFERP-TODO-ESBMT 

            var touchRequest = new StagingUnassignTouchRequest
            {
                locationCode = site.LocationCode,
                touchDate = date.ToString("MM/dd/yyyy"),
                touchTypeIDs = TransportationTouchTypeIds,
                touchStatusIDs = StatusIDs_OpenCompleted  //  (includeAllTouchStatus ? string.Empty : " and QTStatusID not in (3) ");
            };

            SMDBusinessLogic smd = new SMDBusinessLogic();
            PR.Entities.EsbEntities.TouchUnAssign.RootObject unAssignedTouchList = smd.GetUnassignedTouches(touchRequest);

            if (unAssignedTouchList != null)
            {
                touchList = GetUnassignedTouches(unAssignedTouchList);

                foreach (var sfTouch in touchList)
                {
                    //Get Latlong for Origin Address.
                    ComputeLatLongValue(sfTouch.OriginAddress, site, oRouteRepository);

                    //Get Latlong for Destination Address.
                    ComputeLatLongValue(sfTouch.DestAddress, site, oRouteRepository);

                    //Get ETA Settings from Database.
                    sfTouch.ProvidedETASettings = new LocalComponent().GetETASetting(sfTouch.Qorid, sfTouch.TouchTypeAcronym, sfTouch.SequenceNO);
                }
            }

            ////Read json data to table, comment above 5 lines of code after getting data to JSON file. 
            //var jsonData = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetUnassignedTouches-{site.LocationCode}.txt"));
            //touchList = (List<Touch>)JsonConvert.DeserializeObject(jsonData, (typeof(List<Touch>)));

            #endregion

            #region SFERP-TODO-CMTSL - Use this section(selected code) only for updating the used model from ESB JSON data/list.

            //List<Touch> touchList = new List<Touch>();
            //SMDBusinessLogic smd = new SMDBusinessLogic("LocalLogistics");
            //DataSet whDataSet = smd.GetWareHouseTouchesByFacility(corpcode, site.LocationCode, username, password, date, 0, 0);
            //PRAddressAPI prApi = new PRAddressAPI();
            //RouteRepository oRouteRepository = new RouteRepository();
            //LocalComponent local = new LocalComponent();


            //if (whDataSet.Tables[1] != null && whDataSet.Tables[1].Rows.Count > 0)
            //{
            //    Touch model = null;
            //    foreach (DataRow dr in whDataSet.Tables[1].Rows)
            //    {
            //        model = new Touch();
            //        model.CustomerName = $"{dr["sFName"].ToString()} {dr["sLName"].ToString()}";

            //        model.Status = dr["Status"].ToString(); ;
            //        model.UnitNumber = dr["sUnitName"].ToString();

            //        model.TouchType = dr["Touch"].ToString();
            //        model.Qorid = Convert.ToInt32(dr["iQRID_GlobalNum"]);
            //        model.SequenceNO = Convert.ToInt32(dr["iSequenceNum"]);

            //        model.Instructions = Convert.ToString(dr["sInstructions"]);
            //        model.DoorToFront = Convert.ToBoolean(dr["bDoorToFront"]);
            //        model.DoorToRear = Convert.ToBoolean(dr["bDoorToRear"]);
            //        model.PhoneNumber = Convert.ToString(dr["sPhone"]);
            //        model.Unitsize = Convert.ToInt32(dr["dcLength"]).ToString();
            //        model.SiteLinkMileage = Convert.ToDecimal(dr["dcMileage"]);
            //        model.StarsUnitId = string.IsNullOrEmpty(dr["StarsUnitId"].ToString()) ? 0 : Convert.ToInt32(dr["StarsUnitId"]);

            //        model.ScheduledDate = Convert.ToDateTime(dr["dTouch"]);

            //        model.StartTime = dr["QTTimeStrt_sTimeDesc"].ToString();

            //        //Load the origin address
            //        model.OriginAddress = new Entities.Address();
            //        model.OriginAddress.AddressLine1 = Convert.ToString(dr["sAddr1_From"]);
            //        model.OriginAddress.AddressLine2 = Convert.ToString(dr["sAddr2_From"]);
            //        model.OriginAddress.City = Convert.ToString(dr["sCity_From"]);
            //        model.OriginAddress.State = Convert.ToString(dr["sRegion_From"]);
            //        model.OriginAddress.Zip = Convert.ToString(dr["sPostalCode_From"]);
            //        model.OriginAddress.Company = Convert.ToString(dr["sCompany_From"]);

            //        ///In this method checks against to PRLocal database and it the same address available then system will get LatLong values 
            //        ///other wise get LatLong values from Google API
            //        ComputeLatLongValue(model.OriginAddress, site, oRouteRepository);


            //        //Load the destination address
            //        model.DestAddress = new Entities.Address();
            //        model.DestAddress.AddressLine1 = Convert.ToString(dr["sAddr1_To"]);
            //        model.DestAddress.AddressLine2 = Convert.ToString(dr["sAddr2_To"]);
            //        model.DestAddress.City = Convert.ToString(dr["sCity_To"]);
            //        model.DestAddress.State = Convert.ToString(dr["sRegion_To"]);
            //        model.DestAddress.Zip = Convert.ToString(dr["sPostalCode_To"]);
            //        model.DestAddress.Company = Convert.ToString(dr["sCompany_To"]);

            //        ///In this method checks against to PRLocal database and it the same address available then system will get LatLong values 
            //        ///other wise get LatLong values from Google API
            //        ComputeLatLongValue(model.DestAddress, site, oRouteRepository);

            //        model.WeightStnReq = Convert.ToBoolean(dr["IsWeightTicket"]);
            //        model.IsZippyShellQuote = Convert.ToBoolean(dr["IsZippyShellUnit"]);

            //        model.GlobalSiteNum = site.GlobalSiteNum;

            //        model.ProvidedETASettings = local.GetETASetting(model.Qorid, model.TouchTypeAcronym, model.SequenceNO);

            //        touchList.Add(model);
            //    }
            //}
            #endregion

            return touchList;
        }

        private List<Touch> GetUnassignedTouches(PR.Entities.EsbEntities.TouchUnAssign.RootObject unAssignedTouchList)
        {
            List<Touch> finalTouchList = new List<Touch>();

            if (unAssignedTouchList != null)
            {
                foreach (var sfTouch in unAssignedTouchList.SF_UnassignedTouch)
                {
                    var objTouch = new Touch()
                    {
                        Qorid = string.IsNullOrWhiteSpace(sfTouch.QORId) ? 0 : Convert.ToInt32(sfTouch.QORId),
                        CustomerName = string.IsNullOrWhiteSpace(sfTouch.Customer_Name) ? "" : sfTouch.Customer_Name,
                        Status = string.IsNullOrWhiteSpace(sfTouch.TouchStatus) ? "" : sfTouch.TouchStatus,
                        UnitNumber = string.IsNullOrWhiteSpace(sfTouch.UnitName) ? "" : sfTouch.UnitName,
                        TouchType = string.IsNullOrWhiteSpace(sfTouch.TouchTypeFull) ? "" : sfTouch.TouchTypeFull,
                        SequenceNO = string.IsNullOrWhiteSpace(sfTouch.SequenceNo) ? 0 : Convert.ToInt32(sfTouch.SequenceNo),
                        Instructions = string.IsNullOrWhiteSpace(sfTouch.Instructions) ? "" : sfTouch.Instructions,
                        DoorToFront = string.IsNullOrWhiteSpace(sfTouch.DoorToFront) ? false : Convert.ToBoolean(sfTouch.DoorToFront),
                        DoorToRear = string.IsNullOrWhiteSpace(sfTouch.DoorToRear) ? false : Convert.ToBoolean(sfTouch.DoorToRear),
                        PhoneNumber = string.IsNullOrWhiteSpace(sfTouch.PhoneNo) ? "" : sfTouch.PhoneNo,
                        Unitsize = string.IsNullOrWhiteSpace(sfTouch.UnitSize) ? "" : sfTouch.UnitSize,
                        SiteLinkMileage = string.IsNullOrWhiteSpace(sfTouch.TouchMiles) ? 0 : Convert.ToDecimal(sfTouch.TouchMiles),
                        StarsUnitId = string.IsNullOrWhiteSpace(sfTouch.UnitId) ? 0 : Convert.ToInt32(sfTouch.UnitId),

                        ScheduledDate = sfTouch.ScheduledDate == DateTime.MinValue ? DateTime.MinValue : Convert.ToDateTime(sfTouch.ScheduledDate),
                        StartTime = string.IsNullOrWhiteSpace(sfTouch.TouchTime) ? "Anytime" : sfTouch.TouchTime,

                        LDMBillingQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Billing_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_Billing_QORID),
                        LDMContainerType = string.IsNullOrWhiteSpace(sfTouch.UnitTypeId) ? Convert.ToByte(0) : Convert.ToByte(sfTouch.UnitTypeId),
                        LDMQuoteID = string.IsNullOrWhiteSpace(sfTouch.LDM_QuoteID) ? 0 : Convert.ToDecimal(sfTouch.LDM_QuoteID),
                        LDMOrgQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Origin_QORID) ? Convert.ToDecimal(0) : Convert.ToDecimal(sfTouch.LDM_Origin_QORID),
                        LDMDestQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_Destination_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_Destination_QORID),
                        LDMOrigLocationCode = string.IsNullOrWhiteSpace(sfTouch.LDM_Origin_LocationCode) ? "" : Convert.ToString(sfTouch.LDM_Origin_LocationCode),
                        LDMDestLocationCode = string.IsNullOrWhiteSpace(sfTouch.LDM_Destination_LocationCode) ? "" : Convert.ToString(sfTouch.LDM_Destination_LocationCode),
                        LDMArrivalDepartureTime = string.IsNullOrWhiteSpace(sfTouch.LDM_ArrivalDepartureTime) ? "" : sfTouch.LDM_ArrivalDepartureTime,
                        LDMDummyBillingQORId = string.IsNullOrWhiteSpace(sfTouch.LDM_DummyBilling_QORID) ? 0 : Convert.ToDecimal(sfTouch.LDM_DummyBilling_QORID),

                        WeightStnReq = string.IsNullOrWhiteSpace(sfTouch.IsWeightTicket) ? false : Convert.ToBoolean(sfTouch.IsWeightTicket),
                        IsZippyShellQuote = string.IsNullOrWhiteSpace(sfTouch.IsZippyShellQuote) ? false : Convert.ToBoolean(sfTouch.IsZippyShellQuote),

                        GlobalSiteNum = string.IsNullOrWhiteSpace(sfTouch.StoreNo) ? "" : sfTouch.StoreNo,

                        OriginAddress = new Entities.Address
                        {
                            AddressLine1 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address1) ? "" : sfTouch.Origin_Address1,
                            //AddressLine2 = string.IsNullOrWhiteSpace(sfTouch.Origin_Address2) ? "" : sfTouch.Origin_Address2, // Not present in SF
                            State = string.IsNullOrWhiteSpace(sfTouch.Origin_State) ? "" : sfTouch.Origin_State,
                            City = string.IsNullOrWhiteSpace(sfTouch.Origin_City) ? "" : sfTouch.Origin_City,
                            Zip = string.IsNullOrWhiteSpace(sfTouch.Origin_Zip) ? "" : sfTouch.Origin_Zip,
                            Company = string.IsNullOrWhiteSpace(sfTouch.Origin_Company) ? "" : sfTouch.Origin_Company
                        },

                        DestAddress = new Entities.Address
                        {
                            AddressLine1 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address1) ? "" : sfTouch.Destination_Address1,
                            //AddressLine2 = string.IsNullOrWhiteSpace(sfTouch.Destination_Address2) ? "" : sfTouch.Destination_Address2, // Not present in SF
                            State = string.IsNullOrWhiteSpace(sfTouch.Destination_State) ? "" : sfTouch.Destination_State,
                            City = string.IsNullOrWhiteSpace(sfTouch.Destination_City) ? "" : sfTouch.Destination_City,
                            Zip = string.IsNullOrWhiteSpace(sfTouch.Destination_Zip) ? "" : sfTouch.Destination_Zip,
                            Company = string.IsNullOrWhiteSpace(sfTouch.Destination_Company) ? "" : sfTouch.Destination_Company
                        }
                    };

                    finalTouchList.Add(objTouch);
                }
            }
            //else
            //{ 
            //    throw new Exception("EsbMethod.GetUnassignedTouches - No touch data available.");
            //}

            return finalTouchList;
        }

        public List<Touch> GetTransportationTouches(Entities.SiteInfo marketSite, DateTime date, bool isGetLiveSLData)
        {
            List<SiteInfo> lstSiteInfo = new List<SiteInfo>();
            lstSiteInfo.Add(marketSite);
            if (marketSite.IsMarket)
                lstSiteInfo.AddRange(marketSite.MarketFacilities);

            return GetTransportationTouches(lstSiteInfo, date, date, isGetLiveSLData);
        }

        public List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime date, bool isGetLiveSLData)
        {
            return GetTransportationTouches(lstSites, date, date, isGetLiveSLData);
        }

        public List<Touch> GetTransportationTouches(List<SiteInfo> lstSites, DateTime startdate, DateTime enddate, bool isGetLiveSLData)
        {
            CriteriaSitelinkTouches criteriaSitelinkTouches = new CriteriaSitelinkTouches
            {
                FLocationCodes = string.Join(",", lstSites.Select(s => s.LocationCode).ToList()),
                IsGetLiveData = isGetLiveSLData,
                ScheduleFromDate = startdate,
                ScheduleToDate = enddate,
                QTTypeIDs = TransportationTouchTypeIds,
                QTStatusIDs = StatusIDs_OpenCompleted,
                QTRentalstatusIDs = RentalStatusIDs_ActiveCompleted,
                QTRentalTypeIDs = RentalTypeIDs_OrderRental
            };

            return GetTransportationTouches(criteriaSitelinkTouches, lstSites);

        }

        public List<Touch> GetTransportationTouches(CriteriaSitelinkTouches criteriaSitelinkTouches, List<SiteInfo> lstSiteInfo)
        {
            List<Touch> touchList = new List<Touch>();

            SMDBusinessLogic smd = new SMDBusinessLogic("LocalDispatch");
            RouteRepository oRouteRepository = new RouteRepository();

            var slTouches = smd.GetTransportationTouches(criteriaSitelinkTouches);

            Touch touch = null;
            SiteInfo tmpSite = null;

            foreach (var slTouch in slTouches)
            {
                touch = new Touch(slTouch);
                touch.WeightStnReq = slTouch.IsWeightTicket;
                tmpSite = lstSiteInfo.Where(m => m.LocationCode.ToUpper() == touch.FacCode.ToUpper()).FirstOrDefault();

                ComputeLatLongValue(touch.OriginAddress, tmpSite, oRouteRepository);
                ComputeLatLongValue(touch.DestAddress, tmpSite, oRouteRepository);
                touch.GlobalSiteNum = tmpSite.GlobalSiteNum;

                touchList.Add(touch);
            }

            //slTouches.ForEach(sl =>
            //{
            //    touch = new Touch(sl);

            //    touch.WeightStnReq = sl.IsWeightTicket;
            //    tmpSite = lstSiteInfo.Where(m => m.LocationCode == touch.FacCode).FirstOrDefault();

            //    ComputeLatLongValue(touch.OriginAddress, tmpSite, oRouteRepository);
            //    ComputeLatLongValue(touch.DestAddress, tmpSite, oRouteRepository);
            //    touch.GlobalSiteNum = tmpSite.GlobalSiteNum;

            //    touchList.Add(touch);
            //});

            return touchList;
        }

        public void RemovedCachedAddress(Entities.Address address)
        {
            CachedAddresses.Remove(CachedAddresses.Where(x => x.ID == address.ID).FirstOrDefault());
        }

        ///In this method checks against to PRLocal database and it the same address available then system will get LatLong values 
        ///other wise get LatLong values from Google API
        private Entities.Address ComputeLatLongValue(Entities.Address address, SiteInfo site, RouteRepository oRouteRepository)
        {
            Entities.Address thisAddress = CachedAddresses.Where(x => x.AddressLine1 == address.AddressLine1 &&
                                                                        x.AddressLine2 == address.AddressLine2 &&
                                                                        x.City == address.City &&
                                                                        x.State == address.State &&
                                                                        x.Zip == address.Zip &&
                                                                        x.Country == address.Country &&
                                                                        string.IsNullOrEmpty(x.Latitude) == false &&
                                                                        string.IsNullOrEmpty(x.Longitude) == false).FirstOrDefault();

            //If we find this address in cache list, then we can use that lat long values 
            //O.w. need to look in DB or Google api
            if (thisAddress != null)
            {
                address.Latitude = thisAddress.Latitude;
                address.Longitude = thisAddress.Longitude;
                address.FacilityStoreNo = thisAddress.FacilityStoreNo;
                address.AddressType = thisAddress.AddressType;
                address.ID = thisAddress.ID;
            }
            else
            {
                address.AddressType = AddressComparison(address, site.SiteAddress) ? PREnums.AddressType.Warehouse : PREnums.AddressType.Customer;
                //address.FacilityStoreNo = address.AddressType == PREnums.AddressType.Warehouse ? Convert.ToInt32(site.GlobalSiteNum) : (int?)null;
                address.FacilityStoreNo = Convert.ToInt32(site.GlobalSiteNum);

                //This method checks for address entry in local DB if it finds then returns that
                //O.W. need to get lat long from google api and then insert into DB
                address = oRouteRepository.ManageAddress(address);

                //Need to chache this address so that we need to go and search in DB or Google API for lat long values
                CachedAddresses.Add(address);
            }

            try
            {
                address.ExceedMaxDistanceFromFacility = (new PRAddressAPI()).GetStraightLineDistance(site.SiteAddress, address) > site.MaxDistanceLocalTouchesCanServe;
            }
            catch (Exception ex)
            {
                //Some times GetStraightLineDistance throughs an exception when there is no lat long values for a specific zipcode. This is an issue, we need to fix it.
                address.ExceedMaxDistanceFromFacility = false;
            }
            return address;
        }

        private bool AddressComparison(Entities.Address baseAddress, Entities.Address compareAddress)
        {
            return baseAddress.AddressLine1 == compareAddress.AddressLine1 &&
                baseAddress.AddressLine2 == compareAddress.AddressLine2 &&
                baseAddress.City == compareAddress.City &&
                baseAddress.Country == compareAddress.Country &&
                baseAddress.Zip == compareAddress.Zip &&
                baseAddress.State == compareAddress.State;
        }

        public SiteInfo GetFaciltyInformation(string locationCode)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.GetSiteInformation(locationCode);
        }

        public List<SiteInfo> GetFaciltyInformation()
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.GetSiteInformation();
        }

        public SiteInfo GetFaciltyInformationWithMarkets(string storeNum)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.GetSiteInformationWithMarkets(storeNum);
        }

        public List<SiteInfo> GetFaciltyInformationWithMarkets()
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.GetSiteInformationWithMarkets();
        }

        public void CompletTOAndMoveOutTOTouch(int originQorid, string activityBy, string unitName)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            smd.CompletTOAndMoveOutTOTouch(originQorid, activityBy, unitName);
        }

        public void CompleteWarehouseTouch(int billingQorid, string activityBy)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            smd.CompleteWareHouseTouch(billingQorid, activityBy, string.Empty);
        }

        /// <summary>
        /// This method complete IBO, OBO and WH touches from Facility App Staging touches screen
        /// TouchType should be Salesforce/Sitelink name dont pass short form of touch type
        /// </summary>
        /// <param name="Qorid"></param>
        /// <param name="touchType"></param>
        public void CompleteIBOWHOBHExtraTouches(int Qorid, string touchType, int sequenceNum, string activityBy, string unitNo)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            smd.CompleteIBOWHOBHExtraTouches(Qorid, touchType, sequenceNum, activityBy, unitNo);
        }

        public bool CheckIsUnitValidForThisQor(string unitName, int originQorId, string activityBy, out string suggestedUnitName)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            string oUnitInfo = smd.GetAddedUnit(originQorId);

            suggestedUnitName = oUnitInfo.ToUpper();

            return oUnitInfo != null && oUnitInfo.ToUpper() == unitName.ToUpper();
        }

        public void CompletTIAndMoveInTITouch(int destQorid, string unitName, string activityBy, int unitId, int starsId)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            smd.CompletTIAndMoveInTITouch(destQorid, unitName, activityBy, unitId, starsId);
        }

        public void CompleteCCTouchForBillingQor(int billingQor, string activityBy)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            smd.CompleteCCTouchForBillingQor(qorId: billingQor, activityBy: activityBy);
        }

        public bool IsLDMOrderSettled(int qorId) //dummyBillingQorId
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.IsLDMOrderSettled(qorId: qorId);
        }

        //SFERP-TODO-CTUPD - TBD
        public ScheduleCalendar GetCapacity(DateTime StartDate, DateTime EndDate, string LocationCode)
        {
            int noDays = Convert.ToInt32((EndDate - StartDate).TotalDays);

            PRCapacity prc = new PRCapacity();
            return prc.GetCapacity(LocationCode, StartDate, noDays, 0, null, null);
        }

        //SFERP-TODO-CTUPD - Added for TuckOperationalHourssame as ScheduleCalendar Popup.
        public ScheduleCalendar GetFacilityCapacity(DateTime startDate, DateTime endDate, string LocationCode)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic();
            return smd.Get_TG733_GetFacilityCapacityData(startDate, endDate, LocationCode);
        }

        public bool UpdateTouchAssignment(DateTime date, Touch touch, int driverID, string activityBy)
        {
            SMDBusinessLogic smd = new SMDBusinessLogic(activityBy);
            return smd.UpdateTouchAssignment(date, touch, driverID, activityBy);
        }
    }
}
