﻿using System;
using System.Collections.Generic;
using System.Linq;
using PR.LocalLogisticsSolution.Infrastructure.Model;
using PR.LocalLogisticsSolution.Interfaces;
using System.Transactions;
using PR.ExternalInterfaces;
using PR.Entities;
using PR.BusinessLogic;
using System.Data;
using System.Text.RegularExpressions;

namespace PR.LocalLogisticsSolution.Model
{
    public class RouteRepository : IRouteRepository
    {
        //private readonly RouteOptimzationEntities context;
        public RouteRepository()
        {
            //if (context == null)
            //    context = new RouteOptimzationEntities();
        }

        public int SaveTouch(Touch touch)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var context = new RouteOptimzationEntities())
                {
                    context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    //Need to copy touch object into touchinformation object and save it
                    TouchInformation touchInfo = new TouchInformation();
                    touchInfo.OrderNumber = touch.OrderNumber;
                    touchInfo.TouchType = touch.TouchTypeAcronym;
                    touchInfo.QORId = touch.Qorid;
                    touchInfo.SLSeqNo = touch.SequenceNO;
                    touchInfo.FK_TrailerId = touch.TrailerId;
                    touchInfo.WeightStnReq = touch.WeightStnReq;
                    touchInfo.Status = touch.Status;
                    touchInfo.FK_LoadId = touch.LoadId;
                    touchInfo.StartTime = touch.StartTime;

                    touchInfo.Instructions = touch.Instructions;
                    touchInfo.DoorToFront = touch.DoorToFront;
                    touchInfo.DoorToRear = touch.DoorToRear;
                    touchInfo.ScheduledDate = touch.ScheduledDate;
                    touchInfo.PhoneNumber = touch.PhoneNumber;
                    touchInfo.FK_OriginAddressId = touch.OriginAddress.ID;
                    touchInfo.FK_DestinationAddressId = touch.DestAddress.ID;

                    touchInfo.ContainerNo = touch.UnitNumber;
                    touchInfo.StarsUnitId = touch.StarsUnitId;
                    touchInfo.IsGrouped = false;
                    touchInfo.UnitSize = Convert.ToInt32(touch.Unitsize);
                    touchInfo.SiteLinkMileage = touch.SiteLinkMileage;
                    touchInfo.StoreNumber = touch.GlobalSiteNum;
                    touchInfo.IsExceptionalTouch = touch.IsExceptionalTouch;

                    touchInfo.IsZippyShellMove = touch.IsZippyShellQuote; // UnCommented by Sohan, added to TI table

                    var lastTouch = context.TouchInformation.Where(x => x.FK_LoadId == touch.LoadId).OrderByDescending(t => t.RouteSequence).FirstOrDefault();

                    if (lastTouch != null)
                    {
                        touchInfo.RouteSequence = Convert.ToInt32(lastTouch.RouteSequence) + 1;
                    }


                    List<StopInformation> stops = new List<StopInformation>();

                    foreach (var stop in touch.Stops)
                    {
                        StopInformation stopInfo = new StopInformation();
                        stopInfo.TouchInformation = touchInfo;
                        stopInfo.CustomerName = stop.CustomerName;
                        //TODO These dates needs to cross check 
                        stopInfo.ScheduledStartTime = stop.ScheduledStartTime;
                        stopInfo.ScheduledEndTime = stop.ScheduledEndTime;
                        stopInfo.EstimatedEndTime = stop.EstimatedEndDateTime;
                        stopInfo.EstimatedStartTime = stop.EstimatedStartDateTime;
                        stopInfo.FK_OriginAddressId = stop.OriginAddress.ID;
                        stopInfo.FK_DestinationAddressId = stop.DestinationAddress.ID;
                        stopInfo.FK_StopTypeId = stop.StopTypeId;
                        stopInfo.FK_TouchStatus = stop.TouchStatus;
                        stopInfo.IsRequired = stop.IsRequired;

                        //context.StopInformation.AddObject(stopInfo);
                        touchInfo.StopInformation.Add(stopInfo);

                        if (stop.BusyTime != null)
                        {
                            BusyTimeInformation btInfo = new BusyTimeInformation();
                            btInfo.StopInformation = stopInfo;
                            btInfo.BreakTime = stop.BusyTime.BreakTime;
                            btInfo.Comment = stop.BusyTime.Comment;

                            stopInfo.BusyTimeInformation.Add(btInfo);
                        }
                    }
                    context.TouchInformation.Add(touchInfo);
                    context.SaveChanges();
                    scope.Complete();
                    scope.Dispose();

                    return touchInfo.PK_TouchId;
                }
            }
        }

        public bool RemoveTouch(int touchId)
        {
            bool rtFalg = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    var touch = lcontext.TouchInformation.Where(t => t.PK_TouchId == touchId).FirstOrDefault();
                    int loadId = touch.FK_LoadId.Value;

                    touch.StopInformation.ToList().ForEach(s =>
                    {
                        lcontext.SkipTouch.Where(sk => sk.FK_Touch_StopId == s.Pk_Touch_StopId).ToList().ForEach(skip =>
                        {
                            lcontext.SkipTouch.Remove(skip);
                        });

                        //Before removing stop please remove skip touch if it has any stops
                        //var skiptouch = lcontext.SkipTouch.Where(sk => sk.FK_Touch_StopId == s.Pk_Touch_StopId).First();
                        //if (skiptouch != null && skiptouch.Pk_SkipTouchId > 0)
                        //{
                        //    lcontext.SkipTouch.Remove(skiptouch);
                        //}

                        lcontext.StopInformation.Remove(s);
                    });

                    //Remove the sync history to avoid foreign key voilation
                    lcontext.TouchInformationSync.Where(ts => ts.FK_TouchId == touchId).ToList().ForEach(sync =>
                    {
                        lcontext.TouchInformationSync.Remove(sync);
                    });

                    lcontext.TouchInformation.Remove(touch);

                    lcontext.SaveChanges();

                    LoadInformation loadInfo = lcontext.LoadInformation.Find(loadId);
                    if (loadInfo != null && loadInfo.PK_LoadID > 0)
                    {
                        if (loadInfo.TouchInformation.Count == 0)
                        {
                            lcontext.LoadInformation.Remove(loadInfo);
                            lcontext.SaveChanges();
                        }
                    }
                }
                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        public bool RemoveBusyTouchAndStops(int stopId, string activityBy)
        {
            bool rtFalg = false;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                {
                    TouchInformation touch = null;
                    using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
                    {
                        var stop = lcontext.StopInformation.Find(stopId);
                        if (stop != null && stop.Pk_Touch_StopId > 0)
                        {
                            touch = stop.TouchInformation;
                            stop.TouchInformation.LoadInformation.IsOptimized = false;
                            stop.BusyTimeInformation.ToList().ForEach(b => lcontext.BusyTimeInformation.Remove(b));
                            lcontext.StopInformation.Remove(stop);
                            lcontext.TouchInformation.Remove(touch);

                            lcontext.SaveChanges();
                        }
                    }

                    scope.Complete();
                    scope.Dispose();

                    rtFalg = true;

                    //Update Salesforce touch for Touch Assignment. TG-729
                    AddRemoveTouchtoDriverESB(touch, 0, activityBy);
                }

                return rtFalg;
            }
        }

        /// <summary>
        /// Method to assign or remove touch from Driver, Update detilas into Salesforce using ESB endpoint: TG-729
        /// </summary>
        /// <param name="date"></param>
        /// <param name="touch"></param>
        /// <param name="driverID"></param>
        /// <param name="activityBy"></param>
        public void AddRemoveTouchtoDriverESB(TouchInformation touchInfo, int driverID, string activityBy)
        {
            //Added by Sohan 
            Touch touch = new Touch
            {
                Qorid = (int)touchInfo.QORId,
                TouchType = touchInfo.TouchType,
                SequenceNO = (int)touchInfo.SLSeqNo
            };

            new SiteLinkrepository().UpdateTouchAssignment(DateTime.Now, touch, driverID, activityBy);
        }

        public void MoveTouchToAnotherLoad(int touchID, int loadID, bool IsExceptionalTouch = false) //If re-assigining touch to other load is exceptional touch from driver applciation then we will pass this param value as true other wise it is false
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                                                                                                new TransactionOptions
                                                                                                {
                                                                                                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                                                                                                }))
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    var touch = lcontext.TouchInformation.Find(touchID);

                    int? oldLoadId = touch.FK_LoadId;

                    touch.FK_LoadId = loadID;
                    touch.Status = "Open";
                    touch.IsExceptionalTouch = IsExceptionalTouch;

                    var lastTouch = lcontext.TouchInformation.Where(x => x.FK_LoadId == loadID).OrderByDescending(t => t.RouteSequence).FirstOrDefault();
                    int touchSeq = lastTouch != null && lastTouch.RouteSequence.HasValue ? lastTouch.RouteSequence.Value : 0;
                    if (lastTouch != null)
                    {
                        touch.RouteSequence = ++touchSeq;
                    }

                    if (touch.StopInformation.Count > 0)
                    {
                        //var lastStop = lastTouch.StopInformation.OrderByDescending(t => t.Sequence).FirstOrDefault();
                        //int stopSeq = lastStop != null && lastStop.Sequence.HasValue ? lastStop.Sequence.Value : (touchSeq * 3);///here we just assing max sequence number to maintain stops order. next step we will have actual stop sequence.

                        touch.StopInformation.ToList().ForEach(x =>
                        {
                            x.FK_TouchStatus = "Open";
                            x.SkipTouch = false;
                            //x.Sequence = ++stopSeq;
                            x.Sequence = null;

                            lcontext.SkipTouch.Where(sk => sk.FK_Touch_StopId == x.Pk_Touch_StopId).ToList().ForEach(skip =>
                            {
                                lcontext.SkipTouch.Remove(skip);
                            });
                        });
                    }

                    lcontext.SaveChanges();
                    //var touch = new TouchInformation() { PK_TouchId = touchID, FK_LoadId = loadID };

                    //lcontext.TouchInformation.Attach(touch);
                    //TODO need to look for this line to update this values
                    //lcontext.Entry(touch).Property(x => x.FK_LoadId).IsModified = true;

                    //var optStops = context.Optimized_StopInfo.Where(x => x.FK_TouchId == touchID).ToList();
                    //if (optStops.Count > 0)
                    //    optStops.ForEach(x => context.Optimized_StopInfo.Remove(x));

                    //lcontext.SaveChanges();

                    if (oldLoadId.HasValue && oldLoadId > 0)
                    {
                        LoadInformation loadInfo = lcontext.LoadInformation.Find(oldLoadId);
                        if (loadInfo != null && loadInfo.PK_LoadID > 0)
                        {
                            if (loadInfo.TouchInformation.Count == 0)
                            {
                                lcontext.LoadInformation.Remove(loadInfo);
                                lcontext.SaveChanges();
                            }
                        }
                    }
                }
                scope.Complete();
                scope.Dispose();
            }
        }

        //public void RemoveTouchFromLoad(Touch touch)
        //{
        //    using (var context = new RouteOptimzationEntities())
        //    {
        //        //Need to copy touch object into touchinformation object and save it
        //        TouchInformation touchInfo = new TouchInformation();
        //        context.TouchInformation.Remove(touchInfo);
        //        context.SaveChanges();
        //    }
        //}

        public List<Touch> FetchTouchesFromSP(DateTime date, string storeNumber)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                List<Touch> touches = new List<Touch>();

                List<USP_FA_LL_GetTouches_Result> allDBStops = lcontext.USP_FA_LL_GetTouches(storeNumber, date.ToShortDateString()).ToList();

                foreach (var dbTouch in allDBStops)
                {
                    if (!touches.Any(t => t.TouchId == dbTouch.TouchId))
                    {
                        Touch touchObj = ParseTouchFromSP(allDBStops, dbTouch, lcontext);

                        touches.Add(touchObj);
                    }
                }

                return touches;
            }
        }

        public List<Touch> FetchTouches(DateTime date, string storeNumber)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                List<TouchInformation> touchesInfo = lcontext.TouchInformation.Include("LoadInformation").Where(x => (x.LoadInformation.CreatedDate != null && (x.LoadInformation.CreatedDate.Value.Year == date.Year &&
                                                                                                                                                            x.LoadInformation.CreatedDate.Value.Month == date.Month &&
                                                                                                                                                            x.LoadInformation.CreatedDate.Value.Day == date.Day)) && x.LoadInformation.StoreNumber == storeNumber).ToList();
                List<Touch> touches = new List<Touch>();
                foreach (var dbTouch in touchesInfo)
                {
                    Touch touch = MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId));

                    if (dbTouch.StopInformation != null)
                    {
                        foreach (var dbStop in dbTouch.StopInformation)
                        {
                            var originAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_OriginAddressId).Single();
                            var stopDestAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_DestinationAddressId).Single();
                            var stop = MapStops(dbStop, originAddress, stopDestAddress, touch);

                            touch.AddStop(stop);
                        }
                    }

                    //Just to get updated address for stops
                    GetSLUpdatedStopAddress(touch);

                    touches.Add(touch);
                }

                return touches;
            }
        }

        public List<Stops> GetAllStopsByDriver(int driverNumber, DateTime touchDate)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                List<Stops> stops = new List<Stops>();
                var loadId = lcontext.LoadInformation.Where(x => x.DriverID == driverNumber && x.CreatedDate == touchDate).Select(x => x.PK_LoadID).FirstOrDefault();
                if (loadId != 0)
                {
                    var touchList = lcontext.TouchInformation.Where(x => x.FK_LoadId == loadId).ToList();
                    touchList.ForEach(x => FetchStopsByTouchId(x.PK_TouchId, lcontext, ref stops));

                }
                return stops;
            }
        }

        public Touch FetchTouchByTouchId(int touchId)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var dbtouch = lcontext.TouchInformation.Where(t => t.PK_TouchId == touchId).FirstOrDefault();

                Touch rtTouch = MapTouchInfoToTouch(dbtouch, lcontext.Address.Find(dbtouch.FK_OriginAddressId), lcontext.Address.Find(dbtouch.FK_DestinationAddressId));

                foreach (var item in lcontext.StopInformation.Where(s => s.FK_TouchId == dbtouch.PK_TouchId))
                {
                    //var stoptype = lcontext.StopType.Where(s => s.StopTypeId == item.FK_StopTypeId).FirstOrDefault();
                    var originAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();//(from c in lcontext.Address join atype in lcontext.AddressType on c.FK_AddressTypeId equals atype.PK_AddressTypeId where c.PK_AddressId== item.FK_OriginAddressId.Value select c); //
                    var destAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();
                    Stops stop = MapStops(item, originAddress, destAddress, rtTouch);
                    stop.SkipTouchReason = lcontext.SkipTouch.Where(x => x.FK_Touch_StopId == item.Pk_Touch_StopId).Select(x => x.Reason).FirstOrDefault();
                    rtTouch.AddStop(stop);
                }

                //Just to get updated address for stops
                GetSLUpdatedStopAddress(rtTouch);

                return rtTouch;
            }
        }

        private Stops MapStops(StopInformation item, PR.LocalLogisticsSolution.Infrastructure.Model.Address originAddress, PR.LocalLogisticsSolution.Infrastructure.Model.Address destAddress, Touch touch = null)
        {
            BusyTime busyTime = null;
            //We are creating one BusyTime entry for one stop, so we are taking first record from that list
            if (item.BusyTimeInformation != null && item.BusyTimeInformation.Count() > 0)
            {
                busyTime = MapBusyTime(item.BusyTimeInformation.ToList().FirstOrDefault());
            }

            Stops newstop = new Stops(item.Pk_Touch_StopId, item.FK_TouchId.Value, item.StopType.StopType1, item.StopType.StopTypeId, item.CustomerName, item.IsRequired,
                originAddress.PK_AddressId, originAddress.AddressLine1, originAddress.AddressLine2, originAddress.Zip, originAddress.City, originAddress.State, originAddress.Country, originAddress.Latitude, originAddress.Longitude, (PR.UtilityLibrary.PREnums.AddressType)originAddress.FK_AddressTypeId, originAddress.FacilityStoreNo,
                destAddress.PK_AddressId, destAddress.AddressLine1, destAddress.AddressLine2, destAddress.Zip, destAddress.City, destAddress.State, destAddress.Country, destAddress.Latitude, destAddress.Longitude, (PR.UtilityLibrary.PREnums.AddressType)destAddress.FK_AddressTypeId, destAddress.FacilityStoreNo,
                item.Sequence, item.ScheduledStartTime, item.ScheduledEndTime, item.EstimatedStartTime, item.EstimatedEndTime, item.ActualStartTime, item.ActualEndTime, touch, busyTime, item.FK_TouchStatus, item.ActualDistance);

            if (touch != null)
                touch.CustomerName = newstop.CustomerName;

            return newstop;
        }

        private BusyTime MapBusyTime(BusyTimeInformation btInfo)
        {
            BusyTime bt = new BusyTime();
            if (btInfo == null)
                return bt;

            bt.BreakTime = btInfo.BreakTime;
            bt.Comment = btInfo.Comment;
            bt.BreakID = btInfo.PF_BreakID;
            bt.StopID = btInfo.FK_StopID;

            return bt;
        }

        public Load FetchLoadByLoadId(int loadId)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Load rtLoad = null;
                var load = lcontext.LoadInformation.Where(l => l.PK_LoadID == loadId).FirstOrDefault();
                Touch rtTouch;
                Stops stop;

                if (load != null && load.PK_LoadID > 0)
                {
                    rtLoad = new Load(load.PK_LoadID, Convert.ToInt32(load.DriverID), load.StoreNumber, load.FK_TruckID, load.IsTrailerRequired.GetValueOrDefault(), load.IsOptimized, load.IsLocked, load.CreatedDate.Value, load.ScheduleStartDateTime);

                    foreach (var dbTouch in load.TouchInformation.OrderBy(t => t.RouteSequence))
                    {
                        rtTouch = MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId));

                        foreach (var item in lcontext.StopInformation.Where(s => s.FK_TouchId == dbTouch.PK_TouchId).OrderBy(s => s.Sequence))
                        {
                            //var stoptype = lcontext.StopType.Where(s => s.StopTypeId == item.FK__StopTypeId).FirstOrDefault();
                            var originAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();
                            var destAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();

                            stop = MapStops(item, originAddress, destAddress, rtTouch);
                            stop.SkipTouchReason = lcontext.SkipTouch.Where(x => x.FK_Touch_StopId == item.Pk_Touch_StopId).Select(x => x.Reason).FirstOrDefault();
                            rtTouch.AddStop(stop);
                        }

                        //Just to get updated address for stops
                        GetSLUpdatedStopAddress(rtTouch);

                        rtLoad.AddTouch(rtTouch);
                    }
                }

                return rtLoad;
            }
        }

        private void GetSLUpdatedStopAddress(Touch touch)
        {
            if (touch.SLUpdatedStatusType == SLUpdateType.OnlyAddressUpdated || touch.SLUpdatedStatusType == SLUpdateType.AddressAndInfoUpdated)
            {
                switch (touch.TouchTypeAcronym)
                {
                    case "DE":
                    case "DF":
                        if (touch.SLUpdatedTouch.OriginAddress != null && touch.OriginAddress.ID != touch.SLUpdatedTouch.OriginAddress.ID)
                        {
                            touch.PickupStop.SLUpdatedOriginAddress = touch.OriginAddress;
                            touch.DropoffStop.SLUpdatedDestAddress = touch.OriginAddress;
                        }
                        if (touch.SLUpdatedTouch.DestAddress != null && touch.DestAddress.ID != touch.SLUpdatedTouch.DestAddress.ID)
                        {
                            touch.PickupStop.SLUpdatedDestAddress = touch.DestAddress;
                            touch.DropoffStop.SLUpdatedOriginAddress = touch.DestAddress;
                        }
                        //if (touch.PickupStop != null)
                        //{
                        //    touch.PickupStop.IsRequired = true;
                        //    touch.PickupStop.AssignOriginDetails(touch.OriginAddress);
                        //    touch.PickupStop.AssignDestDetails(touch.DestAddress);// CustomerAddress);
                        //}

                        //if (touch.DropoffStop != null)
                        //{
                        //    touch.DropoffStop.IsRequired = true;
                        //    touch.DropoffStop.AssignOriginDetails(touch.DestAddress);
                        //    touch.DropoffStop.AssignDestDetails(touch.OriginAddress);
                        //}
                        break;

                    case "CC":
                        if (touch.SLUpdatedTouch.OriginAddress != null && touch.OriginAddress.ID != touch.SLUpdatedTouch.OriginAddress.ID)
                        {
                            touch.PickupStop.SLUpdatedDestAddress = touch.OriginAddress;
                            touch.DropoffStop.SLUpdatedOriginAddress = touch.OriginAddress;
                        }
                        if (touch.SLUpdatedTouch.DestAddress != null && touch.DestAddress.ID != touch.SLUpdatedTouch.DestAddress.ID)
                        {
                            touch.DropoffStop.SLUpdatedDestAddress = touch.DestAddress;
                            touch.ToFacilityStop.SLUpdatedOriginAddress = touch.DestAddress;
                        }
                        //if (touch.PickupStop != null)
                        //{
                        //    touch.PickupStop.IsRequired = true;
                        //    touch.PickupStop.AssignOriginDetails(FacilityAddress);
                        //    touch.PickupStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                        //}
                        //if (touch.DropoffStop != null)
                        //{
                        //    touch.DropoffStop.IsRequired = true;
                        //    touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        //    touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                        //}
                        //if (touch.ToFacilityStop != null)
                        //{
                        //    touch.ToFacilityStop.IsRequired = true;
                        //    touch.ToFacilityStop.AssignOriginDetails(touch.DestAddress);
                        //    touch.ToFacilityStop.AssignDestDetails(FacilityAddress);
                        //}                        
                        break;

                    case "RE":
                    case "RF":
                        if (touch.SLUpdatedTouch.OriginAddress != null && touch.OriginAddress.ID != touch.SLUpdatedTouch.OriginAddress.ID)
                        {
                            touch.PickupStop.SLUpdatedDestAddress = touch.OriginAddress;
                            touch.DropoffStop.SLUpdatedOriginAddress = touch.OriginAddress;
                        }
                        if (touch.SLUpdatedTouch.DestAddress != null && touch.DestAddress.ID != touch.SLUpdatedTouch.DestAddress.ID)
                        {
                            touch.PickupStop.SLUpdatedOriginAddress = touch.DestAddress;
                            touch.DropoffStop.SLUpdatedDestAddress = touch.DestAddress;
                        }

                        //if (touch.PickupStop != null)
                        //{
                        //    touch.PickupStop.IsRequired = true;
                        //    touch.PickupStop.AssignOriginDetails(touch.DestAddress);
                        //    touch.PickupStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                        //}

                        //if (touch.DropoffStop != null)
                        //{
                        //    touch.DropoffStop.IsRequired = true;
                        //    touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        //    touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                        //}
                        break;
                }
            }
        }

        public List<Touch> FetchTouchByLoadAndGuidId(int loadId, Guid guid)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Touch rtTouch;
                Stops stop;

                List<Touch> lstTouches = new List<Touch>();

                var dbTouches = lcontext.TouchInformation.Where(t => t.FK_LoadId == loadId && t.GroupGUID == guid);
                if (dbTouches != null && dbTouches.Count() > 0)
                {
                    foreach (var dbTouch in dbTouches)
                    {
                        rtTouch = MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId));

                        foreach (var item in lcontext.StopInformation.Where(s => s.FK_TouchId == dbTouch.PK_TouchId).OrderBy(s => s.Sequence))
                        {
                            var originAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();
                            var destAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();

                            stop = MapStops(item, originAddress, destAddress, rtTouch);
                            stop.SkipTouchReason = lcontext.SkipTouch.Where(x => x.FK_Touch_StopId == item.Pk_Touch_StopId).Select(x => x.Reason).FirstOrDefault();
                            rtTouch.AddStop(stop);
                        }

                        //Just to get updated address for stops
                        GetSLUpdatedStopAddress(rtTouch);

                        lstTouches.Add(rtTouch);
                    }
                }

                return lstTouches;
            }
        }

        private void FetchStopsByTouchId(int touchId, RouteOptimzationEntities context, ref List<Stops> stopLists)
        {
            var stops = context.StopInformation.Where(x => x.FK_TouchId == touchId).ToList();

            foreach (var item in stops)
            {
                var originAddress = context.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();
                var destAddress = context.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();
                Stops stop = MapStops(item, originAddress, destAddress);
                stop.SkipTouchReason = context.SkipTouch.Where(x => x.FK_Touch_StopId == item.Pk_Touch_StopId).Select(x => x.Reason).FirstOrDefault();
                stopLists.Add(stop);
            }
        }

        private Touch ParseTouchFromSP(List<USP_FA_LL_GetTouches_Result> allDBStops, USP_FA_LL_GetTouches_Result dbTouch, RouteOptimzationEntities lContext)
        {
            var tempTouch = new Touch(dbTouch.TouchId, dbTouch.LoadID, dbTouch.WeightStnReq, dbTouch.TrailerId.GetValueOrDefault(), dbTouch.Status, dbTouch.StartTime, dbTouch.TouchType, dbTouch.QORId, dbTouch.SLSeqNo, dbTouch.ScheduledDate_Touch, dbTouch.PhoneNumber_Touch
                                             , dbTouch.Instructions, dbTouch.DoorToFront, dbTouch.DoorToRear, dbTouch.IsGrouped.GetValueOrDefault(), dbTouch.FK_TouchGroupId.GetValueOrDefault(), dbTouch.FK_TouchGroupId, dbTouch.GroupGUID, null, dbTouch.StarsUnitId, dbTouch.ContainerNo, dbTouch.RouteSequence_Touch,
                                             dbTouch.UnitSize, dbTouch.SiteLinkMileage_Touch, dbTouch.SLStatusId, dbTouch.IsExceptionalTouch, dbTouch.IsLocked, dbTouch.TruckID, dbTouch.DriverName, dbTouch.DriverID, dbTouch.IsZippyShellMove, dbTouch.StoreNumber_Touch);

            var orgAddr = new Entities.Address()
            {
                ID = dbTouch.AddressID_TouchOrg,
                AddressLine1 = dbTouch.AddressLine1_TouchOrg,
                AddressLine2 = dbTouch.AddressLine2_TouchOrg,
                City = dbTouch.City_TouchOrg,
                State = dbTouch.State_TouchOrg,
                Zip = dbTouch.Zip_TouchOrg,
                Country = dbTouch.Country_TouchOrg,
                Latitude = dbTouch.Latitude_TouchOrg,
                Longitude = dbTouch.Longitude_TouchOrg,
                Company = dbTouch.Company_TouchOrg,
                AddressType = (PR.UtilityLibrary.PREnums.AddressType)Enum.Parse(typeof(PR.UtilityLibrary.PREnums.AddressType), Convert.ToString(dbTouch.AddressTypeId_TouchOrg)),
                FacilityStoreNo = dbTouch.FacilityStoreNo_TouchOrg
            };

            var destAddr = new Entities.Address()
            {
                ID = dbTouch.AddressID_TouchDest,
                AddressLine1 = dbTouch.AddressLine1_TouchDest,
                AddressLine2 = dbTouch.AddressLine2_TouchDest,
                City = dbTouch.City_TouchDest,
                State = dbTouch.State_TouchDest,
                Zip = dbTouch.Zip_TouchDest,
                Country = dbTouch.Country_TouchDest,
                Latitude = dbTouch.Latitude_TouchDest,
                Longitude = dbTouch.Longitude_TouchDest,
                Company = dbTouch.Company_TouchDest,
                AddressType = (PR.UtilityLibrary.PREnums.AddressType)Enum.Parse(typeof(PR.UtilityLibrary.PREnums.AddressType), Convert.ToString(dbTouch.AddressTypeId_TouchDest)),
                FacilityStoreNo = dbTouch.FacilityStoreNo_TouchDest
            };


            Touch touchObj = MapSLUpdatesToTouch(tempTouch, lContext, orgAddr, destAddr);

            foreach (var stop in allDBStops.Where(s => s.TouchId == dbTouch.TouchId))
            {
                var busyTime = new BusyTime() { BreakID = dbTouch.BreakID.GetValueOrDefault(), BreakTime = dbTouch.BreakTime, Comment = dbTouch.BreakComment };

                var tmpStop = new Stops(stop.TouchStopId, stop.TouchId, stop.StopType, stop.StopTypeId, stop.CustomerName, stop.IsRequired, stop.AddressID_StopOrg, stop.AddressLine1_StopOrg,
                    stop.AddressLine2_StopOrg, stop.Zip_StopOrg, stop.City_StopOrg, stop.State_StopOrg, stop.Country_StopOrg, stop.Latitude_StopOrg, stop.Longitude_StopOrg,
                    (PR.UtilityLibrary.PREnums.AddressType)stop.AddressTypeId_StopOrg, stop.FacilityStoreNo_StopOrg, stop.AddressID_StopDest, stop.AddressLine1_StopDest,
                    stop.AddressLine2_StopDest, stop.Zip_StopDest, stop.City_StopDest, stop.State_StopDest, stop.Country_StopDest, stop.Latitude_StopDest, stop.Longitude_StopDest,
                    (PR.UtilityLibrary.PREnums.AddressType)stop.AddressTypeId_StopDest, stop.FacilityStoreNo_StopDest, stop.Sequence_Stop, stop.ScheduledStartTime, stop.ScheduledEndTime, stop.EstimatedStartTime, stop.EstimatedEndTime,
                    stop.ActualStartTime, stop.ActualEndTime, touchObj, busyTime, stop.Status, stop.ActualDistance_Stop);

                touchObj.AddStop(tmpStop);
            }

            //Just to get updated address for stops
            GetSLUpdatedStopAddress(touchObj);

            return touchObj;
        }

        public List<Load> FetchLoadsFromSP(DateTime date, string storeNumber)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                List<Touch> touches = new List<Touch>();
                List<Load> loadLists = new List<Load>();
                Load objLoad;

                List<USP_FA_LL_GetTouches_Result> allDBStops = lcontext.USP_FA_LL_GetTouches(storeNumber, date.ToShortDateString()).ToList();

                foreach (var loadID in allDBStops.Select(x => x.LoadID).Distinct())
                {
                    var localLoad = allDBStops.Where(x => x.LoadID == loadID).FirstOrDefault();

                    objLoad = new Load(localLoad.LoadID, localLoad.DriverID.GetValueOrDefault(), localLoad.StoreNumber_Load, localLoad.TruckID, localLoad.IsTrailerRequired, localLoad.IsOptimized,
                        localLoad.IsLocked, localLoad.CreatedDate_Load.GetValueOrDefault(), localLoad.ScheduledDate_Touch, null, localLoad.DriverName);

                    foreach (var dbTouch in allDBStops.Where(s => s.LoadID == loadID))
                    {

                        if (objLoad.Touches == null || !objLoad.Touches.Any(t => t.TouchId == dbTouch.TouchId))
                        {
                            var touchObj = ParseTouchFromSP(allDBStops, dbTouch, lcontext);

                            objLoad.AddTouch(touchObj);
                        }
                    }

                    loadLists.Add(objLoad);
                }

                return loadLists;
            }
        }

        public List<Load> GetAllLoadsByDate(DateTime date, string storeNumber)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var loads = lcontext.LoadInformation.Where(x => (x.CreatedDate != null && (x.CreatedDate.Value.Year == date.Year &&
                                                                                        x.CreatedDate.Value.Month == date.Month &&
                                                                                        x.CreatedDate.Value.Day == date.Day)) && x.StoreNumber == storeNumber).ToList();
                List<Load> loadLists = new List<Load>();
                Touch touch;
                Load load;
                //List<Stops> stopLists = new List<Stops>();
                Stops stop = null;

                for (int i = 0; i < loads.Count; i++)
                {
                    load = new Load(loads[i].PK_LoadID, loads[i].DriverID.Value, storeNumber, loads[i].FK_TruckID, loads[i].IsTrailerRequired.GetValueOrDefault(), loads[i].IsOptimized, loads[i].IsLocked, loads[i].CreatedDate.Value, loads[i].ScheduleStartDateTime);

                    foreach (var dbTouch in loads[i].TouchInformation)
                    {
                        touch = MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId));

                        foreach (var dbStop in dbTouch.StopInformation)
                        {
                            var originAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_OriginAddressId).Single();
                            var destAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_DestinationAddressId).Single();
                            stop = MapStops(dbStop, originAddress, destAddress, touch);

                            touch.AddStop(stop);
                        }

                        //Just to get updated address for stops
                        GetSLUpdatedStopAddress(touch);

                        load.AddTouch(touch);
                    }

                    //touch.AddStops(GetAllStopsByLoad(Convert.ToInt32(loads[i].PK_LoadID)));

                    loadLists.Add(load);
                }

                return loadLists;
            }
        }

        public List<Load> GetAllLoadsByDate(DateTime date)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var loads = lcontext.LoadInformation.Where(x => (x.CreatedDate != null && (x.CreatedDate.Value.Year == date.Year &&
                                                                                        x.CreatedDate.Value.Month == date.Month &&
                                                                                        x.CreatedDate.Value.Day == date.Day))).ToList();
                List<Load> loadLists = new List<Load>();
                Touch touch;
                Load load;
                //List<Stops> stopLists = new List<Stops>();
                Stops stop = null;

                for (int i = 0; i < loads.Count; i++)
                {
                    load = new Load(loads[i].PK_LoadID, loads[i].DriverID.Value, loads[i].StoreNumber, loads[i].FK_TruckID, loads[i].IsTrailerRequired.GetValueOrDefault(), loads[i].IsOptimized, loads[i].IsLocked, loads[i].CreatedDate.Value, loads[i].ScheduleStartDateTime);

                    foreach (var dbTouch in loads[i].TouchInformation)
                    {
                        touch = MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId));

                        foreach (var dbStop in dbTouch.StopInformation)
                        {
                            var originAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_OriginAddressId).Single();
                            var destAddress = lcontext.Address.Where(x => x.PK_AddressId == dbStop.FK_DestinationAddressId).Single();
                            stop = MapStops(dbStop, originAddress, destAddress, touch);

                            touch.AddStop(stop);
                        }
                        //Just to get updated address for stops
                        GetSLUpdatedStopAddress(touch);

                        load.AddTouch(touch);
                    }

                    //touch.AddStops(GetAllStopsByLoad(Convert.ToInt32(loads[i].PK_LoadID)));

                    loadLists.Add(load);
                }

                return loadLists;
            }
        }

        public List<Stops> GetAllStopsByLoad(int loadId)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                List<Stops> stops = new List<Stops>();
                if (loadId != 0)
                {
                    var touchList = lcontext.TouchInformation.Where(x => x.FK_LoadId == loadId).ToList();
                    touchList.ForEach(x => FetchStopsByTouchId(x.PK_TouchId, lcontext, ref stops));
                }
                return stops;
            }
        }

        private PR.Entities.Address CheckWeightStationForThisTouch(Touch touch)
        {
            PR.Entities.Address rtVal = new PR.Entities.Address();

            //GET WeightStation Address

            return rtVal;
        }

        public int CreateLoad(int driverId, DateTime date, string storeNumber, string storeName, int truckID)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                LoadInformation load;

                var dbLoad = lcontext.LoadInformation.Where(x => (x.CreatedDate != null && (x.CreatedDate.Value.Year == date.Year &&
                                                                                    x.CreatedDate.Value.Month == date.Month &&
                                                                                    x.CreatedDate.Value.Day == date.Day))
                                                                    && x.StoreNumber == storeNumber
                                                                    && x.DriverID == driverId).FirstOrDefault();

                if (dbLoad != null && dbLoad.PK_LoadID > 0)
                {
                    load = dbLoad;
                }
                else
                {
                    load = new LoadInformation { DriverID = driverId, CreatedDate = date, StoreNumber = storeNumber, StoreName = storeName, FK_TruckID = truckID };

                    //load.FK_UserID
                    lcontext.LoadInformation.Add(load);  //SFERP-TODO - LoadInformationTable insert.
                    lcontext.SaveChanges();
                }

                return load.PK_LoadID;
            }
        }

        public bool UpdateLoadTruck(int loadId, int truckId)
        {
            bool rtFalg = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    var load = new LoadInformation() { PK_LoadID = loadId, FK_TruckID = truckId };

                    lcontext.LoadInformation.Attach(load);
                    //TODO need to look for this line to update this values
                    lcontext.Entry(load).Property(x => x.FK_TruckID).IsModified = true;

                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        public bool UpdateLoadOptimized(int loadId, bool isOptimized)
        {
            bool rtFalg = true;
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var load = lcontext.LoadInformation.Find(loadId);

                if (load != null && load.PK_LoadID > 0)
                {
                    load.IsOptimized = isOptimized;

                    //lcontext.LoadInformation.Attach(load);
                    ////TODO need to look for this line to update this values
                    //lcontext.Entry(load).Property(x => x.IsOptimized).IsModified = true;

                    lcontext.SaveChanges();
                }
            }

            return rtFalg;
        }

        public bool UpdateLoadLock(int loadId, bool isLock)
        {
            bool rtFalg = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    var load = new LoadInformation() { PK_LoadID = loadId, IsLocked = isLock };

                    lcontext.LoadInformation.Attach(load);
                    //TODO need to look for this line to update this values
                    lcontext.Entry(load).Property(x => x.IsLocked).IsModified = true;

                    lcontext.SaveChanges();
                }
                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        public bool DeleteLoad(int loadId)
        {
            bool rtFalg = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {

                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");
                    var load = lcontext.LoadInformation.Find(loadId);

                    if (load != null)
                    {
                        bool isTouchesFoud = false;
                        bool isStopsFoud = false;

                        var touches = load.TouchInformation.ToList();
                        for (int touchIndex = touches.Count - 1; touchIndex >= 0; touchIndex--)
                        {
                            if (touches[touchIndex].Status != "Completed")
                            {
                                var stops = touches[touchIndex].StopInformation.ToList();
                                isStopsFoud = false;

                                for (int stopIndex = stops.Count - 1; stopIndex >= 0; stopIndex--)
                                {
                                    int stopid = stops[stopIndex].Pk_Touch_StopId;
                                    if (stopid > 0)
                                    {
                                        lcontext.SkipTouch.Where(sk => sk.FK_Touch_StopId == stopid).ToList().ForEach(skip =>
                                        {
                                            lcontext.SkipTouch.Remove(skip);
                                        });
                                    }
                                    lcontext.StopInformation.Remove(stops[stopIndex]);

                                    isStopsFoud = true;
                                }

                                //If it has any history of sync info plz delete it other wise it foreign key voilation
                                int _touchId = touches[touchIndex].PK_TouchId;
                                lcontext.TouchInformationSync.Where(ts => ts.FK_TouchId == _touchId).ToList().ForEach(sync =>
                                {
                                    lcontext.TouchInformationSync.Remove(sync);
                                });

                                if (isStopsFoud)
                                    lcontext.SaveChanges();

                                lcontext.TouchInformation.Remove(touches[touchIndex]);
                                isTouchesFoud = true;
                            }
                        }
                        if (isTouchesFoud)
                            lcontext.SaveChanges();

                        if (load.TouchInformation.Count() == 0)
                        {
                            lcontext.LoadInformation.Remove(load);
                            lcontext.SaveChanges();
                        }
                    }
                }

                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        /// <summary>
        /// This method returns false if this touch is in use or completed then we will not allow the user not to remove from this load
        /// If it returns true, then this touch can be removed from the load
        /// </summary>
        /// <param name="touchId"></param>
        /// <returns></returns>
        public bool CheckTouchCanRemoveFromLoad(int touchId, out string reasonTypes)
        {
            bool canRemove = true;
            reasonTypes = string.Empty;
            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                //string[] compltSkipTouches = {  "Active" };
                //rtFalg = lcontext.StopInformation.Any(t => (t.FK_TouchId == touchId) && t.FK_TouchStatus != "Open");
                var stops = lcontext.StopInformation.Where(t => t.FK_TouchId == touchId && t.FK_TouchStatus != "Open" && t.FK_TouchStatus != "Skipped").ToList();

                if (stops != null && stops.Count > 0)
                {
                    foreach (var stop in stops)
                    {
                        reasonTypes = reasonTypes + (string.IsNullOrEmpty(reasonTypes) ? "" : ", ") + stop.FK_TouchStatus;
                    }

                    canRemove = false;
                }
            }

            return canRemove;
        }

        public List<TouchHandlingTime> FetchTouchHandlingTimes(string StoreNo)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                List<TouchHandlingTime> lstTht = new List<TouchHandlingTime>();

                var dbList = lcontext.TouchHandlingTimes.Where(x => x.StoreNumber == StoreNo).ToList();

                foreach (var tht in dbList)
                {
                    lstTht.Add(new TouchHandlingTime(tht.PK_ID, tht.StoreNumber, tht.StopType, tht.HandlingTime));
                }
                //foreach (var tht in lcontext.TouchHandlingTimes.Where(x=>x.StoreNumber== StoreNo)
                //{

                //}
                return lstTht;
            }
        }

        public bool UpdateGroupingStatus(List<Touch> touches, Dictionary<int, int> touchesAndOrder)
        {
            bool returnVal = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    foreach (var touch in touches)
                    {
                        var touchInfo = new TouchInformation()
                        {
                            PK_TouchId = touch.TouchId,
                            IsGrouped = touch.IsGrouped,
                            FK_TouchGroupId = touch.TouchGroupId,
                            GroupGUID = touch.GroupGUID
                        };

                        if (touchesAndOrder.ContainsKey(touch.TouchId))
                        {
                            touchInfo.RouteSequence = touchesAndOrder[touch.TouchId];
                            touchesAndOrder.Remove(touch.TouchId);
                        }

                        lcontext.TouchInformation.Attach(touchInfo);
                        var entry = lcontext.Entry(touchInfo);
                        entry.Property(t => t.IsGrouped).IsModified = true;                        //TouchInformation touchInfo = context.TouchInformation.Where(x => x.PK_TouchId == touch.TouchId).Single();
                        entry.Property(t => t.FK_TouchGroupId).IsModified = true;
                        entry.Property(t => t.GroupGUID).IsModified = true;
                        entry.Property(t => t.RouteSequence).IsModified = true;

                        var stopInfos = lcontext.StopInformation.Where(x => x.FK_TouchId == touch.TouchId).ToList();

                        if (stopInfos != null)
                        {
                            Stops stop = null;
                            for (int i = 0; i < stopInfos.Count(); i++)
                            {
                                stop = touch.Stops.First(s => s.TouchStopId == stopInfos[i].Pk_Touch_StopId);

                                if (stop != null)
                                {
                                    stopInfos[i].FK_OriginAddressId = stop.OriginAddress.ID;
                                    stopInfos[i].FK_DestinationAddressId = stop.DestinationAddress.ID;
                                    stopInfos[i].IsRequired = stop.IsRequired;

                                    //stopInfos[i].FK_OriginAddressId = touch.Stops[i].OriginAddress.ID;
                                    //stopInfos[i].FK_DestinationAddressId = touch.Stops[i].DestinationAddress.ID;
                                    //stopInfos[i].IsRequired = touch.Stops[i].IsRequired;

                                    var stopEntry = lcontext.Entry(stopInfos[i]);

                                    stopEntry.Property(t => t.IsRequired).IsModified = true;
                                    stopEntry.Property(t => t.FK_OriginAddressId).IsModified = true;
                                    stopEntry.Property(t => t.FK_DestinationAddressId).IsModified = true;
                                }
                            }
                        }
                    }

                    //Update other touches sequences to make sure everything in order
                    foreach (var touch in touchesAndOrder)
                    {
                        var touchInfo = new TouchInformation()
                        {
                            PK_TouchId = touch.Key,
                            RouteSequence = touch.Value
                        };

                        lcontext.TouchInformation.Attach(touchInfo);
                        var entry = lcontext.Entry(touchInfo);
                        entry.Property(t => t.RouteSequence).IsModified = true;
                    }

                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                returnVal = true;
            }
            return returnVal;
        }

        public bool UpdateUnGroupingStatus(List<Touch> touches, bool isDriverAppUngroupCall = false)
        {
            bool returnVal = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    foreach (var touch in touches)
                    {
                        var touchInfo = new TouchInformation()
                        {
                            PK_TouchId = touch.TouchId,
                            IsGrouped = false,
                            FK_TouchGroupId = null,
                            GroupGUID = null
                        };

                        lcontext.TouchInformation.Attach(touchInfo);
                        var entry = lcontext.Entry(touchInfo);
                        entry.Property(t => t.IsGrouped).IsModified = true;                        //TouchInformation touchInfo = context.TouchInformation.Where(x => x.PK_TouchId == touch.TouchId).Single();
                        entry.Property(t => t.FK_TouchGroupId).IsModified = true;
                        entry.Property(t => t.GroupGUID).IsModified = true;

                        var stopInfos = lcontext.StopInformation.Where(x => x.FK_TouchId == touch.TouchId).ToList();

                        if (stopInfos != null)
                        {
                            Stops stop = null;
                            for (int i = 0; i < stopInfos.Count(); i++)
                            {
                                stop = touch.Stops.First(s => s.TouchStopId == stopInfos[i].Pk_Touch_StopId);

                                if (stop != null)
                                {
                                    stopInfos[i].FK_OriginAddressId = stop.OriginAddress.ID;
                                    stopInfos[i].FK_DestinationAddressId = stop.DestinationAddress.ID;
                                    stopInfos[i].IsRequired = isDriverAppUngroupCall ? stop.IsRequired : true;

                                    //While ungrouping, Dropoff stop sequence should be null because it may cause for missmatch sequence in route level
                                    /*  if (stopInfos[i].FK_StopTypeId == (int)StopType.DROPOFF && (stopInfos[i].Sequence == null || stopInfos[i].Sequence == 0))
                                      {
                                          stopInfos[i].Sequence = touch.PickupStop.StopSequence;
                                      }
                                      else if (stopInfos[i].FK_StopTypeId == (int)StopType.ToFacility && (stopInfos[i].Sequence == null || stopInfos[i].Sequence == 0))
                                      {
                                          stopInfos[i].Sequence = touch.DropoffStop.StopSequence;
                                      }
                                      */

                                    //stopInfos[i].FK_OriginAddressId = touch.Stops[i].OriginAddress.ID;
                                    //stopInfos[i].FK_DestinationAddressId = touch.Stops[i].DestinationAddress.ID;
                                    //stopInfos[i].IsRequired = touch.Stops[i].IsRequired;

                                    var stopEntry = lcontext.Entry(stopInfos[i]);

                                    stopEntry.Property(t => t.IsRequired).IsModified = true;
                                    stopEntry.Property(t => t.FK_OriginAddressId).IsModified = true;
                                    stopEntry.Property(t => t.FK_DestinationAddressId).IsModified = true;
                                }
                                //stopInfos[i].FK_DestinationAddressId = touch.Stops[i].DestinationAddress.ID;
                                //stopInfos[i].FK_OriginAddressId = touch.Stops[i].OriginAddress.ID;
                                //stopInfos[i].IsRequired = true;

                                //var stopEntry = lcontext.Entry(stopInfos[i]);

                                //stopEntry.Property(t => t.IsRequired).IsModified = true;
                                //stopEntry.Property(t => t.FK_OriginAddressId).IsModified = true;
                                //stopEntry.Property(t => t.FK_DestinationAddressId).IsModified = true;
                            }
                        }
                    }

                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                returnVal = true;
            }

            return returnVal;
        }

        public bool UpdateUnGroupingStatusOnlyPreviousTouch(Touch touch)
        {
            bool returnVal = false;
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    var touchInfo = new TouchInformation()
                    {
                        PK_TouchId = touch.TouchId
                    };

                    lcontext.TouchInformation.Attach(touchInfo);
                    var entry = lcontext.Entry(touchInfo);

                    var stopInfos = lcontext.StopInformation.Where(x => x.FK_TouchId == touch.TouchId).ToList();

                    if (stopInfos != null)
                    {
                        Stops stop = null;
                        for (int i = 0; i < stopInfos.Count(); i++)
                        {
                            stop = touch.Stops.First(s => s.TouchStopId == stopInfos[i].Pk_Touch_StopId);

                            if (stop != null)
                            {
                                stopInfos[i].FK_OriginAddressId = stop.OriginAddress.ID;
                                stopInfos[i].FK_DestinationAddressId = stop.DestinationAddress.ID;
                                stopInfos[i].IsRequired = true;

                                //While ungrouping, Dropoff stop sequence should be null because it may cause for missmatch sequence in route level
                                /*  if (stopInfos[i].FK_StopTypeId == (int)StopType.DROPOFF && (stopInfos[i].Sequence == null || stopInfos[i].Sequence == 0))
                                  {
                                      stopInfos[i].Sequence = touch.PickupStop.StopSequence;
                                  }
                                  */
                                var stopEntry = lcontext.Entry(stopInfos[i]);

                                stopEntry.Property(t => t.IsRequired).IsModified = true;
                                stopEntry.Property(t => t.FK_OriginAddressId).IsModified = true;
                                stopEntry.Property(t => t.FK_DestinationAddressId).IsModified = true;
                            }
                        }
                    }


                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                returnVal = true;
            }

            return returnVal;
        }

        public bool SaveDriverStartTimeNTrailerRequired(int loadId, string startTime, bool isTrailerRequired)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var load = lcontext.LoadInformation.Find(loadId);

                DateTime newtime = Convert.ToDateTime(load.CreatedDate.Value.ToShortDateString() + " " + startTime);

                load.ScheduleStartDateTime = newtime;
                load.IsTrailerRequired = isTrailerRequired;

                //var newload = new LoadInformation() { PK_LoadID = loadId, ScheduleStartDateTime =  newtime };
                //lcontext.LoadInformation.Attach(newload);
                //lcontext.Entry(newload).Property(l => l.ScheduleStartDateTime).IsModified = true;

                lcontext.SaveChanges();
            }

            return true;
        }

        //public bool SaveTouchSortOrder(List<int> seqTouchIds, string[] touchTrailerMapIds)
        //{
        //    bool rtFalg = false;

        //    using (TransactionScope scope = new TransactionScope())
        //    {
        //        using (var lcontext = new RouteOptimzationEntities())
        //        {
        //            int order = 0;
        //            Dictionary<int, int> dcTouchTrailerMap = new Dictionary<int, int>();
        //            if (touchTrailerMapIds != null && touchTrailerMapIds.Length > 0)
        //            {
        //                foreach (var touchTrailerId in touchTrailerMapIds)
        //                {
        //                    string[] temp = touchTrailerId.Split(',');
        //                    if (temp.Length == 2)
        //                    {
        //                        dcTouchTrailerMap.Add(Convert.ToInt32(temp[0]), Convert.ToInt32(temp[1]));
        //                    }
        //                }
        //            }
        //            foreach (int touchid in seqTouchIds)
        //            {
        //                order += 1;

        //                var trailerid = (dcTouchTrailerMap != null && dcTouchTrailerMap.ContainsKey(touchid) ? dcTouchTrailerMap[touchid] : 0);

        //                var touch = new TouchInformation() { PK_TouchId = touchid, RouteSequence = order, FK_TrailerId = trailerid };
        //                lcontext.TouchInformation.Attach(touch);
        //                lcontext.Entry(touch).Property(t => t.RouteSequence).IsModified = true;
        //                lcontext.Entry(touch).Property(t => t.FK_TrailerId).IsModified = true;
        //            }
        //            lcontext.SaveChanges();
        //        }

        //        scope.Complete();
        //        scope.Dispose();

        //        rtFalg = true;
        //    }

        //    return rtFalg;
        //}

        public bool SaveSortedTouchInfo(List<Touch> seqTouches, PR.Entities.Address FacilityAddress)
        {
            bool rtFalg = false;

            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    int? loadid = 0;
                    foreach (Touch touch in seqTouches)
                    {
                        var otouch = lcontext.TouchInformation.Find(touch.TouchId);
                        string[] compltSkipTouches = { "Completed", "Skipped" };

                        //Check if this touch has address changes
                        //Here we are handling only non grouped touches... anyway grouped touches are hanlding in touchservice level "UpdateGroupTocuhesAddressUpdatedBeforeSave"
                        if (otouch.GroupGUID == null && (otouch.FK_SLStatus == (int)SLUpdateType.OnlyAddressUpdated || otouch.FK_SLStatus == (int)SLUpdateType.AddressAndInfoUpdated))
                        {
                            Touch rtTouch = MapTouchInfoToTouch(otouch, lcontext.Address.Find(otouch.FK_OriginAddressId), lcontext.Address.Find(otouch.FK_DestinationAddressId));

                            foreach (var item in lcontext.StopInformation.Where(s => s.FK_TouchId == otouch.PK_TouchId))
                            {
                                //var stoptype = lcontext.StopType.Where(s => s.StopTypeId == item.FK_StopTypeId).FirstOrDefault();
                                var originAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();//(from c in lcontext.Address join atype in lcontext.AddressType on c.FK_AddressTypeId equals atype.PK_AddressTypeId where c.PK_AddressId== item.FK_OriginAddressId.Value select c); //
                                var destAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();
                                Stops stop = MapStops(item, originAddress, destAddress, rtTouch);
                                rtTouch.AddStop(stop);
                            }

                            //FetchTouchByTouchId()
                            UpdateStopsBasedonTouchUnGrouping(rtTouch, FacilityAddress);

                            //Check if this touch has address changes
                            foreach (var stop in otouch.StopInformation)
                            {
                                Stops tmpstop = rtTouch.Stops.First(s => s.TouchStopId == stop.Pk_Touch_StopId);

                                if (tmpstop != null)
                                {
                                    stop.FK_OriginAddressId = tmpstop.OriginAddress.ID;
                                    stop.FK_DestinationAddressId = tmpstop.DestinationAddress.ID;
                                    stop.IsRequired = true;
                                }
                            }
                        }

                        if (otouch.FK_SLStatus == (int)SLUpdateType.OnlyAddressUpdated || otouch.FK_SLStatus == (int)SLUpdateType.AddressAndInfoUpdated)
                        {
                            otouch.FK_SLStatus = (int)SLUpdateType.Sync;
                        }

                        if (!compltSkipTouches.Contains(otouch.Status))
                        {
                            //var otouch = new TouchInformation() { PK_TouchId = touch.TouchId, RouteSequence = touch.RouteSequence, FK_TrailerId = touch.TrailerId };
                            //lcontext.TouchInformation.Attach(otouch);
                            otouch.RouteSequence = touch.RouteSequence;
                            otouch.ScheduledDate = touch.ScheduledDate;
                            otouch.FK_TrailerId = touch.TrailerId;

                            foreach (var stop in otouch.StopInformation)
                            {
                                if (!compltSkipTouches.Contains(stop.FK_TouchStatus))
                                {
                                    stop.Sequence = null;
                                    stop.ScheduledStartTime = touch.ScheduledDate;
                                    stop.ScheduledEndTime = touch.ScheduledDate;

                                    stop.EstimatedEndTime = stop.ScheduledStartTime;
                                    stop.EstimatedStartTime = stop.ScheduledEndTime;
                                }
                            }
                        }

                        loadid = loadid.HasValue && loadid.Value > 0 ? loadid : otouch.FK_LoadId;
                    }


                    //We are removing busy touches when use is trying to save touches resequence
                    //we may need to refine this logic to avoid this kind of deletion
                    var busyTouches = lcontext.TouchInformation.Where(t => t.TouchType == "BT" && t.FK_LoadId == loadid);
                    if (busyTouches != null && busyTouches.Count() > 0)
                    {
                        foreach (var busyTouch in busyTouches)
                        {
                            //touch.StopInformation.ToList().ForEach(s => context.StopInformation.Remove(s));

                            busyTouch.StopInformation.ToList().ForEach(s =>
                            {
                                //Remove all entries in BusyTimeInformation for this stop
                                s.BusyTimeInformation.ToList().ForEach(b => lcontext.BusyTimeInformation.Remove(b));
                                lcontext.StopInformation.Remove(s);
                            });

                            lcontext.TouchInformation.Remove(busyTouch);
                        }
                    }
                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        private Touch UpdateStopsBasedonTouchUnGrouping(Touch touch, PR.Entities.Address FacilityAddress)
        {
            switch (touch.TouchTypeAcronym)
            {

                case "DE":
                case "DF":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(touch.OriginAddress);
                        touch.PickupStop.AssignDestDetails(touch.DestAddress);// CustomerAddress);
                    }

                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.DestAddress);
                        touch.DropoffStop.AssignDestDetails(touch.OriginAddress);
                    }
                    //touch.Stops[0].IsRequired = true;
                    //touch.Stops[0].AssignOriginDetails(touch.OriginAddress);
                    //touch.Stops[0].AssignDestDetails(touch.DestAddress);// CustomerAddress);

                    //touch.Stops[1].IsRequired = true;
                    //touch.Stops[1].AssignOriginDetails(touch.DestAddress);
                    //touch.Stops[1].AssignDestDetails(touch.OriginAddress);
                    break;

                case "CC":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(FacilityAddress);
                        touch.PickupStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                    }
                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                    }
                    if (touch.ToFacilityStop != null)
                    {
                        touch.ToFacilityStop.IsRequired = true;
                        touch.ToFacilityStop.AssignOriginDetails(touch.DestAddress);
                        touch.ToFacilityStop.AssignDestDetails(FacilityAddress);
                    }
                    //touch.Stops[0].IsRequired = true;
                    //touch.Stops[0].AssignOriginDetails(FacilityAddress);
                    //touch.Stops[0].AssignDestDetails(touch.OriginAddress);// CustomerAddress);

                    //touch.Stops[1].IsRequired = true;
                    //touch.Stops[1].AssignOriginDetails(touch.OriginAddress);
                    //touch.Stops[1].AssignDestDetails(touch.DestAddress);

                    //touch.Stops[2].IsRequired = true;
                    //touch.Stops[2].AssignOriginDetails(touch.DestAddress);
                    //touch.Stops[2].AssignDestDetails(FacilityAddress);
                    break;

                case "RE":
                case "RF":
                    if (touch.PickupStop != null)
                    {
                        touch.PickupStop.IsRequired = true;
                        touch.PickupStop.AssignOriginDetails(touch.DestAddress);
                        touch.PickupStop.AssignDestDetails(touch.OriginAddress);// CustomerAddress);
                    }

                    if (touch.DropoffStop != null)
                    {
                        touch.DropoffStop.IsRequired = true;
                        touch.DropoffStop.AssignOriginDetails(touch.OriginAddress);
                        touch.DropoffStop.AssignDestDetails(touch.DestAddress);
                    }
                    //touch.Stops[0].IsRequired = true;
                    //touch.Stops[0].AssignOriginDetails(touch.DestAddress);
                    //touch.Stops[0].AssignDestDetails(touch.OriginAddress);// CustomerAddress);

                    //touch.Stops[1].IsRequired = true;
                    //touch.Stops[1].AssignOriginDetails(touch.OriginAddress);
                    //touch.Stops[1].AssignDestDetails(touch.DestAddress);
                    break;
            }
            return touch;
        }

        //public bool SaveOptimizedStopsFromStopIds(List<int> stopIds)
        //{
        //    bool rtVal = false;
        //    using (var lcontext = new RouteOptimzationEntities())
        //    {
        //        var stops = lcontext.StopInformation.Where(s => stopIds.Contains(s.Pk_Touch_StopId));
        //        int order = 0;
        //        //Here we have to iterate from the stopsIds only just to save it in sort order
        //        foreach (var stopid in stopIds)
        //        {
        //            order += 1;
        //            var stop = stops.Where(s => s.Pk_Touch_StopId == stopid).FirstOrDefault();
        //            if (stop != null && stop.Pk_Touch_StopId > 0)
        //            {
        //                Optimized_StopInfo stopInfo = new Optimized_StopInfo();
        //                stopInfo.TouchInformation = stop.TouchInformation;

        //                stopInfo.CustomerName = stop.CustomerName;

        //                stopInfo.DestAddresssLn1 = stop.DestAddresssLn1;
        //                stopInfo.DestAddressLn2 = stop.DestAddressLn2;
        //                stopInfo.DestCity = stop.DestCity;
        //                stopInfo.DestCountry = stop.DestCountry;
        //                stopInfo.DestState = stop.DestState;
        //                stopInfo.DestZip = stop.DestZip;

        //                stopInfo.OriginAddressLn1 = stop.OriginAddressLn1;
        //                stopInfo.OriginAddressLn2 = stop.OriginAddressLn2;
        //                stopInfo.OriginCity = stop.OriginCity;
        //                stopInfo.OriginCountry = stop.OriginCountry;
        //                stopInfo.OriginState = stop.OriginState;
        //                stopInfo.OriginZip = stop.OriginZip;

        //                //TODO These dates needs to cross check 
        //                stopInfo.ScheduledStartTime = stop.ScheduledStartTime;
        //                stopInfo.ScheduledEndTime = stop.ScheduledEndTime;
        //                //stopInfo.ActualStartTime = stop.ActualStartTime;
        //                //stopInfo.ActualEndTime = stop.ActualEndTime;

        //                stopInfo.FK_StopTypeId = stop.FK_StopTypeId;
        //                //stopInfo.FK_TouchStatus = stop.FK_TouchStatus;
        //                stopInfo.Sequence = order;

        //                lcontext.Optimized_StopInfo.Add(stopInfo);
        //            }
        //        }

        //        rtVal = true;
        //    }

        //    return rtVal;
        //}

        public bool SaveOptimizedStopSortOrder(List<Stops> seqStops, int loadId)
        {
            bool rtFalg = false;

            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    foreach (var stop in seqStops)
                    {
                        //var optStop = new Optimized_StopInfo() { Pk_OPtStopInfo = stopid, Sequence = order };
                        //lcontext.Optimized_StopInfo.Attach(optStop);
                        //lcontext.Entry(optStop).Property(t => t.Sequence).IsModified = true;
                        var optStop = new StopInformation()
                        {
                            Pk_Touch_StopId = stop.StopId,
                            Sequence = stop.StopSequence,
                            ScheduledEndTime = stop.ScheduledEndTime,
                            ScheduledStartTime = stop.ScheduledStartTime,

                            EstimatedEndTime = stop.ScheduledEndTime,
                            EstimatedStartTime = stop.ScheduledStartTime
                        };

                        lcontext.StopInformation.Attach(optStop);
                        var entryStop = lcontext.Entry(optStop);
                        if (stop.ActualDistance.HasValue)
                        {
                            optStop.ActualDistance = stop.ActualDistance;
                            entryStop.Property(t => t.ActualDistance).IsModified = true;
                        }
                        entryStop.Property(t => t.Sequence).IsModified = true;
                        entryStop.Property(t => t.ScheduledEndTime).IsModified = true;
                        entryStop.Property(t => t.ScheduledStartTime).IsModified = true;
                        entryStop.Property(t => t.EstimatedEndTime).IsModified = true;
                        entryStop.Property(t => t.EstimatedStartTime).IsModified = true;

                        if (stop.ChangedDestAddressId.HasValue && stop.ChangedDestAddressId.Value > 0)
                        {
                            optStop.FK_DestinationAddressId = stop.ChangedDestAddressId.Value;
                            entryStop.Property(t => t.FK_DestinationAddressId).IsModified = true;
                        }
                        if (stop.ChangedOrgAddressId.HasValue && stop.ChangedOrgAddressId.Value > 0)
                        {
                            optStop.FK_OriginAddressId = stop.ChangedOrgAddressId.Value;
                            entryStop.Property(t => t.FK_OriginAddressId).IsModified = true;
                        }
                    }

                    var load = new LoadInformation() { PK_LoadID = loadId, IsLocked = true, IsOptimized = true };

                    lcontext.LoadInformation.Attach(load);
                    lcontext.Entry(load).Property(x => x.IsLocked).IsModified = true;
                    lcontext.Entry(load).Property(x => x.IsOptimized).IsModified = true;

                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            UpdateAllStopSequence(loadId);

            return rtFalg;
        }

        private void UpdateAllStopSequence(int loadId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    int seqUpdates = 1;
                    var load = lcontext.LoadInformation.Find(loadId);

                    load.TouchInformation.OrderBy(t => t.RouteSequence).ToList().ForEach(touch =>
                    {
                        touch.StopInformation.OrderBy(s => s.Pk_Touch_StopId).ToList().ForEach(
                            stop =>
                            {
                                stop.Sequence = seqUpdates++;
                            });
                    });

                    lcontext.SaveChanges();

                    scope.Complete();
                    scope.Dispose();
                }
            }
        }

        //private bool ValidateStopsAndUpdateSheduleTime(List<Stops> lstStops, DateTime startDateTime)
        //{
        //    List<TouchHandlingTime> oTouchHandlingTime = FetchTouchHandlingTimes();

        //    TouchStopHandlingTime oTouchStopHandlingTime = new TouchStopHandlingTime(oTouchHandlingTime);


        //    foreach (var stop in lstStops)
        //    {
        //        //switch (stop.StopType)
        //        //{
        //        //    case "DE":
        //        //        break;
        //        //    case "DE":
        //        //        break;
        //        //    case "DE":
        //        //        break;
        //        //    case "DE":
        //        //        break;
        //        //    case "DE":
        //        //        break;
        //        //    case "DE":
        //        //        break;
        //        //}
        //    }


        //}

        //public Entities.Address GetLatLongFromLocalDB(Entities.Address address)
        //{
        //    Address addrObject = context.Address.Where(x => x.AddressLine1 == address.AddressLine1 &&
        //                                        x.AddressLine2 == address.AddressLine2 &&
        //                                        x.City == address.City &&
        //                                        x.State == address.State &&
        //                                        x.Zip == address.Zip &&
        //                                        x.Country == address.Country).FirstOrDefault();
        //    if (addrObject != null && string.IsNullOrEmpty(addrObject.Latitude) == false && string.IsNullOrEmpty(addrObject.Longitude) == false)
        //    {
        //        address.Latitude = addrObject.Latitude;
        //        address.Longitude = addrObject.Longitude;
        //    }

        //    return address;
        //}

        public Entities.Address ManageAddress(Entities.Address address)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Infrastructure.Model.Address addrObject = lcontext.Address.Where(x => string.Equals(x.AddressLine1, address.AddressLine1) &&
                                                                                        string.Equals(x.AddressLine2, address.AddressLine2) &&
                                                                                        string.Equals(x.City, address.City) &&
                                                                                        string.Equals(x.State, address.State) &&
                                                                                        string.Equals(x.Zip, address.Zip)).FirstOrDefault();
                if (addrObject != null)
                {
                    address.ID = addrObject.PK_AddressId;
                    address.Latitude = addrObject.Latitude;
                    address.Longitude = addrObject.Longitude;
                    address.AddressType = (PR.UtilityLibrary.PREnums.AddressType)Enum.ToObject(typeof(PR.UtilityLibrary.PREnums.AddressType), addrObject.FK_AddressTypeId.Value);//  new Entities.AddressType { AddressTypeId = addrObject.AddressType.PK_AddressTypeId, AddressType = addrObject.AddressType.AddressType1 };
                                                                                                                                                                                 //This is to hand Existing Data. Some of the addressed already added which are not using for any facility, we can not update FacilityStoreNo through script
                    if (!addrObject.FacilityStoreNo.HasValue && address.FacilityStoreNo.HasValue)
                    {
                        addrObject.FacilityStoreNo = address.FacilityStoreNo;
                        lcontext.SaveChanges();
                    }

                    address.FacilityStoreNo = addrObject.FacilityStoreNo;
                }
                else
                {
                    if (string.IsNullOrEmpty(address.Latitude) || string.IsNullOrEmpty(address.Longitude))
                    {
                        PRAddressAPI prApi = new PRAddressAPI();
                        var info = prApi.GetLatitudeLongitude(address);
                        if (string.IsNullOrEmpty(info.Error))
                        {
                            address.Latitude = info.Latitude;
                            address.Longitude = info.Longitude;
                        }
                        else
                        {
                            address.ValidationError = info.Error;
                        }
                    }

                    addrObject = new Infrastructure.Model.Address();
                    addrObject.AddressLine1 = address.AddressLine1;
                    addrObject.AddressLine2 = address.AddressLine2;
                    addrObject.City = address.City;
                    addrObject.State = address.State;
                    addrObject.Zip = address.Zip;
                    addrObject.Country = address.Country;
                    addrObject.Latitude = address.Latitude;
                    addrObject.Longitude = address.Longitude;
                    addrObject.Company = address.Company;
                    addrObject.FK_AddressTypeId = (int)address.AddressType;
                    addrObject.FacilityStoreNo = address.FacilityStoreNo;
                    lcontext.Address.Add(addrObject);
                    lcontext.SaveChanges();

                    address.ID = addrObject.PK_AddressId;
                }
            }
            return address;
        }

        public void UpdateAddressByID(Entities.Address address)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                PR.LocalLogisticsSolution.Infrastructure.Model.Address addrObject = lcontext.Address.Where(x => x.PK_AddressId == address.ID).FirstOrDefault();
                if (addrObject != null)
                {
                    //addrObject.AddressLine1 = address.AddressLine1;
                    //addrObject.AddressLine2 = address.AddressLine2;
                    //addrObject.City = address.City;
                    //addrObject.State = address.State;
                    //addrObject.Zip = address.Zip;
                    //addrObject.Country = address.Country;
                    addrObject.Latitude = address.Latitude;
                    addrObject.Longitude = address.Longitude;
                    //addrObject.Company = address.Company;
                    //addrObject.FK_AddressTypeId = (int)address.AddressType;

                    lcontext.SaveChanges();
                }
                else
                {
                    throw new Exception("Address not found in the database");
                }
            }
        }

        public void UpdateLatLongForIntesection(Entities.Address address)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                PR.LocalLogisticsSolution.Infrastructure.Model.Address addrObject = lcontext.Address.Where(x => x.PK_AddressId == address.ID).FirstOrDefault();
                if (addrObject != null)
                {
                    addrObject.ActualLatitude = addrObject.Latitude;
                    addrObject.ActualLongitude = addrObject.Longitude;

                    addrObject.Latitude = address.Latitude;
                    addrObject.Longitude = address.Longitude;

                    addrObject.IsIntersectingLatLong = address.IsIntersectingLatLong;


                    lcontext.SaveChanges();
                }
                else
                {
                    throw new Exception("Address not found in the database");
                }
            }
        }

        public List<Trailers> GetTrailers(string storeNumber)
        {
            int storeNum = Convert.ToInt32(storeNumber);
            using (var context1 = new RouteOptimzationEntities())
            {
                context1.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var result = (from td in context1.Trailer
                                  //where td.StoreNo == storeNum && td.TruckStatusId == 1
                              select new PR.LocalLogisticsSolution.Model.Trailers { TrailerID = td.PK_TrailerID, FleetID = td.FleetID }).ToList();
                return result;
            }

        }

        public List<Entities.Address> FetchAllAddressForThisLoad(int loadId)
        {
            List<Entities.Address> lstAddress = new List<Entities.Address>();

            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var load = lcontext.LoadInformation.Find(loadId);

                List<int> addressIds = new List<int>();
                foreach (var touch in load.TouchInformation)
                {
                    if (addressIds.Contains(touch.FK_OriginAddressId) == false)
                    {
                        addressIds.Add(touch.FK_OriginAddressId);
                        Entities.Address oAdd = MapAddressInfoToAddress(lcontext.Address.Find(touch.FK_OriginAddressId));
                        lstAddress.Add(oAdd);
                    }
                    if (addressIds.Contains(touch.FK_DestinationAddressId) == false)
                    {
                        addressIds.Add(touch.FK_DestinationAddressId);
                        Entities.Address oAdd = MapAddressInfoToAddress(lcontext.Address.Find(touch.FK_DestinationAddressId));
                        lstAddress.Add(oAdd);
                    }
                    foreach (var stop in touch.StopInformation)
                    {
                        if (stop.FK_OriginAddressId != null && addressIds.Contains(stop.FK_OriginAddressId.Value) == false)
                        {
                            addressIds.Add(stop.FK_OriginAddressId.Value);
                            Entities.Address oAdd = MapAddressInfoToAddress(lcontext.Address.Find(stop.FK_OriginAddressId.Value));
                            lstAddress.Add(oAdd);
                        }
                        if (addressIds.Contains(stop.FK_DestinationAddressId.Value) == false)
                        {
                            addressIds.Add(stop.FK_DestinationAddressId.Value);
                            Entities.Address oAdd = MapAddressInfoToAddress(lcontext.Address.Find(stop.FK_DestinationAddressId.Value));
                            lstAddress.Add(oAdd);
                        }
                    }
                }
            }

            return lstAddress;
        }

        public List<Load> GetAllLiteLoadByStoreNumAndDate(DateTime date, string storeNumber)
        {
            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var loads = lcontext.LoadInformation.Where(x => (x.CreatedDate != null && (x.CreatedDate.Value.Year == date.Year &&
                                                                                x.CreatedDate.Value.Month == date.Month &&
                                                                                x.CreatedDate.Value.Day == date.Day)) &&
                                                                               x.StoreNumber == storeNumber).ToList();

                List<Load> lstLoad = new List<Load>();

                foreach (var load in loads)
                {
                    var driverName = lcontext.LogisticsUser.Where(x => x.DriverId == load.DriverID.Value).Select(s => s.FirstName + " " + s.LastName).FirstOrDefault();
                    var ans = (from t in lcontext.TouchInformation join s in lcontext.StopInformation on t.PK_TouchId equals s.FK_TouchId orderby s.Sequence descending where t.FK_LoadId == load.PK_LoadID select s.ScheduledEndTime).FirstOrDefault();
                    var nload = new Load(load.PK_LoadID, load.DriverID.Value, load.StoreNumber, load.FK_TruckID, load.IsTrailerRequired, load.IsOptimized, load.IsLocked, load.CreatedDate.Value, load.ScheduleStartDateTime, ans, driverName);

                    lstLoad.Add(nload);
                }

                return lstLoad;
            }
        }

        private Touch MapSLUpdatesToTouch(Touch touchObj, RouteOptimzationEntities lcontext, Entities.Address orgAddr, Entities.Address destAddr)
        {
            LocalComponent local = new LocalComponent();
            SMDBusinessLogic smdObj = new SMDBusinessLogic();

            // SFERP-TODO --Remove 
            Trailers thisTrailer = null;
            if (touchObj.TrailerId > 0)
            {
                thisTrailer = MapTrailerInfoToTrailer(lcontext.Trailer.Find(touchObj.TrailerId)); // SFERP-TODO (Need to Check, Venkat), not using, can remove this method.
            }
            // SFERP-TODO --Remove 

            touchObj.OriginAddress = orgAddr;
            touchObj.DestAddress = destAddr;
            touchObj.TouchOriginAddressWithOutUpdation = new Entities.Address()
            {
                ID = orgAddr.ID,
                AddressLine1 = orgAddr.AddressLine1,
                AddressLine2 = orgAddr.AddressLine2,
                City = orgAddr.City,
                State = orgAddr.State,
                Zip = orgAddr.Zip,
                Country = orgAddr.Country,
                Latitude = orgAddr.Latitude,
                Longitude = orgAddr.Longitude,
                Company = orgAddr.Company,
                AddressType = orgAddr.AddressType,
                FacilityStoreNo = orgAddr.FacilityStoreNo
            };
            touchObj.TouchDestAddressWithOutUpdation = new Entities.Address()
            {
                ID = destAddr.ID,
                AddressLine1 = destAddr.AddressLine1,
                AddressLine2 = destAddr.AddressLine2,
                City = destAddr.City,
                State = destAddr.State,
                Zip = destAddr.Zip,
                Country = destAddr.Country,
                Latitude = destAddr.Latitude,
                Longitude = destAddr.Longitude,
                Company = destAddr.Company,
                AddressType = destAddr.AddressType,
                FacilityStoreNo = destAddr.FacilityStoreNo
            };

            ////// SFERP-TODO-CTUPD, added for TG-721.  
            //touchObj.IsZippyShellQuote = smdObj.GetBrandInfo((int)touchObj.Qorid).BrandType == BrandType.ZippyShell;  // Can removed this call - TBD.

            // SFERP-TODO (Need to check if needed or not)
            ///This part handle, if system finds any updations in Salesforce 
            TouchInformationSync slUpdatedTouch = new TouchInformationSync();
            if (touchObj.SLUpdatedStatusType != SLUpdateType.Nochanges)
            {
                slUpdatedTouch = lcontext.TouchInformationSync.Where(t => t.FK_TouchId == touchObj.TouchId).FirstOrDefault();

                if (slUpdatedTouch != null && slUpdatedTouch.PK_TouchInfSyncId > 0)
                {
                    touchObj.SLUpdatedTouch = new TouchSLUpdateOld(slUpdatedTouch.PK_TouchInfSyncId, slUpdatedTouch.FK_TouchId, slUpdatedTouch.FK_LoadId, slUpdatedTouch.WeightStnReq,
                        slUpdatedTouch.CustomerName, slUpdatedTouch.PhoneNumber, slUpdatedTouch.StartTime, slUpdatedTouch.UnitSize, slUpdatedTouch.OrderNumber, slUpdatedTouch.QORId, slUpdatedTouch.Instructions, slUpdatedTouch.DoorToFront, slUpdatedTouch.DoorToRear, slUpdatedTouch.ContainerNo);

                    if (slUpdatedTouch.FK_OriginAddressId > 0)
                        touchObj.SLUpdatedTouch.OriginAddress = MapAddressInfoToAddress(lcontext.Address.Find(slUpdatedTouch.FK_OriginAddressId));

                    if (slUpdatedTouch.FK_DestinationAddressId > 0)
                        touchObj.SLUpdatedTouch.DestAddress = MapAddressInfoToAddress(lcontext.Address.Find(slUpdatedTouch.FK_DestinationAddressId));
                }
            }
            return touchObj;

        }

        private Touch MapTouchInfoToTouch(TouchInformation touchObj, PR.LocalLogisticsSolution.Infrastructure.Model.Address orgAddress, PR.LocalLogisticsSolution.Infrastructure.Model.Address destAddress)
        {
            LocalComponent local = new LocalComponent();
            SMDBusinessLogic smdObj = new SMDBusinessLogic();
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                Trailers thisTrailer = null;
                if (touchObj.FK_TrailerId != null && touchObj.FK_TrailerId > 0)
                {
                    thisTrailer = MapTrailerInfoToTrailer(lcontext.Trailer.Find(touchObj.FK_TrailerId));
                }

                ////// SFERP-TODO-CTUPD, added for TG-721.
                bool bIsZippy = touchObj.IsZippyShellMove == null ? false : (bool)touchObj.IsZippyShellMove;
                // smdObj.GetBrandInfo((int)touchObj.QORId).BrandType == BrandType.ZippyShell;  // Can removed this call - TBD.

                //// SFERP-TODO (Remove below 2 lines of code + commented code)
                //BrandTypeValidator oBTV = smdObj.ExecBrandValidator(new BrandTypeValidator(touchObj.ContainerNo) { StarsUnitID = touchObj.StarsUnitId });
                //bool bIsZippy = oBTV?.BrandType == BrandType.ZippyShell;

                Touch newobj = new Touch(touchObj.PK_TouchId, Convert.ToInt32(touchObj.FK_LoadId), touchObj.WeightStnReq, touchObj.FK_TrailerId.GetValueOrDefault(), touchObj.Status, touchObj.StartTime, touchObj.TouchType, touchObj.QORId, touchObj.SLSeqNo
                                , touchObj.ScheduledDate, touchObj.PhoneNumber, touchObj.Instructions, touchObj.DoorToFront, touchObj.DoorToRear, touchObj.IsGrouped.GetValueOrDefault(), touchObj.FK_TouchGroupId.GetValueOrDefault(),
                                touchObj.FK_TouchGroupId, touchObj.GroupGUID, thisTrailer, touchObj.StarsUnitId, touchObj.ContainerNo,
                                touchObj.RouteSequence, touchObj.UnitSize.GetValueOrDefault(0), touchObj.SiteLinkMileage, touchObj.FK_SLStatus, touchObj.IsExceptionalTouch, null, null, string.Empty, null,
                                bIsZippy, touchObj.StoreNumber);
                newobj.OriginAddress = MapAddressInfoToAddress(orgAddress);
                newobj.DestAddress = MapAddressInfoToAddress(destAddress);
                newobj.TouchOriginAddressWithOutUpdation = MapAddressInfoToAddress(orgAddress);
                newobj.TouchDestAddressWithOutUpdation = MapAddressInfoToAddress(destAddress);
                newobj.ProvidedETASettings = local.GetETASetting(Convert.ToInt32(touchObj.QORId), touchObj.TouchType, Convert.ToInt32(touchObj.SLSeqNo));


                ////SFERP-TODO (Remove below code after full testing only)
                /////This part handle, if system finds any updations in Salesforce 
                TouchInformationSync slUpdatedTouch = new TouchInformationSync();
                if (newobj.SLUpdatedStatusType != SLUpdateType.Nochanges)
                {
                    slUpdatedTouch = lcontext.TouchInformationSync.Where(t => t.FK_TouchId == touchObj.PK_TouchId).FirstOrDefault();

                    if (slUpdatedTouch != null && slUpdatedTouch.PK_TouchInfSyncId > 0)
                    {
                        newobj.SLUpdatedTouch = new TouchSLUpdateOld(slUpdatedTouch.PK_TouchInfSyncId, slUpdatedTouch.FK_TouchId, slUpdatedTouch.FK_LoadId, slUpdatedTouch.WeightStnReq,
                            slUpdatedTouch.CustomerName, slUpdatedTouch.PhoneNumber, slUpdatedTouch.StartTime, slUpdatedTouch.UnitSize, slUpdatedTouch.OrderNumber, slUpdatedTouch.QORId, slUpdatedTouch.Instructions, slUpdatedTouch.DoorToFront, slUpdatedTouch.DoorToRear, slUpdatedTouch.ContainerNo);

                        if (slUpdatedTouch.FK_OriginAddressId > 0)
                            newobj.SLUpdatedTouch.OriginAddress = MapAddressInfoToAddress(lcontext.Address.Find(slUpdatedTouch.FK_OriginAddressId));

                        if (slUpdatedTouch.FK_DestinationAddressId > 0)
                            newobj.SLUpdatedTouch.DestAddress = MapAddressInfoToAddress(lcontext.Address.Find(slUpdatedTouch.FK_DestinationAddressId));
                    }
                }
                ///////////////////////////////////////

                return newobj;
            }
        }

        private Entities.Address MapAddressInfoToAddress(PR.LocalLogisticsSolution.Infrastructure.Model.Address address)
        {
            if (address == null)
                return null;

            Entities.Address addrObject = new Entities.Address();

            addrObject.ID = address.PK_AddressId;
            addrObject.AddressLine1 = address.AddressLine1;
            addrObject.AddressLine2 = address.AddressLine2;
            addrObject.City = address.City;
            addrObject.State = address.State;
            addrObject.Zip = address.Zip;
            addrObject.Country = address.Country;
            addrObject.Latitude = address.Latitude;
            addrObject.Longitude = address.Longitude;
            addrObject.Company = address.Company;


            if (address.FK_AddressTypeId != null)
            {
                addrObject.AddressType = (PR.UtilityLibrary.PREnums.AddressType)Enum.Parse(typeof(PR.UtilityLibrary.PREnums.AddressType), Convert.ToString(address.FK_AddressTypeId));
                addrObject.FacilityStoreNo = address.FacilityStoreNo;
            }

            return addrObject;
        }

        private Trailers MapTrailerInfoToTrailer(Trailer dbTrailer)
        {
            if (dbTrailer == null)
                return null;

            Trailers trailer = new Trailers();

            trailer.TrailerID = dbTrailer.PK_TrailerID;
            trailer.TrailerPlateNo = dbTrailer.TrailerPlateNo;
            trailer.FleetID = dbTrailer.FleetID;
            trailer.TrailerVIN = dbTrailer.TrailerVIN;
            trailer.TrailerState = dbTrailer.TrailerState;

            return trailer;
        }

        public LogisticsUser GetDriverformation(int driverId)
        {
            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                LogisticsUser lUser = null;
                var user = lcontext.LogisticsUser.Where(x => x.DriverId == driverId).Select(x => x).FirstOrDefault();
                if (user != null)
                {
                    lUser = new LogisticsUser();
                    lUser.ID = user.PK_USerID;
                    lUser.UserType = user.UserTypeID;
                    lUser.DriverID = user.DriverId.HasValue ? user.DriverId.Value : 0;
                    lUser.UserName = user.UserName;
                    lUser.Password = user.Password;
                }

                return lUser;
            }
        }

        public bool CheckIfUserNameExsists(string UserName)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                return lcontext.LogisticsUser.Any(x => x.UserName == UserName);
            }
        }

        public void CreateDriverLogin(Entities.PRDriver driver)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                PR.LocalLogisticsSolution.Infrastructure.Model.LogisticsUser user = new Infrastructure.Model.LogisticsUser();
                user.DriverId = driver.DriverId;
                user.FirstName = driver.FirstName;
                user.LastName = driver.LastName;
                user.UserName = driver.UserName;
                user.Password = driver.Password;
                user.UserCreatedAt = DateTime.Now;
                user.UserUpdatedAt = DateTime.Now;
                user.UserUpdatedBy = 1;
                user.UserTypeID = 1;
                user.UserIsActive = true;
                lcontext.LogisticsUser.Add(user);
                lcontext.SaveChanges();
            }
        }

        public void TouchCancledAtSL(Touch touch)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var rdTouch = new TouchInformation() { PK_TouchId = touch.TouchId, FK_SLStatus = (int)SLUpdateType.Canceled };

                lcontext.TouchInformation.Attach(rdTouch);

                lcontext.Entry(rdTouch).Property(x => x.FK_SLStatus).IsModified = true;

                lcontext.SaveChanges();
            }
        }

        public void TouchUpdateFromSyncJob(Touch touch, Touch tmpSyncTouch)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                TouchInformation dbTouch = lcontext.TouchInformation.Find(touch.TouchId);

                if (dbTouch != null && dbTouch.PK_TouchId > 0)
                {
                    List<StopInformation> si = dbTouch.StopInformation.ToList();
                    TouchInformationSync touchInfoSync = lcontext.TouchInformationSync.Where(t => t.FK_TouchId == dbTouch.PK_TouchId).FirstOrDefault();

                    if (touchInfoSync == null)
                    {
                        touchInfoSync = new TouchInformationSync();
                        lcontext.TouchInformationSync.Add(touchInfoSync);
                    }

                    touchInfoSync.CreateDate = DateTime.Now;
                    touchInfoSync.FK_TouchId = dbTouch.PK_TouchId;
                    touchInfoSync.FK_LoadId = dbTouch.FK_LoadId;
                    if (tmpSyncTouch.doorposSLupdated == true)
                    {
                        touchInfoSync.DoorToFront = dbTouch.DoorToFront;
                        touchInfoSync.DoorToRear = dbTouch.DoorToRear;
                    }
                    if (!string.IsNullOrEmpty(tmpSyncTouch.StartTime))
                        touchInfoSync.StartTime = dbTouch.StartTime;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.CustomerName) && si.Count > 0)
                        touchInfoSync.CustomerName = si[0].CustomerName;

                    if (tmpSyncTouch.WeightStnReq.HasValue)
                        touchInfoSync.WeightStnReq = dbTouch.WeightStnReq;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.PhoneNumber))
                        touchInfoSync.PhoneNumber = dbTouch.PhoneNumber;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.Instructions))
                        touchInfoSync.Instructions = dbTouch.Instructions;

                    if (tmpSyncTouch.OriginAddress != null && tmpSyncTouch.OriginAddress.ID > 0)
                        touchInfoSync.FK_OriginAddressId = dbTouch.FK_OriginAddressId;

                    if (tmpSyncTouch.DestAddress != null && tmpSyncTouch.DestAddress.ID > 0)
                        touchInfoSync.FK_DestinationAddressId = dbTouch.FK_DestinationAddressId;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.UnitNumber))
                        touchInfoSync.ContainerNo = dbTouch.ContainerNo;

                    //Save updated touch info backup saved
                    lcontext.SaveChanges();



                    //Update updated touch info to original tables
                    if (tmpSyncTouch.doorposSLupdated == true)
                    {
                        dbTouch.DoorToFront = tmpSyncTouch.DoorToFront;
                        dbTouch.DoorToRear = tmpSyncTouch.DoorToRear;
                    }
                    if (!string.IsNullOrEmpty(tmpSyncTouch.StartTime))
                        dbTouch.StartTime = tmpSyncTouch.StartTime;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.CustomerName))
                        si.ForEach(s => s.CustomerName = tmpSyncTouch.CustomerName);

                    if (tmpSyncTouch.WeightStnReq.HasValue)
                        dbTouch.WeightStnReq = tmpSyncTouch.WeightStnReq;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.PhoneNumber))
                        dbTouch.PhoneNumber = tmpSyncTouch.PhoneNumber;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.Instructions))
                        dbTouch.Instructions = tmpSyncTouch.Instructions;

                    if (tmpSyncTouch.OriginAddress != null && tmpSyncTouch.OriginAddress.ID > 0)
                        dbTouch.FK_OriginAddressId = tmpSyncTouch.OriginAddress.ID;

                    if (tmpSyncTouch.DestAddress != null && tmpSyncTouch.DestAddress.ID > 0)
                        dbTouch.FK_DestinationAddressId = tmpSyncTouch.DestAddress.ID;

                    if (!string.IsNullOrEmpty(tmpSyncTouch.UnitNumber))
                        dbTouch.ContainerNo = tmpSyncTouch.UnitNumber;

                    //If the touch is open in local and completed in SL, then this touch and stops needs to completed
                    if (!string.IsNullOrEmpty(tmpSyncTouch.Status) && tmpSyncTouch.Status == "Completed" && dbTouch.Status == "Open")
                    {
                        dbTouch.Status = tmpSyncTouch.Status;
                        if (si != null && si.Count > 0)
                        {
                            si.ForEach(s =>
                            {
                                s.FK_TouchStatus = "Completed";
                                s.ActualStartTime = DateTime.Now;
                                s.ActualEndTime = DateTime.Now;
                            });
                        }
                    }

                    dbTouch.FK_SLStatus = (int)tmpSyncTouch.SLUpdatedStatusType;

                    lcontext.SaveChanges();
                }
            }
        }

        #region Event Log

        public void AddEvent(EventLogEntity evnt)
        {
            using (var lcontact = new RouteOptimzationEntities())
            {
                lcontact.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                EventLogSLSync oEvent = new EventLogSLSync
                {
                    ApplicationName = evnt.ApplicationName,
                    StoreNo = evnt.StoreNo,
                    QORId = evnt.QORID,
                    Description = evnt.Description,
                    Activity = evnt.Activity,
                    IPAddress = evnt.IPAddress,
                    CreateDate = evnt.CreateDate,
                    RefID = evnt.RefID
                };

                lcontact.EventLogSLSync.Add(oEvent);
                lcontact.SaveChanges();
            }
        }

        public void AddStackTrace(SystemMessagesEntity evnt)
        {
            using (var lcontact = new RouteOptimzationEntities())
            {
                lcontact.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                SystemMessages oEvent = new SystemMessages
                {
                    UserId = evnt.UserId,
                    MessageType = evnt.MessageType,
                    MessageBody = evnt.MessageBody,
                    MessageArea = evnt.MessageArea,
                    CreateDate = evnt.CreateDate,
                    CallStack = evnt.CallStack
                };

                lcontact.SystemMessages.Add(oEvent);
                lcontact.SaveChanges();
            }
        }

        #endregion

        #region Markets

        public bool SaveSortedMarketTouchInfo(List<Touch> seqTouches, SiteInfo marketFacilites)
        {
            bool rtFalg = false;

            using (TransactionScope scope = new TransactionScope())
            {
                using (var lcontext = new RouteOptimzationEntities())
                {
                    lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                    int? loadid = 0;
                    foreach (Touch touch in seqTouches)
                    {
                        var otouch = lcontext.TouchInformation.Find(touch.TouchId);
                        string[] compltSkipTouches = { "Completed", "Skipped" };

                        //Check if this touch has address changes
                        //Here we are handling only non grouped touches... anyway grouped touches are hanlding in touchservice level "UpdateGroupTocuhesAddressUpdatedBeforeSave"
                        if (otouch.GroupGUID == null && (otouch.FK_SLStatus == (int)SLUpdateType.OnlyAddressUpdated || otouch.FK_SLStatus == (int)SLUpdateType.AddressAndInfoUpdated))
                        {
                            Touch rtTouch = MapTouchInfoToTouch(otouch, lcontext.Address.Find(otouch.FK_OriginAddressId), lcontext.Address.Find(otouch.FK_DestinationAddressId));

                            foreach (var item in lcontext.StopInformation.Where(s => s.FK_TouchId == otouch.PK_TouchId))
                            {

                                var originAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_OriginAddressId).Single();//(from c in lcontext.Address join atype in lcontext.AddressType on c.FK_AddressTypeId equals atype.PK_AddressTypeId where c.PK_AddressId== item.FK_OriginAddressId.Value select c); //
                                var destAddress = lcontext.Address.Where(x => x.PK_AddressId == item.FK_DestinationAddressId).Single();
                                Stops stop = MapStops(item, originAddress, destAddress, rtTouch);
                                rtTouch.AddStop(stop);
                            }

                            //FetchTouchByTouchId()
                            PR.Entities.Address FacilityAddress = GetSiteInformationBySiteNumber(marketFacilites, rtTouch.GlobalSiteNum).SiteAddress;
                            UpdateStopsBasedonTouchUnGrouping(rtTouch, FacilityAddress);

                            //Check if this touch has address changes
                            foreach (var stop in otouch.StopInformation)
                            {
                                Stops tmpstop = rtTouch.Stops.First(s => s.TouchStopId == stop.Pk_Touch_StopId);

                                if (tmpstop != null)
                                {
                                    stop.FK_OriginAddressId = tmpstop.OriginAddress.ID;
                                    stop.FK_DestinationAddressId = tmpstop.DestinationAddress.ID;
                                    stop.IsRequired = true;
                                }
                            }
                        }

                        if (otouch.FK_SLStatus == (int)SLUpdateType.OnlyAddressUpdated || otouch.FK_SLStatus == (int)SLUpdateType.AddressAndInfoUpdated)
                        {
                            otouch.FK_SLStatus = (int)SLUpdateType.Sync;
                        }

                        ///Since user can reschedule skipped touches, we should update route sequence other wise open touch and completed touches may have same route sequence.
                        otouch.RouteSequence = touch.RouteSequence;
                        if (!compltSkipTouches.Contains(otouch.Status))
                        {
                            otouch.ScheduledDate = touch.ScheduledDate;
                            otouch.FK_TrailerId = touch.TrailerId;

                            foreach (var stop in otouch.StopInformation)
                            {
                                if (!compltSkipTouches.Contains(stop.FK_TouchStatus))
                                {
                                    stop.Sequence = null;
                                    stop.ScheduledStartTime = touch.ScheduledDate;
                                    stop.ScheduledEndTime = touch.ScheduledDate;

                                    stop.EstimatedEndTime = stop.ScheduledStartTime;
                                    stop.EstimatedStartTime = stop.ScheduledEndTime;
                                }
                            }
                        }

                        loadid = loadid.HasValue && loadid.Value > 0 ? loadid : otouch.FK_LoadId;
                    }


                    //We are removing busy touches when use is trying to save touches resequence
                    //we may need to refine this logic to avoid this kind of deletion
                    var busyTouches = lcontext.TouchInformation.Where(t => t.TouchType == "BT" && t.FK_LoadId == loadid);
                    if (busyTouches != null && busyTouches.Count() > 0)
                    {
                        foreach (var busyTouch in busyTouches)
                        {
                            busyTouch.StopInformation.ToList().ForEach(s =>
                            {
                                //Remove all entries in BusyTimeInformation for this stop
                                s.BusyTimeInformation.ToList().ForEach(b => lcontext.BusyTimeInformation.Remove(b));
                                lcontext.StopInformation.Remove(s);
                            });

                            lcontext.TouchInformation.Remove(busyTouch);
                        }
                    }
                    lcontext.SaveChanges();
                }

                scope.Complete();
                scope.Dispose();

                rtFalg = true;
            }

            return rtFalg;
        }

        public List<SiteInfo> GetMarketFacilities(int marketId)
        {
            List<SiteInfo> Facilities = new List<SiteInfo>();
            LocalComponent local = new LocalComponent();
            DataSet dsFacility = local.GetRDFacilitiesDataByMarket(marketId);


            if (dsFacility.Tables.Count > 0 && dsFacility.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow facility in dsFacility.Tables[0].Rows)
                {
                    if (Convert.ToInt32(dsFacility.Tables[0].Rows[0]["StoreType"]) > 1 && Convert.ToInt32(dsFacility.Tables[0].Rows[0]["StatusRowId"]) == 70)
                    {
                        SiteLinkrepository repo = new SiteLinkrepository();
                        SiteInfo site = repo.GetFaciltyInformation(facility["SLLocCode"].ToString());
                        site.GlobalSiteNum = facility["StoreNo"].ToString();
                        site.SiteName = facility["CompDBAName"].ToString();
                        site.MarketId = ((facility["FK_MarketId"] == DBNull.Value) ? (int?)null : (Convert.ToInt32(facility["FK_MarketId"].ToString())));

                        Facilities.Add(site);
                    }
                }
            }

            return Facilities;
        }

        public SiteInfo AddMarketFacilities(SiteInfo site)
        {
            List<SiteInfo> Facilities = new List<SiteInfo>();
            LocalComponent local = new LocalComponent();

            if (site.MarketId.HasValue)
            {
                DataSet dsFacility = local.GetRDFacilitiesDataByMarket(site.MarketId.Value);

                if (dsFacility.Tables.Count > 0 && dsFacility.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow facility in dsFacility.Tables[0].Rows)
                    {
                        if ((facility["StoreNo"] != DBNull.Value && site.GlobalSiteNum != facility["StoreNo"].ToString()) && (facility["StoreType"] != DBNull.Value && Convert.ToInt32(facility["StoreType"]) > 1) && (facility["StatusRowId"] != DBNull.Value && Convert.ToInt32(facility["StatusRowId"]) >= 65))
                        {
                            SiteLinkrepository repo = new SiteLinkrepository();
                            SiteInfo lsite = repo.GetFaciltyInformation(facility["SLLocCode"].ToString());
                            lsite.GlobalSiteNum = facility["StoreNo"].ToString();
                            lsite.SiteName = facility["CompDBAName"].ToString();
                            lsite.MarketId = ((facility["FK_MarketId"] == DBNull.Value) ? (int?)null : (Convert.ToInt32(facility["FK_MarketId"].ToString())));

                            site.MarketFacilities.Add(lsite);
                        }
                    }
                }
            }

            return site;
        }

        private SiteInfo GetSiteInformationBySiteNumber(SiteInfo marketSiteInfo, string siteNumber)
        {
            return (marketSiteInfo.GlobalSiteNum == siteNumber ? marketSiteInfo : marketSiteInfo.MarketFacilities.Where(m => m.GlobalSiteNum == siteNumber).FirstOrDefault());
        }

        #endregion

        #region Exceptional touch handle from Driver App

        public List<string> GetCompletedTouchesOrderNumberFacility(string storeNumber, DateTime date)
        {
            List<string> completedTouches = new List<string>();

            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                completedTouches = (from load in lcontext.LoadInformation
                                    join touch in lcontext.TouchInformation on load.PK_LoadID equals touch.FK_LoadId
                                    join stop in lcontext.StopInformation on touch.PK_TouchId equals stop.FK_TouchId
                                    where (stop.FK_TouchStatus == "Completed" || stop.FK_TouchStatus == "InProgress") &&
                                          (load.StoreNumber == storeNumber) &&
                                          (load.CreatedDate.Value.Year == date.Year && load.CreatedDate.Value.Month == date.Month && load.CreatedDate.Value.Day == date.Day)
                                    select touch.OrderNumber).Distinct().ToList();
            }

            return completedTouches;
        }

        public List<Touch> GetAllTouchesForFacility(string storeNumber, DateTime date)
        {
            List<Touch> assignedTouches = new List<Touch>();

            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var touchesInfo = (from load in lcontext.LoadInformation
                                   join touch in lcontext.TouchInformation on load.PK_LoadID equals touch.FK_LoadId
                                   join stop in lcontext.StopInformation on touch.PK_TouchId equals stop.FK_TouchId
                                   where (load.StoreNumber == storeNumber) &&
                                          (load.CreatedDate.Value.Year == date.Year && load.CreatedDate.Value.Month == date.Month && load.CreatedDate.Value.Day == date.Day)
                                   select touch).ToList();

                foreach (var dbTouch in touchesInfo)
                {
                    if (!assignedTouches.Any(s => s.TouchId == dbTouch.PK_TouchId))
                    {
                        assignedTouches.Add(MapTouchInfoToTouch(dbTouch, lcontext.Address.Find(dbTouch.FK_OriginAddressId), lcontext.Address.Find(dbTouch.FK_DestinationAddressId)));
                    }
                }
            }

            return assignedTouches;
        }

        /// <summary>
        /// This method returns false if this touch is in use or completed then we will not allow the user not to remove from this load
        /// If it returns true, then this touch can be removed from the load
        /// </summary>
        /// <param name="touchId"></param>
        /// <returns></returns>
        public Touch GetRecentlyCompletedTouchFromLoad(int loadId)
        {
            using (RouteOptimzationEntities lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var recentlyCompletedTouchId = (from load in lcontext.LoadInformation
                                                join touch in lcontext.TouchInformation on load.PK_LoadID equals touch.FK_LoadId
                                                join stop in lcontext.StopInformation on touch.PK_TouchId equals stop.FK_TouchId
                                                where (stop.FK_TouchStatus == "Completed" || stop.FK_TouchStatus == "Skipped") && load.PK_LoadID == loadId
                                                orderby stop.Sequence descending, touch.RouteSequence descending
                                                select touch.PK_TouchId).FirstOrDefault();

                if (recentlyCompletedTouchId > 0)
                    return FetchTouchByTouchId(recentlyCompletedTouchId);
                else
                    return null;
            }
        }

        public void SaveTouchRouteSequenct(int loadId, int previousTouchId, int exceptionTouchId, bool isExistingTouch)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                var load = lcontext.LoadInformation.Find(loadId);

                if (load != null && load.PK_LoadID > 0)
                {
                    var previousTouch = load.TouchInformation.Where(t => t.PK_TouchId == previousTouchId).FirstOrDefault();
                    int touchRouteSequence = 0;
                    int stopSequence = 0;

                    if (previousTouch != null && previousTouch.RouteSequence.HasValue)
                    {
                        touchRouteSequence = previousTouch.RouteSequence.Value;
                        stopSequence = previousTouch.StopInformation.Where(s => s.Sequence.HasValue).OrderByDescending(s => s.Sequence).FirstOrDefault().Sequence.Value;
                    }

                    int justCompletedTouchRouteSeq = touchRouteSequence;
                    int justCompletedStopSeq = stopSequence;

                    PRAddressAPI pcmInterface = new PRAddressAPI();

                    //Update sequencet for exception touch
                    var exceptionTouch = load.TouchInformation.Where(t => t.PK_TouchId == exceptionTouchId).First();
                    exceptionTouch.RouteSequence = ++justCompletedTouchRouteSeq;
                    DateTime orgDateTime = exceptionTouch.ScheduledDate.Value;
                    DateTime destDateTime = exceptionTouch.ScheduledDate.Value;
                    exceptionTouch.StopInformation.OrderBy(s => s.Sequence).ToList().ForEach(s =>
                    {
                        //IF exception touch is already exiting touch then need not to calculate distance b/w org and dest locations.
                        if (isExistingTouch == false)
                        {
                            PR.Entities.Address originAdd = MapAddressInfoToAddress(lcontext.Address.Find(s.FK_OriginAddressId.Value));
                            PR.Entities.Address destAdd = MapAddressInfoToAddress(lcontext.Address.Find(s.FK_DestinationAddressId.Value));

                            AddressValidator resultAddressValidator = pcmInterface.GetDistance(originAdd, destAdd);
                            string timeInMins = Regex.Replace(resultAddressValidator.Duration, "[^0-9]+", string.Empty);
                            double dTimeInMins = string.IsNullOrWhiteSpace(timeInMins) ? 0 : Convert.ToDouble(timeInMins);
                            destDateTime = orgDateTime.AddMinutes(dTimeInMins);

                            s.ScheduledStartTime = s.EstimatedStartTime = orgDateTime;
                            s.ScheduledEndTime = s.EstimatedEndTime = destDateTime;

                            //Here 20 minutes are touch handling times
                            orgDateTime = destDateTime.AddMinutes(20);
                        }

                        s.Sequence = ++justCompletedStopSeq;
                    });

                    //Update sequencet for all touches which are falling after exception touch
                    foreach (var touch in load.TouchInformation.Where(t => t.RouteSequence > touchRouteSequence).Where(t => t.PK_TouchId != exceptionTouchId).OrderBy(t => t.RouteSequence))
                    {
                        touch.RouteSequence = ++justCompletedTouchRouteSeq;
                        touch.StopInformation.OrderBy(s => s.Sequence).ToList().ForEach(s => s.Sequence = ++justCompletedStopSeq);
                    }

                    lcontext.SaveChanges();
                }
            }
        }

        public void UpdateGUIDForTouches(List<Touch> lstTouches, Guid guid)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                TouchInformation dbTouch;

                foreach (var touch in lstTouches)
                {
                    dbTouch = lcontext.TouchInformation.Find(touch.TouchId);

                    if (dbTouch != null)
                        dbTouch.GroupGUID = guid;
                }

                lcontext.SaveChanges();
            }
        }

        public void UpdateStartAddressAsFacilityForNextTouchOfExceptionTouch(Touch nextTouch)
        {
            using (var lcontext = new RouteOptimzationEntities())
            {
                lcontext.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

                if (nextTouch.PickupStop.TouchStopId > 0)
                {
                    StopInformation dbStop = lcontext.StopInformation.Find(nextTouch.PickupStop.TouchStopId);
                    dbStop.FK_OriginAddressId = nextTouch.PickupStop.OriginAddress.ID;

                    lcontext.SaveChanges();
                }
            }
        }

        #endregion

        private bool IsStrNotEqual(string toCompare, string comparer)
        {
            return !IsStringsEqual(toCompare, comparer);
        }

        private static bool IsStringsEqual(string toCompare, string comparer)
        {
            toCompare = string.IsNullOrEmpty(toCompare) ? "" : toCompare.Trim().ToLower();
            comparer = string.IsNullOrEmpty(comparer) ? "" : comparer.Trim().ToLower();

            return toCompare == comparer;
        }

        public string CheckDriverLoad(int driverId, DateTime date)
        {
            using (var context = new RouteOptimzationEntities())
            {
                var loadinfo = context.LoadInformation.Where(x => (x.CreatedDate != null && (x.CreatedDate.Value.Year == date.Year &&
                                                                                            x.CreatedDate.Value.Month == date.Month &&
                                                                                            x.CreatedDate.Value.Day == date.Day)) && x.DriverID == driverId).FirstOrDefault();

                if (loadinfo != null)
                {
                    return loadinfo.StoreNumber;
                }
            }

            return string.Empty;
        }
    }
}