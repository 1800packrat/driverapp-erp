//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Infrastructure.Model
{
    using System;

    public partial class T72_tblTripdetails
    {
        public decimal T72_TripdetailsId { get; set; }
        public string T72_T71_TripId { get; set; }
        public decimal T72_T70_LdmId { get; set; }
        public decimal T72_T25_OrderId { get; set; }
        public decimal T72_T26_ContainerId { get; set; }
        public string T72_Type { get; set; }
        public Nullable<decimal> T72_Stop_No { get; set; }
        public Nullable<decimal> T72_Miles { get; set; }
        public Nullable<decimal> T72_Hours { get; set; }
        public Nullable<decimal> T72_Facility { get; set; }
        public Nullable<System.DateTime> T72_Scheduled_Date { get; set; }
        public string T72_Scheduled_time { get; set; }
        public Nullable<System.DateTime> T72_Actual_Date { get; set; }
        public string T72_Actual_time { get; set; }
        public Nullable<System.DateTime> T72_Delay_Date { get; set; }
        public string T72_Delay_time { get; set; }
        public Nullable<short> T72_UnitType { get; set; }
        public Nullable<int> T72_TransportationItemId { get; set; }
    }
}
