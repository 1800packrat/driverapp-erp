//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Infrastructure.Model
{
    using System;
    
    public partial class USP_FA_LL_GetTouches_Result
    {
        public int LoadID { get; set; }
        public Nullable<int> DriverID { get; set; }
        public Nullable<int> TruckID { get; set; }
        public Nullable<System.DateTime> CreatedDate_Load { get; set; }
        public Nullable<bool> IsOptimized { get; set; }
        public string StoreNumber_Load { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public string StoreName_Load { get; set; }
        public Nullable<bool> IsTrailerRequired { get; set; }
        public string DriverName { get; set; }
        public int TouchId { get; set; }
        public string OrderNumber { get; set; }
        public Nullable<int> QORId { get; set; }
        public Nullable<int> SLSeqNo { get; set; }
        public string TouchType { get; set; }
        public string ContainerNo { get; set; }
        public int UnitSize { get; set; }
        public string Status { get; set; }
        public string StartTime { get; set; }
        public Nullable<bool> WeightStnReq { get; set; }
        public string Instructions { get; set; }
        public Nullable<System.DateTime> ScheduledDate_Touch { get; set; }
        public Nullable<bool> DoorToFront { get; set; }
        public Nullable<bool> DoorToRear { get; set; }
        public string PhoneNumber_Touch { get; set; }
        public Nullable<bool> IsTouchStarted { get; set; }
        public Nullable<bool> IsExceptionalTouch { get; set; }
        public Nullable<int> FK_TouchGroupId { get; set; }
        public Nullable<System.Guid> GroupGUID { get; set; }
        public Nullable<int> StarsUnitId { get; set; }
        public string StoreNumber_Touch { get; set; }
        public Nullable<int> RouteSequence_Touch { get; set; }
        public Nullable<decimal> SiteLinkMileage_Touch { get; set; }
        public Nullable<int> SLStatusId { get; set; }
        public Nullable<int> TrailerId { get; set; }
        public Nullable<bool> IsGrouped { get; set; }
        public string StopType { get; set; }
        public int TouchStopId { get; set; }
        public string CustomerName { get; set; }
        public int StopTypeId { get; set; }
        public string TouchStatus { get; set; }
        public bool SkipTouch { get; set; }
        public Nullable<bool> IsRequired { get; set; }
        public int Sequence_Stop { get; set; }
        public Nullable<decimal> ActualDistance_Stop { get; set; }
        public Nullable<System.DateTime> ScheduledStartTime { get; set; }
        public Nullable<System.DateTime> ScheduledEndTime { get; set; }
        public Nullable<System.DateTime> EstimatedStartTime { get; set; }
        public Nullable<System.DateTime> EstimatedEndTime { get; set; }
        public Nullable<System.DateTime> ActualStartTime { get; set; }
        public Nullable<System.DateTime> ActualEndTime { get; set; }
        public int AddressID_TouchOrg { get; set; }
        public string AddressLine1_TouchOrg { get; set; }
        public string AddressLine2_TouchOrg { get; set; }
        public string City_TouchOrg { get; set; }
        public string State_TouchOrg { get; set; }
        public string Zip_TouchOrg { get; set; }
        public string Country_TouchOrg { get; set; }
        public string Latitude_TouchOrg { get; set; }
        public string Longitude_TouchOrg { get; set; }
        public Nullable<int> AddressTypeId_TouchOrg { get; set; }
        public string Company_TouchOrg { get; set; }
        public Nullable<int> FacilityStoreNo_TouchOrg { get; set; }
        public Nullable<bool> IsIntersectingLatLong_TouchOrg { get; set; }
        public string ActualLatitude_TouchOrg { get; set; }
        public string ActualLongitude_TouchOrg { get; set; }
        public int AddressID_TouchDest { get; set; }
        public string AddressLine1_TouchDest { get; set; }
        public string AddressLine2_TouchDest { get; set; }
        public string City_TouchDest { get; set; }
        public string State_TouchDest { get; set; }
        public string Zip_TouchDest { get; set; }
        public string Country_TouchDest { get; set; }
        public string Latitude_TouchDest { get; set; }
        public string Longitude_TouchDest { get; set; }
        public Nullable<int> AddressTypeId_TouchDest { get; set; }
        public string Company_TouchDest { get; set; }
        public Nullable<int> FacilityStoreNo_TouchDest { get; set; }
        public Nullable<bool> IsIntersectingLatLong_TouchDest { get; set; }
        public string ActualLatitude_TouchDest { get; set; }
        public string ActualLongitude_TouchDest { get; set; }
        public int AddressID_StopOrg { get; set; }
        public string AddressLine1_StopOrg { get; set; }
        public string AddressLine2_StopOrg { get; set; }
        public string City_StopOrg { get; set; }
        public string State_StopOrg { get; set; }
        public string Zip_StopOrg { get; set; }
        public string Country_StopOrg { get; set; }
        public string Latitude_StopOrg { get; set; }
        public string Longitude_StopOrg { get; set; }
        public Nullable<int> AddressTypeId_StopOrg { get; set; }
        public string Company_StopOrg { get; set; }
        public Nullable<int> FacilityStoreNo_StopOrg { get; set; }
        public Nullable<bool> IsIntersectingLatLong_StopOrg { get; set; }
        public string ActualLatitude_StopOrg { get; set; }
        public string ActualLongitude_StopOrg { get; set; }
        public int AddressID_StopDest { get; set; }
        public string AddressLine1_StopDest { get; set; }
        public string AddressLine2_StopDest { get; set; }
        public string City_StopDest { get; set; }
        public string State_StopDest { get; set; }
        public string Zip_StopDest { get; set; }
        public string Country_StopDest { get; set; }
        public string Latitude_StopDest { get; set; }
        public string Longitude_StopDest { get; set; }
        public Nullable<int> AddressTypeId_StopDest { get; set; }
        public string Company_StopDest { get; set; }
        public Nullable<int> FacilityStoreNo_StopDest { get; set; }
        public Nullable<bool> IsIntersectingLatLong_StopDest { get; set; }
        public string ActualLatitude_StopDest { get; set; }
        public string ActualLongitude_StopDest { get; set; }
        public string FacilityName_StopOrg { get; set; }
        public string FacilityName_StopDest { get; set; }
        public string AddressType_StopOrg { get; set; }
        public string AddressType_StopDest { get; set; }
        public Nullable<System.TimeSpan> BreakTime { get; set; }
        public string BreakComment { get; set; }
        public Nullable<int> BreakID { get; set; }
        public Nullable<bool> IsZippyShellMove { get; set; }
    }
}
