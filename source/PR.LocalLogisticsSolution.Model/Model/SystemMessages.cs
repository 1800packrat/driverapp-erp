//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Infrastructure.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemMessages
    {
        public int PK_ID { get; set; }
        public int UserId { get; set; }
        public string MessageType { get; set; }
        public string MessageBody { get; set; }
        public string MessageArea { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CallStack { get; set; }
    }
}
