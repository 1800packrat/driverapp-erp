//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PR.LocalLogisticsSolution.Infrastructure.Model
{
    using System;

    public partial class T71_tblTrips
    {
        public decimal T71_TripId { get; set; }
        public string T71_Trip { get; set; }
        public Nullable<decimal> T71_T59_CarrierId { get; set; }
        public Nullable<decimal> T71_LDM_Hours { get; set; }
        public Nullable<decimal> T71_LDM_Miles { get; set; }
        public Nullable<decimal> T71_Penalty_Amount { get; set; }
        public string T71_Delay { get; set; }
        public string T71_Confirmed { get; set; }
        public string T71_Driver { get; set; }
        public Nullable<decimal> T71_Trip_Cost { get; set; }
        public Nullable<System.DateTime> T71_From_Date { get; set; }
        public string T71_From_Time { get; set; }
        public Nullable<System.DateTime> T71_To_Date { get; set; }
        public string T71_To_Time { get; set; }
        public string T71_Notes { get; set; }
        public string T71_Reason_For_Carrier_Selection { get; set; }
        public Nullable<decimal> T71_SalesPerson { get; set; }
        public Nullable<short> T71_Visible { get; set; }
        public Nullable<short> T71_DryVanFlatBed { get; set; }
        public Nullable<byte> T71_isSettled { get; set; }
        public string T71_tripComments { get; set; }
        public Nullable<byte> T71_brokerportaldisable { get; set; }
        public Nullable<System.DateTime> T71_IsSettled_Date { get; set; }
        public string T71_TripType { get; set; }
        public Nullable<decimal> T71_Projected_Revenue { get; set; }
        public Nullable<decimal> T71_BuyItNow { get; set; }
        public Nullable<short> T71_IsInvoiceApproved { get; set; }
        public Nullable<System.DateTime> Create_Date { get; set; }
        public Nullable<System.DateTime> T71_AddedDate { get; set; }
    }
}
