﻿using System;
using System.Linq;
using PR.LocalLogisticsSolution.Interfaces;
using System.Data;
using PR.LocalLogisticsSolution.Infrastructure.Model;


namespace PR.LocalLogisticsSolution.Model
{
    public class StarsRepository : IStarsDbRepository
    {
        public StarsRepository()
        { }

        ////SFERP-TODO-TBD -- Added by Sohan
        //public bool IsQuoteSettled(int orderId)
        //{
        //    using (var context = new StarsdbEntities())
        //    {
        //        context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

        //        return context.T25_tblQuote.Any(t => t.T25_Quote_Id == orderId && t.T25_Settled_DateTime != null);
        //    }
        //}


        ////SFERP-TODO-TBD -- Added by Sohan
        //public void MakeOrderSettle(int OrderId)
        //{
        //    using (var context = new StarsdbEntities())
        //    {
        //        context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

        //        var order = new T25_tblQuote() { T25_Quote_Id = OrderId, T25_Settled_DateTime = DateTime.Now };

        //        context.T25_tblQuote.Attach(order);

        //        context.Entry(order).Property(x => x.T25_Settled_DateTime).IsModified = true; 

        //        context.SaveChanges();
        //    }
        //} 

        ////SFERP-TODO-CTRMV -- Added by Sohan
        //public decimal GetUnitRent(decimal QorId, bool is16FtUnit)
        //{
        //    using (var context = new StarsdbEntities())
        //    {
        //        decimal? unitprice;
        //        context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

        //        if (is16FtUnit == true)
        //            unitprice = context.T86_tblQuoteOthersDetails.Where(t => t.T86_T25_Quote_ID == QorId && t.T86_Others_ID == 14).FirstOrDefault().T86_Price;
        //        else
        //            unitprice = context.T86_tblQuoteOthersDetails.Where(t => t.T86_T25_Quote_ID == QorId && t.T86_Others_ID == 18).FirstOrDefault().T86_Price;


        //        return unitprice ?? (decimal)0.0;
        //    }
        //}
    }
}
