﻿using System;
using PR.Entities;
using PR.UtilityLibrary;
using static PR.UtilityLibrary.ERP_Enums;
using PR.DataHandler;
using System.Data;
using PR.Entities.EsbEntities.UpdateResult;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {
        LocalDataHandler handler = null;
        public SMDBusinessLogic()
        {
            handler = new LocalDataHandler();
        }

        private PR.Entities.EsbEntities.UpdateResult.RootObject UpdateTouchStatus(int qorId, PREnums.TouchTypeShort touchType, int sequenceNo, eQTStatus touchStatus, string activityBy, string unitNo, string touchComments = "")
        {
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            try
            {
                object requestObject = new
                {
                    QorId = qorId,
                    touchTypeId = (int)touchType,
                    sequenceNo = sequenceNo > 0 ? sequenceNo.ToString() : "",
                    touchStatus = (int)touchStatus,
                    updatedBy = activityBy,
                    unitNo = string.IsNullOrWhiteSpace(unitNo) ? "" : unitNo,
                    comments = touchComments
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.UpdateTouchStatus, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "Esb-UpdateTouchStatus response is null", "ESB response is null: " + qorId, qorId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objRoot;
        }

        private bool ValidateEsbResponse(RootObject objRoot)
        {
            if (objRoot != null)
            {
                switch (objRoot.ResponseCode)
                {
                    case "200":
                        return true;
                        //default:
                        //    throw new Exception(objRoot.ResponseMessage);  //SEFRP-TODO-CTUPD commented for testing purpose. 
                }
            }
            return false;
        }

        /// <summary>
        /// DONE
        /// Method to udate touch status to OnHold (using ESB endpoint). TG-700
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="touchType"></param>
        /// <param name="sequenceNo"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool HoldTouch(int qorId, PREnums.TouchTypeShort touchType, int sequenceNo, string activityBy, string unitNo)
        {
            bool isSuccess = false;
            // LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                var objRoot = UpdateTouchStatus(qorId, touchType, sequenceNo, eQTStatus.OnHold, activityBy, unitNo);

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        var touchName = PRHelper.GetSMDTouchTypeDictionary()[(int)touchType];
                        var logText = String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, eQTStatus.OnHold.ToString());
                        handler.InsertActivityLog(qorId, "Touch status updated", logText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "ESB-HoldTouch: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-HoldTouch: Response object is null", "ESB response object is null, QORID: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            /*
             * TODO - Add ESB method call to update touch status. Add Below Log accordingly.
             * handler.InsertActivityLog(QorId, "Touch status updated", touchType.ToString() + " touch status put on hold:" + QorId, _userId);
             * handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch status updated", String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, touchStatus.ToString()), _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapiMobile = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();


            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, QorId, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapiMobile = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, sessionId);

            //    Touches touches = GetAppliedTouches(slapiMobile);

            //    Touches.TouchRow drTouchRow;
            //    switch (touchType)
            //    {
            //        case PREnums.TouchTypeShort.DE:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseToCurbEmpty' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.DF:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseToCurbFull' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.RE:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='ReturnToWarehouseEmpty' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.RF:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='ReturnToWarehouseFull' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.CC:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='CurbToCurb'  AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.TI:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='LDM Transfer In' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.TO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='LDM Transfer Out'  AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.WA:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseAccess' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.OBO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='OutByOwner' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.IBO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='InByOwner' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        default:
            //            throw new Exception("Invalid Touch Edit Identifier.");
            //    }
            //    if (drTouchRow != null && drTouchRow.EditIdentifier > 0)
            //    {
            //        UpdateTouchStatus(eQTStatus.OnHold, drTouchRow.EditIdentifier, slapiMobile);

            //        SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapiMobile);
            //        handler.InsertActivityLog(QorId, "Touch status updated", touchType.ToString() + " touch status put on hold:" + QorId, _userId);

            //        slapiMobile.DisposeSLAPI();
            //        slapiMobile = null;
            //    }
            //    else
            //    {
            //        slapiMobile.DisposeSLAPI();
            //        slapiMobile = null;
            //        local.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", "TouchType is invalid", "TouchType is not valid: " + QorId, QorId);
            //        issuccess = false;

            //        throw new Exception("Invalid Touch Edit Identifier.");
            //    }
            //}

            #endregion

            //return issuccess;

        }

        /// <summary>
        /// DONE
        /// Method to complete IBO, OBO and WH touches (using ESB endpoint) from Facility App Staging touches screen. TG-700
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="touchType"></param>
        /// <param name="sequenceNo"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool CompleteIBOWHOBOExtraTouches(int qorId, string touchType, int sequenceNo, string activityBy, string unitNo)
        {
            bool isSuccess = false;

            #region SFERP-TODO-ESBMTD

            try
            {
                int touchTypeId = PRHelper.GetTouchTypeIdByName()[touchType];

                var objRoot = UpdateTouchStatus(qorId, (PREnums.TouchTypeShort)touchTypeId, sequenceNo, eQTStatus.Completed, activityBy, unitNo);

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        var logText = String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchType, eQTStatus.Completed.ToString());
                        handler.InsertActivityLog(qorId, "Touch status updated", logText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "ESB-CompleteIBOWHOBOExtraTouches: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-CompleteIBOWHOBOExtraTouches: Response object is null", "ESB response object is null, QORID: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Update touch status as Complete using ESB method call based on Touck Key (Qorid_touchType_sequenceNum)
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB methodand testing. */

            // TG-700 , ticket created for ESB endpoint

            /* TODO - Add ESB method call to update touch status. 
             * Add Below Log accordingly.
             * handler.InsertActivityLog(Qorid, "Touch status updated", touchType.ToString() + " touch completed for qorId:" + Qorid, _userId);
             * handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch status updated", String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, touchStatus.ToString()), _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi;
            //LocalComponent local = new LocalComponent();
            //LocalDataHandler handler = new LocalDataHandler();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, Qorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());

            //    Touches touches = GetAppliedTouches(slapi);

            //    if (touches != null && touches.Count() > 0)
            //    {
            //        var extratouch = touches.Touch.Select("ServiceType='" + touchType + "' and SequenceNumber = " + sequenceNum);

            //        if (extratouch != null && extratouch.Count() > 0)
            //        {
            //            Touches.TouchRow drTouchRow = (Touches.TouchRow)extratouch[0];

            //            //If EditIdentifier is > 0 means, it is already completed. dont try to complete same touch one more time it through an exception
            //            if (drTouchRow.EditIdentifier > 0)
            //            {
            //                UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);

            //                SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapi);
            //                handler.InsertActivityLog(Qorid, "Touch status updated", touchType.ToString() + " touch completed for qorId:" + Qorid, _userId);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink GetQORList Error", "GetQORList there are not results for this qor", "GetQORList there are not results for this qor in CompleteIBOWHOBOExtraTouches method", Qorid);
            //    throw new Exception("Invalid QORId.");
            //}

            #endregion
        }

        /// <summary>
        /// DONE
        /// Method to complete IBO, OBO and WH extra touches (using ESB endpoint) from Facility App Staging touches screen. TG-700
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="touchType"></param>
        /// <param name="sequenceNo"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool CompleteIBOWHOBHExtraTouches(int qorId, string touchType, int sequenceNo, string activityBy, string unitNo)
        {
            bool isSuccess = false;
            // LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                int touchTypeId = PRHelper.GetTouchTypeIdByName()[touchType];

                var objRoot = UpdateTouchStatus(qorId, (PREnums.TouchTypeShort)touchTypeId, sequenceNo, eQTStatus.Completed, activityBy, unitNo);

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        var logText = String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchType, eQTStatus.Completed.ToString());
                        handler.InsertActivityLog(qorId, "Touch status updated", logText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompleteIBOWHOBHExtraTouches: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.CompleteIBOWHOBHExtraTouches: Response is object null", "ESB response object is null, QORID: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Complete touch using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // TG-700 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to complete touch.  SFERP-TBD  
             * UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);
             * handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch status updated", String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, touchStatus.ToString()), _userId);
               handler.InsertActivityLog(Qorid, "Touch status updated", touchType.ToString() + " touch completed for qorId:" + Qorid, _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi;
            //LocalComponent local = new LocalComponent();
            //LocalDataHandler handler = new LocalDataHandler();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, Qorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());

            //    Touches touches = GetAppliedTouches(slapi);

            //    if (touches != null && touches.Count() > 0)
            //    {
            //        var extratouch = touches.Touch.Select("ServiceType='" + touchType + "' and SequenceNumber = " + sequenceNum);

            //        if (extratouch != null && extratouch.Count() > 0)
            //        {
            //            Touches.TouchRow drTouchRow = (Touches.TouchRow)extratouch[0];

            //            //If EditIdentifier is > 0 means, it is already completed. dont try to complete same touch one more time it through an exception
            //            if (drTouchRow.EditIdentifier > 0)
            //            {
            //                UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);

            //                SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapi);
            //                handler.InsertActivityLog(Qorid, "Touch status updated", touchType.ToString() + " touch completed for qorId:" + Qorid, _userId);
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink GetQORList Error", "GetQORList there are not results for this qor", "GetQORList there are not results for this qor in CompleteIBOWHOBHExtraTouches method", Qorid);
            //    throw new Exception("Invalid QORId.");
            //}

            #endregion
        }

        /// <summary>
        /// DONE
        /// Method to schedule a touch (using ESB endpoint) from Facility App. TG-701
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="touchType"></param>
        /// <param name="sequenceNo"></param>
        /// <param name="scheduleDate"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool ScheduleTouch(int qorId, PREnums.TouchTypeShort touchType, int sequenceNo, DateTime scheduleDate, string touchTime, string activityBy)
        {
            bool isSuccess = false;
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            //LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                object requestObject = new
                {
                    QorId = qorId,
                    touchTypeId = (int)touchType,
                    sequenceNo = sequenceNo,
                    scheduleDate = scheduleDate,
                    touchTime = touchTime,
                    updatedBy = activityBy
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.UpdateTouchSchedule, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "Esb-UpdateTouchSchedule Response is null", "ESB response is null: " + qorId, qorId);
                    return false;
                }

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        var touchName = PRHelper.GetSMDTouchTypeDictionary()[(int)touchType];

                        //check if touch is un-scheduled
                        var activityText = scheduleDate == DateTime.Parse("1/1/1753")
                                ? String.Format("Touch of type {0} has been un-scheduled", touchName)
                                : String.Format("Touch of type {0} has been scheduled", touchName);

                        handler.InsertActivityLog(qorId, "Touch scheduled", activityText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.ScheduleTouch: " + objRoot.ResponseCode, objRoot.ResponseMessage, "Not able to schedule touch '" + touchType.ToString() + "'", qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.ScheduleTouch response object is null", "ESB response object is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD

            /* 
             * Added by Sohan
             * Schedule Touch using ESB method call based on Touck Key (Qorid_touchType_sequenceNum)
             * Comment below section to remove SL dlls and build the code.
             * Remove all below section after adding ScheduleTouch ESB method and testing.
            */

            // TG-701 , ticket created for ESB endpoint

            /*
             * TODO - Add ESB method call to ScheduleTouch. Add Below Log accordingly.
             * TODO - Get information about Start and end time to update for Scheduling the touch.
             * TODO - Get Starts and End columsn/values for time about touch. -- TBD
             * TODO - ScheduleTouch(scheduleDate, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapiMobile);  // For Input Parameters
             * 
             * //IF TOUCH SCHEDULE DATE IS 1753 THEN IT MEANS USER IS TRYIGN TO UNSCHEDULE TOUCH
             * ScheduleTouch(scheduleDate, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapiMobile);
                bool istouchUnschedule = scheduleDate == DateTime.Parse("1/1/1753");
                string ActivityText = string.Empty;
                if (istouchUnschedule)
                    ActivityText = String.Format("Touch of type {0} has been un-scheduled", touchName);
                else
                    ActivityText = String.Format("Touch of type {0} has been scheduled", touchName);

                handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch scheduled", ActivityText, _userId);
             * handler.InsertActivityLog(QorId, "Touch status updated", touchType.ToString() + " touch status put on hold:" + QorId, _userId);
             * handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch status updated", String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, touchStatus.ToString()), _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapiMobile = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();
            ////bool issuccess = true;

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, QorId, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapiMobile = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, sessionId);

            //    Touches touches = GetAppliedTouches(slapiMobile);

            //    Touches.TouchRow drTouchRow;
            //    switch (touchType)
            //    {
            //        case PREnums.TouchTypeShort.DE:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseToCurbEmpty' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.DF:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseToCurbFull' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.RE:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='ReturnToWarehouseEmpty' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.RF:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='ReturnToWarehouseFull' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.CC:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='CurbToCurb'  AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.TI:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='LDM Transfer In' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.TO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='LDM Transfer Out'  AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.WA:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='WarehouseAccess' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.OBO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='OutByOwner' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        case PREnums.TouchTypeShort.IBO:
            //            drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='InByOwner' AND SequenceNumber =" + sequenceNo)[0];
            //            break;
            //        default:
            //            throw new Exception("Invalid Touch Edit Identifier.");

            //    }
            //    if (drTouchRow != null && drTouchRow.EditIdentifier > 0)
            //    {
            //        string starts = drTouchRow["Starts"].ToString();
            //        string end = drTouchRow["End"].ToString();
            //        int startTimeID = GetScheduleTime(starts, slapiMobile);
            //        int endTimeID = GetScheduleTime(end, slapiMobile);
            //        ScheduleTouch(scheduleDate, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapiMobile);

            //        SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapiMobile);

            //        slapiMobile.DisposeSLAPI();
            //        slapiMobile = null;
            //    }
            //    else
            //    {
            //        slapiMobile.DisposeSLAPI();
            //        slapiMobile = null;
            //        local.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", "TouchType is invalid", "TouchType is not valid: " + QorId, QorId);
            //        issuccess = false;

            //        throw new Exception("Invalid Touch Edit Identifier.");
            //    }
            //}

            #endregion

            //return issuccess;
        }

        /// <summary>
        /// Method to update touch Data like Address, Instructions (using ESB endpoint). TG-718
        /// </summary>
        /// <param name="destQorid"></param>
        /// <param name="unitName"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool UpdateTouchData(int qorId, Address startAddress, Address toAddress, string instructions, eQTType touchType, int iSequenceNum, bool updateStartAddress, string activityBy)
        {
            bool isSuccess = false;
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            //LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                updateStartAddress = touchType == eQTType.WarehouseToCurbEmpty || updateStartAddress;

                object requestObject = GetTouchDataUpdateRequest(qorId, startAddress, toAddress, instructions, touchType, iSequenceNum, updateStartAddress, activityBy);

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.UpdateTouchData, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-UpdateTouchData response is null", "ESB response is null: " + qorId, qorId);
                    return false;
                }

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        handler.InsertActivityLog(qorId, "Touch updated", String.Format("Touch with type={0} is updated", touchType.ToString()), _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.UpdateTouchData: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.UpdateTouchData response is null", "ESB response is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Update touch address and Instructions using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // TG-718 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to update touch details.  SFERP-TBD 
                1. SPF-check for the revenue and other rental data updates?
                    SaveQORSLRtMsg(string corpCode, string locationCode, string userName, string password, SLAPIMobileTemp slapiMobile)
                    usp_Update_PRGQuoteorderLog_Price
                    GetPricingInfoData(SLAPIMobileTemp slapiMobile)
                    UpdateRevenueDataInQuoteOrderLog(int qorId, decimal dueAtDelivery, decimal futureTransportationCharge, decimal recurringCharge) 
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapiMobile = null;

            //try
            //{
            //    ///Get TenantID and qtRentalId to update touches
            //    var dsQORList = GetQORList(CorpCode, string.Empty, UserName, Password, 0, string.Empty, QORID, string.Empty);

            //    if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //    {
            //        int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //        //int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //        int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //        string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //        slapiMobile = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, Guid.NewGuid().ToString());

            //        var touches = GetAppliedTouches(slapiMobile);

            //        ///Get touch from touch list and update it
            //        var touch = touches.Touch.Where(t => t.ServiceType.Equals(serviceType.ToString(), StringComparison.OrdinalIgnoreCase) && t.SequenceNumber == iSequenceNum).Single();
            //        var dsTouch = new DataSet();
            //        ///What if EditiIndentifier has -999. this should through exception and exit from the updates? 
            //        if (touch != null && touch.EditIdentifier > 0)
            //        {

            //            if (startAddress != null)
            //            {
            //                if (serviceType == eQTType.WarehouseToCurbEmpty || updateStartAddress)
            //                {
            //                    dsTouch = slapiMobile.P_TouchFromAddressUpdate(touch.EditIdentifier, string.Empty, startAddress.FirstName, string.Empty, startAddress.LastName, startAddress.Company, startAddress.AddressLine1, startAddress.AddressLine2, startAddress.City,
            //                        startAddress.State, startAddress.Zip, startAddress.PhoneNumber, startAddress.MobilePhoneNumber, String.Empty, String.Empty, string.Empty);
            //                }
            //            }
            //            if (toAddress != null)
            //            {
            //                dsTouch = slapiMobile.P_TouchToAddressUpdate(touch.EditIdentifier, String.Empty, toAddress.FirstName, String.Empty, toAddress.LastName, toAddress.Company,
            //                                        toAddress.AddressLine1, toAddress.AddressLine2, toAddress.City, toAddress.State, toAddress.Zip,
            //                                        toAddress.PhoneNumber, toAddress.AlternatePhoneNumber, toAddress.MobilePhoneNumber, String.Empty, String.Empty);
            //            }

            //            if (instructions != String.Empty)
            //            {
            //                dsTouch = slapiMobile.P_TouchInstructionsUpdate(touch.EditIdentifier, instructions);
            //            }
            //        }
            //        else
            //        {
            //            throw new Exception(string.Format("QorID: {0}, TouchType: {1} has invalid edit identified. EditIdentifier-{2}; ", QORID, serviceType.ToString(), touch.EditIdentifier));
            //        }
            //        SaveQORSLRtMsg(CorpCode, locationCode, UserName, Password, slapiMobile);
            //    }
            //}
            //finally
            //{
            //    if (slapiMobile != null)
            //    {
            //        slapiMobile.DisposeSLAPI();
            //        slapiMobile = null;
            //    }
            //}

            #endregion

            //return UpdateStatus;
        }

        private static Entities.EsbEntities.UpdateTouchDataRequest GetTouchDataUpdateRequest(int QORID, Address fromAddress, Address toAddress, string instructions, eQTType touchType, int iSequenceNum, bool updateFromAddress, string activityBy)
        {
            Entities.EsbEntities.UpdateTouchDataRequest touchDataRequest = new Entities.EsbEntities.UpdateTouchDataRequest
            {
                qorId = QORID,
                touchTypeId = (int)touchType,
                sequenceNo = iSequenceNum,
                instructions = instructions,
                updatedBy = activityBy,
                updateFromAddress = false,
                updateToAddress = false
            };

            if (updateFromAddress && fromAddress != null)
            {
                touchDataRequest.updateFromAddress = updateFromAddress;
                touchDataRequest.FromAddress_FirstName = fromAddress.FirstName;
                touchDataRequest.FromAddress_LastName = fromAddress.LastName;
                touchDataRequest.FromAddress_Company = fromAddress.Company;
                touchDataRequest.FromAddress_AddressLine1 = fromAddress.AddressLine1 + (!String.IsNullOrWhiteSpace(fromAddress.AddressLine2) ? " " + fromAddress.AddressLine2 : "");
                touchDataRequest.FromAddress_AddressLine2 = fromAddress.AddressLine2;  // Salesforce don't have AddressLine2 filed in DB
                touchDataRequest.FromAddress_City = fromAddress.City;
                touchDataRequest.FromAddress_State = fromAddress.State;
                touchDataRequest.FromAddress_Zip = fromAddress.Zip;
                touchDataRequest.FromAddress_PhoneNumber = fromAddress.PhoneNumber;
                touchDataRequest.FromAddress_MobilePhoneNumber = fromAddress.MobilePhoneNumber;
                touchDataRequest.FromAddress_AlternatePhoneNumber = fromAddress.MobilePhoneNumber; // As per SL update
            }

            if (toAddress != null)
            {
                touchDataRequest.updateToAddress = true;
                touchDataRequest.ToAddress_FirstName = toAddress.FirstName;
                touchDataRequest.ToAddress_LastName = toAddress.LastName;
                touchDataRequest.ToAddress_Company = toAddress.Company;
                touchDataRequest.ToAddress_AddressLine1 = toAddress.AddressLine1 + (!String.IsNullOrWhiteSpace(toAddress.AddressLine2) ? " " + toAddress.AddressLine2 : "");
                touchDataRequest.ToAddress_AddressLine2 = toAddress.AddressLine2; // Salesforce don't have AddressLine2 filed in DB
                touchDataRequest.ToAddress_City = toAddress.City;
                touchDataRequest.ToAddress_State = toAddress.State;
                touchDataRequest.ToAddress_Zip = toAddress.Zip;
                touchDataRequest.ToAddress_PhoneNumber = toAddress.PhoneNumber;
                touchDataRequest.ToAddress_MobilePhoneNumber = toAddress.MobilePhoneNumber;
                touchDataRequest.ToAddress_AlternatePhoneNumber = toAddress.AlternatePhoneNumber; // As per SL update
            }

            return touchDataRequest;
        }

        /// <summary>
        /// DONE
        /// Method to complete CC touch (using ESB endpoint). TG-707
        /// </summary>
        /// <param name="destQorid"></param>
        /// <param name="unitName"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool CompleteCCTouchForBillingQor(int qorId, string activityBy)
        {
            // Note: qorId is billingQorId here.

            bool isSuccess = false;
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                object requestObject = new
                {
                    billingQorId = qorId,
                    touchTypeId = (int)eQTType.CurbToCurb,
                    scheduleDate = DateTime.Now,
                    touchTime = "Anytime",
                    updatedBy = activityBy
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.CompleteCCTouch, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-CompleteCCTouch response is null", "ESB response is null: " + qorId, qorId);
                    return false;
                }

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        handler.InsertActivityLog(qorId, "Touch scheduled", String.Format("Touch of type {0} has been scheduled", eQTType.CurbToCurb.ToString()), _userId);

                        var logText = String.Format("Touch status has been updated for the touch = {0} to the status = {1}", eQTType.CurbToCurb, eQTStatus.Completed.ToString());
                        handler.InsertActivityLog(qorId, "Touch status updated", logText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompleteCCTouchForBillingQor: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.CompleteCCTouchForBillingQor response object is null", "ESB response object is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompleteCCTouchForBillingQor-General Exception", ex.Message, ex.StackTrace, qorId);
                var exp = ex.Message;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Schedule Touch and update date and time - TBD.
             * Complete CC touch using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // Complete TI Touch Here, call ESB metod to update tocuh completions.
            //Input Paramater: billingQorid, touchType: eQTType.CurbToCurb ; 
            //Output Parameter: Void

            // TG-707 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to update touch status.  SFERP-TBD  
             *   int startTimeID = GetScheduleTime("Anytime", slapi);
                int endTimeID = GetScheduleTime("Anytime", slapi);
                ScheduleTouch(DateTime.Now, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapi);

                UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);
                handler.InsertActivityLog(GetGlobalQORID(slapiMobile), "Touch status updated", String.Format("Touch status has been updated for the touch = {0} to the status = {1}", touchName, touchStatus.ToString()), _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.


            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, billingQorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, sessionId);

            //    Touches touches = GetAppliedTouches(slapi);

            //    if (touches != null && touches.Count() > 0)
            //    {
            //        Touches.TouchRow drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + eQTType.CurbToCurb.ToString() + "'")[0];

            //        if (drTouchRow != null && drTouchRow.EditIdentifier > 0)
            //        {
            //            int startTimeID = GetScheduleTime("Anytime", slapi);
            //            int endTimeID = GetScheduleTime("Anytime", slapi);
            //            ScheduleTouch(DateTime.Now, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapi);

            //            UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);

            //            SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapi);
            //            //handler.InsertActivityLog(billingQorid, "Touch status updated", "Billing Qor CC touch completed and qorId:" + billingQorid, _userId);                        
            //        }
            //    }
            //}
            //else
            //{
            //    var ex = new Exception("Invalid QORId.");
            //    local.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, ex.Message, ex.StackTrace.ToString(), Convert.ToInt32(billingQorid));
            //    throw ex;
            //}

            #endregion
        }

        /// <summary>
        /// Method to complete warehouse touch (using ESB endpoint). TG-719
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool CompleteWareHouseTouch(int qorId, string activityBy, string unitNo)
        {
            bool isSuccess = false;
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;

            #region SFERP-TODO-ESBMTD

            try
            {
                PREnums.TouchTypeShort touchType = (PREnums.TouchTypeShort)(int)eQTType.WarehouseAccess;

                UpdateTouchStatus(qorId, touchType, -1, eQTStatus.Completed, activityBy, unitNo);

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        var logText = String.Format("Touch status has been updated for the touch = {0} to the status = {1}", eQTType.WarehouseAccess.ToString(), eQTStatus.Completed.ToString());
                        handler.InsertActivityLog(qorId, "Touch status updated", logText, _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "ESB-CompleteWareHouseTouch: " + objRoot.ResponseCode, objRoot.ResponseMessage, "QorId: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-CompleteWareHouseTouch: Response object is null", "ESB response object is null, QORID: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompleteWareHouseTouch-General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Complete touch using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // TG-719 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to MoveOut Unit from touch.  SFERP-TBD  
             *  int startTimeID = GetScheduleTime("Anytime", slapi);
                        int endTimeID = GetScheduleTime("Anytime", slapi);
                        if (initialTouchDate == null)
                            initialTouchDate = DateTime.Now;

               ScheduleTouch(initialTouchDate.Value, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapi); 
                UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);
               handler.InsertActivityLog(dummyBillingQorid, "Touch status updated", "WareHouse touch completed for dummy billing qorId:" + dummyBillingQorid, _userId);
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, dummyBillingQorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());

            //    Touches touches = GetAppliedTouches(slapi);
            //    //Touches.TouchRow drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + eQTType.WarehouseAccess.ToString() + "'")[0];

            //    if (touches != null && touches.Count() > 0)
            //    {
            //        var warehousetouch = touches.Touch.Select("ServiceType='" + eQTType.WarehouseAccess.ToString() + "'");

            //        if (warehousetouch != null && warehousetouch.Count() > 0)
            //        {
            //            Touches.TouchRow drTouchRow = (Touches.TouchRow)warehousetouch[0];

            //            int startTimeID = GetScheduleTime("Anytime", slapi);
            //            int endTimeID = GetScheduleTime("Anytime", slapi);
            //            if (initialTouchDate == null)
            //                initialTouchDate = DateTime.Now;

            //            ScheduleTouch(initialTouchDate.Value, startTimeID, endTimeID, drTouchRow.EditIdentifier, slapi);

            //            UpdateTouchStatus(eQTStatus.Completed, drTouchRow.EditIdentifier, slapi);
            //        }
            //    }

            //    SaveQORNew(CorpCode, UserName, Password, locationCode, tenantId, slapi);
            //    handler.InsertActivityLog(dummyBillingQorid, "Touch status updated", "WareHouse touch completed for dummy billing qorId:" + dummyBillingQorid, _userId);
            //}
            //else
            //{
            //    local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink GetQORList Error", "GetQORList there are not results for this qor", "GetQORList there are not results for this qor in CompleteWareHouseTouch method", dummyBillingQorid);
            //    throw new Exception("Invalid QORId.");
            //}

            #endregion

        }

        /// <summary>
        /// Method to complete TransferIn/MoveIn (using ESB endpoint). TG-704 - Pending
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="unitName"></param>
        /// <param name="activityBy"></param>
        /// <param name="unitId"></param>
        /// <param name="starsId"></param>
        /// <returns></returns>
        public bool CompletTIAndMoveInTITouch(int qorId, string unitName, string activityBy, int unitId, int starsId)
        {
            bool isSuccess = false;
            LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                //// Need to add origin amd orderid param in this method definition.
                //object requestObject = new
                //{
                //    destQorid = qorId,
                //    orderId = starsId,
                //    starsUnitId = unitId,
                //    updatedBy = activityBy
                //};

                PREnums.TouchTypeShort touchType = (PREnums.TouchTypeShort)(int)eQTType.LDMTransferIn;

                var objRoot = UpdateTouchStatus(qorId, touchType, -1, eQTStatus.Completed, activityBy, unitName);


                //string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                //var response = ExecuteMethodESB(EsbMethod.CompleteTransferInMoveIn, requestJson);

                //if (!string.IsNullOrWhiteSpace(response))
                //{
                //    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                //    ValidateEsbResponse(objRoot);
                //}
                //else
                //{
                //    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "Esb-CompletTIAndMoveInTITouch response is null", "ESB response is null: " + qorId, qorId);
                //    return false;
                //}

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        handler.InsertActivityLog(qorId, "MoveIn", "Destination QORId has been moved in successfully", _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompletTIAndMoveInTITouch: " + objRoot.ResponseCode, objRoot.ResponseMessage, "destQorid: " + qorId, qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.CompletTIAndMoveInTITouch response object is null", "ESB response object is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompletTIAndMoveInTITouch-General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Complete touch using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // TG-704 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to complete touch.  SFERP-TBD  
             * MoveInTransferInTouch(unitName, slapi, sessionId, destQorid);
                handler.InsertActivityLog(destQorid, "MoveIn", "Destination QORId has been moved in successfully", _userId); 
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, destQorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());

            //    MoveInTransferInTouch(unitName, slapi, sessionId, destQorid);
            //    handler.InsertActivityLog(destQorid, "MoveIn", "Destination QORId has been moved in successfully", _userId);

            //}
            //else
            //{
            //    var ex = new Exception("Invalid QORId.");
            //    local.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, ex.Message, ex.StackTrace.ToString(), Convert.ToInt32(destQorid));
            //    throw ex;
            //}

            #endregion
        }

        /// <summary>
        /// DONE
        /// Method to complete TransferOut/MoveOut (using ESB endpoint). TG-705
        /// </summary>
        /// <param name="qorId"></param> 
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool CompletTOAndMoveOutTOTouch(int qorId, string activityBy, string unitName)
        {
            // Note qorId is Origin QorId here.
            bool isSuccess = false;
            #region SFERP-TODO-ESBMTD

            try
            {
                PREnums.TouchTypeShort touchType = (PREnums.TouchTypeShort)(int)eQTType.LDMTransferOut;

                var objRoot = UpdateTouchStatus(qorId, touchType, -1, eQTStatus.Completed, activityBy, unitName);

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        handler.InsertActivityLog(qorId, qorId, "Touch status updated", "LDM Transfer Out touch completed for origin qorId:" + qorId, _userId, string.Empty, 0);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.CompleteTransferOutMoveOut: " + objRoot.ResponseCode, objRoot.ResponseMessage, "Not able to complete 'Transfer Out touch'", qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.CompleteTransferOutMoveOut response object is null", "ESB response object is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return isSuccess;

            #endregion

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Update touch status as Complete using ESB method call based on Touck Key (Qorid_touchType_sequenceNum)
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB methodand testing. */

            // TG - 705 , ticket created for ESB endpoint

            /* 
             * TODO - Add ESB method call to complete TO touch. 
             *  //Check other than TO touch, any other touches are not completed 
                //If so then through an exception for not to complete / move-out
                var nonTOTouchNotCompleted = touches.Touch.Select("ServiceType <> 'LDM Transfer Out' and (Status = 'Scheduled' or Status = 'Open' or Status = 'OnHold')");
                UpdateTouchStatus(eQTStatus.Completed, drTouchRowReqType.EditIdentifier, slapi); 
               
                slapiMobile.L_MoveOut(CorpCode, locationCode, this.UserName, this.Password, unitId);
                MoveOut(locationCode, unitId, slapi);
                //Add Unit to Destination Side -- TBD
                handler.InsertActivityLog(sLDMOrderNum, orgQorid, "Move Out", "LDM Transfer Out touch completed for origin qorId:" + orgQorid, _userId, string.Empty, 0);

                SLAPIMobileTemp.L_UnitDelete(corpCode, locationCode, userName, password, origUnitId); -- SFERP-TBD
                handler.InsertActivityLog(0, "Unit Deleted", String.Format("Unit deleted, unitId={0}", origUnitId), _userId);
                handler.InsertActivityLog(sLDMOrderNum, orgQorid, "Unit Deleted", "Unit has been deleted from origin, Unit Name:" + unitName, _userId, string.Empty, 0);
 
             * Add Below/Above Log accordingly.
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //LocalDataHandler handler = new LocalDataHandler();
            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, orgQorid, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    int tenantId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["TenantID"]);
            //    int unitId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["UnitID"]);
            //    int qtRentalId = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["QTRentalID"]);
            //    string locationCode = dsQORList.Tables[0].Rows[0]["sLocationCode"].ToString();
            //    string unitName = dsQORList.Tables[0].Rows[0]["sUnitName"].ToString();
            //    int sLDMOrderNum = Convert.ToInt32(dsQORList.Tables[0].Rows[0]["sLDMOrderNum"]);

            //    slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());

            //    Touches touches = GetAppliedTouches(slapi);

            //    //Check other than TO touch, any other touches are not completed 
            //    //If so then through an exception for not to complete / move-out

            //    var nonTOTouchNotCompleted = touches.Touch.Select("ServiceType <> 'LDM Transfer Out' and (Status = 'Scheduled' or Status = 'Open' or Status = 'OnHold')");

            //    if (nonTOTouchNotCompleted != null && nonTOTouchNotCompleted.Length > 0)
            //    {
            //        local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink Error", "Not able to complete 'Transfer Out touch'", "Not able to complete 'Transfer Out touch'", orgQorid);
            //        throw new Exception("Before you complete Transfer Out touch, all other previous touches needs to be completed.");
            //    }
            //    else
            //    {
            //        //Touches.TouchRow drTouchRow = (Touches.TouchRow)touches.Touch.Select("ServiceType='" + eQTType.LDMTransferOut.ToString() + "'")[0];
            //        var drTouchRow = touches.Touch.Select("ServiceType='LDM Transfer Out'");
            //        if (drTouchRow != null && drTouchRow.Length > 0)
            //        {
            //            Touches.TouchRow drTouchRowReqType = (Touches.TouchRow)drTouchRow[0];

            //            if (drTouchRowReqType.EditIdentifier > 0)
            //            {
            //                UpdateTouchStatus(eQTStatus.Completed, drTouchRowReqType.EditIdentifier, slapi);

            //                SaveQOR(CorpCode, UserName, Password, locationCode, slapi);
            //                slapi.DisposeSLAPI();
            //                handler.InsertActivityLog(sLDMOrderNum, orgQorid, "Touch status updated", "LDM Transfer Out touch completed for origin qorId:" + orgQorid, _userId, string.Empty, 0);
            //            }

            //            slapi = EditQuote(CorpCode, locationCode, UserName, Password, qtRentalId, tenantId, (new Guid()).ToString());
            //            MoveOut(locationCode, unitId, slapi);
            //            handler.InsertActivityLog(sLDMOrderNum, orgQorid, "Move Out", "LDM Transfer Out touch completed for origin qorId:" + orgQorid, _userId, string.Empty, 0);

            //            DeleteUnitForLDM(CorpCode, UserName, Password, locationCode, unitName);
            //            handler.InsertActivityLog(sLDMOrderNum, orgQorid, "Unit Deleted", "Unit has been deleted from origin, Unit Name:" + unitName, _userId, string.Empty, 0);
            //        }
            //        else
            //        {
            //            local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink Error", "Not able to complete 'Transfer Out touch'", "Not able to complete 'Transfer Out touch'", orgQorid);
            //            throw new Exception("Error occurred while finding Transfer Out Touch for this order, Not able to complete 'Transfer Out touch'");
            //        }
            //    }
            //}
            //else
            //{
            //    local.InsertPRErrorLog(DateTime.Now, string.Empty, "SiteLink GetQORList Error", "GetQORList there are not results for this qor", "GetQORList there are not results for this qor in CompletTOAndMoveOutTOTouch method", orgQorid);
            //    throw new Exception("Invalid QORId.");
            //}

            #endregion
        }

        /// <summary>
        /// Method to assign touch to Drive and add into Salesforce Database (using ESB endpoint). TG-729 - Pending
        /// </summary>
        /// <param name="date"></param>
        /// <param name="touch"></param>
        /// <param name="driverID"></param>
        /// <param name="activityBy"></param>
        /// <returns></returns>
        public bool UpdateTouchAssignment(DateTime assignDate, SLTouch touch, int driverID, string activityBy)
        {
            bool isSuccess = false;
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            LocalDataHandler handler = new LocalDataHandler();

            #region SFERP-TODO-ESBMTD

            try
            {
                var driverName = "";
                DataSet dsDriver = null;

                var touchTypeShort = touch.GetTouchType(touch.TouchType);
                int qtTypeId = PRHelper.GetTouchTypeIdByShortName()[touch.TouchType];
                var driverAssignDate = (assignDate == DateTime.MinValue || driverID <= 0) ? "" : assignDate.ToString();

                if (driverID > 0)
                {
                    dsDriver = handler.GetLoadDriverDetails(touch.Qorid, touchTypeShort, touch.SequenceNO);
                    driverName = dsDriver.Tables[0].Rows[0]["DriverName"].ToString();
                }

                // Need to add origin amd orderid param in this method definition.
                object requestObject = new
                {
                    QorId = touch.Qorid,
                    touchTypeId = qtTypeId,
                    sequenceNo = touch.SequenceNO,
                    assignedDriver = driverName,
                    assignedDate = driverAssignDate,
                    updatedBy = activityBy
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.UpdateTouchAssignment, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "Esb-UpdateTouchAssignment response is null", "ESB response is null: " + touch.Qorid, touch.Qorid);
                    return false;
                }

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        handler.InsertActivityLog(touch.Qorid, (driverID > 0 ? "Touch Assigned" : "Touch UnAssigned"), "Touch has been assigned to Driver successfully", _userId);
                    }
                    else
                    {
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.UpdateTouchAssignment: " + objRoot.ResponseCode, objRoot.ResponseMessage, "destQorid: " + touch.Qorid, touch.Qorid);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.UpdateTouchAssignment response object is null", "ESB response object is null: " + touch.Qorid, touch.Qorid);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.UpdateTouchAssignment-General Exception", ex.Message, ex.StackTrace, touch.Qorid);
                throw ex;
            }

            return isSuccess;

            #endregion
        }

        /// <summary>
        /// DONE
        /// Method to schedule a touch (using ESB endpoint) from Facility App. TG-701
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="starsId"></param>
        /// <param name="unitId"></param>
        /// <param name="activityBy"></param>
        /// <param name="oUnitName"></param>
        /// <returns></returns>
        public PREnums.MoveInStatus AddUnitCompletTIAndMoveInTITouch111(int qorId, string starsId, int unitId, string activityBy, out string oUnitName)
        {
            oUnitName = string.Empty;
            // Note qorId is destination QorId here. 
            PR.Entities.EsbEntities.UpdateResult.RootObject objRoot = null;
            //LocalDataHandler handler = new LocalDataHandler();

            PREnums.MoveInStatus oMoveInStatus = PREnums.MoveInStatus.GeneralError;

            #region SFERP-TODO-ESBMTD

            try
            {
                // Need to add origin amd orderid param in this method definition.
                object requestObject = new
                {
                    destQorid = qorId,
                    orderId = starsId,
                    starsUnitId = unitId,  // TBD-need to discuss with Carl to be used or not.
                    updatedBy = activityBy
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.CompleteTransferInMoveIn, requestJson);

                if (!string.IsNullOrWhiteSpace(response))
                {
                    objRoot = Newtonsoft.Json.JsonConvert.DeserializeObject<PR.Entities.EsbEntities.UpdateResult.RootObject>(response);
                    ValidateEsbResponse(objRoot);
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "ESB-CompleteTransferInMoveIn response is null", "ESB response is null: " + starsId, qorId);
                    return oMoveInStatus;
                }

                if (objRoot != null)
                {
                    if ((Convert.ToInt32(objRoot.ResponseCode) == 200 && objRoot.ResponseMessage == "OK"))
                    {
                        oUnitName = objRoot.ResponseData.OriginUnitName;

                        oMoveInStatus = PREnums.MoveInStatus.Success;
                        handler.InsertActivityLog(Convert.ToInt32(starsId), qorId, "Unit Added", "Unit has been successfully added to destination location. Unit Name: " + oUnitName, _userId, string.Empty, 0);
                        handler.InsertActivityLog(Convert.ToInt32(starsId), qorId, "Touch status updated", "LDM Transfer In touch completed for origin orderId:" + starsId, _userId, string.Empty, 0);
                    }
                    else
                    {
                        oMoveInStatus = PREnums.MoveInStatus.InvalidServiceType;
                        handler.InsertPRErrorLog(DateTime.Now, _userId, "BL.AddUnitCompletTIAndMoveInTITouch: " + objRoot.ResponseCode, objRoot.ResponseMessage, "Not able to complete 'Transfer Out touch'", qorId);
                    }
                }
                else
                {
                    handler.InsertPRErrorLog(DateTime.Now, _userId, "", "BL.AddUnitCompletTIAndMoveInTITouch response object is null", "ESB response object is null: " + qorId, qorId);
                }
            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, "General Exception", ex.Message, ex.StackTrace, qorId);
                throw ex;
            }

            return oMoveInStatus;

            #endregion 

            #region SFERP-TODO-ESBMTD
            /* Added by Sohan
             * Update touch status as Complete using ESB method call. Parameters - SFERP-TBD
             * Comment below section to remove SL dlls to build the code.
             * Remove all below section after adding Update ESB method and testing. */

            // TG-704 , ticket created for ESB endpoint

            // Complete TI Touch Here, call ESB metod to update tocuh completions.
            // Input Paramater: unitName, QorId; 
            // Output Parameter: oMoveInStatus from ESB

            /* 
             * TODO - Add ESB method call to complete TO touch.  SFERP-TBD
             * Before move-in transfer origin unit to destination
                1. Typically add unit into destination location
                2. Delete from Origin location
                3. SPF-How we will make transfer the unit from origin to Destination, what exactly happens?

                TransferUnitFromOrigToDestForLDM(CorpCode, olocationCode, dlocationCode, UserName, Password, oUnitName, dunitName, false, true);
                SLAPIMobileTemp.L_UnitAdd(corpCode, destLocationCode, userName, password, unitName, destUnitTypeId....) --Internal call in above method
                handler.InsertActivityLog(intStarsID, destQorid, "Unit Added", "Unit has been successfully added to destination location. Unit Name: " + oUnitName, _userId, string.Empty, 0);

                oMoveInStatus = MoveInTransferInTouch(oUnitName, slapi, sessionId, destQorid);
                slapiMobile.P_MoveIn(TouchEditIdentifier, unitId, DateTime.Today); --Internal call in above method
                handler.InsertActivityLog(QorId, "MoveIn", "QORId has been moved-in successfully", _userId);
                handler.InsertActivityLog(intStarsID, destQorid, "MoveIn", "Destination QORId has been moved in successfully", _userId, string.Empty, 0);  
             */

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //SLAPIMobileTemp slapi = new SLAPIMobileTemp();
            //LocalComponent local = new LocalComponent();
            //DataSet dsUnit = new DataSet();
            //LocalDataHandler handler = new LocalDataHandler();
            //oUnitName = string.Empty;

            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, 0, StarsId);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    DataRow[] thisUnitQors = dsQORList.Tables[0].Select("iExternalUnitID = " + StarsUnitId);

            //    if (thisUnitQors != null && thisUnitQors.Length > 0)
            //    {
            //        DataRow destRec = thisUnitQors.Where(q => Convert.ToInt32(q["iQRID_GlobalNum"]) == destQorid).FirstOrDefault();
            //        //LDMTypeID 1=Origin and 2=Destination
            //        //DataRow orgRec = thisUnitQors.Where(q => Convert.ToInt32(q["LDMTypeID"]) == 1).FirstOrDefault();
            //        DataRow originQorid = handler.GetLDMOriginQorID(Convert.ToInt32(StarsId), StarsUnitId).Tables[0].AsEnumerable().Where(x => x["Location"].ToString() == "ORIGIN" && Convert.ToInt32(x["T27_26_Container_Id"]) == StarsUnitId).FirstOrDefault();

            //        if (originQorid["T27_RATS_QRId"] == null)
            //        {
            //            throw new Exception("This order is not yet converted to sold, please contact administrator.");
            //        }

            //        DataRow orgRec = thisUnitQors.Where(q => Convert.ToInt32(q["iQRID_GlobalNum"]) == Convert.ToInt32(originQorid["T27_RATS_QRId"])).FirstOrDefault();

            //        int dtenantId = Convert.ToInt32(destRec["TenantID"]);
            //        int dunitId = Convert.ToInt32(destRec["UnitID"]);
            //        int dqtRentalId = Convert.ToInt32(destRec["QTRentalID"]);
            //        string dlocationCode = destRec["sLocationCode"].ToString();
            //        string dunitName = destRec["sUnitName"].ToString();

            //        int otenantId = Convert.ToInt32(orgRec["TenantID"]);
            //        int ounitId = Convert.ToInt32(orgRec["UnitID"]);
            //        int oqtRentalId = Convert.ToInt32(orgRec["QTRentalID"]);
            //        string olocationCode = orgRec["sLocationCode"].ToString();
            //        oUnitName = orgRec["sUnitName"].ToString();

            //        int intStarsID = Convert.ToInt32(StarsId);

            //        //Before move-in transfer origin unit to destination... Typically add unit into destination location
            //        TransferUnitFromOrigToDestForLDM(CorpCode, olocationCode, dlocationCode, UserName, Password, oUnitName, dunitName, false, true);
            //        handler.InsertActivityLog(intStarsID, destQorid, "Unit Added", "Unit has been successfully added to destination location. Unit Name: " + oUnitName, _userId, string.Empty, 0);

            //        slapi = EditQuote(CorpCode, dlocationCode, UserName, Password, dqtRentalId, dtenantId, sessionId);

            //        oMoveInStatus = MoveInTransferInTouch(oUnitName, slapi, sessionId, destQorid);
            //        handler.InsertActivityLog(intStarsID, destQorid, "MoveIn", "Destination QORId has been moved in successfully", _userId, string.Empty, 0);
            //    }
            //}
            //else
            //{
            //    var ex = new Exception("Invalid QuoteID.");
            //    local.InsertPRErrorLog(DateTime.Now, String.Empty, String.Empty, ex.Message, Convert.ToString(ex.StackTrace), Convert.ToInt32(destQorid));
            //    throw ex;
            //}

            #endregion

            // return oMoveInStatus;
        }

    }
}
