using System;
using System.Collections.Generic;
using PR.DataHandler;
using System.Net;
using System.IO;
using System.Text;
using PR.Entities;

namespace PR.BusinessLogic
{
    public abstract class BaseBusinessLogicClass
    {
        #region ESB endpoint configuration

        private EsbEndPointConfig _endpointConfig;

        public EsbEndPointConfig EndpointConfig
        {
            get { return _endpointConfig; }
            set { _endpointConfig = value; }
        }

        protected BaseBusinessLogicClass()
        {
            Data.LoadSettings();
            GetEsbEndPointConfig();
        }

        private void GetEsbEndPointConfig()
        {
            var dsConfig = new LocalDataHandler().GetEsbEndPintConfig();

            if (dsConfig != null)
            {
                _endpointConfig = new EsbEndPointConfig
                {
                    Url = dsConfig.Tables[0].Rows[0]["EndPointUrl"].ToString(),
                    Username = dsConfig.Tables[0].Rows[0]["UserName"].ToString(),
                    Password = dsConfig.Tables[0].Rows[0]["Password"].ToString()
                };
            }
        }

        #endregion

        public string GetTimeSlaught(string timeDescription)
        {
            string timeSlaught = String.Empty;
            switch (timeDescription)
            {
                case "ANYTIME":
                    timeSlaught = "ANYTIME";
                    break;
                case "AM":
                    timeSlaught = "AM";
                    break;
                case "PM":
                    timeSlaught = "PM";
                    break;
                case "06:30":
                    timeSlaught = "AM";
                    break;
                case "07:00":
                    timeSlaught = "AM";
                    break;
                case "07:30":
                    timeSlaught = "AM";
                    break;
                case "08:00":
                    timeSlaught = "AM";
                    break;
                case "08:30":
                    timeSlaught = "AM";
                    break;
                case "09:00":
                    timeSlaught = "AM";
                    break;
                case "09:30":
                    timeSlaught = "AM";
                    break;
                case "10:00":
                    timeSlaught = "AM";
                    break;
                case "10:30":
                    timeSlaught = "AM";
                    break;
                case "11:00":
                    timeSlaught = "AM";
                    break;
                case "11:30":
                    timeSlaught = "AM";
                    break;
                case "12:00":
                    timeSlaught = "PM";
                    break;
                case "12:30":
                    timeSlaught = "PM";
                    break;
                case "13:00":
                    timeSlaught = "PM";
                    break;
                case "13:30":
                    timeSlaught = "PM";
                    break;
                case "14:00":
                    timeSlaught = "PM";
                    break;
                case "14:30":
                    timeSlaught = "PM";
                    break;
                case "15:00":
                    timeSlaught = "PM";
                    break;
                case "15:30":
                    timeSlaught = "PM";
                    break;
                case "16:00":
                    timeSlaught = "PM";
                    break;
                case "16:30":
                    timeSlaught = "PM";
                    break;
                case "17:00":
                    timeSlaught = "PM";
                    break;
                case "17:30":
                    timeSlaught = "PM";
                    break;
                case "18:00":
                    timeSlaught = "PM";
                    break;
                case "18:30":
                    timeSlaught = "PM";
                    break;
                case "19:00":
                    timeSlaught = "PM";
                    break;
                case "19:30":
                    timeSlaught = "PM";
                    break;
                case "20:00":
                    timeSlaught = "PM";
                    break;
                case "20:30":
                    timeSlaught = "PM";
                    break;
                case "21:00":
                    timeSlaught = "PM";
                    break;
                default:
                    timeSlaught = "ANYTIME";
                    break;
            }
            return timeSlaught;
        }

        public string ExecuteMethodESB(string methodName, string requestJson)
        {
            string responseText = "";
            try
            {

                HttpWebResponse httpResponse;

                //string requestUrl =  "http://esb.1800packrat.com:40020/" + methodName

                var requestUrl = this.EndpointConfig.Url + methodName;

                var webRequest = System.Net.HttpWebRequest.Create(requestUrl);
                byte[] postData = new UTF8Encoding().GetBytes(requestJson);

                //String encodedCreds = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes("Packrat2019" + ":" + "SanFran2005!"));
                String encodedCreds = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(this.EndpointConfig.Username + ":" + this.EndpointConfig.Password));

                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.Headers.Add("Authorization", string.Format("Basic {0}", encodedCreds));

                webRequest.PreAuthenticate = true;
                webRequest.ContentLength = postData.Length;

                using (var dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(postData, 0, postData.Length);
                }

                httpResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    responseText = streamReader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return responseText;
        }

        public string ExecuteGetMethod(PR.Entities.EsbEndPointConfig endpointConfig, string methodName, string requestJson)
        {
            string responseText = "";
            try
            {

                HttpWebResponse httpResponse;

                string esbUrl = endpointConfig.Url; // "http://esb.1800packrat.com:40020/";

                var requestUrl = esbUrl + methodName;

                //Sample-2
                var webRequest = System.Net.HttpWebRequest.Create(requestUrl);
                byte[] postData = new UTF8Encoding().GetBytes(requestJson);

                //String encodedCreds = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes("Packrat2019" + ":" + "SanFran2005!"));
                String encodedCreds = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(endpointConfig.Username + ":" + endpointConfig.Password));

                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.Headers.Add("Authorization", string.Format("Basic {0}", encodedCreds));

                webRequest.PreAuthenticate = true;
                webRequest.ContentLength = postData.Length;

                using (var dataStream = webRequest.GetRequestStream())
                {
                    dataStream.Write(postData, 0, postData.Length);
                }

                httpResponse = (System.Net.HttpWebResponse)webRequest.GetResponse();

                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    responseText = streamReader.ReadToEnd();
                }

            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
            return responseText;
        }

    }
}
