using System;
using System.Collections.Generic;
using System.Data;
using PR.Entities;
using PR.ExternalInterfaces;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {
        private string _userId = String.Empty;

        public SMDBusinessLogic(string logUserId)
        {
            _userId = logUserId;
        }

        /// <summary>
        /// Method to get list of all Sites/Facilities
        /// </summary>
        /// <returns></returns>
        public List<SiteInfo> GetSiteInformation()
        {
            DataSet dsSite = new DataSet();
            LocalComponent local = new LocalComponent();
            return local.GetSiteInformation();
        }

        /// <summary>
        /// Method to get Site/Facility based on Location Code.
        /// </summary> 
        /// <param name="locationCode"></param> 
        /// <returns></returns>
        public SiteInfo GetSiteInformation(string locationCode)
        {
            LocalComponent local = new LocalComponent();
            return local.GetSiteInformation(locationCode);
        }

        /// <summary>
        /// Method to get Site/Facility based on storeNumber.
        /// </summary>
        /// <param name="storeNumber"></param>
        /// <returns></returns>
        public SiteInfo GetSiteInformationWithMarkets(string storeNumber)
        {
            LocalComponent local = new LocalComponent();

            return local.GetSiteInformationWithMarkets(storeNumber);
        }

        /// <summary>
        /// Method to get list of Site/Facility along with market Facilities.
        /// </summary> 
        /// <param name="locationCode"></param> 
        /// <returns></returns>
        public List<SiteInfo> GetSiteInformationWithMarkets()
        {
            LocalComponent local = new LocalComponent();

            return local.GetSiteInformationWithMarkets();
        }

        /// <summary>
        /// Method to Calculate distance by using PC*Miler service
        /// </summary>
        /// <param name="orgination"></param>
        /// <param name="destination"></param>
        /// <returns></returns> 
        public AddressValidator GetDistance(Address origination, Address destination)
        { 
            PRAddressAPI pcmInterface = new PRAddressAPI();             
            AddressValidator pcmReturn = new AddressValidator();

            pcmReturn = pcmInterface.GetDistance(origination, destination);

            return pcmReturn;
        }

        /// <summary>
        /// Mesthod to Validate address through ZP4 service.
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public ValidatedAddress ValidateAddress(Address address)
        {
            PRAddressAPI oPRAddressAPI = new PRAddressAPI();
            ValidatedAddress validAddress = new ValidatedAddress();

            validAddress = oPRAddressAPI.ValidateAddress(address);

            return validAddress;
        }

        /// <summary>
        /// Method to get Preferred Delivery Date for any touch based on QORID
        /// </summary>
        /// <param name="qorid"></param>
        /// <returns></returns>
        public DateTime GetPreferredDeliveryDate(int qorid)
        {
            DateTime preferredDeliveryDate = DateTime.MinValue;
            LocalComponent local = new LocalComponent();
            DataSet dsPRGQuoteorderLog = new DataSet();
            dsPRGQuoteorderLog = local.GetPRGQuoteOrderLogData(qorid);
            if (dsPRGQuoteorderLog.Tables.Count > 0 && dsPRGQuoteorderLog.Tables[0].Rows.Count > 0 && dsPRGQuoteorderLog.Tables[0].Rows[0]["PreferredDeliveryDate"] != DBNull.Value)
            {
                preferredDeliveryDate = Convert.ToDateTime(dsPRGQuoteorderLog.Tables[0].Rows[0]["PreferredDeliveryDate"]);
            }
            return preferredDeliveryDate;
        }


    }
}
