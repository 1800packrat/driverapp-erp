﻿using System;
using System.Data;
using PR.Entities;
using PR.UtilityLibrary;
using Newtonsoft.Json;
using System.IO;
using System.Collections.Generic;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {

        /// <summary>
        /// PENDING 
        /// Method to get Quote info for particular QORID
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.QuoteBase.RootObject GetQuoteInfo(int qorId)
        {
            PR.Entities.EsbEntities.QuoteBase.RootObject quoteInfo = null;
            var requestObject = new
            {
                qorId
            };

            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetQuoteInfo, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetCustomerInfo_57871.txt"));

            //if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
            //    dsQuoteInfo = JsonConvert.DeserializeObject<DataSet>(response);

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                quoteInfo = JsonConvert.DeserializeObject<PR.Entities.EsbEntities.QuoteBase.RootObject>(response);



            return quoteInfo;
        }

        /// <summary>
        /// PENDING-TEST
        /// Method to get customer info for particular facility 
        /// </summary>
        /// <param name="requestObject"></param>
        /// <returns></returns>
        public PR.Entities.EsbEntities.CustomerData.RootObject GetCustomerInfo(string locationCode = "", int customerId = 0, int qorId = 0)
        {
            PR.Entities.EsbEntities.CustomerData.RootObject customerInfo = null;
            try
            {
                var requestObject = new
                {
                    LocationCode = locationCode,
                    CustomerID = customerId > 0 ? customerId.ToString() : "",
                    qorId = qorId > 0 ? qorId.ToString() : ""
                };

                string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

                var response = ExecuteMethodESB(EsbMethod.GetCustomerInfo, requestJson);

                ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
                //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetCustomerInfo_57871.txt"));

                if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                    customerInfo = JsonConvert.DeserializeObject<PR.Entities.EsbEntities.CustomerData.RootObject>(response);

            }
            catch (Exception ex)
            {
                handler.InsertPRErrorLog(DateTime.Now, _userId, ex.GetType().ToString(), ex.Message, ex.StackTrace.ToString(), -1);
            }

            return customerInfo;

        }


        ///// <summary>
        ///// SFERP-TODO-ESBMTD - Not Required Now.
        ///// Method to get customer info particular facility - Future Need.
        ///// </summary>
        ///// <param name="requestObject"></param>
        ///// <returns></returns>
        //public DataSet GetQorDetails(string qorId)
        //{
        //    //SFERP-TODO-ADDSTB
        //    #region SFERP-TODO-ESBMTD
        //    var requestObject = new
        //    {
        //        qorId = qorId
        //    };

        //    DataSet dsCustomerInfo = null;
        //    string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

        //    var response = ExecuteMethodESB(EsbMethod.GetQORDetails, requestJson);

        //    if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
        //        dsCustomerInfo = JsonConvert.DeserializeObject<DataSet>(response);

        //    #endregion

        //    return dsCustomerInfo;

        //}

        public bool IsQuoteMovedIn(int qorId)
        {
            try
            {
                PR.Entities.EsbEntities.QuoteBase.RootObject quote = GetQuoteInfo(qorId: qorId);

                if (quote != null && quote.SF_QuoteInfo != null)
                {
                    return Convert.ToBoolean(quote.SF_QuoteInfo.IsQuoteMovedIn);
                }
                else
                {
                    throw new Exception("EsbMethod.IsQuoteMovedIn - Quote info not available.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            } 

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.
            //var dsQORList = GetQORList(CorpCode, String.Empty, UserName, Password, 0, String.Empty, qorId, String.Empty);
            //if (dsQORList.Tables.Count > 0 && dsQORList.Tables[0].Rows.Count > 0)
            //{
            //    string rentalType = dsQORList.Tables[0].Rows[0]["sRentalType"].ToString();
            //    if (rentalType == "Rental" || rentalType == "Completed")
            //    {
            //        return true;
            //    }
            //}

            //return false;
            #endregion
        }

        /// <summary>
        /// PENDING-TEST
        /// SFERP-TODO-ESBMTD
        /// Method to get POS Items list for QorId. UnitId is replaced with QorId since we are getting UnitId from QorId itself from Sitelink
        /// </summary>
        /// <param name="qorId"></param>
        /// <returns></returns>
        public POSItems GetAppliedPOSItems(int qorId)
        {
            //SFERP-TODO-ADDSTB
            #region SFERP-TODO-ESBMTD

            POSItems posItems = new POSItems();
            POSItems.POSItemDataTable dtPOSItem = posItems.POSItem;

            var requestObject = new
            {
                qorId = qorId
            };

            DataSet dsPOSItems = null;
            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetAppliedPOSItems, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetAppliedPOSItems_2912.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
                dsPOSItems = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsPOSItems != null && dsPOSItems.Tables.Count > 0 && dsPOSItems.Tables[0] != null && dsPOSItems.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow row in dsPOSItems.Tables[0].Rows)
                {
                    POSItems.POSItemRow posRow = dtPOSItem.NewPOSItemRow();
                    posRow.ItemDescription = row["Description"].ToString();
                    posRow.Quantity = Convert.ToInt32(row["Quantity"]);
                    posRow.Price = Convert.ToDecimal(row["Price"]);
                    posRow.SubTotal = posRow.Price * posRow.Quantity;
                    posRow.Tax = Convert.ToDecimal(row["Tax1"]) + Convert.ToDecimal(row["Tax2"]);
                    posRow.Total = posRow.SubTotal + posRow.Tax;

                    //Below are not being used anywhere. - TBD
                    //posRow.Discounts = Convert.ToDecimal(row["dcDiscAmt"]);
                    //posRow.EditIdentifier = Convert.ToInt32(row["iEditIdentifier"]);
                    //posRow.ChargeDescriptionId = Convert.ToInt32(row["ChargeDescID"]);

                    dtPOSItem.AddPOSItemRow(posRow);
                }
            }
            return posItems;

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //POSItems posItems = new POSItems();
            //POSItems.POSItemDataTable dtPOSItem = posItems.POSItem;

            //dsPOSItems = slapiMobile.P_POSItemsApplied(unitId);
            //if (dsPOSItems.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsPOSItems.Tables["RT"].Rows[0]["Ret_Code"]) == -1)
            //    {
            //        throw new Exception("Invalid Unitid");
            //    }
            //    else if (Convert.ToInt32(dsPOSItems.Tables["RT"].Rows[0]["Ret_Code"]) == -90)
            //    {
            //        throw new DatasetNotInitializedException("Working dataset has not been initialized");
            //    }
            //    else if (Convert.ToInt32(dsPOSItems.Tables["RT"].Rows[0]["Ret_Code"]) == -99)
            //    {
            //        throw new Exception("General Exception");
            //    }
            //    else if (Convert.ToInt32(dsPOSItems.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}
            //if (dsPOSItems.Tables["Table1"] != null)
            //{
            //    foreach (DataRow row in dsPOSItems.Tables["Table1"].Rows)
            //    {
            //        POSItems.POSItemRow posRow = dtPOSItem.NewPOSItemRow();
            //        posRow.ItemDescription = row["sDesc"].ToString();
            //        posRow.Quantity = Convert.ToInt32(row["dcQty"]);
            //        posRow.Price = Convert.ToDecimal(row["dcPrice"]);
            //        posRow.SubTotal = posRow.Price * posRow.Quantity;
            //        posRow.Discounts = Convert.ToDecimal(row["dcDiscAmt"]);
            //        posRow.Tax = Convert.ToDecimal(row["dcTax1"]) + Convert.ToDecimal(row["dcTax2"]);
            //        posRow.Total = posRow.SubTotal + posRow.Tax;
            //        posRow.EditIdentifier = Convert.ToInt32(row["iEditIdentifier"]);
            //        posRow.ChargeDescriptionId = Convert.ToInt32(row["ChargeDescID"]);

            //        dtPOSItem.AddPOSItemRow(posRow);
            //    }
            //}
            //return posItems;
            #endregion
        }

        /// <summary>
        /// SFERP-TODO-CTRMV - NOT REQUIRED NOW 
        /// Method to get reccuring Items list for QorId. Unit Id is replaced with QorId since we are getting UnitId from QorId itself from Sitelink.
        /// </summary> 
        /// <param name="qorId"></param>
        /// <returns></returns>
        public RecurringItems GetAppliedRecurringItems(int qorId)
        {
            #region SFERP-TODO-ESBMTD

            RecurringItems recurringItems = new RecurringItems();
            RecurringItems.RecurringItemDataTable dtRecurringItem = recurringItems.RecurringItem;

            var requestObject = new
            {
                qorId = qorId
            };

            DataSet dsRecurringItems = null;
            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            //var response = ExecuteMethodESB(EsbMethod.GetAppliedRecurringItems, requestJson);

            //SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetAppliedRecurringItems_2912.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                dsRecurringItems = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsRecurringItems != null && dsRecurringItems.Tables.Count > 0 && dsRecurringItems.Tables[0] != null && dsRecurringItems.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drRecc in dsRecurringItems.Tables[0].Rows)
                {
                    if (Convert.ToBoolean(drRecc["Recurring_Applied"]))
                    {
                        if ((drRecc["Recurring_Description"].ToString() == "ProtectionPlan") || (drRecc["Recurring_Description"].ToString() == "Protection Plan"))
                        {
                            #region Testing - 11 

                            List<CPPItem> cppList = GetCppItems(); // Get CPP items Master List
                            bool isCPPAdded = false;

                            foreach (CPPItem cppItem in cppList)
                            {
                                if (Convert.ToDecimal(drRecc["Recurring_Price"]) == Convert.ToDecimal(cppItem.Price))
                                {
                                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
                                    row.ItemDescription = cppItem.Description;
                                    row.Quantity = Convert.ToInt32(drRecc["Recurring_Quantity"]);

                                    dtRecurringItem.AddRecurringItemRow(row);
                                    isCPPAdded = true;
                                    break;
                                }
                            }
                            if (!isCPPAdded)
                            {
                                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
                                row.ItemDescription = drRecc["Recurring_Description"].ToString();
                                row.Quantity = Convert.ToInt32(drRecc["Recurring_Quantity"]);

                                //Below are not being used anywhere. - TBD
                                //row.ChargeDescId = Convert.ToInt32(drRecc["Recurring_ChargeDescID"]); 
                                //row.Charge = Convert.ToDecimal(drRecc["Recurring_Price"]);
                                //row.Subtotal = row.Quantity * row.Charge;
                                //row.Discounts = Convert.ToDecimal(drRecc["Recurring_Discount"]);
                                //row.Tax = (Convert.ToDecimal(drRecc["Recurring_Tax1"]) + Convert.ToDecimal(drRecc["Recurring_Tax2"])) * row.Quantity;
                                //row.TaxRate = Convert.ToDecimal(drRecc["Recurring_Tax1"]) + Convert.ToDecimal(drRecc["Recurring_Tax2"]);
                                //row.Total = row.Subtotal + row.Tax - row.Discounts;
                                dtRecurringItem.AddRecurringItemRow(row);
                            }

                            #endregion
                        }
                        else
                        {
                            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
                            row.ItemDescription = drRecc["Recurring_Description"].ToString();
                            row.Quantity = Convert.ToInt32(drRecc["Recurring_Quantity"]);

                            //Below are not being used anywhere. - TBD
                            //row.ChargeDescId = Convert.ToInt32(drRecc["Recurring_ChargeDescID"]); 
                            //row.Charge = Convert.ToDecimal(drRecc["Recurring_Price"]);
                            //row.Subtotal = row.Quantity * row.Charge;
                            //row.Discounts = Convert.ToDecimal(drRecc["Recurring_Discount"]);
                            //row.Tax = (Convert.ToDecimal(drRecc["Recurring_Tax1"]) + Convert.ToDecimal(drRecc["Recurring_Tax2"])) * row.Quantity;
                            //row.TaxRate = Convert.ToDecimal(drRecc["Recurring_Tax1"]) + Convert.ToDecimal(drRecc["Recurring_Tax2"]);
                            //row.Total = row.Subtotal + row.Tax - row.Discounts;
                            dtRecurringItem.AddRecurringItemRow(row);
                        }
                    }
                }
            }
            //else
            //{
            //    throw new Exception("EsbMethod.GetAppliedRecurringItems - Recurring Items not available.");
            //}

            return recurringItems;


            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //DataSet dsRecurringItems = new DataSet();
            //RecurringItems recurringItems = new RecurringItems();
            //RecurringItems.RecurringItemDataTable dtRecurringItem = recurringItems.RecurringItem;

            //dsRecurringItems = slapiMobile.P_RecurringChargesApplied(unitId);

            //if (dsRecurringItems.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsRecurringItems.Tables["RT"].Rows[0]["Ret_Code"]) == -1)
            //    {
            //        throw new Exception("Invalid Unitid");
            //    }
            //    else if (Convert.ToInt32(dsRecurringItems.Tables["RT"].Rows[0]["Ret_Code"]) == -90)
            //    {
            //        throw new DatasetNotInitializedException("Working dataset has not been initialized");
            //    }
            //    else if (Convert.ToInt32(dsRecurringItems.Tables["RT"].Rows[0]["Ret_Code"]) == -99)
            //    {
            //        throw new Exception("General Exception");
            //    }
            //    else if (Convert.ToInt32(dsRecurringItems.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}

            //if (dsRecurringItems.Tables["RecurringChargesApplied"] != null)
            //{
            //    DataTable dt = dsRecurringItems.Tables["RecurringChargesApplied"];
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring1_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring1_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring1_Description"].ToString() == "Protection Plan"))
            //        {
            //            #region Testing - 11
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring1_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring1_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring1_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring1_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring1_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring1_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring1_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring1_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring1_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring1_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }

            //            #endregion
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring1_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring1_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring1_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring1_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring1_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring1_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring2_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring2_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring2_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring2_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring2_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring2_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring2_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring2_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring2_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring2_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring2_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring2_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring2_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring2_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring2_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring2_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring2_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring2_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring2_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring3_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring3_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring3_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring3_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring3_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring3_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring3_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring3_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring3_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring3_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring3_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring3_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring3_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring3_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring3_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring3_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring3_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring3_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring3_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring4_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring4_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring4_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring4_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring4_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring4_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring4_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring4_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring4_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring4_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring4_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring4_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring4_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring4_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring4_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring4_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring4_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring4_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring4_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring5_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring5_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring5_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring5_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring5_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring5_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring5_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring5_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring5_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring5_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring5_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring5_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring5_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring5_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring5_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring5_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring5_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring5_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring5_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring6_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring6_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring6_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring6_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring6_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring6_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring6_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring6_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring6_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring6_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring6_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring6_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring6_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring6_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring6_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring6_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring6_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring6_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring6_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring7_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring7_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring7_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring7_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring7_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring7_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring7_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring7_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring7_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring7_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring7_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring7_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring7_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring7_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring7_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring7_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring7_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring7_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring7_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //    if (Convert.ToBoolean(dt.Rows[0]["Recurring8_Applied"]))
            //    {
            //        if ((dt.Rows[0]["Recurring8_Description"].ToString() == "ProtectionPlan") || (dt.Rows[0]["Recurring8_Description"].ToString() == "Protection Plan"))
            //        {
            //            DataSet dsCPP = new DataSet();
            //            LocalComponent localComponent = new LocalComponent();
            //            dsCPP = localComponent.GetCPPInfo();
            //            bool isCPPAdded = false;

            //            foreach (DataRow drCPP in dsCPP.Tables[0].Rows)
            //            {
            //                if (Convert.ToDecimal(dt.Rows[0]["Recurring8_Price"]) == Convert.ToDecimal(drCPP["Price"]))
            //                {
            //                    RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                    row.ItemDescription = drCPP["CPPDescription"].ToString();
            //                    row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring8_ChargeDescID"]);
            //                    row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring8_Quantity"]);
            //                    row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring8_Price"]);
            //                    row.Subtotal = row.Quantity * row.Charge;
            //                    row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring8_Discount"]);
            //                    row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"])) * row.Quantity;
            //                    row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"]);
            //                    row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                    dtRecurringItem.AddRecurringItemRow(row);
            //                    isCPPAdded = true;
            //                    break;
            //                }
            //            }
            //            if (!isCPPAdded)
            //            {
            //                RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //                row.ItemDescription = dt.Rows[0]["Recurring8_Description"].ToString();
            //                row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring8_ChargeDescID"]);
            //                row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring8_Quantity"]);
            //                row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring8_Price"]);
            //                row.Subtotal = row.Quantity * row.Charge;
            //                row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring8_Discount"]);
            //                row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"])) * row.Quantity;
            //                row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"]);
            //                row.Total = row.Subtotal + row.Tax - row.Discounts;
            //                dtRecurringItem.AddRecurringItemRow(row);
            //            }
            //        }
            //        else
            //        {
            //            RecurringItems.RecurringItemRow row = dtRecurringItem.NewRecurringItemRow();
            //            row.ItemDescription = dt.Rows[0]["Recurring8_Description"].ToString();
            //            row.ChargeDescId = Convert.ToInt32(dt.Rows[0]["Recurring8_ChargeDescID"]);
            //            row.Quantity = Convert.ToInt32(dt.Rows[0]["Recurring8_Quantity"]);
            //            row.Charge = Convert.ToDecimal(dt.Rows[0]["Recurring8_Price"]);
            //            row.Subtotal = row.Quantity * row.Charge;
            //            row.Discounts = Convert.ToDecimal(dt.Rows[0]["Recurring8_Discount"]);
            //            row.Tax = (Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"])) * row.Quantity;
            //            row.TaxRate = Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax1"]) + Convert.ToDecimal(dt.Rows[0]["Recurring8_Tax2"]);
            //            row.Total = row.Subtotal + row.Tax - row.Discounts;
            //            dtRecurringItem.AddRecurringItemRow(row);
            //        }
            //    }
            //}


            //return recurringItems;


            #endregion
        }

        /// <summary>
        /// SFERP-TODO-CTRMV - NOT REQUIRED NOW 
        /// Method to get pricing info for QorId. Unit Id is replaced with QorId since we are getting UnitId from QorId itself from Sitelink.
        /// </summary>
        /// <param name="qorId"></param>
        /// <returns></returns>
        public PricingInfo GetPricingInfoData(int qorId)
        {
            #region SFERP-TODO-ESBMTD      

            PricingInfo pricingInfo = null;

            var requestObject = new
            {
                qorId = qorId
            };

            DataSet dsPricing = null;
            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            //var response = ExecuteMethodESB(EsbMethod.GetPricingInfo, requestJson);

            //SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetPricingInfoData_Pricing.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.")
                dsPricing = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsPricing != null && dsPricing.Tables.Count > 0 && dsPricing.Tables[0] != null && dsPricing.Tables[0].Rows.Count > 0)
            {
                pricingInfo = new PricingInfo();
                pricingInfo.DueAtDelivery = Convert.ToDecimal(dsPricing.Tables[0].Rows[0]["CurrentDueAmtSum"]);
                pricingInfo.RecurringMonthlyCharge = Convert.ToDecimal(dsPricing.Tables[0].Rows[0]["RecurringAmtSum"]);
                pricingInfo.FutureTransportationCharge = Convert.ToDecimal(dsPricing.Tables[0].Rows[0]["FutureDeliveriesAmtSum"]);

                //Below are not being used anywhere. - TBD
                //pricingInfo.PayAsYouGo = Convert.ToBoolean(dsPricing.Tables[0].Rows[0]["bPayAsYouGo"]);
                //pricingInfo.RecurringMonthlyChargeDiscountedPeriod = 2223.33M; // Convert.ToDecimal(dsPricing.Tables[0].Rows[0]["RecurringMonthlyTotalDiscountPeriod"]); //commneted for testing
            }


            return pricingInfo;

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //DataSet dsPricing = new DataSet();
            //dsPricing = slapiMobile.P_QORTotalPrice();
            //PricingInfo pricingInfo = new PricingInfo();

            //if (dsPricing.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsPricing.Tables["RT"].Rows[0]["Ret_Code"]) == -1)
            //    {
            //        throw new Exception("General Failure");
            //    }
            //    else if (Convert.ToInt32(dsPricing.Tables["RT"].Rows[0]["Ret_Code"]) == -90)
            //    {
            //        throw new DatasetNotInitializedException("Working dataset has not been initialized");
            //    }
            //    else if (Convert.ToInt32(dsPricing.Tables["RT"].Rows[0]["Ret_Code"]) == -99)
            //    {
            //        throw new Exception("General Exception");
            //    }
            //    else if (Convert.ToInt32(dsPricing.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}

            //if (dsPricing.Tables["AmountDue"] != null)
            //{
            //    pricingInfo.DueAtDelivery = Convert.ToDecimal(dsPricing.Tables["AmountDue"].Rows[0]["dcCurrentDueamtSum"]);
            //    pricingInfo.RecurringMonthlyCharge = Convert.ToDecimal(dsPricing.Tables["AmountDue"].Rows[0]["dcRecurringAmtSum"]);
            //    pricingInfo.FutureTransportationCharge = Convert.ToDecimal(dsPricing.Tables["AmountDue"].Rows[0]["dcFutureDeliveriesAmtSum"]);
            //    //Hardcoding PayAsYouGo to True, and this should be changed back when SL will fix the issue.
            //    pricingInfo.PayAsYouGo = Convert.ToBoolean(dsPricing.Tables["AmountDue"].Rows[0]["bPayAsYouGo"]);
            //    // pricingInfo.PayAsYouGo = true;
            //}

            ////If a promo with more than one month discount is applied calculate the recurring monthly charge within
            ////the discounted period differently
            //DataSet dsPromo = new DataSet();
            //dsPromo = slapiMobile.P_PromotionApplied();
            //decimal recurringMonthlyTotalDiscountPeriod = 0M;
            //if (dsPromo.Tables["PromotionApplied"] != null)
            //{
            //    if (dsPromo.Tables["PromotionApplied"].Rows.Count > 0)
            //    {
            //        int promoGlobalNum = 0;
            //        promoGlobalNum = Convert.ToInt32(dsPromo.Tables["PromotionApplied"].Rows[0]["iPromoGlobalNum"]);
            //        if (promoGlobalNum > 0)
            //        {
            //            LocalComponent local = new LocalComponent();
            //            ConcessionPlan concessionPlan = new ConcessionPlan();
            //            concessionPlan = local.GetConcessionPlan(promoGlobalNum, "Rent", slapiMobile.LocationCode);
            //            if (concessionPlan.ConcessionGlobalNumber > 0)
            //            {
            //                if (concessionPlan.ExpireMonths > 1)
            //                {
            //                    //UnitInfo unitInfo = GetAddedUnit(slapiMobile);
            //                    UnitInfo unitInfo = GetUnitInfoByQorId(qorId); //Added by Sohan - QorId to get unitInfo.
            //                    RecurringItems recurringItems = GetAppliedRecurringItems(qorId);

            //                    //Sum the total price for the unit and recurring items for the total month.y recurring charge
            //                    //within the discounted period.
            //                    recurringMonthlyTotalDiscountPeriod += Convert.ToDecimal(unitInfo.Units.Rows[0]["TotalCharge"]);
            //                    foreach (RecurringItems.RecurringItemRow row in recurringItems.RecurringItem.Rows)
            //                    {
            //                        recurringMonthlyTotalDiscountPeriod += Convert.ToDecimal(row.Total);
            //                    }
            //                    if (recurringMonthlyTotalDiscountPeriod > 0)
            //                        pricingInfo.RecurringMonthlyChargeDiscountedPeriod = recurringMonthlyTotalDiscountPeriod;
            //                }
            //            }
            //        }
            //    }
            //}

            //return pricingInfo;

            #endregion
        }

        /// <summary>
        /// SFERP-TODO-CTRMV - NOT REQUIRED NOW 
        /// Method to get Transportation charges based on QorId.
        /// </summary>
        /// <param name="qorId"></param>
        /// <returns></returns>
        public TransportationItems GetAppliedTransportationCharges(int qorId)
        {
            //SFERP-TODO-ADDSTB
            #region SFERP-TODO-ESBMTD

            DataSet dsTransportationItems = new DataSet();
            TransportationItems transportationItems = new TransportationItems();
            TransportationItems.TransportationItemDataTable dtTransportation = transportationItems.TransportationItem;

            var requestObject = new
            {
                qorId = qorId
            };

            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetAppliedTransportationCharges, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetSLTransportationCharges_2912.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
                dsTransportationItems = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsTransportationItems != null && dsTransportationItems.Tables.Count > 0 && dsTransportationItems.Tables[0] != null && dsTransportationItems.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow drTransportation in dsTransportationItems.Tables[0].Rows)
                {
                    TransportationItems.TransportationItemRow transRow = dtTransportation.NewTransportationItemRow();
                    transRow.Description = drTransportation["Description"].ToString();
                    transRow.Net = Convert.ToDecimal(drTransportation["Amount"]);

                    //Below are not being used anywhere. - TBD
                    //transRow.Charge = Convert.ToDecimal(drTransportation["dcPrice"]);
                    //transRow.Distance = Convert.ToDecimal(drTransportation["dcDistance"]);
                    //transRow.ExcessDistance = Convert.ToDecimal(drTransportation["dcDistanceExcess"]);
                    //transRow.ExcessCharge = Convert.ToDecimal(drTransportation["dcDistanceExcessChgAmt"]);
                    //transRow.SubTotal = transRow.Charge + transRow.ExcessCharge;
                    //transRow.Discounts = Convert.ToDecimal(drTransportation["dcDiscAmt"]);

                    //transRow.Tax = Convert.ToDecimal(drTransportation["dcTax1"]) + Convert.ToDecimal(drTransportation["dcTax2"]);
                    //transRow.Payment = Convert.ToDecimal(drTransportation["dcPayment"]);
                    //transRow.Balance = Convert.ToDecimal(drTransportation["dcBalance"]);
                    //transRow.MileageRate = Convert.ToDecimal(drTransportation["dcMileageRate"]);
                    //transRow.ChargeDescriptionID = Convert.ToInt32(drTransportation["ChargeDescID"]);
                    //transRow.EditIdentifier = Convert.ToInt32(drTransportation["iEditIdentifier"]);
                    dtTransportation.AddTransportationItemRow(transRow);
                }
            }
            return transportationItems; ;

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //DataSet dsTransportationItems = new DataSet();
            //dsTransportationItems = slapiMobile.P_TransportationChargesApplied();
            //TransportationItems transportationItems = new TransportationItems();
            //TransportationItems.TransportationItemDataTable dtTransportation = transportationItems.TransportationItem;

            //if (dsTransportationItems.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsTransportationItems.Tables["RT"].Rows[0]["Ret_Code"]) == -82)
            //    {
            //        throw new Exception("No units have been added to the QOR");
            //    }
            //    else if (Convert.ToInt32(dsTransportationItems.Tables["RT"].Rows[0]["Ret_Code"]) == -90)
            //    {
            //        throw new DatasetNotInitializedException("Working dataset has not been initialized.");
            //    }
            //    else if (Convert.ToInt32(dsTransportationItems.Tables["RT"].Rows[0]["Ret_Code"]) == -99)
            //    {
            //        throw new Exception("General Exception.");
            //    }
            //    else if (Convert.ToInt32(dsTransportationItems.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}

            //if (dsTransportationItems.Tables["Table1"] != null)
            //{
            //    foreach (DataRow drTransportation in dsTransportationItems.Tables["Table1"].Rows)
            //    {
            //        if (!Convert.ToBoolean(drTransportation["bDelete"]))
            //        {
            //TransportationItems.TransportationItemRow row = dtTransportation.NewTransportationItemRow();
            //row.Description = drTransportation["sDesc"].ToString();
            //row.Charge = Convert.ToDecimal(drTransportation["dcPrice"]);
            //row.Distance = Convert.ToDecimal(drTransportation["dcDistance"]);
            //row.ExcessDistance = Convert.ToDecimal(drTransportation["dcDistanceExcess"]);
            //row.ExcessCharge = Convert.ToDecimal(drTransportation["dcDistanceExcessChgAmt"]);
            //row.SubTotal = row.Charge + row.ExcessCharge;
            //row.Discounts = Convert.ToDecimal(drTransportation["dcDiscAmt"]);
            //row.Net = Convert.ToDecimal(drTransportation["dcAmt"]);
            //row.Tax = Convert.ToDecimal(drTransportation["dcTax1"]) + Convert.ToDecimal(drTransportation["dcTax2"]);
            //row.Payment = Convert.ToDecimal(drTransportation["dcPayment"]);
            //row.Balance = Convert.ToDecimal(drTransportation["dcBalance"]);
            //row.MileageRate = Convert.ToDecimal(drTransportation["dcMileageRate"]);
            //row.ChargeDescriptionID = Convert.ToInt32(drTransportation["ChargeDescID"]);
            //row.EditIdentifier = Convert.ToInt32(drTransportation["iEditIdentifier"]);
            //dtTransportation.AddTransportationItemRow(row);
            //        }
            //    }
            //}
            //return transportationItems;

            #endregion
        }

        /// <summary>
        /// PENDING-TEST
        /// SFERP-TODO-ESBMTD
        /// Method to get Applied Touches based on QorId.
        /// </summary>
        /// <param name="QORid"></param>
        /// <returns></returns>
        public DataSet GetAppliedTouches(int qorId)
        {
            //SFERP-TODO-ADDSTB
            #region SFERP-TODO-ESBMTD

            DataSet dsAppliedTouches = new DataSet();

            var requestObject = new
            {
                qorId = qorId
            };

            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetAppliedTouches, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetAppliedTouches.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
                dsAppliedTouches = JsonConvert.DeserializeObject<DataSet>(response);

            return dsAppliedTouches;

            #endregion

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.

            //DataSet dsTouches = new DataSet();
            //DataSet dsCompletedtouches = new DataSet();
            //dsTouches = slapiMobile.P_TouchesApplied();
            //dsCompletedtouches = slapiMobile.P_TouchesAppliedAllHistory();
            //Touches touches = new Touches();
            //Touches.TouchDataTable dtTouch = touches.Touch;

            //TransportationItems transportationItems = new TransportationItems();

            //if (dsTouches.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) == -1)
            //    {
            //        throw new Exception("General failure.");
            //    }
            //    else if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) == -81)
            //    {
            //        throw new Exception("Invalid Touch Edit Identifier");
            //    }
            //    else if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) == -90)
            //    {
            //        // throw new DatasetNotInitializedException("Working dataset has not been initialized.");
            //    }
            //    else if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) == -99)
            //    {
            //        throw new Exception("General Exception.");
            //    }
            //    else if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}

            //if (dsCompletedtouches.Tables["RT"] != null)
            //{
            //    if (Convert.ToInt32(dsTouches.Tables["RT"].Rows[0]["Ret_Code"]) < 0)
            //    {
            //        throw new Exception("General exception occurred.");
            //    }
            //}

            ////Get the corresponding TransportationItem record
            //transportationItems = GetAppliedTransportationCharges(QORid);  //QORid - Added by Sohan

            //#region P_TouchesAppliedAllHistory

            ////First add the completed touch records.
            //if (dsCompletedtouches.Tables["QTsHistory"] != null)
            //{
            //    foreach (DataRow drCompletedTouch in dsCompletedtouches.Tables["QTsHistory"].Select("QTStatusId=2", "dDelivered Asc"))
            //    {
            //        Touches.TouchRow row = dtTouch.NewTouchRow();
            //        row.QTID = Convert.ToInt32(drCompletedTouch["QTID"]);
            //        row.Service = drCompletedTouch["sTypeCategory"].ToString();
            //        row.ServiceType = drCompletedTouch["sTypeCategory"].ToString();
            //        if (drCompletedTouch["dDelivered"].ToString() != String.Empty)
            //        {
            //            row.DeliveryDate = Convert.ToDateTime(drCompletedTouch["dDelivered"]);
            //        }
            //        else
            //        {
            //            row.DeliveryDate = DateTime.MinValue;
            //        }
            //        switch (Convert.ToInt32(drCompletedTouch["QTTypeID"]))
            //        {
            //            case 2:
            //                row.Service = "DE-Deliver Empty";
            //                break;
            //            case 3:
            //                row.Service = "DF-Deliver Full";
            //                break;
            //            case 4:
            //                row.Service = "CC-Curb To Curb";
            //                break;
            //            case 5:
            //                row.Service = "RE-Return Empty";
            //                break;
            //            case 6:
            //                row.Service = "RF-Return Full";
            //                break;
            //            case 11:
            //                row.Service = "LDM:Out-LDM Transfer Out";
            //                break;
            //            case 10:
            //                row.Service = "LDM:In-LDM Transfer In";
            //                break;
            //            case 7:
            //                row.Service = "WA-Whse Access";
            //                break;
            //            case 8:
            //                row.Service = "IBO-In By Owner";
            //                break;
            //            case 9:
            //                row.Service = "OBO-Out By Owner";
            //                break;
            //        }

            //        row.Status = drCompletedTouch["sStatusCategory"].ToString();
            //        row.Starts = String.Empty;
            //        row.End = String.Empty;
            //        row.AddrFrom_FName = drCompletedTouch["AddrFrom_sFName"].ToString();
            //        row.AddrFrom_LName = drCompletedTouch["AddrFrom_sLName"].ToString();
            //        row.AddrFrom_Addr1 = drCompletedTouch["AddrFrom_sAddr1"].ToString();
            //        row.AddrFrom_Addr2 = drCompletedTouch["AddrFrom_sAddr2"].ToString();
            //        row.AddrFrom_City = drCompletedTouch["AddrFrom_sCity"].ToString();
            //        row.AddrFrom_State = drCompletedTouch["AddrFrom_sRegion"].ToString();
            //        row.AddrFrom_Zip = drCompletedTouch["AddrFrom_sPostalCode"].ToString();
            //        row.AddrFrom_Company = drCompletedTouch["AddrFrom_sCompany"].ToString();
            //        row.AddrFrom_PhoneNum = drCompletedTouch["AddrFrom_sPhone1"].ToString();
            //        row.AddrFrom_MobilePhoneNumber = drCompletedTouch["AddrFrom_sPhone2"].ToString();
            //        row.AddrTo_FName = drCompletedTouch["AddrTo_sFName"].ToString();
            //        row.AddrTo_LName = drCompletedTouch["AddrTo_sLName"].ToString();
            //        row.AddrTo_Addr1 = drCompletedTouch["AddrTo_sAddr1"].ToString();
            //        row.AddrTo_Addr2 = drCompletedTouch["AddrTo_sAddr2"].ToString();
            //        row.AddrTo_City = drCompletedTouch["AddrTo_sCity"].ToString();
            //        row.AddrTo_State = drCompletedTouch["AddrTo_sRegion"].ToString();
            //        row.AddrTo_Zip = drCompletedTouch["AddrTo_sPostalCode"].ToString();
            //        row.AddrTo_Company = drCompletedTouch["AddrTo_sCompany"].ToString();
            //        row.AddrTo_PhoneNum = drCompletedTouch["AddrTo_sPhone1"].ToString();
            //        row.AddrTo_MobilePhoneNum = drCompletedTouch["AddrTo_sPhone2"].ToString();
            //        row.Comments = drCompletedTouch["sFinalComments"].ToString();
            //        row.Directions = drCompletedTouch["sDirections"].ToString();
            //        row.Instructions = drCompletedTouch["sInstructions"].ToString();
            //        row.Drivewaytype_Sloped = Convert.ToBoolean(drCompletedTouch["bDrivewaySloped"]);
            //        row.Drivewaytype_Soft = Convert.ToBoolean(drCompletedTouch["bDrivewaySoft"]);
            //        row.Drivewaytype_Dirt = Convert.ToBoolean(drCompletedTouch["bDrivewayDirt"]);
            //        row.Drivewaytype_Grvel = Convert.ToBoolean(drCompletedTouch["bDrivewayGravel"]);
            //        row.Drivewaytype_Paved = Convert.ToBoolean(drCompletedTouch["bDrivewayAsphalt"]);
            //        row.Drivewaytype_Bricked = Convert.ToBoolean(drCompletedTouch["bDrivewayBrick"]);
            //        row.Drivewaytype_Curbed = Convert.ToBoolean(drCompletedTouch["bDrivewayCurb"]);
            //        row.Drivewaytype_Other = Convert.ToBoolean(drCompletedTouch["bDrivewayOther"]);
            //        row.DoorFacesRear = Convert.ToBoolean(drCompletedTouch["bDoorToRear"]);
            //        row.Obstacle_PowerLines = Convert.ToBoolean(drCompletedTouch["bObstaclePowerLines"]);
            //        row.Obstacle_Trees = Convert.ToBoolean(drCompletedTouch["bObstacleTrees"]);
            //        row.Obstacle_Landscaping = Convert.ToBoolean(drCompletedTouch["bObstacleLandscaping"]);
            //        row.Obstacle_Sprinklers = Convert.ToBoolean(drCompletedTouch["bObstacleSprinklers"]);
            //        row.Obstacle_Septic = Convert.ToBoolean(drCompletedTouch["bObstacleSeptic"]);
            //        row.Obstacle_Fences = Convert.ToBoolean(drCompletedTouch["bObstacleFence"]);
            //        row.Obstacle_Other = Convert.ToBoolean(drCompletedTouch["bObstacleOther"]);
            //        row.Distance = Convert.ToDecimal(drCompletedTouch["dcMileage"]);
            //        row.EditIdentifier = -999;
            //        row.ChargeDescriptionID = 0;
            //        //row.SequenceNumber = 0;
            //        row.SequenceNumber = (drCompletedTouch["iSequenceNum"] != null ? Convert.ToInt32(drCompletedTouch["iSequenceNum"]) : 0);

            //        dtTouch.AddTouchRow(row);
            //    }
            //}

            //#endregion

            //#region  P_TouchesApplied

            //if (dsTouches.Tables["AppliedTouches"] != null)
            //{
            //    foreach (DataRow drTouch in dsTouches.Tables["AppliedTouches"].Rows)
            //    {
            //        if (drTouch["Status"].ToString() != "Cancelled")
            //        {
            //            Touches.TouchRow row = dtTouch.NewTouchRow();
            //            row.QTID = Convert.ToInt32(drTouch["QTID"]);
            //            row.Service = drTouch["Service"].ToString();
            //            row.ServiceType = drTouch["sTypeCategory"].ToString();
            //            if (drTouch["Delivery"].ToString() != String.Empty)
            //            {
            //                row.DeliveryDate = Convert.ToDateTime(drTouch["Delivery"]);
            //            }
            //            else
            //            {
            //                row.DeliveryDate = DateTime.MinValue;
            //            }

            //            if ((row.DeliveryDate != DateTime.MinValue) && (drTouch["Status"].ToString() == "Open"))
            //            {
            //                row.Status = "Scheduled";
            //            }
            //            else
            //            {
            //                row.Status = drTouch["Status"].ToString();
            //            }

            //            row.Starts = drTouch["Starts"].ToString();
            //            row.End = drTouch["Ends"].ToString();
            //            row.AddrFrom_FName = drTouch["sAddrFrom_sFName"].ToString();
            //            row.AddrFrom_LName = drTouch["sAddrFrom_sLName"].ToString();
            //            row.AddrFrom_Addr1 = drTouch["sAddrFrom_sAddr1"].ToString();
            //            row.AddrFrom_Addr2 = drTouch["sAddrFrom_sAddr2"].ToString();
            //            row.AddrFrom_City = drTouch["sAddrFrom_sCity"].ToString();
            //            row.AddrFrom_State = drTouch["sAddrFrom_sRegion"].ToString();
            //            row.AddrFrom_Zip = drTouch["sAddrFrom_sPostalCode"].ToString();
            //            row.AddrFrom_Company = drTouch["sAddrFrom_sCompany"].ToString();
            //            row.AddrFrom_PhoneNum = drTouch["sAddrFrom_sPhone1"].ToString();
            //            row.AddrFrom_MobilePhoneNumber = drTouch["sAddrFrom_sPhone2"].ToString();
            //            row.AddrTo_FName = drTouch["sAddrTo_sFName"].ToString();
            //            row.AddrTo_LName = drTouch["sAddrTo_sLName"].ToString();
            //            row.AddrTo_Addr1 = drTouch["sAddrTo_sAddr1"].ToString();
            //            row.AddrTo_Addr2 = drTouch["sAddrTo_sAddr2"].ToString();
            //            row.AddrTo_City = drTouch["sAddrTo_sCity"].ToString();
            //            row.AddrTo_State = drTouch["sAddrTo_sRegion"].ToString();
            //            row.AddrTo_Zip = drTouch["sAddrTo_sPostalCode"].ToString();
            //            row.AddrTo_Company = drTouch["sAddrTo_sCompany"].ToString();
            //            row.AddrTo_PhoneNum = drTouch["sAddrTo_sPhone1"].ToString();
            //            row.AddrTo_MobilePhoneNum = drTouch["sAddrTo_sPhone2"].ToString();
            //            row.Comments = drTouch["sComments"].ToString();
            //            row.Directions = drTouch["sDirections"].ToString();
            //            row.Instructions = drTouch["sInstructions"].ToString();
            //            row.Drivewaytype_Sloped = Convert.ToBoolean(drTouch["bDrivewayType_Sloped"]);
            //            row.Drivewaytype_Soft = Convert.ToBoolean(drTouch["bDrivewayType_Soft"]);
            //            row.Drivewaytype_Dirt = Convert.ToBoolean(drTouch["bDrivewayType_Dirt"]);
            //            row.Drivewaytype_Grvel = Convert.ToBoolean(drTouch["bDrivewayType_Gravel"]);
            //            row.Drivewaytype_Paved = Convert.ToBoolean(drTouch["bDrivewayType_Paved"]);
            //            row.Drivewaytype_Bricked = Convert.ToBoolean(drTouch["bDrivewayType_Bricked"]);
            //            row.Drivewaytype_Curbed = Convert.ToBoolean(drTouch["bDrivewayType_Curb"]);
            //            row.Drivewaytype_Other = Convert.ToBoolean(drTouch["bDrivewayType_OtherWithComments"]);
            //            row.DoorFacesRear = Convert.ToBoolean(drTouch["bPositionOnTruck_DoorFacesRear"]);
            //            row.Obstacle_PowerLines = Convert.ToBoolean(drTouch["bObstacles_PowerLines"]);
            //            row.Obstacle_Trees = Convert.ToBoolean(drTouch["bObstacles_TreesOrLowBranches"]);
            //            row.Obstacle_Landscaping = Convert.ToBoolean(drTouch["bObstacles_Landscaping"]);
            //            row.Obstacle_Sprinklers = Convert.ToBoolean(drTouch["bObstacles_Sprinklers"]);
            //            row.Obstacle_Septic = Convert.ToBoolean(drTouch["bObstacles_SepticOrOtherUnderground"]);
            //            row.Obstacle_Fences = Convert.ToBoolean(drTouch["bObstacles_FenceOrOtherStructure"]);
            //            row.Obstacle_Other = Convert.ToBoolean(drTouch["bObstacles_OtherWithComments"]);
            //            row.Distance = Convert.ToDecimal(drTouch["Distance"]);
            //            row.EditIdentifier = Convert.ToInt32(drTouch["iEditIdentifier"]);
            //            row.ChargeDescriptionID = Convert.ToInt32(drTouch["ChargeDescID"]);
            //            row.SequenceNumber = Convert.ToInt32(drTouch["iSequenceNum"]);

            //            if (transportationItems.TransportationItem.Select("ChargeDescriptionID = " + row.ChargeDescriptionID).Length > 0)
            //            {
            //                TransportationItems.TransportationItemRow dr = (TransportationItems.TransportationItemRow)transportationItems.TransportationItem.Select("ChargeDescriptionID = " + row.ChargeDescriptionID)[0];
            //                row.Charge = dr.Charge;
            //                //row.Distance = dr.Distance;
            //                row.ExcessDistance = dr.ExcessDistance;
            //                row.ExcessCharge = dr.ExcessCharge;
            //                row.SubTotal = dr.SubTotal;
            //                row.Discounts = dr.Discounts;
            //                row.Net = dr.Net;
            //                row.Tax = dr.Tax;
            //            }

            //            dtTouch.AddTouchRow(row);
            //        }
            //    }
            //}

            //#endregion

            //return touches;

            #endregion

        }

        /// <summary>
        /// PENDING-TEST
        /// SFERP-TODO-ESBMTD
        /// Method to get CPP items list based on QORID and cppLevel selected for the Unit/QOR
        /// </summary>
        /// <param name="qorId"></param>
        /// <param name="cppLevel"></param>
        /// <returns></returns>
        public List<CPPItem> GetAppliedCppItems(string qorId, string cppLevel)
        {
            //SFERP-TODO-ADDSTB
            #region SFERP-TODO-ESBMTD
            List<CPPItem> cppItems = null;
            DataSet dsAppliedCppItems = new DataSet();

            var requestObject = new
            {
                qorId = qorId,
                cppLevel = cppLevel
            };

            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetAppliedCppItems, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetCppItems.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
                dsAppliedCppItems = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsAppliedCppItems != null && dsAppliedCppItems.Tables.Count > 0 && dsAppliedCppItems.Tables[0] != null && dsAppliedCppItems.Tables[0].Rows.Count > 0)
            {
                cppItems = new List<CPPItem>();
                foreach (DataRow drCpp in dsAppliedCppItems.Tables[0].Rows)
                {
                    cppItems.Add(new CPPItem
                    {
                        Price = Convert.ToString(drCpp["Price"]),
                        Description = Convert.ToString(drCpp["Description"])
                    });
                }
            }

            return cppItems;

            #endregion
        }


        /// <summary>
        /// PENDING-TEST
        /// SFERP-TODO-ESBMTD
        /// Method to get Master list of CPP items for selection in change CPP controller.
        /// </summary>
        /// <returns></returns>
        public List<CPPItem> GetCppItems(decimal price = 0)
        {
            //SFERP-TODO-ADDSTB
            #region SFERP-TODO-ESBMTD
            List<CPPItem> cppItems = null;
            DataSet dsCppItems = new DataSet();

            var requestObject = new
            {
                Price = price <= 0 ? "" : price.ToString()
            };

            string requestJson = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(requestObject);

            var response = ExecuteMethodESB(EsbMethod.GetCppItems, requestJson);

            ////SPERP-TODO-CTRMV , this section to get data from stub/dummy json files only.  
            //var response = CommonUtility.ReadFromFile(Path.Combine(AppConfigKeys.ERPFilesPath(), $"GetCppItems.txt"));

            if (!string.IsNullOrWhiteSpace(response) && response != "No rows found.") //Either DataSet or JSON - TBD
                dsCppItems = JsonConvert.DeserializeObject<DataSet>(response);

            if (dsCppItems != null && dsCppItems.Tables.Count > 0 && dsCppItems.Tables[0] != null && dsCppItems.Tables[0].Rows.Count > 0)
            {
                cppItems = new List<CPPItem>();
                foreach (DataRow drCpp in dsCppItems.Tables[0].Rows)
                {
                    cppItems.Add(new CPPItem
                    {
                        Price = Convert.ToString(drCpp["ListPrice"]),
                        Description = Convert.ToString(drCpp["Description"])

                        //Add more items from esb - TODO
                    });
                }
            }

            return cppItems;

            #endregion
        }

    }
}
