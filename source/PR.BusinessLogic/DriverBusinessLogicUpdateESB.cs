﻿using System;
using PR.UtilityLibrary;

namespace PR.BusinessLogic
{
    public partial class SMDBusinessLogic : BaseBusinessLogicClass
    {

        /// <summary>
        /// PENDING-TEST  
        /// Complete Non DE or RF touch in Salesforce.
        /// </summary>
        /// <param name="qorid"></param>
        /// <param name="touchType"></param>
        /// <param name="sequenceNo"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="activityBy"></param>
        /// <param name="unitNo"></param>
        /// <param name="driverComments"></param>
        /// <returns></returns>
        public string CompleteSitelinkTouch(int qorid, string touchType, int sequenceNo, DateTime startTime, DateTime endTime, string activityBy, string unitNo, string driverComments)
        {
            try
            { 
                //Added by Sohan
                //Added DE and RE touch for Saleforce database update. was not there for SL calls.
                if (touchType == "RF" || touchType == "DF" || touchType == "CC" || touchType == "DE" || touchType == "RE")
                {
                    var touchTypeShort = PREnums.TouchTypeShort.RF;

                    switch (touchType)
                    {
                        case "RF": touchTypeShort = PREnums.TouchTypeShort.RF; break;
                        case "DF": touchTypeShort = PREnums.TouchTypeShort.DF; break;
                        case "CC": touchTypeShort = PREnums.TouchTypeShort.CC; break;
                        case "DE": touchTypeShort = PREnums.TouchTypeShort.DE; break;
                        case "RE": touchTypeShort = PREnums.TouchTypeShort.RE; break;
                    } 

                    UpdateTouchStatus(qorid, touchTypeShort, sequenceNo, ERP_Enums.eQTStatus.Completed, activityBy, unitNo, driverComments);

                    return "Success";
                }
                else
                {
                    return "Invalid Touchtype";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            #region SFERP-TODO-CMTSL - Comment this section to remove SL dlls to build the code.
            //try
            //{
            //    SMDBusinessLogic smd = new SMDBusinessLogic(userId);

            //    int qorid = smd.GetGlobalQORID(slapiMobile);

            //    decimal touchprice = 0;

            //    UnitInfo uinfo = new UnitInfo();

            //    //SFERP-TODO-CTRMV
            //    uinfo = smd.GetAddedUnit(slapiMobile);

            //    if ((touchType == "RF") || (touchType == "DF") || (touchType == "CC"))
            //    {
            //        var touchTypeShort = PREnums.TouchTypeShort.RF;
            //        if (touchType == PREnums.TouchTypeShort.DF.ToString())
            //            touchTypeShort = PREnums.TouchTypeShort.DF;
            //        else if (touchType == PREnums.TouchTypeShort.CC.ToString())
            //            touchTypeShort = PREnums.TouchTypeShort.CC;

            //        //Complete SL Touch and process the payment
            //        smd.UpdateTouchStatus(eQTStatus.Completed, editIdentifier, slapiMobile);

            //        //Payment has to process for only Local actions but not LDM
            //        if (blIsLdm == false)
            //        {
            //            TransportationItems titems = smd.GetAppliedTransportationCharges(slapiMobile);
            //            TransportationItems.TransportationItemDataTable dtTransportation = titems.TransportationItem;

            //            foreach (TransportationItems.TransportationItemRow tRow in dtTransportation.Rows)
            //            {
            //                if (tRow.EditIdentifier == editIdentifier)
            //                {
            //                    touchprice = tRow.Balance;
            //                }
            //            }
            //        }


            //        smd.SaveQORNew(slapiMobile.CorpCode, slapiMobile.Username, slapiMobile.Password, slapiMobile.LocationCode, slapiMobile.TenantID, slapiMobile);

            //        return "Success";
            //    }
            //    else
            //    {
            //        return "Invalid Touchtype";
            //    }

            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}
            #endregion

        }

    }
}
